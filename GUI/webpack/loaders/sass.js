const ExtractTextPlugin = require('extract-text-webpack-plugin')
const { env } = require('../configuration.js')

module.exports = {
  test: /\.(scss|sass|css)$/i,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      { loader: 'css-loader', options: { url: false, minimize: env.NODE_ENV === 'production' } },
      'postcss-loader',
      'sass-loader'
    ]
  })
}
