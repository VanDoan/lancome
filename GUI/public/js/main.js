$(document).ready(function(){
	$('.selectpicker').selectpicker();

	$('.btn-search').click(function(){
		$('.wrap-input-search').toggleClass('active');
	});
		
	$('.search-resp').click(function(){
		$('.wrap-input-search').toggleClass('active');
	});

	// mascaras
	$('.switch .nav li ').click(function(e){
		e.preventDefault();
		$(this).parents('.nav').children('li').removeClass('active');
		$(this).addClass('active');
		var index = $(this).index();
		$(this).parents('.list-images-items').children('img').removeClass('active');		
		$(this).parents('.list-images-items').children('img').eq(index).addClass('active');
	});
});