const { join, resolve } = require('path')
const { env } = require('process')
const configPath = resolve('webpack')

const configs = require(join(configPath, env.NODE_ENV))

module.exports = configs
