#!/bin/bash
pwd
rm -rf var/composer_home/*
rm -rf var/generation/*
rm -rf var/view_preprocessed/*
rm -rf pub/static/*

php bin/magento setup:static-content:deploy

rm -rf var/cache/* && rm -rf var/page_cache/*

php bin/magento setup:upgrade
php bin/magento indexer:reindex
php bin/magento cache:clean
php bin/magento cache:flush
chmod 777 -R var/
pwd
#chown -R lancome.lancome *
chown -R lancome.lancome /home/lancome/
ls -la
echo '=============== DEV MAGENO SHELL SCRIPT END ===============\n'

