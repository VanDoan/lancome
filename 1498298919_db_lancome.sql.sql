-- Magento DB backup
--
-- Host: localhost    Database: magento_lancome
-- ------------------------------------------------------
-- Server version: 5.6.36-82.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_passwords`
--

DROP TABLE IF EXISTS `admin_passwords`;
CREATE TABLE `admin_passwords` (
  `password_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Password Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User Id',
  `password_hash` varchar(100) DEFAULT NULL COMMENT 'Password Hash',
  `expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Expires',
  `last_updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last Updated',
  PRIMARY KEY (`password_id`),
  KEY `ADMIN_PASSWORDS_USER_ID` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Admin Passwords';

--
-- Dumping data for table `admin_passwords`
--

LOCK TABLES `admin_passwords` WRITE;
/*!40000 ALTER TABLE `admin_passwords` DISABLE KEYS */;
INSERT INTO `admin_passwords` VALUES ('1','2','f1cf25b5c2592e3e62ace678eb9c9f09936e1c04db1ebb849bbd651d6e6b59cf:VctiAki0Agq5nOhY1OAAkt5DKhB4wECX:1','1505058718','1497282718'),('2','3','ccdc4d368142a1a524151c53bf35724f47a2cc65f9589de67b0e95109f01bdd8:pagizOupXV717bxPr08lJ0BRFKY0egXk:1','1505058756','1497282756'),('3','4','d4346a274ac5fc4dc28de81e0dc956d2ec0479dc8b9baecab6dc9091a674a324:jFGl6rXt4zxlBJNmK627bIroY3loSiFs:1','1505121942','1497345942'),('4','5','d592ce873dba37a7c9d2b35041498a569095e35ad3f0d881542a1f0ee0030188:L7r5aBMNU1PC27glNd8dJDCutyNer2Hj:1','1505128691','1497352691'),('5','8','95834e002ac198c095b06cc1c651e0817bcaa185fb73bf9f468c583a1c6c55cc:47PAH070mTswfkxmT1prHwk7QhhGJgZd:1','1505314903','1497538903');
/*!40000 ALTER TABLE `admin_passwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_system_messages`
--

DROP TABLE IF EXISTS `admin_system_messages`;
CREATE TABLE `admin_system_messages` (
  `identity` varchar(100) NOT NULL COMMENT 'Message id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`identity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin System Messages';

--
-- Table structure for table `admin_user`
--

DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `firstname` varchar(32) DEFAULT NULL COMMENT 'User First Name',
  `lastname` varchar(32) DEFAULT NULL COMMENT 'User Last Name',
  `email` varchar(128) DEFAULT NULL COMMENT 'User Email',
  `username` varchar(40) DEFAULT NULL COMMENT 'User Login',
  `password` varchar(255) NOT NULL COMMENT 'User Password',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'User Created Time',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'User Modified Time',
  `logdate` timestamp NULL DEFAULT NULL COMMENT 'User Last Login Time',
  `lognum` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'User Login Number',
  `reload_acl_flag` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Reload ACL',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'User Is Active',
  `extra` text COMMENT 'User Extra Data',
  `rp_token` text COMMENT 'Reset Password Link Token',
  `rp_token_created_at` timestamp NULL DEFAULT NULL COMMENT 'Reset Password Link Token Creation Date',
  `interface_locale` varchar(16) NOT NULL DEFAULT 'en_US' COMMENT 'Backend interface locale',
  `failures_num` smallint(6) DEFAULT '0' COMMENT 'Failure Number',
  `first_failure` timestamp NULL DEFAULT NULL COMMENT 'First Failure',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Expiration Lock Dates',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ADMIN_USER_USERNAME` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Admin User Table';

--
-- Dumping data for table `admin_user`
--

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` VALUES ('1','admin','admin','admin@shopstack.asia','admin','f1a9411703ec4e8c8b839229c21a927fbdfe4d80ce30d49a326067525bbe2ae4:DP4dlWkvLDIkpFHeUr5FrNh2oPBqjsiP:1','2017-06-06 07:48:36','2017-06-20 10:31:33','2017-06-20 10:31:33',5,0,1,'a:1:{s:11:\"configState\";a:13:{s:7:\"web_url\";s:1:\"1\";s:7:\"web_seo\";s:1:\"1\";s:12:\"web_unsecure\";s:1:\"1\";s:15:\"general_country\";s:1:\"1\";s:14:\"general_region\";s:1:\"1\";s:25:\"general_store_information\";s:1:\"1\";s:14:\"general_locale\";s:1:\"1\";s:25:\"general_single_store_mode\";s:1:\"0\";s:16:\"currency_options\";s:1:\"1\";s:21:\"currency_yahoofinance\";s:1:\"0\";s:16:\"currency_fixerio\";s:1:\"0\";s:20:\"currency_webservicex\";s:1:\"0\";s:15:\"currency_import\";s:1:\"0\";}}',NULL,NULL,'en_US',0,NULL,NULL),('2','johnyrot','johnyrot','john@shopstack.asia','johnyrot','f1cf25b5c2592e3e62ace678eb9c9f09936e1c04db1ebb849bbd651d6e6b59cf:VctiAki0Agq5nOhY1OAAkt5DKhB4wECX:1','2017-06-12 15:51:58','2017-06-16 04:52:12','2017-06-16 04:52:12',14,0,1,'N;',NULL,NULL,'en_US',0,NULL,NULL),('3','shopstack','shopstack','shopstack@shopstack.asia','shopstack','ccdc4d368142a1a524151c53bf35724f47a2cc65f9589de67b0e95109f01bdd8:pagizOupXV717bxPr08lJ0BRFKY0egXk:1','2017-06-12 15:52:36','2017-06-21 06:51:50','2017-06-21 06:51:50',28,0,1,'a:1:{s:11:\"configState\";a:16:{s:15:\"general_country\";s:1:\"0\";s:20:\"catalog_fields_masks\";s:1:\"1\";s:14:\"catalog_review\";s:1:\"1\";s:16:\"catalog_frontend\";s:1:\"1\";s:20:\"catalog_productalert\";s:1:\"1\";s:25:\"catalog_productalert_cron\";s:1:\"1\";s:19:\"catalog_placeholder\";s:1:\"1\";s:25:\"catalog_recently_products\";s:1:\"1\";s:21:\"catalog_product_video\";s:1:\"1\";s:14:\"catalog_search\";s:1:\"1\";s:13:\"catalog_price\";s:1:\"0\";s:26:\"catalog_layered_navigation\";s:1:\"1\";s:11:\"catalog_seo\";s:1:\"0\";s:18:\"catalog_navigation\";s:1:\"1\";s:20:\"catalog_downloadable\";s:1:\"0\";s:22:\"catalog_custom_options\";s:1:\"0\";}}',NULL,NULL,'en_US',0,NULL,NULL),('4','acommerce','acommerce','acommerce@techteam.asia','acommerce','d4346a274ac5fc4dc28de81e0dc956d2ec0479dc8b9baecab6dc9091a674a324:jFGl6rXt4zxlBJNmK627bIroY3loSiFs:1','2017-06-13 09:25:42','2017-06-19 05:32:48','2017-06-19 05:32:48',9,0,1,'N;',NULL,NULL,'en_US',0,NULL,NULL),('5','tuyet','tuyet','tuyet@shopstack.asia','tuyet','d592ce873dba37a7c9d2b35041498a569095e35ad3f0d881542a1f0ee0030188:L7r5aBMNU1PC27glNd8dJDCutyNer2Hj:1','2017-06-13 11:18:11','2017-06-13 11:18:11',NULL,0,0,1,'N;',NULL,NULL,'en_US',0,NULL,NULL),('6','admin1','admin1','admin1@shopstack.asia','admin1','25b52d8ae6ac5199c6a8224d91c33f8c54663f8d50e25664ffc9bd93c80c3823:oCSSxaX18by63RWgGcK3aVvealvUieot:1','2017-06-13 14:12:14','2017-06-13 14:12:14',NULL,0,0,1,'N;',NULL,NULL,'en_US',0,NULL,NULL),('7','admin4','admin4','admin4@shopstack.asia','admin4','cbb416fa2f375ad1e3ce02e9687e5c2cec665b73aa7f1cef6159f1d6047be4d6:FAS3a2gCBjhAB8qtc5HYKtp8PS0Bdzrz:1','2017-06-14 10:20:47','2017-06-14 10:20:47',NULL,0,0,1,'N;',NULL,NULL,'en_US',0,NULL,NULL),('8','hoa','tam','hoa@techteam.asia','hoa','95834e002ac198c095b06cc1c651e0817bcaa185fb73bf9f468c583a1c6c55cc:47PAH070mTswfkxmT1prHwk7QhhGJgZd:1','2017-06-15 15:01:43','2017-06-24 10:07:59','2017-06-24 10:07:59',63,0,1,'a:1:{s:11:\"configState\";a:66:{s:15:\"general_country\";s:1:\"0\";s:14:\"general_region\";s:1:\"0\";s:14:\"general_locale\";s:1:\"1\";s:7:\"web_url\";s:1:\"1\";s:7:\"web_seo\";s:1:\"0\";s:12:\"web_unsecure\";s:1:\"1\";s:12:\"admin_emails\";s:1:\"0\";s:13:\"admin_startup\";s:1:\"1\";s:9:\"admin_url\";s:1:\"0\";s:14:\"admin_security\";s:1:\"1\";s:15:\"admin_dashboard\";s:1:\"0\";s:13:\"admin_captcha\";s:1:\"0\";s:16:\"currency_options\";s:1:\"1\";s:21:\"currency_yahoofinance\";s:1:\"1\";s:16:\"currency_fixerio\";s:1:\"1\";s:20:\"currency_webservicex\";s:1:\"1\";s:15:\"currency_import\";s:1:\"1\";s:9:\"dev_debug\";s:1:\"1\";s:34:\"dev_front_end_development_workflow\";s:1:\"0\";s:12:\"dev_restrict\";s:1:\"0\";s:12:\"dev_template\";s:1:\"0\";s:20:\"dev_translate_inline\";s:1:\"0\";s:6:\"dev_js\";s:1:\"0\";s:7:\"dev_css\";s:1:\"0\";s:9:\"dev_image\";s:1:\"0\";s:10:\"dev_static\";s:1:\"0\";s:8:\"dev_grid\";s:1:\"0\";s:25:\"general_store_information\";s:1:\"1\";s:11:\"tax_classes\";s:1:\"0\";s:15:\"tax_calculation\";s:1:\"0\";s:11:\"tax_display\";s:1:\"0\";s:16:\"tax_cart_display\";s:1:\"0\";s:17:\"tax_sales_display\";s:1:\"0\";s:18:\"sales_gift_options\";s:1:\"1\";s:13:\"sales_general\";s:1:\"0\";s:17:\"sales_totals_sort\";s:1:\"0\";s:13:\"sales_reorder\";s:1:\"0\";s:14:\"sales_identity\";s:1:\"0\";s:19:\"sales_minimum_order\";s:1:\"0\";s:15:\"sales_dashboard\";s:1:\"0\";s:12:\"sales_orders\";s:1:\"0\";s:10:\"sales_msrp\";s:1:\"0\";s:31:\"instagramsection_instagramgroup\";s:1:\"1\";s:19:\"sociallogin_general\";s:1:\"1\";s:20:\"sociallogin_facebook\";s:1:\"1\";s:18:\"sociallogin_google\";s:1:\"1\";s:19:\"sociallogin_twitter\";s:1:\"1\";s:18:\"sociallogin_amazon\";s:1:\"0\";s:20:\"sociallogin_linkedin\";s:1:\"0\";s:17:\"sociallogin_yahoo\";s:1:\"0\";s:22:\"sociallogin_foursquare\";s:1:\"0\";s:21:\"sociallogin_instagram\";s:1:\"0\";s:21:\"sociallogin_vkontakte\";s:1:\"0\";s:18:\"sociallogin_github\";s:1:\"0\";s:12:\"blog_general\";s:1:\"1\";s:17:\"blog_product_post\";s:1:\"1\";s:12:\"blog_sidebar\";s:1:\"1\";s:20:\"blog_monthly_archive\";s:1:\"1\";s:12:\"blog_comment\";s:1:\"1\";s:8:\"blog_seo\";s:1:\"1\";s:10:\"blog_share\";s:1:\"1\";s:25:\"general_single_store_mode\";s:1:\"1\";s:11:\"web_default\";s:1:\"1\";s:10:\"web_cookie\";s:1:\"1\";s:11:\"web_session\";s:1:\"1\";s:24:\"web_browser_capabilities\";s:1:\"1\";}}',NULL,NULL,'en_AU',0,NULL,NULL);
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_session`
--

DROP TABLE IF EXISTS `admin_user_session`;
CREATE TABLE `admin_user_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `session_id` varchar(128) NOT NULL COMMENT 'Session id value',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin User ID',
  `status` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Current Session status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `ip` varchar(15) NOT NULL COMMENT 'Remote user IP',
  PRIMARY KEY (`id`),
  KEY `ADMIN_USER_SESSION_SESSION_ID` (`session_id`),
  KEY `ADMIN_USER_SESSION_USER_ID` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COMMENT='Admin User sessions table';

--
-- Dumping data for table `admin_user_session`
--

LOCK TABLES `admin_user_session` WRITE;
/*!40000 ALTER TABLE `admin_user_session` DISABLE KEYS */;
INSERT INTO `admin_user_session` VALUES ('1','rtlbjfkfh2m5pbooeji71uhj25','1',1,'2017-06-07 03:25:54','2017-06-07 03:26:58','203.146.162.82'),('2','0rdnhhm72nqjk9k9020ovgve37','1',1,'2017-06-12 15:42:10','2017-06-12 16:11:33','14.207.166.193'),('3','eqv9efblabtevq0epcqr2npge6','3',1,'2017-06-12 15:58:20','2017-06-12 16:16:38','124.122.151.11'),('4','lamjj04spq39tm6aoj1s5fsd82','2',1,'2017-06-13 02:19:30','2017-06-13 04:02:05','203.146.162.82'),('5','lapd2qpibq15hq0fu27vocudl4','3',1,'2017-06-13 02:44:01','2017-06-13 02:44:02','124.122.151.11'),('6','7ta4srh2dlqv866d4dmpfhfme0','3',1,'2017-06-13 03:13:51','2017-06-13 03:23:57','124.122.151.11'),('7','hrdjpqco0g2fmfm7f5f0954ot7','3',1,'2017-06-13 03:41:45','2017-06-13 03:43:15','124.122.151.11'),('8','90s3ntnoink6nskeei91682qd2','3',2,'2017-06-13 06:19:20','2017-06-13 06:49:36','58.8.137.110'),('9','2lfal1paq6vbhu60lft41ohg11','3',1,'2017-06-13 06:49:36','2017-06-13 07:34:53','58.8.137.110'),('10','i59hpptdqf4c37g0qdop6bo0d2','3',1,'2017-06-13 07:56:17','2017-06-13 08:35:17','58.8.137.110'),('11','ve1i1rkog3o864eo39kup79hh7','2',1,'2017-06-13 08:24:50','2017-06-13 08:46:03','203.146.162.82'),('12','n9qa7ulfr2murnthkdq18pda01','2',1,'2017-06-13 09:20:30','2017-06-13 09:34:55','203.146.162.82'),('13','m7affu72v1suqqte6lsrpb3a02','4',1,'2017-06-13 09:28:37','2017-06-13 09:32:22','54.254.236.53'),('14','jt1ibg4200abd5u06kigck7gc6','3',1,'2017-06-13 09:54:13','2017-06-13 10:01:38','58.8.137.110'),('15','eq31flj5tg3ahrgo0o3l9up9t0','2',1,'2017-06-13 10:44:05','2017-06-13 11:02:07','203.146.162.82'),('16','1gojt7j7h4th11ftrut3vfnud5','3',2,'2017-06-13 10:54:50','2017-06-13 11:16:27','58.8.137.110'),('17','s04ltmik91ud9o9ib0rbn23cb1','3',1,'2017-06-13 11:16:27','2017-06-13 11:37:50','58.8.137.110'),('18','8m7jomioklovpqcbpd2uhsmps7','2',1,'2017-06-13 11:17:07','2017-06-13 11:18:11','203.146.162.82'),('19','lu4rp9u2rm7ci02u2hscc43q62','3',1,'2017-06-13 11:53:25','2017-06-13 12:30:14','58.8.137.110'),('20','lmnv11hslnq86p8qcjb2d2r5o2','3',1,'2017-06-13 17:50:00','2017-06-13 17:50:18','124.122.150.120'),('21','2cggukbsqrv5pm3gccad8u3b24','4',1,'2017-06-14 02:59:12','2017-06-14 03:01:27','43.249.61.218'),('22','2eiapoj1blp8sepsrffa4gvlu3','4',2,'2017-06-14 04:00:15','2017-06-14 04:18:50','210.1.7.114'),('23','oivt7c7su3b1bpep5ulmm49ih5','4',1,'2017-06-14 04:18:50','2017-06-14 04:25:55','210.1.7.114'),('24','c5i0f5v13vkrrk7sg7r3nff0j5','3',1,'2017-06-14 04:36:22','2017-06-14 04:43:38','58.11.47.8'),('25','eqlvjeh51356r1s1bnmi8dahn2','4',1,'2017-06-14 04:51:18','2017-06-14 05:37:06','210.1.7.114'),('26','3ao6or9ucrritf49q27rjggag6','4',1,'2017-06-14 07:12:08','2017-06-14 07:12:10','43.249.61.218'),('27','n7sdonjcd7bt2ujub1d8eck3h0','2',1,'2017-06-14 08:40:28','2017-06-14 08:51:04','203.146.162.82'),('28','t7fqb8ihei5erm8u372pamait7','2',1,'2017-06-14 09:06:33','2017-06-14 09:06:39','203.146.162.82'),('29','15hh9a80hr2t2s1cmj3q7g21f4','2',2,'2017-06-14 09:23:18','2017-06-14 09:30:02','203.146.162.82'),('30','6s4let7a5v8fkkrr0nc6rk66k6','1',1,'2017-06-14 09:29:45','2017-06-14 09:30:07','203.146.162.82'),('31','03ri0n00jgcobf74809uo0u705','2',1,'2017-06-14 09:30:02','2017-06-14 09:33:26','203.146.162.82'),('32','e1kvhvn3kkffro7qtsnk72dmv7','1',1,'2017-06-14 10:21:26','2017-06-14 10:22:02','203.146.162.82'),('33','fsm4atsc1tpr281o8q9h9ggjd7','2',1,'2017-06-14 10:22:29','2017-06-14 10:29:49','203.146.162.82'),('34','cbkhav9oi76g4dctefriqh3cn1','3',1,'2017-06-14 14:13:32','2017-06-14 14:13:34','124.122.150.120'),('35','70dd7bvjoco3rg9oltdcm70og5','3',1,'2017-06-14 16:04:25','2017-06-14 16:05:32','58.11.52.167'),('36','uqeh7aht4jfcvg3o3iu4rjvig3','3',1,'2017-06-14 17:26:55','2017-06-14 17:29:11','58.11.52.167'),('37','lrma47h01ne939i7p680q7stv5','2',1,'2017-06-15 14:59:11','2017-06-15 15:01:43','14.207.179.116'),('38','1vu4ip8e0sqf54oosnn7k0fv34','2',1,'2017-06-16 03:19:54','2017-06-16 03:21:13','203.146.162.82'),('39','3r3faj3bnmo32dbeqag3l19117','3',1,'2017-06-16 03:29:45','2017-06-16 03:29:55','58.11.53.17'),('40','989u1g6igr1n2qt3s5aeg02bi6','3',1,'2017-06-16 03:52:00','2017-06-16 04:04:48','209.234.248.127'),('41','5v5gi0jh5nhhr7rancp6h41vo2','2',0,'2017-06-16 04:08:43','2017-06-16 04:45:07','203.146.162.82'),('42','goqbn77bh71bq2vq3nr8ffv967','8',2,'2017-06-16 04:25:43','2017-06-16 04:29:23','113.176.185.221'),('43','86mapu0j5tdhuh6o5eq7tuvpc3','8',2,'2017-06-16 04:29:23','2017-06-16 04:31:19','113.176.185.221'),('44','09oo12p6h3616qbv6eoi39c5c2','8',2,'2017-06-16 04:31:19','2017-06-16 04:32:13','113.176.185.221'),('45','r8n5ie8e8rrc8ufoc2sed7l875','3',1,'2017-06-16 04:31:56','2017-06-16 04:37:32','209.234.248.127'),('46','m65pgkulp0r6gg2copea0ag9v2','8',2,'2017-06-16 04:32:13','2017-06-16 04:32:31','219.91.133.57'),('47','aqisuldmvbshl9o0dh4mum5mf5','8',2,'2017-06-16 04:32:31','2017-06-16 04:34:38','113.176.185.221'),('48','7s2543afcoj1sdkp1pt0q3glm7','8',1,'2017-06-16 04:34:38','2017-06-16 04:43:37','219.91.133.57'),('49','4k7v7od1e3nd6154re197923c2','2',1,'2017-06-16 04:52:12','2017-06-16 05:11:52','203.146.162.82'),('50','0j7injfoptm98u5393qsebn6l3','3',1,'2017-06-16 05:11:21','2017-06-16 05:46:01','58.11.239.97'),('51','9r568qt4e10pjpua9id6daa9u7','8',1,'2017-06-16 07:36:25','2017-06-16 07:37:04','117.3.47.51'),('52','0vnmhjm53ih936qc7kf8fjk4l1','3',1,'2017-06-16 07:39:30','2017-06-16 07:52:17','223.24.68.200'),('53','hcpacf47miqhf56mudm2qlbl87','8',1,'2017-06-16 08:32:05','2017-06-16 08:32:06','117.2.222.253'),('54','7hktm17n9pi7kdj9tujds3c0g4','8',1,'2017-06-16 08:42:42','2017-06-16 08:47:30','219.91.133.57'),('55','8961vhbaupcjqtmfpivvuvkda3','8',1,'2017-06-16 08:45:41','2017-06-16 08:46:03','113.176.185.221'),('56','l9sans7uar88ehl58g14g3kn74','3',1,'2017-06-16 13:54:06','2017-06-16 14:14:55','58.11.239.97'),('57','19al17hng2d6c89ikkoh7ijf83','3',1,'2017-06-16 14:17:15','2017-06-16 14:42:49','58.11.239.97'),('58','aitlcfr0fl9j113fhhb9u4tcq1','4',1,'2017-06-19 02:44:09','2017-06-19 02:45:01','54.254.236.53'),('59','sdeva0q1kgmv6l3t33urniisq2','4',1,'2017-06-19 03:37:57','2017-06-19 04:53:35','54.254.236.53'),('60','943ir9g5be46hbemlc3s105m76','8',1,'2017-06-19 04:16:12','2017-06-19 04:16:13','175.100.146.12'),('61','q5ef0ia60m5a2oqsn11mtmipk6','8',1,'2017-06-19 04:51:30','2017-06-19 04:51:52','116.98.249.23'),('62','rp457bekregrrgpb6tnua48jj7','4',1,'2017-06-19 05:32:48','2017-06-19 06:14:50','54.254.236.53'),('63','rf3i4n0isj5avjr1eku6g7tid5','8',1,'2017-06-19 07:01:07','2017-06-19 07:03:29','175.100.146.12'),('64','jss734lrqj1fdqd2q6algvsdc3','8',1,'2017-06-19 07:29:36','2017-06-19 07:29:54','14.174.136.98'),('65','hp2cgkbkiumasd9je1vcf11655','8',1,'2017-06-19 07:39:29','2017-06-19 07:40:18','14.174.136.98'),('66','ana2elpsog764ggtka8khhfij6','8',1,'2017-06-19 08:18:35','2017-06-19 08:18:37','14.174.136.98'),('67','20kd22kv32769tragjfnlq6hc2','8',1,'2017-06-19 09:03:49','2017-06-19 09:10:04','14.174.136.98'),('68','lt2osmuk2oesnpbe9fffo65sa0','8',1,'2017-06-19 09:10:50','2017-06-19 09:20:53','116.98.249.23'),('69','54i9cgn634dsk1kqcme6g7r9q6','8',1,'2017-06-19 11:03:52','2017-06-19 11:20:16','14.174.136.98'),('70','sa2ucjpup4t85uoht7lvjs6kj1','8',1,'2017-06-19 11:25:40','2017-06-19 11:31:33','14.174.136.98'),('71','trtbbjhaus7d4ri9ou9uk2mkc3','8',1,'2017-06-19 11:34:33','2017-06-19 11:37:24','14.174.136.98'),('72','ae9mj654bdu17mtl7sc7pogqu5','8',1,'2017-06-19 16:49:10','2017-06-19 16:49:12','116.103.78.80'),('73','44dc875200qvu0aomhqah7mro2','8',1,'2017-06-20 00:29:31','2017-06-20 00:29:58','117.3.47.51'),('74','1j4mb0u54ujro4u69ml2k9mt47','8',1,'2017-06-20 01:01:17','2017-06-20 01:01:18','14.191.105.124'),('75','tnraboa63tvr5p855kr6ufht92','8',1,'2017-06-20 01:32:37','2017-06-20 01:48:33','117.2.212.148'),('76','dihdd79o3rfj4n6r9php3310m5','8',1,'2017-06-20 02:16:41','2017-06-20 02:16:42','117.2.212.148'),('77','b0ds3958ucegg2papa9q31n7r3','8',1,'2017-06-20 06:20:48','2017-06-20 06:20:50','117.2.212.148'),('78','op04s9vqnimpn519dcbp116i33','8',1,'2017-06-20 06:47:47','2017-06-20 07:09:31','117.2.212.148'),('79','i9p3kd3p00omtha3aevd3n00p2','8',1,'2017-06-20 07:29:28','2017-06-20 07:29:33','117.2.212.148'),('80','ujrrbra6aghguf1q4qs8aq5fq6','8',1,'2017-06-20 07:42:15','2017-06-20 07:42:16','117.2.212.148'),('81','hefe2o8a3dd27e75iarpmh1p57','8',1,'2017-06-20 08:14:52','2017-06-20 08:14:53','117.2.212.148'),('82','4ocbe5m9ds5b5fquo4lqmm0vu4','1',1,'2017-06-20 10:31:33','2017-06-20 10:31:34','203.146.162.82'),('83','b5ncbsq37osj07p3o20vqqk725','8',1,'2017-06-20 11:09:44','2017-06-20 11:10:36','219.91.133.13'),('84','cssf91dihigjasbs164pdn4rq4','8',1,'2017-06-20 11:12:11','2017-06-20 11:13:31','219.91.133.13'),('85','5p1cbgf7qrtk4t9vtnboc5pdk5','8',1,'2017-06-20 11:48:59','2017-06-20 11:59:45','175.100.147.180'),('86','c4vp2lqinnq6rh51l8c0sqa551','8',1,'2017-06-20 12:05:42','2017-06-20 12:09:43','175.100.147.180'),('87','j4i6p2t69jm5o671kn7ot0eo47','8',1,'2017-06-20 12:14:17','2017-06-20 12:21:09','175.100.147.180'),('88','5t96en89eugiibk003vtgo1l80','8',1,'2017-06-20 12:23:56','2017-06-20 12:24:36','14.165.39.4'),('89','00bh52hme2pfkqcoamq75r6vf5','8',1,'2017-06-20 12:40:23','2017-06-20 13:01:29','175.100.147.180'),('90','iijqeu7vunc85h5d8gimv7rq02','8',1,'2017-06-20 13:15:36','2017-06-20 13:15:38','14.165.39.4'),('91','8gu43a26ugrro7krninpsqhq81','8',1,'2017-06-20 14:09:06','2017-06-20 14:19:21','14.165.39.4'),('92','gbm6pa8n3ml5p16p5qoisbbg91','8',1,'2017-06-21 03:27:43','2017-06-21 03:27:58','113.176.185.231'),('93','k4cll4ntjbah687v84uhn35q86','8',1,'2017-06-21 04:15:57','2017-06-21 04:16:44','219.91.138.69'),('94','d1r1hc1iig3cu9cvarv6mbrdj7','3',1,'2017-06-21 05:07:08','2017-06-21 05:07:51','58.11.37.186'),('95','5rl2i487fs5nc160a3njkn25c5','3',1,'2017-06-21 05:23:23','2017-06-21 05:28:17','58.11.37.186'),('96','chsrp18g8vc6ior6avn1nebis4','3',1,'2017-06-21 06:02:57','2017-06-21 06:02:57','58.11.37.186'),('97','839aabt5683addi2f1o6a7clk0','3',1,'2017-06-21 06:02:57','2017-06-21 06:20:18','58.11.37.186'),('98','qnie581ravrda7cemt52eutkd3','8',1,'2017-06-21 06:33:11','2017-06-21 06:34:23','113.176.185.231'),('99','05ko0m8sr42d82fjoq7geo2b60','3',1,'2017-06-21 06:51:50','2017-06-21 07:01:43','58.11.37.186'),('100','f04mf3jrkh69gmcgg0a2ca4gc0','8',1,'2017-06-21 07:20:00','2017-06-21 07:20:16','116.103.209.185'),('101','c23e2ff57ijo6n2brbh3v78b46','8',1,'2017-06-21 07:26:24','2017-06-21 07:27:58','113.176.185.231'),('102','644fuicv96osjoivrg6gu5lnc5','8',1,'2017-06-21 07:57:36','2017-06-21 08:20:09','113.160.250.213'),('103','5k6r63fvdgg2lu9rhc0usca5e2','8',1,'2017-06-21 08:17:40','2017-06-21 08:25:38','113.176.185.231'),('104','mm2v9s12lo9gont67bjli0d3c0','8',1,'2017-06-21 09:11:27','2017-06-21 09:15:39','113.176.185.231'),('105','h1t5qdcch7nf6g11d0bpoqi903','8',1,'2017-06-21 09:35:46','2017-06-21 09:40:11','113.176.185.231'),('106','dj5j9biefib7rg02o6ff8qhnr7','8',1,'2017-06-22 01:08:44','2017-06-22 01:08:44','113.176.186.251'),('107','c7icitvla2qsqmvqvbvbsaqg06','8',1,'2017-06-22 01:08:45','2017-06-22 01:16:38','113.176.186.251'),('108','1m9bjquvld7asn5q9vc08nrj90','8',1,'2017-06-22 03:07:47','2017-06-22 03:13:21','113.176.186.251'),('109','11n6g1qqdqs64e27m2k3af4fo1','8',1,'2017-06-22 06:55:12','2017-06-22 07:02:10','113.176.186.251'),('110','0rjjosq5lud1em4l24a6a1gae1','8',1,'2017-06-22 07:23:05','2017-06-22 07:23:07','113.176.186.251'),('111','h5ohuebq7fh8552a40mv69ail6','8',1,'2017-06-22 08:36:53','2017-06-22 08:44:30','113.176.186.251'),('112','fnf2j5kbuv5afhfpgaoqd4btc5','8',1,'2017-06-22 09:54:52','2017-06-22 09:56:36','113.176.186.251'),('113','3ak9lubml0111frrp1sfq69792','8',1,'2017-06-23 07:34:08','2017-06-23 07:37:46','14.174.141.5'),('114','att5ln9gtjcrnh97q340n4qek2','8',1,'2017-06-23 08:55:34','2017-06-23 08:59:29','117.2.207.73'),('115','m8ucbur394malf9991quv6lqh4','8',1,'2017-06-23 09:20:13','2017-06-23 09:22:40','14.174.141.5'),('116','2rel7irb157r7m7rmju14vrvp4','8',1,'2017-06-24 06:49:05','2017-06-24 07:18:17','175.100.146.96'),('117','vsuc7q3vpe72omtqmqpco8n165','8',1,'2017-06-24 09:10:10','2017-06-24 09:10:31','14.165.39.4'),('118','aa34cuoir9amu0cea40iiituq3','8',1,'2017-06-24 09:19:36','2017-06-24 09:33:35','14.233.38.98'),('119','n05lhisu32tgc6mefdffhh5i40','8',1,'2017-06-24 10:07:59','2017-06-24 10:08:39','14.233.38.98');
/*!40000 ALTER TABLE `admin_user_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adminnotification_inbox`
--

DROP TABLE IF EXISTS `adminnotification_inbox`;
CREATE TABLE `adminnotification_inbox` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Notification id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `description` text COMMENT 'Description',
  `url` varchar(255) DEFAULT NULL COMMENT 'Url',
  `is_read` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification read',
  `is_remove` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification might be removed',
  PRIMARY KEY (`notification_id`),
  KEY `ADMINNOTIFICATION_INBOX_SEVERITY` (`severity`),
  KEY `ADMINNOTIFICATION_INBOX_IS_READ` (`is_read`),
  KEY `ADMINNOTIFICATION_INBOX_IS_REMOVE` (`is_remove`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Adminnotification Inbox';

--
-- Dumping data for table `adminnotification_inbox`
--

LOCK TABLES `adminnotification_inbox` WRITE;
/*!40000 ALTER TABLE `adminnotification_inbox` DISABLE KEYS */;
INSERT INTO `adminnotification_inbox` VALUES ('1',1,'2017-06-21 16:29:32','Magento Community Edition 2.0.15 is Now Available with an Important PayPal Update. Upgrade by June 30, 2017 to Avoid Service Disruptions – 6/21/2017','As of June 30, 2017, PayPal Instant Payment Notifications will no longer allow you to use HTTP to post messages back to PayPal for verification. To avoid service disruptions, all merchants using PayPal and Magento 2.0.x must upgrade to Community Edition 2.0.15 by June 30, 2017. All Magento 2.1.x versions already support this change, so no update is required. The new version is available from  the Community Edition Download Page (https://magento.com/tech-resources/download) and more information is available from PayPal at https://www.paypal-knowledge.com/infocenter/index?page=content&widgetview=true&id=FAQ1916&viewlocale=en_US','https://magento.com/tech-resources/download ',1,0);
/*!40000 ALTER TABLE `adminnotification_inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorization_role`
--

DROP TABLE IF EXISTS `authorization_role`;
CREATE TABLE `authorization_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Role ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Role ID',
  `tree_level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Tree Level',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Sort Order',
  `role_type` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Role Type',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `user_type` varchar(16) DEFAULT NULL COMMENT 'User Type',
  `role_name` varchar(50) DEFAULT NULL COMMENT 'Role Name',
  PRIMARY KEY (`role_id`),
  KEY `AUTHORIZATION_ROLE_PARENT_ID_SORT_ORDER` (`parent_id`,`sort_order`),
  KEY `AUTHORIZATION_ROLE_TREE_LEVEL` (`tree_level`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Admin Role Table';

--
-- Dumping data for table `authorization_role`
--

LOCK TABLES `authorization_role` WRITE;
/*!40000 ALTER TABLE `authorization_role` DISABLE KEYS */;
INSERT INTO `authorization_role` VALUES ('1','0',1,1,'G','0','2','Administrators'),('2','1',2,0,'U','1','2','admin'),('3','1',2,0,'U','2','2','johnyrot'),('4','1',2,0,'U','3','2','shopstack'),('5','1',2,0,'U','4','2','acommerce'),('6','1',2,0,'U','5','2','tuyet'),('7','1',2,0,'U','6','2','admin1'),('8','1',2,0,'U','7','2','admin4'),('9','1',2,0,'U','8','2','hoa');
/*!40000 ALTER TABLE `authorization_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorization_rule`
--

DROP TABLE IF EXISTS `authorization_rule`;
CREATE TABLE `authorization_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule ID',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Role ID',
  `resource_id` varchar(255) DEFAULT NULL COMMENT 'Resource ID',
  `privileges` varchar(20) DEFAULT NULL COMMENT 'Privileges',
  `permission` varchar(10) DEFAULT NULL COMMENT 'Permission',
  PRIMARY KEY (`rule_id`),
  KEY `AUTHORIZATION_RULE_RESOURCE_ID_ROLE_ID` (`resource_id`,`role_id`),
  KEY `AUTHORIZATION_RULE_ROLE_ID_RESOURCE_ID` (`role_id`,`resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin Rule Table';

--
-- Dumping data for table `authorization_rule`
--

LOCK TABLES `authorization_rule` WRITE;
/*!40000 ALTER TABLE `authorization_rule` DISABLE KEYS */;
INSERT INTO `authorization_rule` VALUES ('1','1','Magento_Backend::all',NULL,'allow');
/*!40000 ALTER TABLE `authorization_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
CREATE TABLE `cache` (
  `id` varchar(200) NOT NULL COMMENT 'Cache Id',
  `data` mediumblob COMMENT 'Cache Data',
  `create_time` int(11) DEFAULT NULL COMMENT 'Cache Creation Time',
  `update_time` int(11) DEFAULT NULL COMMENT 'Time of Cache Updating',
  `expire_time` int(11) DEFAULT NULL COMMENT 'Cache Expiration Time',
  PRIMARY KEY (`id`),
  KEY `CACHE_EXPIRE_TIME` (`expire_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Caches';

--
-- Table structure for table `cache_tag`
--

DROP TABLE IF EXISTS `cache_tag`;
CREATE TABLE `cache_tag` (
  `tag` varchar(100) NOT NULL COMMENT 'Tag',
  `cache_id` varchar(200) NOT NULL COMMENT 'Cache Id',
  PRIMARY KEY (`tag`,`cache_id`),
  KEY `CACHE_TAG_CACHE_ID` (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Caches';

--
-- Table structure for table `captcha_log`
--

DROP TABLE IF EXISTS `captcha_log`;
CREATE TABLE `captcha_log` (
  `type` varchar(32) NOT NULL COMMENT 'Type',
  `value` varchar(32) NOT NULL COMMENT 'Value',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Count',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Update Time',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Count Login Attempts';

--
-- Table structure for table `catalog_category_entity`
--

DROP TABLE IF EXISTS `catalog_category_entity`;
CREATE TABLE `catalog_category_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attriute Set ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Category ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `path` varchar(255) NOT NULL COMMENT 'Tree Path',
  `position` int(11) NOT NULL COMMENT 'Position',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT 'Tree Level',
  `children_count` int(11) NOT NULL COMMENT 'Child Count',
  PRIMARY KEY (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_LEVEL` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Table';

--
-- Dumping data for table `catalog_category_entity`
--

LOCK TABLES `catalog_category_entity` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity` DISABLE KEYS */;
INSERT INTO `catalog_category_entity` VALUES ('1',3,'0','2017-06-06 07:48:30','2017-06-16 04:23:41','1','0','0','95'),('2',3,'1','2017-06-06 07:48:30','2017-06-16 04:23:41','1/2','1','1','94'),('3',3,'2','2017-06-12 16:00:16','2017-06-13 12:03:19','1/2/3','1','2','30'),('4',3,'2','2017-06-12 16:00:29','2017-06-13 12:22:45','1/2/4','2','2','40'),('5',3,'2','2017-06-12 16:00:49','2017-06-13 12:27:01','1/2/5','3','2','16'),('6',3,'2','2017-06-12 16:01:18','2017-06-13 10:58:38','1/2/6','4','2','0'),('7',3,'2','2017-06-12 16:01:47','2017-06-13 10:58:38','1/2/7','5','2','0'),('8',3,'2','2017-06-12 16:02:05','2017-06-13 10:58:38','1/2/8','6','2','0'),('9',3,'2','2017-06-13 03:28:10','2017-06-13 10:58:38','1/2/9','7','2','0'),('10',3,'3','2017-06-13 11:07:19','2017-06-13 11:56:16','1/2/3/10','1','3','7'),('11',3,'3','2017-06-13 11:36:21','2017-06-13 11:58:28','1/2/3/11','2','3','6'),('12',3,'3','2017-06-13 11:36:38','2017-06-13 11:59:57','1/2/3/12','3','3','5'),('13',3,'3','2017-06-13 11:36:51','2017-06-13 12:00:51','1/2/3/13','4','3','1'),('14',3,'3','2017-06-13 11:37:03','2017-06-13 12:03:19','1/2/3/14','5','3','6'),('15',3,'10','2017-06-13 11:37:16','2017-06-13 11:37:16','1/2/3/10/15','1','4','0'),('16',3,'10','2017-06-13 11:37:29','2017-06-13 11:37:29','1/2/3/10/16','2','4','0'),('17',3,'10','2017-06-13 11:37:49','2017-06-13 11:37:49','1/2/3/10/17','3','4','0'),('18',3,'10','2017-06-13 11:54:13','2017-06-13 11:54:13','1/2/3/10/18','4','4','0'),('19',3,'10','2017-06-13 11:54:50','2017-06-13 11:54:50','1/2/3/10/19','5','4','0'),('20',3,'10','2017-06-13 11:55:24','2017-06-13 11:55:24','1/2/3/10/20','6','4','0'),('21',3,'10','2017-06-13 11:56:16','2017-06-13 11:56:16','1/2/3/10/21','7','4','0'),('22',3,'11','2017-06-13 11:56:29','2017-06-13 11:56:29','1/2/3/11/22','1','4','0'),('23',3,'11','2017-06-13 11:56:41','2017-06-13 11:56:41','1/2/3/11/23','2','4','0'),('24',3,'11','2017-06-13 11:56:56','2017-06-13 11:56:56','1/2/3/11/24','3','4','0'),('25',3,'11','2017-06-13 11:57:20','2017-06-13 11:57:20','1/2/3/11/25','4','4','0'),('26',3,'11','2017-06-13 11:58:13','2017-06-13 11:58:13','1/2/3/11/26','5','4','0'),('27',3,'11','2017-06-13 11:58:28','2017-06-13 11:58:28','1/2/3/11/27','6','4','0'),('28',3,'12','2017-06-13 11:58:42','2017-06-13 11:58:42','1/2/3/12/28','1','4','0'),('29',3,'12','2017-06-13 11:59:09','2017-06-13 11:59:09','1/2/3/12/29','2','4','0'),('30',3,'12','2017-06-13 11:59:21','2017-06-13 11:59:21','1/2/3/12/30','3','4','0'),('31',3,'12','2017-06-13 11:59:44','2017-06-13 11:59:44','1/2/3/12/31','4','4','0'),('32',3,'12','2017-06-13 11:59:57','2017-06-13 11:59:57','1/2/3/12/32','5','4','0'),('33',3,'13','2017-06-13 12:00:51','2017-06-13 12:00:51','1/2/3/13/33','1','4','0'),('34',3,'14','2017-06-13 12:01:03','2017-06-13 12:01:03','1/2/3/14/34','1','4','0'),('35',3,'14','2017-06-13 12:01:15','2017-06-13 12:01:15','1/2/3/14/35','2','4','0'),('36',3,'14','2017-06-13 12:01:30','2017-06-13 12:01:30','1/2/3/14/36','3','4','0'),('37',3,'14','2017-06-13 12:02:36','2017-06-13 12:02:36','1/2/3/14/37','4','4','0'),('38',3,'14','2017-06-13 12:03:02','2017-06-13 12:03:02','1/2/3/14/38','5','4','0'),('39',3,'14','2017-06-13 12:03:19','2017-06-13 12:03:19','1/2/3/14/39','6','4','0'),('40',3,'4','2017-06-13 12:04:10','2017-06-13 12:12:43','1/2/4/40','1','3','12'),('41',3,'4','2017-06-13 12:04:26','2017-06-13 12:17:14','1/2/4/41','2','3','12'),('42',3,'4','2017-06-13 12:04:44','2017-06-13 12:21:52','1/2/4/42','3','3','8'),('43',3,'4','2017-06-13 12:04:56','2017-06-13 12:22:45','1/2/4/43','4','3','4'),('44',3,'40','2017-06-13 12:06:38','2017-06-13 12:06:38','1/2/4/40/44','1','4','0'),('45',3,'40','2017-06-13 12:07:13','2017-06-13 12:07:13','1/2/4/40/45','2','4','0'),('46',3,'40','2017-06-13 12:07:44','2017-06-13 12:07:44','1/2/4/40/46','3','4','0'),('47',3,'40','2017-06-13 12:08:15','2017-06-13 12:08:15','1/2/4/40/47','4','4','0'),('48',3,'40','2017-06-13 12:08:50','2017-06-13 12:08:50','1/2/4/40/48','5','4','0'),('49',3,'40','2017-06-13 12:09:23','2017-06-13 12:09:23','1/2/4/40/49','6','4','0'),('50',3,'40','2017-06-13 12:10:27','2017-06-13 12:10:27','1/2/4/40/50','7','4','0'),('51',3,'40','2017-06-13 12:11:22','2017-06-13 12:11:22','1/2/4/40/51','8','4','0'),('52',3,'40','2017-06-13 12:11:39','2017-06-13 12:11:39','1/2/4/40/52','9','4','0'),('53',3,'40','2017-06-13 12:12:06','2017-06-13 12:12:06','1/2/4/40/53','10','4','0'),('54',3,'40','2017-06-13 12:12:25','2017-06-13 12:12:25','1/2/4/40/54','11','4','0'),('55',3,'40','2017-06-13 12:12:43','2017-06-13 12:12:43','1/2/4/40/55','12','4','0'),('56',3,'41','2017-06-13 12:13:04','2017-06-13 12:13:04','1/2/4/41/56','1','4','0'),('57',3,'41','2017-06-13 12:13:19','2017-06-13 12:13:19','1/2/4/41/57','2','4','0'),('58',3,'41','2017-06-13 12:14:06','2017-06-13 12:14:06','1/2/4/41/58','3','4','0'),('59',3,'41','2017-06-13 12:14:25','2017-06-13 12:14:25','1/2/4/41/59','4','4','0'),('60',3,'41','2017-06-13 12:14:40','2017-06-13 12:14:40','1/2/4/41/60','5','4','0'),('61',3,'41','2017-06-13 12:14:55','2017-06-13 12:14:55','1/2/4/41/61','6','4','0'),('62',3,'41','2017-06-13 12:15:19','2017-06-13 12:15:19','1/2/4/41/62','7','4','0'),('63',3,'41','2017-06-13 12:15:41','2017-06-13 12:15:41','1/2/4/41/63','8','4','0'),('64',3,'41','2017-06-13 12:16:09','2017-06-13 12:16:09','1/2/4/41/64','9','4','0'),('65',3,'41','2017-06-13 12:16:32','2017-06-13 12:16:32','1/2/4/41/65','10','4','0'),('66',3,'41','2017-06-13 12:16:55','2017-06-13 12:16:55','1/2/4/41/66','11','4','0'),('67',3,'41','2017-06-13 12:17:14','2017-06-13 12:17:14','1/2/4/41/67','12','4','0'),('68',3,'42','2017-06-13 12:17:49','2017-06-13 12:17:49','1/2/4/42/68','1','4','0'),('69',3,'42','2017-06-13 12:18:08','2017-06-13 12:18:08','1/2/4/42/69','2','4','0'),('70',3,'42','2017-06-13 12:18:36','2017-06-13 12:18:36','1/2/4/42/70','3','4','0'),('71',3,'42','2017-06-13 12:18:49','2017-06-13 12:18:49','1/2/4/42/71','4','4','0'),('72',3,'42','2017-06-13 12:19:42','2017-06-13 12:19:42','1/2/4/42/72','5','4','0'),('73',3,'42','2017-06-13 12:21:20','2017-06-13 12:21:20','1/2/4/42/73','6','4','0'),('74',3,'42','2017-06-13 12:21:41','2017-06-13 12:21:41','1/2/4/42/74','7','4','0'),('75',3,'42','2017-06-13 12:21:52','2017-06-13 12:21:52','1/2/4/42/75','8','4','0'),('76',3,'43','2017-06-13 12:22:08','2017-06-13 12:22:08','1/2/4/43/76','1','4','0'),('77',3,'43','2017-06-13 12:22:20','2017-06-13 12:22:20','1/2/4/43/77','2','4','0'),('78',3,'43','2017-06-13 12:22:33','2017-06-13 12:22:33','1/2/4/43/78','3','4','0'),('79',3,'43','2017-06-13 12:22:45','2017-06-13 12:22:45','1/2/4/43/79','4','4','0'),('80',3,'5','2017-06-13 12:23:16','2017-06-13 12:26:45','1/2/5/80','1','3','13'),('81',3,'5','2017-06-13 12:23:35','2017-06-13 12:27:01','1/2/5/81','2','3','1'),('82',3,'80','2017-06-13 12:23:50','2017-06-13 12:23:50','1/2/5/80/82','1','4','0'),('83',3,'80','2017-06-13 12:24:07','2017-06-13 12:24:07','1/2/5/80/83','2','4','0'),('84',3,'80','2017-06-13 12:24:25','2017-06-13 12:24:25','1/2/5/80/84','3','4','0'),('85',3,'80','2017-06-13 12:24:40','2017-06-13 12:24:40','1/2/5/80/85','4','4','0'),('86',3,'80','2017-06-13 12:24:57','2017-06-13 12:24:57','1/2/5/80/86','5','4','0'),('87',3,'80','2017-06-13 12:25:12','2017-06-13 12:25:12','1/2/5/80/87','6','4','0'),('88',3,'80','2017-06-13 12:25:27','2017-06-13 12:25:27','1/2/5/80/88','7','4','0'),('89',3,'80','2017-06-13 12:25:40','2017-06-13 12:25:40','1/2/5/80/89','8','4','0'),('90',3,'80','2017-06-13 12:25:53','2017-06-13 12:25:53','1/2/5/80/90','9','4','0'),('91',3,'80','2017-06-13 12:26:08','2017-06-13 12:26:08','1/2/5/80/91','10','4','0'),('92',3,'80','2017-06-13 12:26:21','2017-06-13 12:26:21','1/2/5/80/92','11','4','0'),('93',3,'80','2017-06-13 12:26:33','2017-06-13 12:26:33','1/2/5/80/93','12','4','0'),('94',3,'80','2017-06-13 12:26:45','2017-06-13 12:26:45','1/2/5/80/94','13','4','0'),('95',3,'81','2017-06-13 12:27:01','2017-06-13 12:27:01','1/2/5/81/95','1','4','0'),('96',3,'2','2017-06-16 04:23:41','2017-06-16 04:23:41','1/2/96','8','2','0');
/*!40000 ALTER TABLE `catalog_category_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_datetime`
--

DROP TABLE IF EXISTS `catalog_category_entity_datetime`;
CREATE TABLE `catalog_category_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Datetime Attribute Backend Table';

--
-- Table structure for table `catalog_category_entity_decimal`
--

DROP TABLE IF EXISTS `catalog_category_entity_decimal`;
CREATE TABLE `catalog_category_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Decimal Attribute Backend Table';

--
-- Table structure for table `catalog_category_entity_int`
--

DROP TABLE IF EXISTS `catalog_category_entity_int`;
CREATE TABLE `catalog_category_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=474 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Integer Attribute Backend Table';

--
-- Dumping data for table `catalog_category_entity_int`
--

LOCK TABLES `catalog_category_entity_int` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_int` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_int` VALUES ('1',69,0,'1','1'),('2',46,0,'2','1'),('3',69,0,'2','1'),('4',46,0,'3','1'),('5',54,0,'3','1'),('6',69,0,'3','1'),('7',70,0,'3','0'),('8',71,0,'3','0'),('9',46,0,'4','1'),('10',54,0,'4','1'),('11',69,0,'4','1'),('12',70,0,'4','0'),('13',71,0,'4','0'),('14',46,0,'5','1'),('15',54,0,'5','1'),('16',69,0,'5','1'),('17',70,0,'5','0'),('18',71,0,'5','0'),('19',46,0,'6','1'),('20',54,0,'6','1'),('21',69,0,'6','1'),('22',70,0,'6','0'),('23',71,0,'6','0'),('24',46,0,'7','1'),('25',54,0,'7','1'),('26',69,0,'7','1'),('27',70,0,'7','0'),('28',71,0,'7','0'),('29',46,0,'8','1'),('30',54,0,'8','1'),('31',69,0,'8','1'),('32',70,0,'8','0'),('33',71,0,'8','0'),('34',46,0,'9','1'),('35',54,0,'9','1'),('36',69,0,'9','0'),('37',70,0,'9','0'),('38',71,0,'9','0'),('39',46,0,'10','1'),('40',54,0,'10','1'),('41',69,0,'10','1'),('42',70,0,'10','0'),('43',71,0,'10','0'),('44',46,0,'11','1'),('45',54,0,'11','1'),('46',69,0,'11','1'),('47',70,0,'11','0'),('48',71,0,'11','0'),('49',46,0,'12','1'),('50',54,0,'12','1'),('51',69,0,'12','1'),('52',70,0,'12','0'),('53',71,0,'12','0'),('54',46,0,'13','1'),('55',54,0,'13','1'),('56',69,0,'13','1'),('57',70,0,'13','0'),('58',71,0,'13','0'),('59',46,0,'14','1'),('60',54,0,'14','1'),('61',69,0,'14','1'),('62',70,0,'14','0'),('63',71,0,'14','0'),('64',46,0,'15','1'),('65',54,0,'15','1'),('66',69,0,'15','1'),('67',70,0,'15','0'),('68',71,0,'15','0'),('69',46,0,'16','1'),('70',54,0,'16','1'),('71',69,0,'16','1'),('72',70,0,'16','0'),('73',71,0,'16','0'),('74',46,0,'17','1'),('75',54,0,'17','1'),('76',69,0,'17','1'),('77',70,0,'17','0'),('78',71,0,'17','0'),('79',46,0,'18','1'),('80',54,0,'18','1'),('81',69,0,'18','1'),('82',70,0,'18','0'),('83',71,0,'18','0'),('84',46,0,'19','1'),('85',54,0,'19','1'),('86',69,0,'19','1'),('87',70,0,'19','0'),('88',71,0,'19','0'),('89',46,0,'20','1'),('90',54,0,'20','1'),('91',69,0,'20','1'),('92',70,0,'20','0'),('93',71,0,'20','0'),('94',46,0,'21','1'),('95',54,0,'21','1'),('96',69,0,'21','1'),('97',70,0,'21','0'),('98',71,0,'21','0'),('99',46,0,'22','1'),('100',54,0,'22','1'),('101',69,0,'22','1'),('102',70,0,'22','0'),('103',71,0,'22','0'),('104',46,0,'23','1'),('105',54,0,'23','1'),('106',69,0,'23','1'),('107',70,0,'23','0'),('108',71,0,'23','0'),('109',46,0,'24','1'),('110',54,0,'24','1'),('111',69,0,'24','1'),('112',70,0,'24','0'),('113',71,0,'24','0'),('114',46,0,'25','1'),('115',54,0,'25','1'),('116',69,0,'25','1'),('117',70,0,'25','0'),('118',71,0,'25','0'),('119',46,0,'26','1'),('120',54,0,'26','1'),('121',69,0,'26','1'),('122',70,0,'26','0'),('123',71,0,'26','0'),('124',46,0,'27','1'),('125',54,0,'27','1'),('126',69,0,'27','1'),('127',70,0,'27','0'),('128',71,0,'27','0'),('129',46,0,'28','1'),('130',54,0,'28','1'),('131',69,0,'28','1'),('132',70,0,'28','0'),('133',71,0,'28','0'),('134',46,0,'29','1'),('135',54,0,'29','1'),('136',69,0,'29','1'),('137',70,0,'29','0'),('138',71,0,'29','0'),('139',46,0,'30','1'),('140',54,0,'30','1'),('141',69,0,'30','1'),('142',70,0,'30','0'),('143',71,0,'30','0'),('144',46,0,'31','1'),('145',54,0,'31','1'),('146',69,0,'31','1'),('147',70,0,'31','0'),('148',71,0,'31','0'),('149',46,0,'32','1'),('150',54,0,'32','1'),('151',69,0,'32','1'),('152',70,0,'32','0'),('153',71,0,'32','0'),('154',46,0,'33','1'),('155',54,0,'33','1'),('156',69,0,'33','1'),('157',70,0,'33','0'),('158',71,0,'33','0'),('159',46,0,'34','1'),('160',54,0,'34','1'),('161',69,0,'34','1'),('162',70,0,'34','0'),('163',71,0,'34','0'),('164',46,0,'35','1'),('165',54,0,'35','1'),('166',69,0,'35','1'),('167',70,0,'35','0'),('168',71,0,'35','0'),('169',46,0,'36','1'),('170',54,0,'36','1'),('171',69,0,'36','1'),('172',70,0,'36','0'),('173',71,0,'36','0'),('174',46,0,'37','1'),('175',54,0,'37','1'),('176',69,0,'37','1'),('177',70,0,'37','0'),('178',71,0,'37','0'),('179',46,0,'38','1'),('180',54,0,'38','1'),('181',69,0,'38','1'),('182',70,0,'38','0'),('183',71,0,'38','0'),('184',46,0,'39','1'),('185',54,0,'39','1'),('186',69,0,'39','1'),('187',70,0,'39','0'),('188',71,0,'39','0'),('189',46,0,'40','1'),('190',54,0,'40','1'),('191',69,0,'40','1'),('192',70,0,'40','0'),('193',71,0,'40','0'),('194',46,0,'41','1'),('195',54,0,'41','1'),('196',69,0,'41','1'),('197',70,0,'41','0'),('198',71,0,'41','0'),('199',46,0,'42','1'),('200',54,0,'42','1'),('201',69,0,'42','1'),('202',70,0,'42','0'),('203',71,0,'42','0'),('204',46,0,'43','1'),('205',54,0,'43','1'),('206',69,0,'43','1'),('207',70,0,'43','0'),('208',71,0,'43','0'),('209',46,0,'44','1'),('210',54,0,'44','1'),('211',69,0,'44','1'),('212',70,0,'44','0'),('213',71,0,'44','0'),('214',46,0,'45','1'),('215',54,0,'45','1'),('216',69,0,'45','1'),('217',70,0,'45','0'),('218',71,0,'45','0'),('219',46,0,'46','1'),('220',54,0,'46','1'),('221',69,0,'46','1'),('222',70,0,'46','0'),('223',71,0,'46','0'),('224',46,0,'47','1'),('225',54,0,'47','1'),('226',69,0,'47','1'),('227',70,0,'47','0'),('228',71,0,'47','0'),('229',46,0,'48','1'),('230',54,0,'48','1'),('231',69,0,'48','1'),('232',70,0,'48','0'),('233',71,0,'48','0'),('234',46,0,'49','1'),('235',54,0,'49','1'),('236',69,0,'49','1'),('237',70,0,'49','0'),('238',71,0,'49','0'),('239',46,0,'50','1'),('240',54,0,'50','1'),('241',69,0,'50','1'),('242',70,0,'50','0'),('243',71,0,'50','0'),('244',46,0,'51','1'),('245',54,0,'51','1'),('246',69,0,'51','1'),('247',70,0,'51','0'),('248',71,0,'51','0'),('249',46,0,'52','1'),('250',54,0,'52','1'),('251',69,0,'52','1'),('252',70,0,'52','0'),('253',71,0,'52','0'),('254',46,0,'53','1'),('255',54,0,'53','1'),('256',69,0,'53','1'),('257',70,0,'53','0'),('258',71,0,'53','0'),('259',46,0,'54','1'),('260',54,0,'54','1'),('261',69,0,'54','1'),('262',70,0,'54','0'),('263',71,0,'54','0'),('264',46,0,'55','1'),('265',54,0,'55','1'),('266',69,0,'55','1'),('267',70,0,'55','0'),('268',71,0,'55','0'),('269',46,0,'56','1'),('270',54,0,'56','1'),('271',69,0,'56','1'),('272',70,0,'56','0'),('273',71,0,'56','0'),('274',46,0,'57','1'),('275',54,0,'57','1'),('276',69,0,'57','1'),('277',70,0,'57','0'),('278',71,0,'57','0'),('279',46,0,'58','1'),('280',54,0,'58','1'),('281',69,0,'58','1'),('282',70,0,'58','0'),('283',71,0,'58','0'),('284',46,0,'59','1'),('285',54,0,'59','1'),('286',69,0,'59','1'),('287',70,0,'59','0'),('288',71,0,'59','0'),('289',46,0,'60','1'),('290',54,0,'60','1'),('291',69,0,'60','1'),('292',70,0,'60','0'),('293',71,0,'60','0'),('294',46,0,'61','1'),('295',54,0,'61','1'),('296',69,0,'61','1'),('297',70,0,'61','0'),('298',71,0,'61','0'),('299',46,0,'62','1'),('300',54,0,'62','1'),('301',69,0,'62','1'),('302',70,0,'62','0'),('303',71,0,'62','0'),('304',46,0,'63','1'),('305',54,0,'63','1'),('306',69,0,'63','1'),('307',70,0,'63','0'),('308',71,0,'63','0'),('309',46,0,'64','1'),('310',54,0,'64','1'),('311',69,0,'64','1'),('312',70,0,'64','0'),('313',71,0,'64','0'),('314',46,0,'65','1'),('315',54,0,'65','1'),('316',69,0,'65','1'),('317',70,0,'65','0'),('318',71,0,'65','0'),('319',46,0,'66','1'),('320',54,0,'66','1'),('321',69,0,'66','1'),('322',70,0,'66','0'),('323',71,0,'66','0'),('324',46,0,'67','1'),('325',54,0,'67','1'),('326',69,0,'67','1'),('327',70,0,'67','0'),('328',71,0,'67','0'),('329',46,0,'68','1'),('330',54,0,'68','1'),('331',69,0,'68','1'),('332',70,0,'68','0'),('333',71,0,'68','0'),('334',46,0,'69','1'),('335',54,0,'69','1'),('336',69,0,'69','1'),('337',70,0,'69','0'),('338',71,0,'69','0'),('339',46,0,'70','1'),('340',54,0,'70','1'),('341',69,0,'70','1'),('342',70,0,'70','0'),('343',71,0,'70','0'),('344',46,0,'71','1'),('345',54,0,'71','1'),('346',69,0,'71','1'),('347',70,0,'71','0'),('348',71,0,'71','0'),('349',46,0,'72','1'),('350',54,0,'72','1'),('351',69,0,'72','1'),('352',70,0,'72','0'),('353',71,0,'72','0'),('354',46,0,'73','1'),('355',54,0,'73','1'),('356',69,0,'73','1'),('357',70,0,'73','0'),('358',71,0,'73','0'),('359',46,0,'74','1'),('360',54,0,'74','1'),('361',69,0,'74','1'),('362',70,0,'74','0'),('363',71,0,'74','0'),('364',46,0,'75','1'),('365',54,0,'75','1'),('366',69,0,'75','1'),('367',70,0,'75','0'),('368',71,0,'75','0'),('369',46,0,'76','1'),('370',54,0,'76','1'),('371',69,0,'76','1'),('372',70,0,'76','0'),('373',71,0,'76','0'),('374',46,0,'77','1'),('375',54,0,'77','1'),('376',69,0,'77','1'),('377',70,0,'77','0'),('378',71,0,'77','0'),('379',46,0,'78','1'),('380',54,0,'78','1'),('381',69,0,'78','1'),('382',70,0,'78','0'),('383',71,0,'78','0'),('384',46,0,'79','1'),('385',54,0,'79','1'),('386',69,0,'79','1'),('387',70,0,'79','0'),('388',71,0,'79','0'),('389',46,0,'80','1'),('390',54,0,'80','1'),('391',69,0,'80','1'),('392',70,0,'80','0'),('393',71,0,'80','0'),('394',46,0,'81','1'),('395',54,0,'81','1'),('396',69,0,'81','1'),('397',70,0,'81','0'),('398',71,0,'81','0'),('399',46,0,'82','1'),('400',54,0,'82','1'),('401',69,0,'82','1'),('402',70,0,'82','0'),('403',71,0,'82','0'),('404',46,0,'83','1'),('405',54,0,'83','1'),('406',69,0,'83','1'),('407',70,0,'83','0'),('408',71,0,'83','0'),('409',46,0,'84','1'),('410',54,0,'84','1'),('411',69,0,'84','1'),('412',70,0,'84','0'),('413',71,0,'84','0'),('414',46,0,'85','1'),('415',54,0,'85','1'),('416',69,0,'85','1'),('417',70,0,'85','0'),('418',71,0,'85','0'),('419',46,0,'86','1'),('420',54,0,'86','1'),('421',69,0,'86','1'),('422',70,0,'86','0'),('423',71,0,'86','0'),('424',46,0,'87','1'),('425',54,0,'87','1'),('426',69,0,'87','1'),('427',70,0,'87','0'),('428',71,0,'87','0'),('429',46,0,'88','1'),('430',54,0,'88','1'),('431',69,0,'88','1'),('432',70,0,'88','0'),('433',71,0,'88','0'),('434',46,0,'89','1'),('435',54,0,'89','1'),('436',69,0,'89','1'),('437',70,0,'89','0'),('438',71,0,'89','0'),('439',46,0,'90','1'),('440',54,0,'90','1'),('441',69,0,'90','1'),('442',70,0,'90','0'),('443',71,0,'90','0'),('444',46,0,'91','1'),('445',54,0,'91','1'),('446',69,0,'91','1'),('447',70,0,'91','0'),('448',71,0,'91','0'),('449',46,0,'92','1'),('450',54,0,'92','1'),('451',69,0,'92','1'),('452',70,0,'92','0'),('453',71,0,'92','0'),('454',46,0,'93','1'),('455',54,0,'93','1'),('456',69,0,'93','1'),('457',70,0,'93','0'),('458',71,0,'93','0'),('459',46,0,'94','1'),('460',54,0,'94','1'),('461',69,0,'94','1'),('462',70,0,'94','0'),('463',71,0,'94','0'),('464',46,0,'95','1'),('465',54,0,'95','1'),('466',69,0,'95','1'),('467',70,0,'95','0'),('468',71,0,'95','0'),('469',46,0,'96','1'),('470',54,0,'96','1'),('471',69,0,'96','0'),('472',70,0,'96','0'),('473',71,0,'96','0');
/*!40000 ALTER TABLE `catalog_category_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_text`
--

DROP TABLE IF EXISTS `catalog_category_entity_text`;
CREATE TABLE `catalog_category_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Text Attribute Backend Table';

--
-- Table structure for table `catalog_category_entity_varchar`
--

DROP TABLE IF EXISTS `catalog_category_entity_varchar`;
CREATE TABLE `catalog_category_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=385 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Varchar Attribute Backend Table';

--
-- Dumping data for table `catalog_category_entity_varchar`
--

LOCK TABLES `catalog_category_entity_varchar` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_varchar` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_varchar` VALUES ('1',45,0,'1','Root Catalog'),('2',45,0,'2','Default Category'),('3',52,0,'2','PRODUCTS'),('4',45,0,'3','Make-Up'),('5',52,0,'3','PRODUCTS'),('6',117,0,'3','make-up'),('7',118,0,'3','make-up'),('8',45,0,'4','Skincare'),('9',52,0,'4','PRODUCTS'),('10',117,0,'4','skincare'),('11',118,0,'4','skincare'),('12',45,0,'5','Fragrance'),('13',52,0,'5','PRODUCTS'),('14',117,0,'5','fragrance'),('15',118,0,'5','fragrance'),('16',45,0,'6','Best Sellers'),('17',52,0,'6','PRODUCTS'),('18',117,0,'6','best-sellers'),('19',118,0,'6','best-sellers'),('20',45,0,'7','Online Exclusives'),('21',52,0,'7','PRODUCTS'),('22',117,0,'7','online-exclusives'),('23',118,0,'7','online-exclusives'),('24',45,0,'8','Magazine'),('25',52,0,'8','PRODUCTS'),('26',117,0,'8','magazine'),('27',118,0,'8','magazine'),('28',45,0,'9','Gift Sets'),('29',52,0,'9','PRODUCTS'),('30',117,0,'9','gift-sets'),('31',118,0,'9','gift-sets'),('36',45,0,'10','FACE'),('37',52,0,'10','PRODUCTS'),('38',117,0,'10','face'),('39',118,0,'10','make-up/face'),('40',45,0,'11','EYES'),('41',52,0,'11','PRODUCTS'),('42',117,0,'11','eyes'),('43',118,0,'11','make-up/eyes'),('44',45,0,'12','LIPS'),('45',52,0,'12','PRODUCTS'),('46',117,0,'12','lips'),('47',118,0,'12','make-up/lips'),('48',45,0,'13','NAILS'),('49',52,0,'13','PRODUCTS'),('50',117,0,'13','nails'),('51',118,0,'13','make-up/nails'),('52',45,0,'14','MORE MAKE-UP'),('53',52,0,'14','PRODUCTS'),('54',117,0,'14','more-make-up'),('55',118,0,'14','make-up/more-make-up'),('56',45,0,'15','Foundation'),('57',52,0,'15','PRODUCTS'),('58',117,0,'15','foundation'),('59',118,0,'15','make-up/face/foundation'),('60',45,0,'16','Primer'),('61',52,0,'16','PRODUCTS'),('62',117,0,'16','primer'),('63',118,0,'16','make-up/face/primer'),('64',45,0,'17','Tinted Moisturisers'),('65',52,0,'17','PRODUCTS'),('66',117,0,'17','tinted-moisturisers'),('67',118,0,'17','make-up/face/tinted-moisturisers'),('68',45,0,'18','Concealer'),('69',52,0,'18','PRODUCTS'),('70',117,0,'18','concealer'),('71',118,0,'18','make-up/face/concealer'),('72',45,0,'19','Powder'),('73',52,0,'19','PRODUCTS'),('74',117,0,'19','powder'),('75',118,0,'19','make-up/face/powder'),('76',45,0,'20','Blushes & Bronzers'),('77',52,0,'20','PRODUCTS'),('78',117,0,'20','blushes-and-bronzers'),('79',118,0,'20','make-up/face/blushes-and-bronzers'),('80',45,0,'21','Highlighter'),('81',52,0,'21','PRODUCTS'),('82',117,0,'21','highlighter'),('83',118,0,'21','make-up/face/highlighter'),('84',45,0,'22','Mascara'),('85',52,0,'22','PRODUCTS'),('86',117,0,'22','mascara'),('87',118,0,'22','make-up/eyes/mascara'),('88',45,0,'23','Waterproof Mascara'),('89',52,0,'23','PRODUCTS'),('90',117,0,'23','waterproof-mascara'),('91',118,0,'23','make-up/eyes/waterproof-mascara'),('92',45,0,'24','Eyeshadows'),('93',52,0,'24','PRODUCTS'),('94',117,0,'24','eyeshadows'),('95',118,0,'24','make-up/eyes/eyeshadows'),('96',45,0,'25','Eyeliners & Eye Pencils'),('97',52,0,'25','PRODUCTS'),('98',117,0,'25','eyeliners-and-eye-pencils'),('99',118,0,'25','make-up/eyes/eyeliners-and-eye-pencils'),('100',45,0,'26','Eyebrows'),('101',52,0,'26','PRODUCTS'),('102',117,0,'26','eyebrows'),('103',118,0,'26','make-up/eyes/eyebrows'),('104',45,0,'27','Eye Makeup Remover'),('105',52,0,'27','PRODUCTS'),('106',117,0,'27','eye-makeup-remover'),('107',118,0,'27','make-up/eyes/eye-makeup-remover'),('108',45,0,'28','Lipsticks'),('109',52,0,'28','PRODUCTS'),('110',117,0,'28','lipsticks'),('111',118,0,'28','make-up/lips/lipsticks'),('112',45,0,'29','Lip Oil'),('113',52,0,'29','PRODUCTS'),('114',117,0,'29','lip-oil'),('115',118,0,'29','make-up/lips/lip-oil'),('116',45,0,'30','Lip Gloss'),('117',52,0,'30','PRODUCTS'),('118',117,0,'30','lip-gloss'),('119',118,0,'30','make-up/lips/lip-gloss'),('120',45,0,'31','Lip Liner & Pencils'),('121',52,0,'31','PRODUCTS'),('122',117,0,'31','lip-liner-and-pencils'),('123',118,0,'31','make-up/lips/lip-liner-and-pencils'),('124',45,0,'32','Liquid Lipstick'),('125',52,0,'32','PRODUCTS'),('126',117,0,'32','liquid-lipstick'),('127',118,0,'32','make-up/lips/liquid-lipstick'),('128',45,0,'33','Nail Colour'),('129',52,0,'33','PRODUCTS'),('130',117,0,'33','nail-colour'),('131',118,0,'33','make-up/nails/nail-colour'),('132',45,0,'34','Complexion Finder'),('133',52,0,'34','PRODUCTS'),('134',117,0,'34','complexion-finder'),('135',118,0,'34','make-up/more-make-up/complexion-finder'),('136',45,0,'35','Mascara Finder'),('137',52,0,'35','PRODUCTS'),('138',117,0,'35','mascara-finder'),('139',118,0,'35','make-up/more-make-up/mascara-finder'),('140',45,0,'36','Gift Finder'),('141',52,0,'36','PRODUCTS'),('142',117,0,'36','gift-finder'),('143',118,0,'36','make-up/more-make-up/gift-finder'),('144',45,0,'37','Summer 2017 Makeup Collection'),('145',52,0,'37','PRODUCTS'),('146',117,0,'37','summer-2017-makeup-collection'),('147',118,0,'37','make-up/more-make-up/summer-2017-makeup-collection'),('148',45,0,'38','Olympia Le Tan Makeup Collection'),('149',52,0,'38','PRODUCTS'),('150',117,0,'38','olympia-le-tan-makeup-collection'),('151',118,0,'38','make-up/more-make-up/olympia-le-tan-makeup-collection'),('152',45,0,'39','Engraving Makeup'),('153',52,0,'39','PRODUCTS'),('154',117,0,'39','engraving-makeup'),('155',118,0,'39','make-up/more-make-up/engraving-makeup'),('156',45,0,'40','BY PRODUCT CATEGORY'),('157',52,0,'40','PRODUCTS'),('158',117,0,'40','by-product-category'),('159',118,0,'40','skincare/by-product-category'),('160',45,0,'41','BY SKIN NEED'),('161',52,0,'41','PRODUCTS'),('162',117,0,'41','by-skin-need'),('163',118,0,'41','skincare/by-skin-need'),('164',45,0,'42','BY RANGE'),('165',52,0,'42','PRODUCTS'),('166',117,0,'42','by-range'),('167',118,0,'42','skincare/by-range'),('168',45,0,'43','MORE SKINCARE'),('169',52,0,'43','PRODUCTS'),('170',117,0,'43','more-skincare'),('171',118,0,'43','skincare/more-skincare'),('172',45,0,'44','The Serums'),('173',52,0,'44','PRODUCTS'),('174',117,0,'44','the-serums'),('175',118,0,'44','skincare/by-product-category/the-serums'),('176',45,0,'45','The Concentrates'),('177',52,0,'45','PRODUCTS'),('178',117,0,'45','the-concentrates'),('179',118,0,'45','skincare/by-product-category/the-concentrates'),('180',45,0,'46','The Creams'),('181',52,0,'46','PRODUCTS'),('182',117,0,'46','the-creams'),('183',118,0,'46','skincare/by-product-category/the-creams'),('184',45,0,'47','The Night Creams'),('185',52,0,'47','PRODUCTS'),('186',117,0,'47','the-night-creams'),('187',118,0,'47','skincare/by-product-category/the-night-creams'),('188',45,0,'48','The Exceptional Skincare'),('189',52,0,'48','PRODUCTS'),('190',117,0,'48','the-exceptional-skincare'),('191',118,0,'48','skincare/by-product-category/the-exceptional-skincare'),('192',45,0,'49','The Eye Care & Lip Care'),('193',52,0,'49','PRODUCTS'),('194',117,0,'49','the-eye-care-and-lip-care'),('195',118,0,'49','skincare/by-product-category/the-eye-care-and-lip-care'),('197',45,0,'50','The Cleansers & Toners'),('198',52,0,'50','PRODUCTS'),('199',117,0,'50','the-cleansers-and-toners'),('200',118,0,'50','skincare/by-product-category/the-cleansers-and-toners'),('201',45,0,'51','The Masks & Exfoliators'),('202',52,0,'51','PRODUCTS'),('203',117,0,'51','the-masks-and-exfoliators'),('204',118,0,'51','skincare/by-product-category/the-masks-and-exfoliators'),('205',45,0,'52','The Bodycare'),('206',52,0,'52','PRODUCTS'),('207',117,0,'52','the-bodycare'),('208',118,0,'52','skincare/by-product-category/the-bodycare'),('209',45,0,'53','The Suncare'),('210',52,0,'53','PRODUCTS'),('211',117,0,'53','the-suncare'),('212',118,0,'53','skincare/by-product-category/the-suncare'),('213',45,0,'54','The Men Care'),('214',52,0,'54','PRODUCTS'),('215',117,0,'54','the-men-care'),('216',118,0,'54','skincare/by-product-category/the-men-care'),('217',45,0,'55','The Fluids'),('218',52,0,'55','PRODUCTS'),('219',117,0,'55','the-fluids'),('220',118,0,'55','skincare/by-product-category/the-fluids'),('221',45,0,'56','Anti-Aging'),('222',52,0,'56','PRODUCTS'),('223',117,0,'56','anti-aging'),('224',118,0,'56','skincare/by-skin-need/anti-aging'),('225',45,0,'57','Lack Of Firmness'),('226',52,0,'57','PRODUCTS'),('227',117,0,'57','lack-of-firmness'),('228',118,0,'57','skincare/by-skin-need/lack-of-firmness'),('229',45,0,'58','Pores'),('230',52,0,'58','PRODUCTS'),('231',117,0,'58','pores'),('232',118,0,'58','skincare/by-skin-need/pores'),('233',45,0,'59','Wrinkles'),('234',52,0,'59','PRODUCTS'),('235',117,0,'59','wrinkles'),('236',118,0,'59','skincare/by-skin-need/wrinkles'),('237',45,0,'60','Dark Spots'),('238',52,0,'60','PRODUCTS'),('239',117,0,'60','dark-spots'),('240',118,0,'60','skincare/by-skin-need/dark-spots'),('241',45,0,'61','Exceptional Regeneration'),('242',52,0,'61','PRODUCTS'),('243',117,0,'61','exceptional-regeneration'),('244',118,0,'61','skincare/by-skin-need/exceptional-regeneration'),('245',45,0,'62','Lack Of Homogeneity'),('246',52,0,'62','PRODUCTS'),('247',117,0,'62','lack-of-homogeneity'),('248',118,0,'62','skincare/by-skin-need/lack-of-homogeneity'),('249',45,0,'63','Dullness'),('250',52,0,'63','PRODUCTS'),('251',117,0,'63','dullness'),('252',118,0,'63','skincare/by-skin-need/dullness'),('253',45,0,'64','Hydration'),('254',52,0,'64','PRODUCTS'),('255',117,0,'64','hydration'),('256',118,0,'64','skincare/by-skin-need/hydration'),('257',45,0,'65','Dryness'),('258',52,0,'65','PRODUCTS'),('259',117,0,'65','dryness'),('260',118,0,'65','skincare/by-skin-need/dryness'),('261',45,0,'66','Redness'),('262',52,0,'66','PRODUCTS'),('263',117,0,'66','redness'),('264',118,0,'66','skincare/by-skin-need/redness'),('265',45,0,'67','Fatigue'),('266',52,0,'67','PRODUCTS'),('267',117,0,'67','fatigue'),('268',118,0,'67','skincare/by-skin-need/fatigue'),('269',45,0,'68','Génifique'),('270',52,0,'68','PRODUCTS'),('271',117,0,'68','genifique'),('272',118,0,'68','skincare/by-range/genifique'),('273',45,0,'69','Hydra Zen'),('274',52,0,'69','PRODUCTS'),('275',117,0,'69','hydra-zen'),('276',118,0,'69','skincare/by-range/hydra-zen'),('277',45,0,'70','Rénergie'),('278',52,0,'70','PRODUCTS'),('279',117,0,'70','renergie'),('280',118,0,'70','skincare/by-range/renergie'),('281',45,0,'71','Absolue'),('282',52,0,'71','PRODUCTS'),('283',117,0,'71','absolue'),('284',118,0,'71','skincare/by-range/absolue'),('285',45,0,'72','Visionnaire'),('286',52,0,'72','PRODUCTS'),('287',117,0,'72','visionnaire'),('288',118,0,'72','skincare/by-range/visionnaire'),('289',45,0,'73','Blanc Expert'),('290',52,0,'73','PRODUCTS'),('291',117,0,'73','blanc-expert'),('292',118,0,'73','skincare/by-range/blanc-expert'),('293',45,0,'74','Énergie De Vie'),('294',52,0,'74','PRODUCTS'),('295',117,0,'74','energie-de-vie'),('296',118,0,'74','skincare/by-range/energie-de-vie'),('297',45,0,'75','Nutrix'),('298',52,0,'75','PRODUCTS'),('299',117,0,'75','nutrix'),('300',118,0,'75','skincare/by-range/nutrix'),('301',45,0,'76','Skin Beauty Consultation'),('302',52,0,'76','PRODUCTS'),('303',117,0,'76','skin-beauty-consultation'),('304',118,0,'76','skincare/more-skincare/skin-beauty-consultation'),('305',45,0,'77','Gift Finder'),('306',52,0,'77','PRODUCTS'),('307',117,0,'77','gift-finder'),('308',118,0,'77','skincare/more-skincare/gift-finder'),('309',45,0,'78','Olympia Le Tan Makeup Collection'),('310',52,0,'78','PRODUCTS'),('311',117,0,'78','olympia-le-tan-makeup-collection'),('312',118,0,'78','skincare/more-skincare/olympia-le-tan-makeup-collection'),('313',45,0,'79','Summer 2017 Makeup Collection'),('314',52,0,'79','PRODUCTS'),('315',117,0,'79','summer-2017-makeup-collection'),('316',118,0,'79','skincare/more-skincare/summer-2017-makeup-collection'),('317',45,0,'80','WOMEN\'S PERFUMES'),('318',52,0,'80','PRODUCTS'),('319',117,0,'80','women-s-perfumes'),('320',118,0,'80','fragrance/women-s-perfumes'),('321',45,0,'81','MEN\'S PERFUME'),('322',52,0,'81','PRODUCTS'),('323',117,0,'81','men-s-perfume'),('324',118,0,'81','fragrance/men-s-perfume'),('325',45,0,'82','La Vie Est Belle'),('326',52,0,'82','PRODUCTS'),('327',117,0,'82','la-vie-est-belle'),('328',118,0,'82','fragrance/women-s-perfumes/la-vie-est-belle'),('329',45,0,'83','Trésor'),('330',52,0,'83','PRODUCTS'),('331',117,0,'83','tresor'),('332',118,0,'83','fragrance/women-s-perfumes/tresor'),('333',45,0,'84','Trésor Midnight Rose'),('334',52,0,'84','PRODUCTS'),('335',117,0,'84','tresor-midnight-rose'),('336',118,0,'84','fragrance/women-s-perfumes/tresor-midnight-rose'),('337',45,0,'85','Trésor In Love'),('338',52,0,'85','PRODUCTS'),('339',117,0,'85','tresor-in-love'),('340',118,0,'85','fragrance/women-s-perfumes/tresor-in-love'),('341',45,0,'86','La Nuit Trésor'),('342',52,0,'86','PRODUCTS'),('343',117,0,'86','la-nuit-tresor'),('344',118,0,'86','fragrance/women-s-perfumes/la-nuit-tresor'),('345',45,0,'87','The World Of Ô'),('346',52,0,'87','PRODUCTS'),('347',117,0,'87','the-world-of-o'),('348',118,0,'87','fragrance/women-s-perfumes/the-world-of-o'),('349',45,0,'88','Miracle'),('350',52,0,'88','PRODUCTS'),('351',117,0,'88','miracle'),('352',118,0,'88','fragrance/women-s-perfumes/miracle'),('353',45,0,'89','Hypnôse'),('354',52,0,'89','PRODUCTS'),('355',117,0,'89','hypnose'),('356',118,0,'89','fragrance/women-s-perfumes/hypnose'),('357',45,0,'90','Poême'),('358',52,0,'90','PRODUCTS'),('359',117,0,'90','poeme'),('360',118,0,'90','fragrance/women-s-perfumes/poeme'),('361',45,0,'91','Magie Noire'),('362',52,0,'91','PRODUCTS'),('363',117,0,'91','magie-noire'),('364',118,0,'91','fragrance/women-s-perfumes/magie-noire'),('365',45,0,'92','Maison Lancôme'),('366',52,0,'92','PRODUCTS'),('367',117,0,'92','maison-lancome'),('368',118,0,'92','fragrance/women-s-perfumes/maison-lancome'),('369',45,0,'93','Mother\'s Day'),('370',52,0,'93','PRODUCTS'),('371',117,0,'93','mother-s-day'),('372',118,0,'93','fragrance/women-s-perfumes/mother-s-day'),('373',45,0,'94','Engraving Fragrance'),('374',52,0,'94','PRODUCTS'),('375',117,0,'94','engraving-fragrance'),('376',118,0,'94','fragrance/women-s-perfumes/engraving-fragrance'),('377',45,0,'95','Hypnôse Men'),('378',52,0,'95','PRODUCTS'),('379',117,0,'95','hypnose-men'),('380',118,0,'95','fragrance/men-s-perfume/hypnose-men'),('381',45,0,'96','Samples'),('382',52,0,'96','PRODUCTS'),('383',117,0,'96','samples'),('384',118,0,'96','samples');
/*!40000 ALTER TABLE `catalog_category_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product`
--

DROP TABLE IF EXISTS `catalog_category_product`;
CREATE TABLE `catalog_category_product` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`entity_id`,`category_id`,`product_id`),
  UNIQUE KEY `CATALOG_CATEGORY_PRODUCT_CATEGORY_ID_PRODUCT_ID` (`category_id`,`product_id`),
  KEY `CATALOG_CATEGORY_PRODUCT_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Category Linkage Table';

--
-- Dumping data for table `catalog_category_product`
--

LOCK TABLES `catalog_category_product` WRITE;
/*!40000 ALTER TABLE `catalog_category_product` DISABLE KEYS */;
INSERT INTO `catalog_category_product` VALUES ('2','3','2','1'),('3','3','3','1'),('4','5','4','1'),('5','80','4','1'),('6','81','4','1'),('7','5','5','1'),('8','80','5','1'),('9','81','5','1'),('10','5','6','1'),('11','80','6','1'),('12','81','6','1'),('13','5','7','1'),('14','80','7','1'),('15','81','7','1'),('16','5','8','1'),('17','80','8','1'),('18','81','8','1'),('19','5','9','1'),('20','80','9','1'),('21','81','9','1'),('22','5','10','1'),('23','80','10','1'),('24','81','10','1'),('25','4','2','1'),('26','10','2','1'),('27','15','2','1'),('28','16','2','1');
/*!40000 ALTER TABLE `catalog_category_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product_index`
--

DROP TABLE IF EXISTS `catalog_category_product_index`;
CREATE TABLE `catalog_category_product_index` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) DEFAULT NULL COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  PRIMARY KEY (`category_id`,`product_id`,`store_id`),
  KEY `CAT_CTGR_PRD_IDX_PRD_ID_STORE_ID_CTGR_ID_VISIBILITY` (`product_id`,`store_id`,`category_id`,`visibility`),
  KEY `CAT_CTGR_PRD_IDX_STORE_ID_CTGR_ID_VISIBILITY_IS_PARENT_POSITION` (`store_id`,`category_id`,`visibility`,`is_parent`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Index';

--
-- Dumping data for table `catalog_category_product_index`
--

LOCK TABLES `catalog_category_product_index` WRITE;
/*!40000 ALTER TABLE `catalog_category_product_index` DISABLE KEYS */;
INSERT INTO `catalog_category_product_index` VALUES ('2','2','1',1,1,4),('2','3','1',1,1,4),('2','10','1',1,1,4),('3','2','1',1,1,4),('3','3','1',1,1,4),('4','2','1',1,1,4),('5','10','1',1,1,4),('10','2','1',1,1,4),('15','2','1',1,1,4),('16','2','1',1,1,4),('80','10','1',1,1,4),('81','10','1',1,1,4),('2','2','1',1,2,4),('2','3','1',1,2,4),('2','10','1',1,2,4),('3','2','1',1,2,4),('3','3','1',1,2,4),('4','2','1',1,2,4),('5','10','1',1,2,4),('10','2','1',1,2,4),('15','2','1',1,2,4),('16','2','1',1,2,4),('80','10','1',1,2,4),('81','10','1',1,2,4);
/*!40000 ALTER TABLE `catalog_category_product_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product_index_tmp`
--

DROP TABLE IF EXISTS `catalog_category_product_index_tmp`;
CREATE TABLE `catalog_category_product_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  KEY `CAT_CTGR_PRD_IDX_TMP_PRD_ID_CTGR_ID_STORE_ID` (`product_id`,`category_id`,`store_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Indexer Temp Table';

--
-- Table structure for table `catalog_compare_item`
--

DROP TABLE IF EXISTS `catalog_compare_item`;
CREATE TABLE `catalog_compare_item` (
  `catalog_compare_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Compare Item ID',
  `visitor_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Visitor ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`catalog_compare_item_id`),
  KEY `CATALOG_COMPARE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `CATALOG_COMPARE_ITEM_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  KEY `CATALOG_COMPARE_ITEM_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `CATALOG_COMPARE_ITEM_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Compare Table';

--
-- Table structure for table `catalog_eav_attribute`
--

DROP TABLE IF EXISTS `catalog_eav_attribute`;
CREATE TABLE `catalog_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `frontend_input_renderer` varchar(255) DEFAULT NULL COMMENT 'Frontend Input Renderer',
  `is_global` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Global',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `is_searchable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable',
  `is_filterable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable',
  `is_comparable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Comparable',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `is_html_allowed_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is HTML Allowed On Front',
  `is_used_for_price_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Price Rules',
  `is_filterable_in_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable In Search',
  `used_in_product_listing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used In Product Listing',
  `used_for_sort_by` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Sorting',
  `apply_to` varchar(255) DEFAULT NULL COMMENT 'Apply To',
  `is_visible_in_advanced_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible In Advanced Search',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_wysiwyg_enabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is WYSIWYG Enabled',
  `is_used_for_promo_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Promo Rules',
  `is_required_in_admin_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Required In Admin Store',
  `is_used_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used in Grid',
  `is_visible_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible in Grid',
  `is_filterable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable in Grid',
  `search_weight` float NOT NULL DEFAULT '1' COMMENT 'Search Weight',
  `additional_data` text COMMENT 'Additional swatch attributes data',
  PRIMARY KEY (`attribute_id`),
  KEY `CATALOG_EAV_ATTRIBUTE_USED_FOR_SORT_BY` (`used_for_sort_by`),
  KEY `CATALOG_EAV_ATTRIBUTE_USED_IN_PRODUCT_LISTING` (`used_in_product_listing`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog EAV Attribute Table';

--
-- Dumping data for table `catalog_eav_attribute`
--

LOCK TABLES `catalog_eav_attribute` WRITE;
/*!40000 ALTER TABLE `catalog_eav_attribute` DISABLE KEYS */;
INSERT INTO `catalog_eav_attribute` VALUES (45,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(46,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(47,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',1,0,0,0,0,0,'1',NULL),(48,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(49,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(50,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(51,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(52,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(53,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(54,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(55,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(56,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(57,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(58,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(59,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(60,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(61,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(62,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(63,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(64,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(65,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(66,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(67,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Sortby\\Available',0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(68,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Sortby\\DefaultSortby',0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(69,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(70,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(71,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(72,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Pricestep',0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(73,NULL,0,1,1,0,0,0,0,0,0,1,1,NULL,1,'0',0,0,0,0,0,0,'5',NULL),(74,NULL,1,1,1,0,1,0,0,0,0,0,0,NULL,1,'0',0,0,0,0,0,0,'6',NULL),(75,NULL,0,1,1,0,1,0,1,0,0,0,0,NULL,1,'0',1,0,0,0,0,0,'1',NULL),(76,NULL,0,1,1,0,1,0,1,0,0,1,0,NULL,1,'0',1,0,0,1,0,0,'1',NULL),(77,NULL,1,1,1,1,0,0,0,0,0,1,1,'simple,virtual,bundle,downloadable,configurable',1,'0',0,0,0,0,0,0,'1',NULL),(78,NULL,1,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(79,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,0,'1',NULL),(80,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,0,'1',NULL),(81,NULL,1,1,0,0,0,0,0,0,0,0,0,'simple,virtual,downloadable',0,'0',0,0,0,1,0,1,'1',NULL),(82,'Magento\\Catalog\\Block\\Adminhtml\\Product\\Helper\\Form\\Weight',1,1,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(83,NULL,1,1,1,1,1,0,0,0,0,0,0,'simple',1,'0',0,0,0,1,0,1,'1',NULL),(84,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(85,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(86,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(87,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(88,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(89,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(90,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(91,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(92,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,0,0,0,'1',NULL),(94,NULL,2,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(95,NULL,2,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(96,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(97,'Magento\\Framework\\Data\\Form\\Element\\Hidden',2,1,1,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,1,0,0,0,'1',NULL),(98,NULL,0,0,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,0,0,0,'1',NULL),(99,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,1,0,0,0,'1',NULL),(100,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(101,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(102,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(103,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(104,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(105,'Magento\\Catalog\\Block\\Adminhtml\\Product\\Helper\\Form\\Category',1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(106,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(107,NULL,1,0,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(108,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(109,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(110,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(111,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(112,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(113,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(114,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,bundle,grouped,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(115,'Magento\\CatalogInventory\\Block\\Adminhtml\\Form\\Field\\Stock',1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(116,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(117,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(118,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(119,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,1,0,1,'1',NULL),(120,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(121,'Magento\\Msrp\\Block\\Adminhtml\\Product\\Helper\\Form\\Type',1,1,0,0,0,0,0,0,0,1,0,'simple,virtual,downloadable,bundle,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(122,'Magento\\Msrp\\Block\\Adminhtml\\Product\\Helper\\Form\\Type\\Price',2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,downloadable,bundle,configurable',0,'0',0,0,0,0,0,0,'1',NULL),(123,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(124,NULL,1,1,0,0,0,0,0,0,0,0,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(125,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(126,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(127,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,'0',0,0,0,0,0,0,'1',NULL),(128,NULL,1,0,0,0,0,0,0,0,0,1,0,'downloadable',0,'0',0,0,0,0,0,0,'1',NULL),(129,NULL,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,'0',0,0,0,0,0,0,'1',NULL),(130,NULL,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,'0',0,0,0,0,0,0,'1',NULL),(131,NULL,1,0,0,0,0,0,0,0,0,1,0,'downloadable',0,'0',0,0,0,0,0,0,'1',NULL),(132,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,'0',0,0,0,0,0,0,'1',NULL),(133,NULL,2,1,1,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,'0',0,0,0,1,0,1,'1',NULL),(134,'Magento\\GiftMessage\\Block\\Adminhtml\\Product\\Helper\\Form\\Config',1,1,0,0,0,0,0,0,0,0,0,NULL,0,'0',0,0,0,1,0,0,'1',NULL),(135,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(136,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(137,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(138,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(139,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(141,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(142,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',0,0,0,0,1,0,'1',NULL),(143,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',0,0,0,0,1,0,'1',NULL),(144,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(145,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(147,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',0,0,0,0,1,0,'1',NULL),(148,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',0,0,0,0,1,0,'1',NULL),(149,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',0,0,0,0,1,0,'1',NULL),(150,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(151,NULL,0,1,1,1,1,1,1,0,0,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(152,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(155,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',0,0,0,0,1,0,'1',NULL),(156,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',0,0,0,0,1,0,'1',NULL),(157,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,'0',0,0,0,0,1,0,'1',NULL),(158,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(160,NULL,0,1,1,1,1,1,1,0,1,1,0,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(161,NULL,0,1,1,1,1,1,1,0,1,1,0,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(162,NULL,0,1,1,1,1,1,1,0,1,1,0,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(163,NULL,0,1,1,1,1,1,1,0,1,1,0,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(164,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(165,NULL,0,1,1,1,1,1,1,0,1,1,0,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(166,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(167,NULL,0,1,1,1,1,1,1,0,1,1,0,NULL,1,'0',0,1,0,0,1,0,'1',NULL),(168,NULL,0,1,1,1,1,1,1,0,1,1,1,NULL,1,'0',0,1,0,1,1,1,'1',NULL),(169,NULL,0,1,1,1,1,1,1,0,1,1,0,NULL,1,'0',0,1,0,1,1,1,'1',NULL),(170,NULL,1,1,0,0,0,1,1,0,0,1,0,NULL,0,'0',0,0,0,1,1,1,'1','a:3:{s:17:\"swatch_input_type\";s:6:\"visual\";s:28:\"update_product_preview_image\";s:1:\"0\";s:28:\"use_product_image_for_swatch\";s:1:\"0\";}');
/*!40000 ALTER TABLE `catalog_eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_bundle_option`
--

DROP TABLE IF EXISTS `catalog_product_bundle_option`;
CREATE TABLE `catalog_product_bundle_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `type` varchar(255) DEFAULT NULL COMMENT 'Type',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_OPTION_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option';

--
-- Table structure for table `catalog_product_bundle_option_value`
--

DROP TABLE IF EXISTS `catalog_product_bundle_option_value`;
CREATE TABLE `catalog_product_bundle_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_BUNDLE_OPTION_VALUE_OPTION_ID_STORE_ID` (`option_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option Value';

--
-- Table structure for table `catalog_product_bundle_price_index`
--

DROP TABLE IF EXISTS `catalog_product_bundle_price_index`;
CREATE TABLE `catalog_product_bundle_price_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `min_price` decimal(12,4) NOT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) NOT NULL COMMENT 'Max Price',
  PRIMARY KEY (`entity_id`,`website_id`,`customer_group_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_CUSTOMER_GROUP_ID` (`customer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Price Index';

--
-- Table structure for table `catalog_product_bundle_selection`
--

DROP TABLE IF EXISTS `catalog_product_bundle_selection`;
CREATE TABLE `catalog_product_bundle_selection` (
  `selection_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Selection Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `parent_product_id` int(10) unsigned NOT NULL COMMENT 'Parent Product Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  `selection_qty` decimal(12,4) DEFAULT NULL COMMENT 'Selection Qty',
  `selection_can_change_qty` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Selection Can Change Qty',
  PRIMARY KEY (`selection_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_OPTION_ID` (`option_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection';

--
-- Table structure for table `catalog_product_bundle_selection_price`
--

DROP TABLE IF EXISTS `catalog_product_bundle_selection_price`;
CREATE TABLE `catalog_product_bundle_selection_price` (
  `selection_id` int(10) unsigned NOT NULL COMMENT 'Selection Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  PRIMARY KEY (`selection_id`,`website_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection Price';

--
-- Table structure for table `catalog_product_bundle_stock_index`
--

DROP TABLE IF EXISTS `catalog_product_bundle_stock_index`;
CREATE TABLE `catalog_product_bundle_stock_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `stock_status` smallint(6) DEFAULT '0' COMMENT 'Stock Status',
  PRIMARY KEY (`entity_id`,`website_id`,`stock_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Stock Index';

--
-- Table structure for table `catalog_product_entity`
--

DROP TABLE IF EXISTS `catalog_product_entity`;
CREATE TABLE `catalog_product_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set ID',
  `type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'Type ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `has_options` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Has Options',
  `required_options` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required Options',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  PRIMARY KEY (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_ATTRIBUTE_SET_ID` (`attribute_set_id`),
  KEY `CATALOG_PRODUCT_ENTITY_SKU` (`sku`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Table';

--
-- Dumping data for table `catalog_product_entity`
--

LOCK TABLES `catalog_product_entity` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity` DISABLE KEYS */;
INSERT INTO `catalog_product_entity` VALUES ('2',4,'simple','162072-LAC-test2',0,0,'2017-06-13 07:00:29','2017-06-13 07:00:29'),('3',4,'simple','A00357-LAC-test3',0,0,'2017-06-13 07:13:55','2017-06-13 07:13:55'),('4',4,'simple','trésor in love temps  tmp-Red',0,0,'2017-06-20 12:56:15','2017-06-20 12:56:15'),('5',4,'simple','trésor in love temps  tmp-Green',0,0,'2017-06-20 12:56:15','2017-06-20 12:56:15'),('6',4,'simple','trésor in love temps  tmp-Yellow',0,0,'2017-06-20 12:56:15','2017-06-20 12:56:15'),('7',4,'simple','trésor in love temps  tmp-Black',0,0,'2017-06-20 12:56:16','2017-06-20 12:56:16'),('8',4,'simple','trésor in love temps  tmp-White',0,0,'2017-06-20 12:56:16','2017-06-20 12:56:16'),('9',4,'simple','trésor in love temps  tmp-Brown',0,0,'2017-06-20 12:56:16','2017-06-20 12:56:16'),('10',4,'configurable','trésor in love temps  tmp',1,1,'2017-06-20 12:56:16','2017-06-20 12:56:16');
/*!40000 ALTER TABLE `catalog_product_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_datetime`
--

DROP TABLE IF EXISTS `catalog_product_entity_datetime`;
CREATE TABLE `catalog_product_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Datetime Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_datetime`
--

LOCK TABLES `catalog_product_entity_datetime` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_datetime` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_datetime` VALUES ('1',94,0,'2','2017-06-13 00:00:00'),('2',94,0,'3','2017-06-13 00:00:00'),('3',95,0,'2','2018-06-30 00:00:00'),('4',95,0,'3','2018-06-30 00:00:00');
/*!40000 ALTER TABLE `catalog_product_entity_datetime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_decimal`
--

DROP TABLE IF EXISTS `catalog_product_entity_decimal`;
CREATE TABLE `catalog_product_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Decimal Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_decimal`
--

LOCK TABLES `catalog_product_entity_decimal` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_decimal` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_decimal` VALUES ('3',77,0,'2','28.9000'),('4',82,0,'2','0.5000'),('5',77,0,'3','13.0000'),('6',82,0,'3','0.5000'),('7',77,0,'4','10.0000'),('8',82,0,'4','1.0000'),('9',77,0,'5','10.0000'),('10',82,0,'5','1.0000'),('11',77,0,'6','10.0000'),('12',82,0,'6','1.0000'),('13',77,0,'7','10.0000'),('14',82,0,'7','1.0000'),('15',77,0,'8','10.0000'),('16',82,0,'8','1.0000'),('17',77,0,'9','10.0000'),('18',82,0,'9','1.0000'),('19',77,0,'10','10.0000'),('20',82,0,'10','1.0000');
/*!40000 ALTER TABLE `catalog_product_entity_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_gallery`
--

DROP TABLE IF EXISTS `catalog_product_entity_gallery`;
CREATE TABLE `catalog_product_entity_gallery` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Gallery Attribute Backend Table';

--
-- Table structure for table `catalog_product_entity_int`
--

DROP TABLE IF EXISTS `catalog_product_entity_int`;
CREATE TABLE `catalog_product_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_INT_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Integer Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_int`
--

LOCK TABLES `catalog_product_entity_int` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_int` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_int` VALUES ('20',97,0,'2','1'),('21',99,0,'2','4'),('22',115,0,'2','1'),('23',133,0,'2','2'),('25',136,0,'2','8'),('39',97,0,'3','1'),('40',99,0,'3','4'),('41',115,0,'3','1'),('42',133,0,'3','2'),('44',136,0,'3','9'),('62',152,0,'2','62'),('63',97,0,'4','1'),('64',99,0,'4','1'),('65',115,0,'4','1'),('66',133,0,'4','0'),('67',152,0,'4','62'),('68',170,0,'4','204'),('69',97,0,'5','1'),('70',99,0,'5','1'),('71',115,0,'5','1'),('72',133,0,'5','0'),('73',152,0,'5','62'),('74',170,0,'5','205'),('75',97,0,'6','1'),('76',99,0,'6','1'),('77',115,0,'6','1'),('78',133,0,'6','0'),('79',152,0,'6','62'),('80',170,0,'6','206'),('81',97,0,'7','1'),('82',99,0,'7','1'),('83',115,0,'7','1'),('84',133,0,'7','0'),('85',152,0,'7','62'),('86',170,0,'7','207'),('87',97,0,'8','1'),('88',99,0,'8','1'),('89',115,0,'8','1'),('90',133,0,'8','0'),('91',152,0,'8','62'),('92',170,0,'8','208'),('93',97,0,'9','1'),('94',99,0,'9','1'),('95',115,0,'9','1'),('96',133,0,'9','0'),('97',152,0,'9','62'),('98',170,0,'9','209'),('99',97,0,'10','1'),('100',99,0,'10','4'),('101',115,0,'10','1'),('102',133,0,'10','0'),('103',152,0,'10','62');
/*!40000 ALTER TABLE `catalog_product_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery`;
CREATE TABLE `catalog_product_entity_media_gallery` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  `media_type` varchar(32) NOT NULL DEFAULT 'image' COMMENT 'Media entry type',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visibility status',
  PRIMARY KEY (`value_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_media_gallery`
--

LOCK TABLES `catalog_product_entity_media_gallery` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery` VALUES ('1',90,'/s/a/sample1.jpg','image',0),('2',90,'/s/a/sample2.jpg','image',0),('3',90,'/s/a/sample3.jpg','image',0),('4',90,'/p/1/p1_1.jpg','image',0);
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value`;
CREATE TABLE `catalog_product_entity_media_gallery_value` (
  `value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Value ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Disabled',
  `record_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Record Id',
  PRIMARY KEY (`record_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_VALUE_ID` (`value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Value Table';

--
-- Dumping data for table `catalog_product_entity_media_gallery_value`
--

LOCK TABLES `catalog_product_entity_media_gallery_value` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery_value` VALUES ('3',0,'3',NULL,'1',0,'10'),('4',0,'10',NULL,'1',0,'16'),('2',0,'2',NULL,'1',0,'17');
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value_to_entity`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value_to_entity`;
CREATE TABLE `catalog_product_entity_media_gallery_value_to_entity` (
  `value_id` int(10) unsigned NOT NULL COMMENT 'Value media Entry ID',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Product entity ID',
  UNIQUE KEY `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_VAL_ID_ENTT_ID` (`value_id`,`entity_id`),
  KEY `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Media value to Product entity table';

--
-- Dumping data for table `catalog_product_entity_media_gallery_value_to_entity`
--

LOCK TABLES `catalog_product_entity_media_gallery_value_to_entity` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value_to_entity` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery_value_to_entity` VALUES ('2','2'),('3','3'),('4','10');
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value_to_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value_video`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value_video`;
CREATE TABLE `catalog_product_entity_media_gallery_value_video` (
  `value_id` int(10) unsigned NOT NULL COMMENT 'Media Entity ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `provider` varchar(32) DEFAULT NULL COMMENT 'Video provider ID',
  `url` text COMMENT 'Video URL',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `description` text COMMENT 'Page Meta Description',
  `metadata` text COMMENT 'Video meta data',
  UNIQUE KEY `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_VAL_ID_STORE_ID` (`value_id`,`store_id`),
  KEY `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_STORE_ID_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Video Table';

--
-- Table structure for table `catalog_product_entity_text`
--

DROP TABLE IF EXISTS `catalog_product_entity_text`;
CREATE TABLE `catalog_product_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Text Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_text`
--

LOCK TABLES `catalog_product_entity_text` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_text` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_text` VALUES ('8',75,0,'2','<p><span>Watch lashes turn into a doll-eyed look with Lanc&ocirc;me&rsquo;s Hypn&ocirc;se Doll Eyes.</span><br /><br /><span>A must-have mascara to create beautiful lashes that look extended and lifted, Hypn&ocirc;se Doll Eyes evenly coats lashes without weighing them down.&nbsp;</span><br /><br /><span>Sweep on using our cone shaped brush and watch your eyes transform into a dazzling wide-eyed look.</span><br /><br /><span>HOW TO APPLY HYPN&Ocirc;SE DOLL EYES</span><br /><span>&bull; Our unique cone shaped brush ensures the ideal load with each application, even in the inner corner of the eyes.&nbsp;</span><br /><span>&bull; Apply your mascara from the base of your lashes working through to the tips, in zigzag movements, for maximum coverage.</span></p>'),('9',76,0,'2','DOLL-EYED EFFECT MASCARA'),('10',85,0,'2','HYPNÔSE DOLL EYES'),('11',142,0,'2','• Volumized, extended and lifted lashes\r\n• Wide-eye effect\r\n• Flirtatious lash fringe for a bold, curved and clump-free finish\r\n• Precise, separated corner and bottom lashes'),('12',143,0,'2','AQUA / WATER / EAU • PARAFFIN • COPERNICIA CERIFERA CERA / CARNAUBA WAX / CIRE DE CARNAUBA • STEARETH-2 • CERA ALBA / BEESWAX / CIRE DABEILLE • ACACIA SENEGAL / ACACIA SENEGAL GUM • ALCOHOL DENAT. • STEARETH-2 • CETYL ALCOHOL • POTASSIUM CETYL PHOSPHATE • PEG/PPG-17/18 DIMETHICONE • TDI/TRIMELLITIC ANHYDRIDE • COPOLYMER • SODIUM POLYMETHACRYLATE • SILICA • 2-OLEAMIDO-1,3-OCTADECANEDIOL • PHENETHYL ALCOHOL • NYLON-12 • DISODIUM EDTA • HYDROXYETHYLCELLULOSE • CAPRYLYL GLYCOL • PANTHENOL • PENTYLENE GLYCOL • POLYQUATERNIUM-1 • SODIUM DEHYDROACETATE • [+/- MAY CONTAIN CI 777 / ULTRAMARINES • CI 77491 • CI 77499 / IRON OXIDES • MICA]\r\n\r\n* This ingredient list is subject to change, customers should refer to the product packaging for the most up-to-date ingredient list.'),('13',147,0,'2','Its unique cone shaped brush'),('14',156,0,'2','• New unique cone-shaped brush for an extremely smooth application, perfect for hard-to-reach corner and bottom lashes\r\n• Nylon bristles combined with the exclusive FiberShineTM formula to sculpt, curl and load every single lash'),('15',75,0,'3','<p><span>Meet Monsieur Big. Forget all the others.</span><br /><br /><span>UP TO 12X MORE VOLUME:&nbsp;</span><br /><span>Its volumizing brush provides big impact at first stroke, for bigger than life lashes, like never before.&nbsp;</span><br /><br /><span>BIG SOFT BRUSH :</span><br /><span>Its bristles made of exceptionally soft fibers hug lashes with incredible softness and ensure a smooth application, while separating and lifting lashes effortlessly.&nbsp;</span><br /><br /><span>NO CLUMPS, NO SMUDGES, NO TOUCH UPS: The innovative formula easily glides on lashes and leaves them perfectly put for up to 24 hours. Its ultra-dark pigments create the blackest intensity possible.&nbsp;</span><br /><br /><span>It\'s a match!</span></p>'),('16',76,0,'3','BIG VOLUME MASCARA UP TO 24 HOUR WEAR'),('17',85,0,'3','MONSIEUR BIG MASCARA - TRAVEL SIZE'),('18',142,0,'3','• Big Volume: up to 12x more volume than bare lashes\r\n• No flake, no smudge: up to 24 hour wear\r\n• Intense Color: Lancôme\'s Blackest Mascara\r\n• Easy, soft application\r\n• Tested under ophtalmological control\r\n• Suitable for sensitive eyes and contact lens wearers'),('19',143,0,'3','G768262 - INGREDIENTS : • AQUA/WATER • PARAFFIN • POTASSIUM CETYL PHOSPHATE • COPERNICIA CERIFERA CERA/CARNAUBA WAX • ETHYLENE/ACRYLIC ACID COPOLYMER • STYRENE/ACRYLATES/AMMONIUM METHACRYLATE COPOLYMER• CERA ALBA/BEESWAX • SYNTHETIC BEESWAX • BIS-DIGLYCERYL POLYACYLADIPATE-2 • POLYBUTENE • CETYL ALCOHOL • DIVINYLDIMETHICONE/DIMETHICONE COPOLYMER • STEARETH-20 • GLYCERYL DIBEHENATE • STEARETH-2 • C12-13 PARETH-23 • C12-13 PARETH-3 • TRIBEHENIN • ACACIA SENEGAL GUM • GLYVERYL BEHENATE • SODIUM LAURETH SULFATE • DISODIUM EDTA• HYDROGENATED JOJOBA OIL • HYDRGENATED PALM OIL • HYDROXYETHYLCELLULOSE • CAPRYLYL GLYCOL • TERTASODIUM EDTA • ETHYLENEDIAMINE/STEARYL DIMER DILINOLEATE COPOLYMER • BUTYLENE GLYCOL • BHT • PENTAERYTHRITYL TETRA-DI-BUTYL HYDROXYHYDROCINNAMATE • POTASSIUM SORBATE • SODIUM DEHYDROACETATE •+AG12/IRON OXIDES] PHENOXYETHANOL • [+/- MAY CONTAIN • CI (F.I.L. B179856/1) \r\n\r\n* This ingredient list is subject to change, customers should refer to the product packaging for the most up-to-date ingredient list.'),('20',147,0,'3','Big volume, with no effort. \r\n\r\nA brush made of soft, wavy bristles to encapsulate formula for big volume with a gentle touch.\r\n\r\nIntense look all day long with no flakes & no smudges.\r\n\r\nLancôme\'s blackest mascara yet.'),('21',156,0,'3','A BIG FORMULA, inspired by lipsticks:\r\nSupple waxes\r\n& binding polymers for optimal adhesion\r\n& for big, buildable volume. \r\nFilm-forming polymers \r\nto protect lashes all day long : no flakes, no smudges.\r\nUltra-black iron oxide to capture light for the blackest black. \r\n\r\nA BIG BRUSH:\r\nSoft, wavy bristles for big volume and sensorial application. \r\nBulk reservoirs to encapsulate formula and deliver the just the right amount. \r\nThe creamy formula glides on lashes to coat them effortlessly.'),('22',75,0,'4','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('23',76,0,'4','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('24',85,0,'4','trésor in love temps  tmp'),('25',75,0,'5','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('26',76,0,'5','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('27',85,0,'5','trésor in love temps  tmp'),('28',75,0,'6','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('29',76,0,'6','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('30',85,0,'6','trésor in love temps  tmp'),('31',75,0,'7','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('32',76,0,'7','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('33',85,0,'7','trésor in love temps  tmp'),('34',75,0,'8','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('35',76,0,'8','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('36',85,0,'8','trésor in love temps  tmp'),('37',75,0,'9','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('38',76,0,'9','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('39',85,0,'9','trésor in love temps  tmp'),('40',75,0,'10','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('41',76,0,'10','Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('42',85,0,'10','trésor in love temps  tmp');
/*!40000 ALTER TABLE `catalog_product_entity_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_tier_price`
--

DROP TABLE IF EXISTS `catalog_product_entity_tier_price`;
CREATE TABLE `catalog_product_entity_tier_price` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `all_groups` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Applicable To All Customer Groups',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group ID',
  `qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'QTY',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_E8AB433B9ACB00343ABB312AD2FAB087` (`entity_id`,`all_groups`,`customer_group_id`,`qty`,`website_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TIER_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Attribute Backend Table';

--
-- Table structure for table `catalog_product_entity_varchar`
--

DROP TABLE IF EXISTS `catalog_product_entity_varchar`;
CREATE TABLE `catalog_product_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Varchar Attribute Backend Table';

--
-- Dumping data for table `catalog_product_entity_varchar`
--

LOCK TABLES `catalog_product_entity_varchar` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_varchar` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_varchar` VALUES ('20',73,0,'2','HYPNÔSE DOLL EYES'),('21',84,0,'2','HYPNÔSE DOLL EYES'),('22',86,0,'2','HYPNÔSE DOLL EYES Watch lashes turn into a doll-eyed look with Lancôme’s Hypnôse Doll Eyes.A must-have mascara to create beautiful lashes that look extended and lifted, Hypnôse Doll Eyes evenly coats lashes without weighing them down. Sweep on using our c'),('23',87,0,'2','/s/a/sample2.jpg'),('24',88,0,'2','/s/a/sample2.jpg'),('25',89,0,'2','/s/a/sample2.jpg'),('26',106,0,'2','container2'),('27',114,0,'2','TH'),('28',119,0,'2','dummy-test2'),('29',132,0,'2','/s/a/sample2.jpg'),('30',134,0,'2','2'),('35',73,0,'3','MONSIEUR BIG MASCARA - TRAVEL SIZE'),('36',84,0,'3','MONSIEUR BIG MASCARA - TRAVEL SIZE'),('37',86,0,'3','MONSIEUR BIG MASCARA - TRAVEL SIZE Meet Monsieur Big. Forget all the others.UP TO 12X MORE VOLUME: Its volumizing brush provides big impact at first stroke, for bigger than life lashes, like never before. BIG SOFT BRUSH :Its bristles made of exceptionally'),('38',87,0,'3','/s/a/sample3.jpg'),('39',88,0,'3','/s/a/sample3.jpg'),('40',89,0,'3','/s/a/sample3.jpg'),('41',106,0,'3','container2'),('42',114,0,'3','TH'),('43',119,0,'3','monsieur-big-mascara-travel-size'),('44',132,0,'3','/s/a/sample3.jpg'),('45',134,0,'3','2'),('50',160,0,'2','125'),('51',161,0,'2','128'),('52',162,0,'2','146'),('53',163,0,'2','153'),('54',165,0,'2','174'),('59',160,0,'3','125'),('60',161,0,'3','136'),('61',162,0,'3','145'),('62',163,0,'3','155'),('63',165,0,'3','171'),('92',73,0,'4','trésor in love temps  tmp-Red'),('93',84,0,'4','trésor in love temps  tmp'),('94',86,0,'4','trésor in love temps  tmp Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typ'),('95',106,0,'4','container2'),('96',119,0,'4','tresor-in-love-temps-tmp-red'),('97',134,0,'4','0'),('98',73,0,'5','trésor in love temps  tmp-Green'),('99',84,0,'5','trésor in love temps  tmp'),('100',86,0,'5','trésor in love temps  tmp Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typ'),('101',106,0,'5','container2'),('102',119,0,'5','tresor-in-love-temps-tmp-green'),('103',134,0,'5','0'),('104',73,0,'6','trésor in love temps  tmp-Yellow'),('105',84,0,'6','trésor in love temps  tmp'),('106',86,0,'6','trésor in love temps  tmp Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typ'),('107',106,0,'6','container2'),('108',119,0,'6','tresor-in-love-temps-tmp-yellow'),('109',134,0,'6','0'),('110',73,0,'7','trésor in love temps  tmp-Black'),('111',84,0,'7','trésor in love temps  tmp'),('112',86,0,'7','trésor in love temps  tmp Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typ'),('113',106,0,'7','container2'),('114',119,0,'7','tresor-in-love-temps-tmp-black'),('115',134,0,'7','0'),('116',73,0,'8','trésor in love temps  tmp-White'),('117',84,0,'8','trésor in love temps  tmp'),('118',86,0,'8','trésor in love temps  tmp Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typ'),('119',106,0,'8','container2'),('120',119,0,'8','tresor-in-love-temps-tmp-white'),('121',134,0,'8','0'),('122',73,0,'9','trésor in love temps  tmp-Brown'),('123',84,0,'9','trésor in love temps  tmp'),('124',86,0,'9','trésor in love temps  tmp Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typ'),('125',106,0,'9','container2'),('126',119,0,'9','tresor-in-love-temps-tmp-brown'),('127',134,0,'9','0'),('128',73,0,'10','trésor in love temps  tmp'),('129',84,0,'10','trésor in love temps  tmp'),('130',86,0,'10','trésor in love temps  tmp Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typ'),('131',87,0,'10','/p/1/p1_1.jpg'),('132',88,0,'10','/p/1/p1_1.jpg'),('133',89,0,'10','/p/1/p1_1.jpg'),('134',106,0,'10','container2'),('135',119,0,'10','tresor-in-love-temps-tmp'),('136',134,0,'10','2');
/*!40000 ALTER TABLE `catalog_product_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav`
--

DROP TABLE IF EXISTS `catalog_product_index_eav`;
CREATE TABLE `catalog_product_index_eav` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Index Table';

--
-- Dumping data for table `catalog_product_index_eav`
--

LOCK TABLES `catalog_product_index_eav` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav` DISABLE KEYS */;
INSERT INTO `catalog_product_index_eav` VALUES ('2',136,1,'8'),('2',136,2,'8'),('3',136,1,'9'),('3',136,2,'9'),('2',152,1,'62'),('2',152,2,'62'),('10',152,1,'62'),('10',152,2,'62'),('2',160,1,'125'),('2',160,2,'125'),('3',160,1,'125'),('3',160,2,'125'),('2',161,1,'128'),('2',161,2,'128'),('3',161,1,'136'),('3',161,2,'136'),('2',162,1,'146'),('2',162,2,'146'),('3',162,1,'145'),('3',162,2,'145'),('2',163,1,'153'),('2',163,2,'153'),('3',163,1,'155'),('3',163,2,'155'),('2',165,1,'174'),('2',165,2,'174'),('3',165,1,'171'),('3',165,2,'171');
/*!40000 ALTER TABLE `catalog_product_index_eav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_decimal`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal`;
CREATE TABLE `catalog_product_index_eav_decimal` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Index Table';

--
-- Table structure for table `catalog_product_index_eav_decimal_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_idx`;
CREATE TABLE `catalog_product_index_eav_decimal_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Index Table';

--
-- Table structure for table `catalog_product_index_eav_decimal_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_tmp`;
CREATE TABLE `catalog_product_index_eav_decimal_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Temp Table';

--
-- Table structure for table `catalog_product_index_eav_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_idx`;
CREATE TABLE `catalog_product_index_eav_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Index Table';

--
-- Dumping data for table `catalog_product_index_eav_idx`
--

LOCK TABLES `catalog_product_index_eav_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav_idx` DISABLE KEYS */;
INSERT INTO `catalog_product_index_eav_idx` VALUES ('2',136,1,'8'),('2',136,2,'8'),('3',136,1,'9'),('3',136,2,'9'),('2',152,1,'62'),('2',152,2,'62'),('10',152,1,'62'),('10',152,2,'62'),('2',160,1,'125'),('2',160,2,'125'),('3',160,1,'125'),('3',160,2,'125'),('2',161,1,'128'),('2',161,2,'128'),('3',161,1,'136'),('3',161,2,'136'),('2',162,1,'146'),('2',162,2,'146'),('3',162,1,'145'),('3',162,2,'145'),('2',163,1,'153'),('2',163,2,'153'),('3',163,1,'155'),('3',163,2,'155'),('2',165,1,'174'),('2',165,2,'174'),('3',165,1,'171'),('3',165,2,'171');
/*!40000 ALTER TABLE `catalog_product_index_eav_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_tmp`;
CREATE TABLE `catalog_product_index_eav_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Temp Table';

--
-- Table structure for table `catalog_product_index_price`
--

DROP TABLE IF EXISTS `catalog_product_index_price`;
CREATE TABLE `catalog_product_index_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_MIN_PRICE` (`min_price`),
  KEY `CAT_PRD_IDX_PRICE_WS_ID_CSTR_GROUP_ID_MIN_PRICE` (`website_id`,`customer_group_id`,`min_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Index Table';

--
-- Dumping data for table `catalog_product_index_price`
--

LOCK TABLES `catalog_product_index_price` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price` DISABLE KEYS */;
INSERT INTO `catalog_product_index_price` VALUES ('2',0,1,2,'28.9000','28.9000','28.9000','28.9000',NULL),('2',1,1,2,'28.9000','28.9000','28.9000','28.9000',NULL),('2',2,1,2,'28.9000','28.9000','28.9000','28.9000',NULL),('2',3,1,2,'28.9000','28.9000','28.9000','28.9000',NULL),('3',0,1,2,'13.0000','13.0000','13.0000','13.0000',NULL),('3',1,1,2,'13.0000','13.0000','13.0000','13.0000',NULL),('3',2,1,2,'13.0000','13.0000','13.0000','13.0000',NULL),('3',3,1,2,'13.0000','13.0000','13.0000','13.0000',NULL),('4',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('4',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('4',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('4',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('5',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('5',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('5',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('5',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('6',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('6',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('6',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('6',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('7',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('7',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('7',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('7',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('8',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('8',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('8',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('8',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('9',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('9',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('9',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('9',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('10',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('10',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('10',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('10',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL);
/*!40000 ALTER TABLE `catalog_product_index_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_bundle_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_idx`;
CREATE TABLE `catalog_product_index_price_bundle_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Idx';

--
-- Table structure for table `catalog_product_index_price_bundle_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_idx`;
CREATE TABLE `catalog_product_index_price_bundle_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Idx';

--
-- Table structure for table `catalog_product_index_price_bundle_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Tmp';

--
-- Table structure for table `catalog_product_index_price_bundle_sel_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_idx`;
CREATE TABLE `catalog_product_index_price_bundle_sel_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Idx';

--
-- Table structure for table `catalog_product_index_price_bundle_sel_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_sel_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Tmp';

--
-- Table structure for table `catalog_product_index_price_bundle_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Tmp';

--
-- Table structure for table `catalog_product_index_price_cfg_opt_agr_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_idx`;
CREATE TABLE `catalog_product_index_price_cfg_opt_agr_idx` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Index Table';

--
-- Table structure for table `catalog_product_index_price_cfg_opt_agr_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_tmp`;
CREATE TABLE `catalog_product_index_price_cfg_opt_agr_tmp` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Temp Table';

--
-- Table structure for table `catalog_product_index_price_cfg_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_idx`;
CREATE TABLE `catalog_product_index_price_cfg_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Index Table';

--
-- Table structure for table `catalog_product_index_price_cfg_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_tmp`;
CREATE TABLE `catalog_product_index_price_cfg_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Temp Table';

--
-- Table structure for table `catalog_product_index_price_downlod_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_downlod_idx`;
CREATE TABLE `catalog_product_index_price_downlod_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indexer Table for price of downloadable products';

--
-- Table structure for table `catalog_product_index_price_downlod_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_downlod_tmp`;
CREATE TABLE `catalog_product_index_price_downlod_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Temporary Indexer Table for price of downloadable products';

--
-- Table structure for table `catalog_product_index_price_final_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_final_idx`;
CREATE TABLE `catalog_product_index_price_final_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Index Table';

--
-- Table structure for table `catalog_product_index_price_final_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_final_tmp`;
CREATE TABLE `catalog_product_index_price_final_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Temp Table';

--
-- Table structure for table `catalog_product_index_price_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_idx`;
CREATE TABLE `catalog_product_index_price_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_MIN_PRICE` (`min_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Index Table';

--
-- Dumping data for table `catalog_product_index_price_idx`
--

LOCK TABLES `catalog_product_index_price_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_idx` DISABLE KEYS */;
INSERT INTO `catalog_product_index_price_idx` VALUES ('2',0,1,2,'28.9000','28.9000','28.9000','28.9000',NULL),('2',1,1,2,'28.9000','28.9000','28.9000','28.9000',NULL),('2',2,1,2,'28.9000','28.9000','28.9000','28.9000',NULL),('2',3,1,2,'28.9000','28.9000','28.9000','28.9000',NULL),('3',0,1,2,'13.0000','13.0000','13.0000','13.0000',NULL),('3',1,1,2,'13.0000','13.0000','13.0000','13.0000',NULL),('3',2,1,2,'13.0000','13.0000','13.0000','13.0000',NULL),('3',3,1,2,'13.0000','13.0000','13.0000','13.0000',NULL),('4',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('4',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('4',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('4',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('5',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('5',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('5',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('5',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('6',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('6',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('6',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('6',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('7',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('7',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('7',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('7',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('8',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('8',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('8',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('8',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('9',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('9',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('9',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('9',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('10',0,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('10',1,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('10',2,1,0,'10.0000','10.0000','10.0000','10.0000',NULL),('10',3,1,0,'10.0000','10.0000','10.0000','10.0000',NULL);
/*!40000 ALTER TABLE `catalog_product_index_price_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_opt_agr_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_idx`;
CREATE TABLE `catalog_product_index_price_opt_agr_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Index Table';

--
-- Table structure for table `catalog_product_index_price_opt_agr_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_tmp`;
CREATE TABLE `catalog_product_index_price_opt_agr_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Temp Table';

--
-- Table structure for table `catalog_product_index_price_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_idx`;
CREATE TABLE `catalog_product_index_price_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Index Table';

--
-- Table structure for table `catalog_product_index_price_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_tmp`;
CREATE TABLE `catalog_product_index_price_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Temp Table';

--
-- Table structure for table `catalog_product_index_price_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_tmp`;
CREATE TABLE `catalog_product_index_price_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_MIN_PRICE` (`min_price`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Temp Table';

--
-- Table structure for table `catalog_product_index_tier_price`
--

DROP TABLE IF EXISTS `catalog_product_index_tier_price`;
CREATE TABLE `catalog_product_index_tier_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_TIER_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Index Table';

--
-- Table structure for table `catalog_product_index_website`
--

DROP TABLE IF EXISTS `catalog_product_index_website`;
CREATE TABLE `catalog_product_index_website` (
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `website_date` date DEFAULT NULL COMMENT 'Website Date',
  `rate` float DEFAULT '1' COMMENT 'Rate',
  PRIMARY KEY (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_WEBSITE_WEBSITE_DATE` (`website_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Website Index Table';

--
-- Dumping data for table `catalog_product_index_website`
--

LOCK TABLES `catalog_product_index_website` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_website` DISABLE KEYS */;
INSERT INTO `catalog_product_index_website` VALUES (1,'2017-06-24','1');
/*!40000 ALTER TABLE `catalog_product_index_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link`
--

DROP TABLE IF EXISTS `catalog_product_link`;
CREATE TABLE `catalog_product_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `linked_product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Linked Product ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `CATALOG_PRODUCT_LINK_LINK_TYPE_ID_PRODUCT_ID_LINKED_PRODUCT_ID` (`link_type_id`,`product_id`,`linked_product_id`),
  KEY `CATALOG_PRODUCT_LINK_PRODUCT_ID` (`product_id`),
  KEY `CATALOG_PRODUCT_LINK_LINKED_PRODUCT_ID` (`linked_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Product Linkage Table';

--
-- Table structure for table `catalog_product_link_attribute`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute`;
CREATE TABLE `catalog_product_link_attribute` (
  `product_link_attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Link Attribute ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  `product_link_attribute_code` varchar(32) DEFAULT NULL COMMENT 'Product Link Attribute Code',
  `data_type` varchar(32) DEFAULT NULL COMMENT 'Data Type',
  PRIMARY KEY (`product_link_attribute_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_LINK_TYPE_ID` (`link_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Attribute Table';

--
-- Dumping data for table `catalog_product_link_attribute`
--

LOCK TABLES `catalog_product_link_attribute` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute` DISABLE KEYS */;
INSERT INTO `catalog_product_link_attribute` VALUES (1,1,'position','int'),(2,4,'position','int'),(3,5,'position','int'),(4,3,'position','int'),(5,3,'qty','decimal');
/*!40000 ALTER TABLE `catalog_product_link_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_attribute_decimal`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_decimal`;
CREATE TABLE `catalog_product_link_attribute_decimal` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_DEC_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_DECIMAL_LINK_ID` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Decimal Attribute Table';

--
-- Table structure for table `catalog_product_link_attribute_int`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_int`;
CREATE TABLE `catalog_product_link_attribute_int` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_INT_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_INT_LINK_ID` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Integer Attribute Table';

--
-- Table structure for table `catalog_product_link_attribute_varchar`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_varchar`;
CREATE TABLE `catalog_product_link_attribute_varchar` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_VCHR_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_VARCHAR_LINK_ID` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Varchar Attribute Table';

--
-- Table structure for table `catalog_product_link_type`
--

DROP TABLE IF EXISTS `catalog_product_link_type`;
CREATE TABLE `catalog_product_link_type` (
  `link_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Type ID',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  PRIMARY KEY (`link_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Type Table';

--
-- Dumping data for table `catalog_product_link_type`
--

LOCK TABLES `catalog_product_link_type` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_type` DISABLE KEYS */;
INSERT INTO `catalog_product_link_type` VALUES (1,'relation'),(3,'super'),(4,'up_sell'),(5,'cross_sell');
/*!40000 ALTER TABLE `catalog_product_link_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_option`
--

DROP TABLE IF EXISTS `catalog_product_option`;
CREATE TABLE `catalog_product_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `type` varchar(50) DEFAULT NULL COMMENT 'Type',
  `is_require` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Required',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `max_characters` int(10) unsigned DEFAULT NULL COMMENT 'Max Characters',
  `file_extension` varchar(50) DEFAULT NULL COMMENT 'File Extension',
  `image_size_x` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size X',
  `image_size_y` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size Y',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Table';

--
-- Table structure for table `catalog_product_option_price`
--

DROP TABLE IF EXISTS `catalog_product_option_price`;
CREATE TABLE `catalog_product_option_price` (
  `option_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Price ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_price_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_PRICE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRICE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Price Table';

--
-- Table structure for table `catalog_product_option_title`
--

DROP TABLE IF EXISTS `catalog_product_option_title`;
CREATE TABLE `catalog_product_option_title` (
  `option_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Title ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_title_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TITLE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Title Table';

--
-- Table structure for table `catalog_product_option_type_price`
--

DROP TABLE IF EXISTS `catalog_product_option_type_price`;
CREATE TABLE `catalog_product_option_type_price` (
  `option_type_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Price ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_type_price_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Price Table';

--
-- Table structure for table `catalog_product_option_type_title`
--

DROP TABLE IF EXISTS `catalog_product_option_type_title`;
CREATE TABLE `catalog_product_option_type_title` (
  `option_type_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Title ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_type_title_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Title Table';

--
-- Table structure for table `catalog_product_option_type_value`
--

DROP TABLE IF EXISTS `catalog_product_option_type_value`;
CREATE TABLE `catalog_product_option_type_value` (
  `option_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_type_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_VALUE_OPTION_ID` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Value Table';

--
-- Table structure for table `catalog_product_relation`
--

DROP TABLE IF EXISTS `catalog_product_relation`;
CREATE TABLE `catalog_product_relation` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  PRIMARY KEY (`parent_id`,`child_id`),
  KEY `CATALOG_PRODUCT_RELATION_CHILD_ID` (`child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Relation Table';

--
-- Dumping data for table `catalog_product_relation`
--

LOCK TABLES `catalog_product_relation` WRITE;
/*!40000 ALTER TABLE `catalog_product_relation` DISABLE KEYS */;
INSERT INTO `catalog_product_relation` VALUES ('10','4'),('10','5'),('10','6'),('10','7'),('10','8'),('10','9');
/*!40000 ALTER TABLE `catalog_product_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_super_attribute`
--

DROP TABLE IF EXISTS `catalog_product_super_attribute`;
CREATE TABLE `catalog_product_super_attribute` (
  `product_super_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Super Attribute ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`product_super_attribute_id`),
  UNIQUE KEY `CATALOG_PRODUCT_SUPER_ATTRIBUTE_PRODUCT_ID_ATTRIBUTE_ID` (`product_id`,`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Table';

--
-- Dumping data for table `catalog_product_super_attribute`
--

LOCK TABLES `catalog_product_super_attribute` WRITE;
/*!40000 ALTER TABLE `catalog_product_super_attribute` DISABLE KEYS */;
INSERT INTO `catalog_product_super_attribute` VALUES ('4','10',170,0);
/*!40000 ALTER TABLE `catalog_product_super_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_super_attribute_label`
--

DROP TABLE IF EXISTS `catalog_product_super_attribute_label`;
CREATE TABLE `catalog_product_super_attribute_label` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Super Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `use_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Use Default Value',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_SPR_ATTR_LBL_PRD_SPR_ATTR_ID_STORE_ID` (`product_super_attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Label Table';

--
-- Dumping data for table `catalog_product_super_attribute_label`
--

LOCK TABLES `catalog_product_super_attribute_label` WRITE;
/*!40000 ALTER TABLE `catalog_product_super_attribute_label` DISABLE KEYS */;
INSERT INTO `catalog_product_super_attribute_label` VALUES ('4','4',0,0,'Color');
/*!40000 ALTER TABLE `catalog_product_super_attribute_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_super_link`
--

DROP TABLE IF EXISTS `catalog_product_super_link`;
CREATE TABLE `catalog_product_super_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `CATALOG_PRODUCT_SUPER_LINK_PRODUCT_ID_PARENT_ID` (`product_id`,`parent_id`),
  KEY `CATALOG_PRODUCT_SUPER_LINK_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Link Table';

--
-- Dumping data for table `catalog_product_super_link`
--

LOCK TABLES `catalog_product_super_link` WRITE;
/*!40000 ALTER TABLE `catalog_product_super_link` DISABLE KEYS */;
INSERT INTO `catalog_product_super_link` VALUES ('19','4','10'),('20','5','10'),('21','6','10'),('22','7','10'),('23','8','10'),('24','9','10');
/*!40000 ALTER TABLE `catalog_product_super_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_website`
--

DROP TABLE IF EXISTS `catalog_product_website`;
CREATE TABLE `catalog_product_website` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`product_id`,`website_id`),
  KEY `CATALOG_PRODUCT_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Website Linkage Table';

--
-- Dumping data for table `catalog_product_website`
--

LOCK TABLES `catalog_product_website` WRITE;
/*!40000 ALTER TABLE `catalog_product_website` DISABLE KEYS */;
INSERT INTO `catalog_product_website` VALUES ('2',1),('3',1),('4',1),('5',1),('6',1),('7',1),('8',1),('9',1),('10',1);
/*!40000 ALTER TABLE `catalog_product_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_url_rewrite_product_category`
--

DROP TABLE IF EXISTS `catalog_url_rewrite_product_category`;
CREATE TABLE `catalog_url_rewrite_product_category` (
  `url_rewrite_id` int(10) unsigned NOT NULL COMMENT 'url_rewrite_id',
  `category_id` int(10) unsigned NOT NULL COMMENT 'category_id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'product_id',
  KEY `CATALOG_URL_REWRITE_PRODUCT_CATEGORY_CATEGORY_ID_PRODUCT_ID` (`category_id`,`product_id`),
  KEY `CAT_URL_REWRITE_PRD_CTGR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` (`product_id`),
  KEY `FK_BB79E64705D7F17FE181F23144528FC8` (`url_rewrite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='url_rewrite_relation';

--
-- Dumping data for table `catalog_url_rewrite_product_category`
--

LOCK TABLES `catalog_url_rewrite_product_category` WRITE;
/*!40000 ALTER TABLE `catalog_url_rewrite_product_category` DISABLE KEYS */;
INSERT INTO `catalog_url_rewrite_product_category` VALUES ('226','3','3'),('228','3','3'),('232','81','10'),('236','81','10'),('230','5','10'),('234','5','10'),('231','80','10'),('235','80','10'),('238','3','2'),('244','3','2'),('240','10','2'),('246','10','2'),('241','15','2'),('247','15','2'),('242','16','2'),('248','16','2'),('239','4','2'),('245','4','2');
/*!40000 ALTER TABLE `catalog_url_rewrite_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock`
--

DROP TABLE IF EXISTS `cataloginventory_stock`;
CREATE TABLE `cataloginventory_stock` (
  `stock_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Stock Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_name` varchar(255) DEFAULT NULL COMMENT 'Stock Name',
  PRIMARY KEY (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock';

--
-- Dumping data for table `cataloginventory_stock`
--

LOCK TABLES `cataloginventory_stock` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock` VALUES (1,0,'Default');
/*!40000 ALTER TABLE `cataloginventory_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_item`
--

DROP TABLE IF EXISTS `cataloginventory_stock_item`;
CREATE TABLE `cataloginventory_stock_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `stock_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Id',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `min_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Min Qty',
  `use_config_min_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Qty',
  `is_qty_decimal` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Qty Decimal',
  `backorders` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Backorders',
  `use_config_backorders` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Backorders',
  `min_sale_qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'Min Sale Qty',
  `use_config_min_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Sale Qty',
  `max_sale_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Max Sale Qty',
  `use_config_max_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Max Sale Qty',
  `is_in_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is In Stock',
  `low_stock_date` timestamp NULL DEFAULT NULL COMMENT 'Low Stock Date',
  `notify_stock_qty` decimal(12,4) DEFAULT NULL COMMENT 'Notify Stock Qty',
  `use_config_notify_stock_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Notify Stock Qty',
  `manage_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Manage Stock',
  `use_config_manage_stock` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Manage Stock',
  `stock_status_changed_auto` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Status Changed Automatically',
  `use_config_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Qty Increments',
  `qty_increments` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Increments',
  `use_config_enable_qty_inc` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Enable Qty Increments',
  `enable_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Enable Qty Increments',
  `is_decimal_divided` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `CATALOGINVENTORY_STOCK_ITEM_PRODUCT_ID_WEBSITE_ID` (`product_id`,`website_id`),
  KEY `CATALOGINVENTORY_STOCK_ITEM_WEBSITE_ID` (`website_id`),
  KEY `CATALOGINVENTORY_STOCK_ITEM_STOCK_ID` (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Item';

--
-- Dumping data for table `cataloginventory_stock_item`
--

LOCK TABLES `cataloginventory_stock_item` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_item` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_item` VALUES ('2','2',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('3','3',1,'10.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('4','4',1,'9999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('5','5',1,'9999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('6','6',1,'9999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('7','7',1,'9999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('8','8',1,'9999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('9','9',1,'9999.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0),('10','10',1,'0.0000','0.0000',1,0,0,1,'1.0000',1,'10000.0000',1,1,NULL,'1.0000',1,1,1,0,1,'1.0000',1,0,0,0);
/*!40000 ALTER TABLE `cataloginventory_stock_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status`;
CREATE TABLE `cataloginventory_stock_status` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status';

--
-- Dumping data for table `cataloginventory_stock_status`
--

LOCK TABLES `cataloginventory_stock_status` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_status` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_status` VALUES ('2',0,1,'10.0000',1),('3',0,1,'10.0000',1),('4',0,1,'9999.0000',1),('5',0,1,'9999.0000',1),('6',0,1,'9999.0000',1),('7',0,1,'9999.0000',1),('8',0,1,'9999.0000',1),('9',0,1,'9999.0000',1),('10',0,1,'0.0000',1);
/*!40000 ALTER TABLE `cataloginventory_stock_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status_idx`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status_idx`;
CREATE TABLE `cataloginventory_stock_status_idx` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_IDX_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_IDX_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Idx';

--
-- Dumping data for table `cataloginventory_stock_status_idx`
--

LOCK TABLES `cataloginventory_stock_status_idx` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_status_idx` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_status_idx` VALUES ('2',0,1,'10.0000',1),('3',0,1,'10.0000',1),('4',0,1,'9999.0000',1),('5',0,1,'9999.0000',1),('6',0,1,'9999.0000',1),('7',0,1,'9999.0000',1),('8',0,1,'9999.0000',1),('9',0,1,'9999.0000',1),('10',0,1,'0.0000',1);
/*!40000 ALTER TABLE `cataloginventory_stock_status_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status_tmp`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status_tmp`;
CREATE TABLE `cataloginventory_stock_status_tmp` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_TMP_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_TMP_WEBSITE_ID` (`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Tmp';

--
-- Table structure for table `catalogrule`
--

DROP TABLE IF EXISTS `catalogrule`;
CREATE TABLE `catalogrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  PRIMARY KEY (`rule_id`),
  KEY `CATALOGRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule';

--
-- Table structure for table `catalogrule_customer_group`
--

DROP TABLE IF EXISTS `catalogrule_customer_group`;
CREATE TABLE `catalogrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `CATALOGRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Customer Groups Relations';

--
-- Table structure for table `catalogrule_group_website`
--

DROP TABLE IF EXISTS `catalogrule_group_website`;
CREATE TABLE `catalogrule_group_website` (
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`,`website_id`),
  KEY `CATALOGRULE_GROUP_WEBSITE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Group Website';

--
-- Table structure for table `catalogrule_product`
--

DROP TABLE IF EXISTS `catalogrule_product`;
CREATE TABLE `catalogrule_product` (
  `rule_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `from_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'From Time',
  `to_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'To time',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `action_operator` varchar(10) DEFAULT 'to_fixed' COMMENT 'Action Operator',
  `action_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Action Amount',
  `action_stop` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Action Stop',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_product_id`),
  UNIQUE KEY `IDX_EAA51B56FF092A0DCB795D1CEF812B7B` (`rule_id`,`from_time`,`to_time`,`website_id`,`customer_group_id`,`product_id`,`sort_order`),
  KEY `CATALOGRULE_PRODUCT_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_PRODUCT_WEBSITE_ID` (`website_id`),
  KEY `CATALOGRULE_PRODUCT_FROM_TIME` (`from_time`),
  KEY `CATALOGRULE_PRODUCT_TO_TIME` (`to_time`),
  KEY `CATALOGRULE_PRODUCT_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product';

--
-- Table structure for table `catalogrule_product_price`
--

DROP TABLE IF EXISTS `catalogrule_product_price`;
CREATE TABLE `catalogrule_product_price` (
  `rule_product_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product PriceId',
  `rule_date` date NOT NULL COMMENT 'Rule Date',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `rule_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rule Price',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `latest_start_date` date DEFAULT NULL COMMENT 'Latest StartDate',
  `earliest_end_date` date DEFAULT NULL COMMENT 'Earliest EndDate',
  PRIMARY KEY (`rule_product_price_id`),
  UNIQUE KEY `CATRULE_PRD_PRICE_RULE_DATE_WS_ID_CSTR_GROUP_ID_PRD_ID` (`rule_date`,`website_id`,`customer_group_id`,`product_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_WEBSITE_ID` (`website_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product Price';

--
-- Table structure for table `catalogrule_website`
--

DROP TABLE IF EXISTS `catalogrule_website`;
CREATE TABLE `catalogrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `CATALOGRULE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Websites Relations';

--
-- Table structure for table `catalogsearch_fulltext_scope1`
--

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope1`;
CREATE TABLE `catalogsearch_fulltext_scope1` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope1';

--
-- Dumping data for table `catalogsearch_fulltext_scope1`
--

LOCK TABLES `catalogsearch_fulltext_scope1` WRITE;
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope1` DISABLE KEYS */;
INSERT INTO `catalogsearch_fulltext_scope1` VALUES ('2','73','HYPNÔSE DOLL EYES'),('2','74','162072-LAC-test2'),('2','75','Watch lashes turn into a doll-eyed look with Lanc&ocirc;me&rsquo;s Hypn&ocirc;se Doll Eyes.A must-have mascara to create beautiful lashes that look extended and lifted, Hypn&ocirc;se Doll Eyes evenly coats lashes without weighing them down.&nbsp;Sweep on using our cone shaped brush and watch your eyes transform into a dazzling wide-eyed look.HOW TO APPLY HYPN&Ocirc;SE DOLL EYES&bull; Our unique cone shaped brush ensures the ideal load with each application, even in the inner corner of the eyes.&nbsp;&bull; Apply your mascara from the base of your lashes working through to the tips, in zigzag movements, for maximum coverage.'),('2','76','DOLL-EYED EFFECT MASCARA'),('2','136','Black'),('2','152','Oily'),('2','160','Face'),('2','161','Lack of firmness'),('2','162','Oiliness'),('2','163','Hypoallergenic'),('2','165','Lengthening'),('3','73','MONSIEUR BIG MASCARA - TRAVEL SIZE'),('3','74','A00357-LAC-test3'),('3','75','Meet Monsieur Big. Forget all the others.UP TO 12X MORE VOLUME:&nbsp;Its volumizing brush provides big impact at first stroke, for bigger than life lashes, like never before.&nbsp;BIG SOFT BRUSH :Its bristles made of exceptionally soft fibers hug lashes with incredible softness and ensure a smooth application, while separating and lifting lashes effortlessly.&nbsp;NO CLUMPS, NO SMUDGES, NO TOUCH UPS: The innovative formula easily glides on lashes and leaves them perfectly put for up to 24 hours. Its ultra-dark pigments create the blackest intensity possible.&nbsp;It\'s a match!'),('3','76','BIG VOLUME MASCARA UP TO 24 HOUR WEAR'),('3','136','Red'),('3','160','Face'),('3','161','Redness'),('3','162','Firmness'),('3','163','Ophthalmologist Tested'),('3','165','Curling'),('10','73','trésor in love temps tmp | trésor in love temps tmp-Red | trésor in love temps tmp-Green | trésor in love temps tmp-Yellow | trésor in love temps tmp-Black | trésor in love temps tmp-White | trésor in love temps tmp-Brown'),('10','74','trésor in love temps tmp'),('10','75','Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('10','76','Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('10','152','Oily | Oily | Oily | Oily | Oily | Oily | Oily');
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogsearch_fulltext_scope2`
--

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope2`;
CREATE TABLE `catalogsearch_fulltext_scope2` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope2';

--
-- Dumping data for table `catalogsearch_fulltext_scope2`
--

LOCK TABLES `catalogsearch_fulltext_scope2` WRITE;
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope2` DISABLE KEYS */;
INSERT INTO `catalogsearch_fulltext_scope2` VALUES ('2','73','HYPNÔSE DOLL EYES'),('2','74','162072-LAC-test2'),('2','75','Watch lashes turn into a doll-eyed look with Lanc&ocirc;me&rsquo;s Hypn&ocirc;se Doll Eyes.A must-have mascara to create beautiful lashes that look extended and lifted, Hypn&ocirc;se Doll Eyes evenly coats lashes without weighing them down.&nbsp;Sweep on using our cone shaped brush and watch your eyes transform into a dazzling wide-eyed look.HOW TO APPLY HYPN&Ocirc;SE DOLL EYES&bull; Our unique cone shaped brush ensures the ideal load with each application, even in the inner corner of the eyes.&nbsp;&bull; Apply your mascara from the base of your lashes working through to the tips, in zigzag movements, for maximum coverage.'),('2','76','DOLL-EYED EFFECT MASCARA'),('2','136','Black'),('2','152','Oily'),('2','160','Face'),('2','161','Lack of firmness'),('2','162','Oiliness'),('2','163','Hypoallergenic'),('2','165','Lengthening'),('3','73','MONSIEUR BIG MASCARA - TRAVEL SIZE'),('3','74','A00357-LAC-test3'),('3','75','Meet Monsieur Big. Forget all the others.UP TO 12X MORE VOLUME:&nbsp;Its volumizing brush provides big impact at first stroke, for bigger than life lashes, like never before.&nbsp;BIG SOFT BRUSH :Its bristles made of exceptionally soft fibers hug lashes with incredible softness and ensure a smooth application, while separating and lifting lashes effortlessly.&nbsp;NO CLUMPS, NO SMUDGES, NO TOUCH UPS: The innovative formula easily glides on lashes and leaves them perfectly put for up to 24 hours. Its ultra-dark pigments create the blackest intensity possible.&nbsp;It\'s a match!'),('3','76','BIG VOLUME MASCARA UP TO 24 HOUR WEAR'),('3','136','Red'),('3','160','Face'),('3','161','Redness'),('3','162','Firmness'),('3','163','Ophthalmologist Tested'),('3','165','Curling'),('10','73','trésor in love temps tmp | trésor in love temps tmp-Red | trésor in love temps tmp-Green | trésor in love temps tmp-Yellow | trésor in love temps tmp-Black | trésor in love temps tmp-White | trésor in love temps tmp-Brown'),('10','74','trésor in love temps tmp'),('10','75','Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('10','76','Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry. | Lorem Ipsum is simply dummy text of the printing and typesetting industry.'),('10','152','Oily | Oily | Oily | Oily | Oily | Oily | Oily');
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkout_agreement`
--

DROP TABLE IF EXISTS `checkout_agreement`;
CREATE TABLE `checkout_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `content` text COMMENT 'Content',
  `content_height` varchar(25) DEFAULT NULL COMMENT 'Content Height',
  `checkbox_text` text COMMENT 'Checkbox Text',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `is_html` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Html',
  `mode` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Applied mode',
  PRIMARY KEY (`agreement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement';

--
-- Table structure for table `checkout_agreement_store`
--

DROP TABLE IF EXISTS `checkout_agreement_store`;
CREATE TABLE `checkout_agreement_store` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`agreement_id`,`store_id`),
  KEY `CHECKOUT_AGREEMENT_STORE_STORE_ID_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement Store';

--
-- Table structure for table `cms_block`
--

DROP TABLE IF EXISTS `cms_block`;
CREATE TABLE `cms_block` (
  `block_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Block ID',
  `title` varchar(255) NOT NULL COMMENT 'Block Title',
  `identifier` varchar(255) NOT NULL COMMENT 'Block String Identifier',
  `content` mediumtext COMMENT 'Block Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Block Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Block Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Block Active',
  PRIMARY KEY (`block_id`),
  FULLTEXT KEY `CMS_BLOCK_TITLE_IDENTIFIER_CONTENT` (`title`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='CMS Block Table';

--
-- Dumping data for table `cms_block`
--

LOCK TABLES `cms_block` WRITE;
/*!40000 ALTER TABLE `cms_block` DISABLE KEYS */;
INSERT INTO `cms_block` VALUES (1,'Home Top Banner ','home_top_banner','<p>Home Top Banner&nbsp;</p>','2017-06-14 09:24:10','2017-06-14 09:24:10',1),(2,'Home Product Carousel ','home_product_carousel','<p>Home Product Carousel&nbsp;</p>','2017-06-14 09:24:28','2017-06-14 09:24:28',1),(3,'Home Online Exclusives','home_online-exclusives','<p>home_online-exclusive</p>','2017-06-14 09:24:46','2017-06-14 09:24:46',1),(4,'Home Mid Content 1','home_mid_content_1','home_mid_content_1\r\n','2017-06-14 09:25:09','2017-06-14 09:26:04',1),(5,'Home Mid Content 2 ','home_mid_content_2','Home Mid Content 2','2017-06-14 09:25:40','2017-06-14 09:25:40',1),(6,'Home Bottom Content','home_bottom_content','Home Bottom Content','2017-06-14 09:26:20','2017-06-14 09:26:20',1),(7,'Header Need Help ','header_need_help','<p>header_need_help</p>','2017-06-14 09:26:46','2017-06-14 09:26:46',1),(8,'Header Why Shop Online ','header_why_shop_online','<p>Header Why Shop Online&nbsp;</p>','2017-06-14 09:27:11','2017-06-14 09:27:11',1),(9,'Footer Search','footer_search','<p>footer_search</p>','2017-06-14 09:27:34','2017-06-14 09:27:34',1),(10,'Footer Navigation','footer_navigation','<p>Footer Navigation&nbsp;</p>','2017-06-14 09:27:50','2017-06-14 09:27:50',1),(11,'Footer Links','footer_links','<p>Footer Links&nbsp;</p>','2017-06-14 09:28:11','2017-06-14 09:28:11',1),(12,'Footer Newsletter','footer_newslettter','<p>footer_newslettter</p>','2017-06-14 09:30:34','2017-06-14 09:30:34',1),(13,'Footer Bottom','footer_bottom','<p>Footer Bottom&nbsp;</p>','2017-06-14 09:30:56','2017-06-14 09:30:56',1),(14,'Footer Social','footer_social','<p>footer_social</p>','2017-06-14 09:31:14','2017-06-14 09:31:14',1),(15,'Product Detail Block 1 ','product_detail_block_1','<p>Product Detail Block 1&nbsp;</p>','2017-06-14 09:31:39','2017-06-14 09:31:39',1),(16,'Product Detail Block 2','product_detail_block_2','<p>Product Detail Block 2&nbsp;</p>','2017-06-14 09:31:54','2017-06-14 09:31:54',1),(17,'Navigation Skin Care Left Banner','navigation_skin_care_left_banner','<div class=\"thumbnail\">\r\n<div class=\"pull-left\"><img src=\"{{media url=\"wysiwyg/navigation-submenu/skin-care/bottom-left/skincare-bot-left.png\"}}\" alt=\"\" /></div>\r\n<div class=\"thumbnail-content\">\r\n<h3>VISIONNAIRE NUIT</h3>\r\n<p>BEAUTY SLEEP PERFECTOR</p>\r\n<a href=\"#\"><span class=\"small-link\">Discover</span></a></div>\r\n</div>','2017-06-14 09:32:46','2017-06-20 07:01:27',1),(18,'Navigation Skin Care Right Banner','navigation_skin_care_right_banner','<div class=\"thumbnail\">\r\n<div class=\"pull-left\"><img src=\"{{media url=\"wysiwyg/navigation-submenu/skin-care/bottom-right/skincare-bot-right.png\"}}\" alt=\"\" /></div>\r\n<div class=\"thumbnail-content\">\r\n<h3>SKINCARE SETS</h3>\r\n<p>LIMITED EDITION SETS ARE HERE</p>\r\n<a href=\"#\"><span class=\"small-link\">Shop now</span></a></div>\r\n</div>','2017-06-14 09:33:09','2017-06-20 07:02:40',1),(19,'Navigation Skin Care Top Right Banner','navigation_skin_care_top_right_banner','<p>Navigation Skin Care Top Right Banner</p>','2017-06-14 10:23:18','2017-06-14 10:23:18',1),(20,'Navigation Skin Care Our Selection','navigation_skin_care_our_selection','<div class=\"our-selection\"><a href=\"#\"><span class=\"our-selection-sub-title\">OUR SELECTION</span></a>\r\n<ul class=\"our-selection-sub-menu\">\r\n<li><a href=\"#\">What\'s New</a></li>\r\n<li><a href=\"#\">Best Sellers</a></li>\r\n<li><a href=\"#\">Online Exclusives</a></li>\r\n</ul>\r\n</div>','2017-06-14 10:24:09','2017-06-19 11:10:23',1),(21,'Navigation Makeup Left Banner','navigation_makeup_left_banner','<div class=\"thumbnail\">\r\n<div class=\"pull-left\"><img src=\"{{media url=\"wysiwyg/navigation-submenu/make-up/bottom-left/makeup-bot-left.png\"}}\" alt=\"\" /></div>\r\n<div class=\"thumbnail-content\">\r\n<h3>L\'ABSOLU ROUGE D&Eacute;FINITION</h3>\r\n<p>BOLD COULOURS. HIGH PRECISION</p>\r\n<a href=\"#\"><span class=\"small-link\">Shop now</span></a></div>\r\n</div>','2017-06-14 10:24:55','2017-06-20 07:04:36',1),(22,'Navigation Makeup Right Banner','navigation_makeup_right_banner','<div class=\"thumbnail\">\r\n<div class=\"pull-left\"><img src=\"{{media url=\"wysiwyg/navigation-submenu/make-up/bottom-right/makeup-bot-right.png\"}}\" alt=\"\" /></div>\r\n<div class=\"thumbnail-content\">\r\n<h3>MAKEUP SETS</h3>\r\n<p>LIMITED EDITION SETS ARE HERE</p>\r\n<a href=\"#\"><span class=\"small-link\">Shop now</span></a></div>\r\n</div>','2017-06-14 10:25:23','2017-06-20 07:05:58',1),(23,'Navigation Makeup Top Right Banner','navigation_make_top_right_banner','<p>Navigation Makeup Top Right Banner</p>','2017-06-14 10:25:51','2017-06-14 10:25:51',1),(24,'Navigation Makeup Our Selection','navigation_makeup_our_selection','<div class=\"our-selection\"><a href=\"#\"><span class=\"our-selection-sub-title\">OUR SELECTION</span></a>\r\n<ul class=\"our-selection-sub-menu\">\r\n<li><a href=\"#\">What\'s New</a></li>\r\n<li><a href=\"#\">Best Sellers</a></li>\r\n<li><a href=\"#\">Online Exclusives</a></li>\r\n</ul>\r\n</div>','2017-06-14 10:26:35','2017-06-19 11:10:42',1),(25,'Navigation Fragrance Left Banner','navigation_fragrance_left_banner','<div class=\"thumbnail\">\r\n<div class=\"pull-left\"><img src=\"{{media url=\"wysiwyg/navigation-submenu/fragrance/bottom-left/frangrance-bot-left.png\"}}\" alt=\"\" /></div>\r\n<div class=\"thumbnail-content\">\r\n<h3>LA VIE EST BELLE</h3>\r\n<p>EAU DE PARFUM</p>\r\n<a href=\"#\"><span class=\"small-link\">Shop now</span></a></div>\r\n</div>','2017-06-14 10:28:17','2017-06-20 07:07:27',1),(26,'Navigation Fragrance Right Banner','navigation_fragrance_right_banner','<div class=\"thumbnail\">\r\n<div class=\"pull-left\"><img src=\"{{media url=\"wysiwyg/navigation-submenu/fragrance/bottom-right/frangrance-bot-right.png\"}}\" alt=\"\" /></div>\r\n<div class=\"thumbnail-content\">\r\n<h3>HYPN&Ocirc;SE MEN</h3>\r\n<p>EAU DE TOILETTE</p>\r\n<a href=\"#\"><span class=\"small-link\">Shop now</span></a></div>\r\n</div>','2017-06-14 10:28:55','2017-06-20 07:08:45',1),(27,'Navigation Fragrance Top Right Banner','navigation_fragrance_top_right_banner','<div class=\"thumbnail thumbnail-bot\">\r\n<div class=\"full-width-img\"><img class=\"img-responsive\" src=\"{{media url=\"wysiwyg/navigation-submenu/fragrance/top-right/frangrance-top-right.png\"}}\" alt=\"la nuitresor\" /></div>\r\n<div class=\"thumbnail-content\">\r\n<h3>LA NUIT TR&Eacute;SOR</h3>\r\n<p>EAU DE TOILETTE</p>\r\n<a href=\"#\"><span class=\"small-link\">Shop now</span></a></div>\r\n</div>','2017-06-14 10:29:23','2017-06-20 01:48:31',1),(28,'Navigation Fragrance Our Selection','navigation_fragrance_our_selection','<div class=\"our-selection\"><a href=\"#\"><span class=\"our-selection-sub-title\">OUR SELECTION</span></a>\r\n<ul class=\"our-selection-sub-menu\">\r\n<li><a href=\"#\">What\'s New</a></li>\r\n<li><a href=\"#\">Best Sellers</a></li>\r\n<li><a href=\"#\">Online Exclusives</a></li>\r\n</ul>\r\n</div>','2017-06-14 10:29:47','2017-06-19 11:11:15',1),(29,'Checkout Banner','checkout_banner','<p>Checkout Top Right Banner</p>','2017-06-16 03:21:10','2017-06-16 04:21:59',1),(30,'Account Create Top Banner','create_account_top_banner','<p>Account Create Top Banner</p>','2017-06-16 04:10:55','2017-06-16 04:10:55',1),(31,'Account Sign In Banner 2 ','account_sign_in_banner_2','<p>Account Sign In Banner 2&nbsp;</p>','2017-06-16 04:12:39','2017-06-16 04:12:39',1),(32,'Account Sign In Banner 1','account_sign_in_banner_1','Account Sign In Banner 1','2017-06-16 04:13:09','2017-06-16 04:13:09',1),(33,'Cart Banner 1','cart_banner_1','<p>Cart Banner 1&nbsp;</p>','2017-06-16 04:22:13','2017-06-16 04:22:13',1),(34,'Cart Banner 2','cart_banner_2','Cart Banner 2','2017-06-16 04:22:29','2017-06-16 04:22:29',1);
/*!40000 ALTER TABLE `cms_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_block_store`
--

DROP TABLE IF EXISTS `cms_block_store`;
CREATE TABLE `cms_block_store` (
  `block_id` smallint(6) NOT NULL COMMENT 'Block ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`block_id`,`store_id`),
  KEY `CMS_BLOCK_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Block To Store Linkage Table';

--
-- Dumping data for table `cms_block_store`
--

LOCK TABLES `cms_block_store` WRITE;
/*!40000 ALTER TABLE `cms_block_store` DISABLE KEYS */;
INSERT INTO `cms_block_store` VALUES (1,0),(2,0),(3,0),(4,0),(5,0),(6,0),(7,0),(8,0),(9,0),(10,0),(11,0),(12,0),(13,0),(14,0),(15,0),(16,0),(17,0),(18,0),(19,0),(20,0),(21,0),(22,0),(23,0),(24,0),(25,0),(26,0),(27,0),(28,0),(29,0),(30,0),(31,0),(32,0),(33,0),(34,0);
/*!40000 ALTER TABLE `cms_block_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_page`
--

DROP TABLE IF EXISTS `cms_page`;
CREATE TABLE `cms_page` (
  `page_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Page ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Page Title',
  `page_layout` varchar(255) DEFAULT NULL COMMENT 'Page Layout',
  `meta_keywords` text COMMENT 'Page Meta Keywords',
  `meta_description` text COMMENT 'Page Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Page String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Page Content Heading',
  `content` mediumtext COMMENT 'Page Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Page Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Page Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Page Active',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Page Sort Order',
  `layout_update_xml` text COMMENT 'Page Layout Update Content',
  `custom_theme` varchar(100) DEFAULT NULL COMMENT 'Page Custom Theme',
  `custom_root_template` varchar(255) DEFAULT NULL COMMENT 'Page Custom Template',
  `custom_layout_update_xml` text COMMENT 'Page Custom Layout Update Content',
  `custom_theme_from` date DEFAULT NULL COMMENT 'Page Custom Theme Active From Date',
  `custom_theme_to` date DEFAULT NULL COMMENT 'Page Custom Theme Active To Date',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Page Meta Title',
  PRIMARY KEY (`page_id`),
  KEY `CMS_PAGE_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `CMS_PAGE_TITLE_META_KEYWORDS_META_DESCRIPTION_IDENTIFIER_CONTENT` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='CMS Page Table';

--
-- Dumping data for table `cms_page`
--

LOCK TABLES `cms_page` WRITE;
/*!40000 ALTER TABLE `cms_page` DISABLE KEYS */;
INSERT INTO `cms_page` VALUES (1,'404 Not Found','2columns-right','Page keywords','Page description','no-route','Whoops, our bad...','<dl>\r\n<dt>The page you requested was not found, and we have a fine guess why.</dt>\r\n<dd>\r\n<ul class=\"disc\">\r\n<li>If you typed the URL directly, please make sure the spelling is correct.</li>\r\n<li>If you clicked on a link to get here, the link is outdated.</li>\r\n</ul></dd>\r\n</dl>\r\n<dl>\r\n<dt>What can you do?</dt>\r\n<dd>Have no fear, help is near! There are many ways you can get back on track with Magento Store.</dd>\r\n<dd>\r\n<ul class=\"disc\">\r\n<li><a href=\"#\" onclick=\"history.go(-1); return false;\">Go back</a> to the previous page.</li>\r\n<li>Use the search bar at the top of the page to search for your products.</li>\r\n<li>Follow these links to get you back on track!<br /><a href=\"{{store url=\"\"}}\">Store Home</a> <span class=\"separator\">|</span> <a href=\"{{store url=\"customer/account\"}}\">My Account</a></li></ul></dd></dl>\r\n','2017-06-06 07:48:28','2017-06-06 07:48:28',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Home page','1column','','','home','','{{block class=\"Magento\\Framework\\View\\Element\\Template\" template=\"Magento_Theme::html/onlineexclusive.phtml\"}} {{block class=\"Magento\\Framework\\View\\Element\\Template\" template=\"Magento_Theme::html/bannerblock.phtml\"}} {{block class=\"Magento\\Framework\\View\\Element\\Template\" template=\"Magento_Theme::html/parisinspire.phtml\"}} {{widget type=\"Solwin\\Instagram\\Block\\Widget\\Instagram\" title=\"#lancome\" numberimage=\"50\"}}','2017-06-06 07:48:28','2017-06-24 06:52:07',1,0,'<referenceContainer name=\"main\">\r\n<block class=\"Magento\\Framework\\View\\Element\\Template\" name=\"home_page_slider\" template=\"Magento_Theme::html/home_content.phtml\"/>\r\n<container name=\"exclusive\" htmlTag=\"div\" htmlClass=\"home_block\" after=\"-\">\r\n            </container>\r\n</referenceContainer>',NULL,NULL,NULL,NULL,NULL,''),(3,'Enable Cookies','1column',NULL,NULL,'enable-cookies','What are Cookies?','<div class=\"enable-cookies cms-content\">\r\n<p>\"Cookies\" are little pieces of data we send when you visit our store. Cookies help us get to know you better and personalize your experience. Plus they help protect you and other shoppers from fraud.</p>\r\n<p style=\"margin-bottom: 20px;\">Set your browser to accept cookies so you can buy items, save items, and receive customized recommendations. Here’s how:</p>\r\n<ul>\r\n<li><a href=\"https://support.google.com/accounts/answer/61416?hl=en\" target=\"_blank\">Google Chrome</a></li>\r\n<li><a href=\"http://windows.microsoft.com/en-us/internet-explorer/delete-manage-cookies\" target=\"_blank\">Internet Explorer</a></li>\r\n<li><a href=\"http://support.apple.com/kb/PH19214\" target=\"_blank\">Safari</a></li>\r\n<li><a href=\"https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences\" target=\"_blank\">Mozilla/Firefox</a></li>\r\n</ul>\r\n</div>','2017-06-06 07:48:28','2017-06-06 07:48:28',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Privacy and Cookie Policy','1column',NULL,NULL,'privacy-policy-cookie-restriction-mode','Privacy and Cookie Policy','<div class=\"privacy-policy cms-content\">\n    <div class=\"message info\">\n        <span>\n            Please replace this text with you Privacy Policy.\n            Please add any additional cookies your website uses below (e.g. Google Analytics).\n        </span>\n    </div>\n    <p>\n        This privacy policy sets out how this website (hereafter \"the Store\") uses and protects any information that\n        you give the Store while using this website. The Store is committed to ensuring that your privacy is protected.\n        Should we ask you to provide certain information by which you can be identified when using this website, then\n        you can be assured that it will only be used in accordance with this privacy statement. The Store may change\n        this policy from time to time by updating this page. You should check this page from time to time to ensure\n        that you are happy with any changes.\n    </p>\n    <h2>What we collect</h2>\n    <p>We may collect the following information:</p>\n    <ul>\n        <li>name</li>\n        <li>contact information including email address</li>\n        <li>demographic information such as postcode, preferences and interests</li>\n        <li>other information relevant to customer surveys and/or offers</li>\n    </ul>\n    <p>\n        For the exhaustive list of cookies we collect see the <a href=\"#list\">List of cookies we collect</a> section.\n    </p>\n    <h2>What we do with the information we gather</h2>\n    <p>\n        We require this information to understand your needs and provide you with a better service,\n        and in particular for the following reasons:\n    </p>\n    <ul>\n        <li>Internal record keeping.</li>\n        <li>We may use the information to improve our products and services.</li>\n        <li>\n            We may periodically send promotional emails about new products, special offers or other information which we\n            think you may find interesting using the email address which you have provided.\n        </li>\n        <li>\n            From time to time, we may also use your information to contact you for market research purposes.\n            We may contact you by email, phone, fax or mail. We may use the information to customise the website\n            according to your interests.\n        </li>\n    </ul>\n    <h2>Security</h2>\n    <p>\n        We are committed to ensuring that your information is secure. In order to prevent unauthorised access or\n        disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and\n        secure the information we collect online.\n    </p>\n    <h2>How we use cookies</h2>\n    <p>\n        A cookie is a small file which asks permission to be placed on your computer\'s hard drive.\n        Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit\n        a particular site. Cookies allow web applications to respond to you as an individual. The web application\n        can tailor its operations to your needs, likes and dislikes by gathering and remembering information about\n        your preferences.\n    </p>\n    <p>\n        We use traffic log cookies to identify which pages are being used. This helps us analyse data about web page\n        traffic and improve our website in order to tailor it to customer needs. We only use this information for\n        statistical analysis purposes and then the data is removed from the system.\n    </p>\n    <p>\n        Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find\n        useful and which you do not. A cookie in no way gives us access to your computer or any information about you,\n        other than the data you choose to share with us. You can choose to accept or decline cookies.\n        Most web browsers automatically accept cookies, but you can usually modify your browser setting\n        to decline cookies if you prefer. This may prevent you from taking full advantage of the website.\n    </p>\n    <h2>Links to other websites</h2>\n    <p>\n        Our website may contain links to other websites of interest. However, once you have used these links\n        to leave our site, you should note that we do not have any control over that other website.\n        Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst\n        visiting such sites and such sites are not governed by this privacy statement.\n        You should exercise caution and look at the privacy statement applicable to the website in question.\n    </p>\n    <h2>Controlling your personal information</h2>\n    <p>You may choose to restrict the collection or use of your personal information in the following ways:</p>\n    <ul>\n        <li>\n            whenever you are asked to fill in a form on the website, look for the box that you can click to indicate\n            that you do not want the information to be used by anybody for direct marketing purposes\n        </li>\n        <li>\n            if you have previously agreed to us using your personal information for direct marketing purposes,\n            you may change your mind at any time by letting us know using our Contact Us information\n        </li>\n    </ul>\n    <p>\n        We will not sell, distribute or lease your personal information to third parties unless we have your permission\n        or are required by law to do so. We may use your personal information to send you promotional information\n        about third parties which we think you may find interesting if you tell us that you wish this to happen.\n    </p>\n    <p>\n        You may request details of personal information which we hold about you under the Data Protection Act 1998.\n        A small fee will be payable. If you would like a copy of the information held on you please email us this\n        request using our Contact Us information.\n    </p>\n    <p>\n        If you believe that any information we are holding on you is incorrect or incomplete,\n        please write to or email us as soon as possible, at the above address.\n        We will promptly correct any information found to be incorrect.\n    </p>\n    <h2><a name=\"list\"></a>List of cookies we collect</h2>\n    <p>The table below lists the cookies we collect and what information they store.</p>\n    <table class=\"data-table data-table-definition-list\">\n        <thead>\n        <tr>\n            <th>Cookie Name</th>\n            <th>Cookie Description</th>\n        </tr>\n        </thead>\n        <tbody>\n            <tr>\n                <th>FORM_KEY</th>\n                <td>Stores randomly generated key used to prevent forged requests.</td>\n            </tr>\n            <tr>\n                <th>PHPSESSID</th>\n                <td>Your session ID on the server.</td>\n            </tr>\n            <tr>\n                <th>GUEST-VIEW</th>\n                <td>Allows guests to view and edit their orders.</td>\n            </tr>\n            <tr>\n                <th>PERSISTENT_SHOPPING_CART</th>\n                <td>A link to information about your cart and viewing history, if you have asked for this.</td>\n            </tr>\n            <tr>\n                <th>STF</th>\n                <td>Information on products you have emailed to friends.</td>\n            </tr>\n            <tr>\n                <th>STORE</th>\n                <td>The store view or language you have selected.</td>\n            </tr>\n            <tr>\n                <th>USER_ALLOWED_SAVE_COOKIE</th>\n                <td>Indicates whether a customer allowed to use cookies.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-SESSID</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-STORAGE</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-STORAGE-SECTION-INVALIDATION</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-CACHE-TIMEOUT</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>SECTION-DATA-IDS</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>PRIVATE_CONTENT_VERSION</th>\n                <td>Facilitates caching of content on the browser to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>X-MAGENTO-VARY</th>\n                <td>Facilitates caching of content on the server to make pages load faster.</td>\n            </tr>\n            <tr>\n                <th>MAGE-TRANSLATION-FILE-VERSION</th>\n                <td>Facilitates translation of content to other languages.</td>\n            </tr>\n            <tr>\n                <th>MAGE-TRANSLATION-STORAGE</th>\n                <td>Facilitates translation of content to other languages.</td>\n            </tr>\n        </tbody>\n    </table>\n</div>','2017-06-06 07:48:28','2017-06-06 07:48:28',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Privacy Policy','1column','','','privacy-policy','','','2017-06-12 16:03:31','2017-06-12 16:03:31',1,0,'',NULL,NULL,NULL,NULL,NULL,''),(6,'Terms & Conditions','1column','','','terms-conditions','','','2017-06-12 16:04:21','2017-06-12 16:04:21',1,0,'',NULL,NULL,NULL,NULL,NULL,''),(7,'Cookies','1column','','','cookies','','','2017-06-12 16:05:42','2017-06-12 16:05:42',1,0,'',NULL,NULL,NULL,NULL,NULL,'');
/*!40000 ALTER TABLE `cms_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_page_store`
--

DROP TABLE IF EXISTS `cms_page_store`;
CREATE TABLE `cms_page_store` (
  `page_id` smallint(6) NOT NULL COMMENT 'Page ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`page_id`,`store_id`),
  KEY `CMS_PAGE_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Page To Store Linkage Table';

--
-- Dumping data for table `cms_page_store`
--

LOCK TABLES `cms_page_store` WRITE;
/*!40000 ALTER TABLE `cms_page_store` DISABLE KEYS */;
INSERT INTO `cms_page_store` VALUES (1,0),(2,0),(3,0),(4,0),(5,0),(6,0),(7,0);
/*!40000 ALTER TABLE `cms_page_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_config_data`
--

DROP TABLE IF EXISTS `core_config_data`;
CREATE TABLE `core_config_data` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Config Id',
  `scope` varchar(8) NOT NULL DEFAULT 'default' COMMENT 'Config Scope',
  `scope_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Config Scope Id',
  `path` varchar(255) NOT NULL DEFAULT 'general' COMMENT 'Config Path',
  `value` text COMMENT 'Config Value',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `CORE_CONFIG_DATA_SCOPE_SCOPE_ID_PATH` (`scope`,`scope_id`,`path`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8 COMMENT='Config Data';

--
-- Dumping data for table `core_config_data`
--

LOCK TABLES `core_config_data` WRITE;
/*!40000 ALTER TABLE `core_config_data` DISABLE KEYS */;
INSERT INTO `core_config_data` VALUES ('1','default','0','web/seo/use_rewrites','1'),('2','default','0','web/unsecure/base_url','http://lanc0m3.vinobe.com/'),('3','default','0','web/secure/base_url','https://lanc0m3.vinobe.com/'),('4','default','0','general/locale/code','en_US'),('5','default','0','web/secure/use_in_frontend',NULL),('6','default','0','web/secure/use_in_adminhtml',NULL),('7','default','0','general/locale/timezone','UTC'),('8','default','0','currency/options/base','USD'),('9','default','0','currency/options/default','USD'),('10','default','0','currency/options/allow','GBP,USD'),('11','default','0','general/region/display_all','1'),('12','default','0','general/region/state_required','AT,BR,CA,EE,FI,LV,LT,RO,ES,CH,US'),('13','default','0','catalog/category/root_id','2'),('14','default','0','general/country/default','TH'),('15','default','0','general/store_information/name','Lancome Thailand'),('16','default','0','general/store_information/phone',NULL),('17','default','0','general/store_information/hours',NULL),('18','default','0','general/store_information/country_id','TH'),('19','default','0','general/store_information/region_id',NULL),('20','default','0','general/store_information/postcode',NULL),('21','default','0','general/store_information/city','Bangkok'),('22','default','0','general/store_information/street_line1',NULL),('23','default','0','general/store_information/street_line2',NULL),('24','default','0','general/store_information/merchant_vat_number',NULL),('25','default','0','general/single_store_mode/enabled','0'),('26','default','0','admin/security/admin_account_sharing','1'),('27','default','0','admin/security/use_case_sensitive_login','0'),('28','stores','1','design/head/title_prefix',NULL),('29','stores','1','design/head/title_suffix',NULL),('30','stores','1','design/head/includes',NULL),('31','stores','1','design/header/logo_width',NULL),('32','stores','1','design/header/logo_height',NULL),('33','stores','1','design/footer/absolute_footer',NULL),('34','stores','1','design/theme/theme_id','5'),('35','stores','1','design/pagination/pagination_frame_skip',NULL),('36','stores','1','design/pagination/anchor_text_for_previous',NULL),('37','stores','1','design/pagination/anchor_text_for_next',NULL),('38','stores','1','design/watermark/image_size',NULL),('39','stores','1','design/watermark/image_imageOpacity',NULL),('40','stores','1','design/watermark/small_image_size',NULL),('41','stores','1','design/watermark/small_image_imageOpacity',NULL),('42','stores','1','design/watermark/thumbnail_size',NULL),('43','stores','1','design/watermark/thumbnail_imageOpacity',NULL),('44','stores','1','design/email/logo_alt',NULL),('45','stores','1','design/email/logo_width',NULL),('46','stores','1','design/email/logo_height',NULL),('47','stores','1','design/watermark/swatch_image_size',NULL),('48','stores','1','design/watermark/swatch_image_imageOpacity',NULL),('49','stores','1','design/header/logo_src','stores/1/logo.png'),('50','stores','2','design/theme/theme_id','5'),('51','stores','2','design/pagination/pagination_frame_skip',NULL),('52','stores','2','design/pagination/anchor_text_for_previous',NULL),('53','stores','2','design/pagination/anchor_text_for_next',NULL),('54','stores','2','design/head/title_prefix',NULL),('55','stores','2','design/head/title_suffix',NULL),('56','stores','2','design/head/includes',NULL),('57','stores','2','design/header/logo_width',NULL),('58','stores','2','design/header/logo_height',NULL),('59','stores','2','design/footer/absolute_footer',NULL),('60','stores','2','design/watermark/image_size',NULL),('61','stores','2','design/watermark/image_imageOpacity',NULL),('62','stores','2','design/watermark/small_image_size',NULL),('63','stores','2','design/watermark/small_image_imageOpacity',NULL),('64','stores','2','design/watermark/thumbnail_size',NULL),('65','stores','2','design/watermark/thumbnail_imageOpacity',NULL),('66','stores','2','design/email/logo_alt',NULL),('67','stores','2','design/email/logo_width',NULL),('68','stores','2','design/email/logo_height',NULL),('69','stores','2','design/watermark/swatch_image_size',NULL),('70','stores','2','design/watermark/swatch_image_imageOpacity',NULL),('71','stores','2','design/header/logo_src','stores/2/logo.png'),('72','default','0','currency/yahoofinance/timeout','100'),('73','default','0','currency/fixerio/timeout','100'),('74','default','0','currency/import/service','yahoofinance'),('75','default','0','crontab/default/jobs/currency_rates_update/schedule/cron_expr','0 0 * * *'),('76','default','0','currency/import/time','00,00,00'),('77','default','0','currency/import/frequency','D'),('78','default','0','currency/import/error_email',NULL),('79','default','0','dev/restrict/allow_ips',NULL),('80','default','0','dev/debug/template_hints_storefront','0'),('81','default','0','dev/debug/template_hints_admin','0'),('82','default','0','dev/debug/template_hints_blocks','0'),('83','default','0','dev/template/allow_symlink','0'),('84','default','0','dev/translate_inline/active','0'),('85','default','0','dev/translate_inline/active_admin','0'),('86','default','0','dev/js/enable_js_bundling','0'),('87','default','0','dev/js/merge_files','0'),('88','default','0','dev/js/minify_files','0'),('89','default','0','dev/css/merge_css_files','0'),('90','default','0','dev/css/minify_files','0'),('91','default','0','dev/static/sign','1'),('92','default','0','sales/general/hide_customer_ip','0'),('93','default','0','sales/identity/address',NULL),('94','default','0','sales/identity/logo',NULL),('95','default','0','sales/identity/logo_html',NULL),('96','default','0','sales/minimum_order/active','0'),('97','default','0','sales/minimum_order/amount',NULL),('98','default','0','sales/minimum_order/description',NULL),('99','default','0','sales/minimum_order/error_message',NULL),('100','default','0','sales/minimum_order/multi_address','0'),('101','default','0','sales/minimum_order/multi_address_description',NULL),('102','default','0','sales/minimum_order/multi_address_error_message',NULL),('103','default','0','sales/gift_options/allow_order','1'),('104','default','0','sales/gift_options/allow_items','1'),('105','default','0','instagramsection/instagramgroup/active','1'),('106','default','0','instagramsection/instagramgroup/userid','4220111921'),('107','default','0','instagramsection/instagramgroup/accesstoken','4220111921.1677ed0.a23ebf230ae344b5b1ef89e8a868eb4e'),('108','default','0','sociallogin/general/is_enabled','1'),('109','default','0','sociallogin/general/send_password','0'),('110','default','0','sociallogin/general/social_display','1,2,3'),('111','default','0','sociallogin/general/popup_login','1'),('112','default','0','sociallogin/general/link_trigger','.header .links, .section-item-content .header.links'),('113','default','0','sociallogin/general/popup_effect','mfp-move-from-top'),('114','default','0','sociallogin/general/style_management','#3399cc'),('115','default','0','sociallogin/general/custom_css',NULL),('116','default','0','sociallogin/facebook/is_enabled','1'),('117','default','0','sociallogin/facebook/app_id','1'),('118','default','0','sociallogin/facebook/app_secret','1'),('119','default','0','sociallogin/google/is_enabled','1'),('120','default','0','sociallogin/google/app_id','1'),('121','default','0','sociallogin/google/app_secret','1'),('122','default','0','sociallogin/amazon/is_enabled','0'),('123','default','0','sociallogin/twitter/is_enabled','1'),('124','default','0','sociallogin/twitter/app_id','1'),('125','default','0','sociallogin/twitter/app_secret','1'),('126','default','0','sociallogin/linkedin/is_enabled','0'),('127','default','0','sociallogin/yahoo/is_enabled','0'),('128','default','0','sociallogin/foursquare/is_enabled','0'),('129','default','0','sociallogin/instagram/is_enabled','0'),('130','default','0','sociallogin/vkontakte/is_enabled','0'),('131','default','0','sociallogin/github/is_enabled','0'),('132','default','0','blog/general/enabled','1'),('133','default','0','blog/general/name','Blog'),('134','default','0','blog/general/url_prefix','blog'),('135','default','0','blog/general/url_suffix','.html'),('136','default','0','blog/general/toplinks','1'),('137','default','0','blog/general/footer','1'),('138','default','0','blog/general/related_post','5'),('139','default','0','blog/general/display_style','1'),('140','default','0','blog/general/display_author','1'),('141','default','0','blog/general/display_sitemap','1'),('142','default','0','blog/general/date_type','1'),('143','default','0','blog/general/font_color','#1abc9c'),('144','default','0','blog/general/pagination','10'),('145','default','0','blog/product_post/enable_post','1'),('146','default','0','blog/product_post/post_limit','10'),('147','default','0','blog/sidebar/number_recent_posts','5'),('148','default','0','blog/sidebar/number_mostview_posts','5'),('149','default','0','blog/sidebar/sidebar_left_right','1'),('150','default','0','blog/sidebar/search/enable_search','1'),('151','default','0','blog/sidebar/search/search_limit','10'),('152','default','0','blog/sidebar/search/min_chars','1'),('153','default','0','blog/sidebar/search/show_image','1'),('154','default','0','blog/sidebar/search/description','100'),('155','default','0','blog/monthly_archive/enable_monthly','1'),('156','default','0','blog/monthly_archive/number_records','5'),('157','default','0','blog/monthly_archive/date_type_monthly','1'),('158','default','0','blog/comment/type','4'),('159','default','0','blog/seo/meta_title',NULL),('160','default','0','blog/seo/meta_description',NULL),('161','default','0','blog/seo/meta_keywords',NULL),('162','default','0','blog/seo/snippet/enabled','1'),('163','default','0','blog/seo/snippet/organize_name',NULL),('164','default','0','blog/seo/snippet/upload_image_id',NULL),('165','default','0','blog/share/enabled','1'),('166','default','0','blog/share/pubid_id','ra-568cdc40e9eab41d');
/*!40000 ALTER TABLE `core_config_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cron_schedule`
--

DROP TABLE IF EXISTS `cron_schedule`;
CREATE TABLE `cron_schedule` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Schedule Id',
  `job_code` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Job Code',
  `status` varchar(7) NOT NULL DEFAULT 'pending' COMMENT 'Status',
  `messages` text COMMENT 'Messages',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `scheduled_at` timestamp NULL DEFAULT NULL COMMENT 'Scheduled At',
  `executed_at` timestamp NULL DEFAULT NULL COMMENT 'Executed At',
  `finished_at` timestamp NULL DEFAULT NULL COMMENT 'Finished At',
  PRIMARY KEY (`schedule_id`),
  KEY `CRON_SCHEDULE_JOB_CODE` (`job_code`),
  KEY `CRON_SCHEDULE_SCHEDULED_AT_STATUS` (`scheduled_at`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cron Schedule';

--
-- Table structure for table `customer_address_entity`
--

DROP TABLE IF EXISTS `customer_address_entity`;
CREATE TABLE `customer_address_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `city` varchar(255) NOT NULL COMMENT 'City',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `country_id` varchar(255) NOT NULL COMMENT 'Country',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `firstname` varchar(255) NOT NULL COMMENT 'First Name',
  `lastname` varchar(255) NOT NULL COMMENT 'Last Name',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middle Name',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Zip/Postal Code',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `region` varchar(255) DEFAULT NULL COMMENT 'State/Province',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'State/Province',
  `street` text NOT NULL COMMENT 'Street Address',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `telephone` varchar(255) NOT NULL COMMENT 'Phone Number',
  `vat_id` varchar(255) DEFAULT NULL COMMENT 'VAT number',
  `vat_is_valid` int(10) unsigned DEFAULT NULL COMMENT 'VAT number validity',
  `vat_request_date` varchar(255) DEFAULT NULL COMMENT 'VAT number validation request date',
  `vat_request_id` varchar(255) DEFAULT NULL COMMENT 'VAT number validation request ID',
  `vat_request_success` int(10) unsigned DEFAULT NULL COMMENT 'VAT number validation request success',
  PRIMARY KEY (`entity_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity';

--
-- Table structure for table `customer_address_entity_datetime`
--

DROP TABLE IF EXISTS `customer_address_entity_datetime`;
CREATE TABLE `customer_address_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Datetime';

--
-- Table structure for table `customer_address_entity_decimal`
--

DROP TABLE IF EXISTS `customer_address_entity_decimal`;
CREATE TABLE `customer_address_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Decimal';

--
-- Table structure for table `customer_address_entity_int`
--

DROP TABLE IF EXISTS `customer_address_entity_int`;
CREATE TABLE `customer_address_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Int';

--
-- Table structure for table `customer_address_entity_text`
--

DROP TABLE IF EXISTS `customer_address_entity_text`;
CREATE TABLE `customer_address_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Text';

--
-- Table structure for table `customer_address_entity_varchar`
--

DROP TABLE IF EXISTS `customer_address_entity_varchar`;
CREATE TABLE `customer_address_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Varchar';

--
-- Table structure for table `customer_eav_attribute`
--

DROP TABLE IF EXISTS `customer_eav_attribute`;
CREATE TABLE `customer_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `input_filter` varchar(255) DEFAULT NULL COMMENT 'Input Filter',
  `multiline_count` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Multiline Count',
  `validate_rules` text COMMENT 'Validate Rules',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `data_model` varchar(255) DEFAULT NULL COMMENT 'Data Model',
  `is_used_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used in Grid',
  `is_visible_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible in Grid',
  `is_filterable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable in Grid',
  `is_searchable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable in Grid',
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute';

--
-- Dumping data for table `customer_eav_attribute`
--

LOCK TABLES `customer_eav_attribute` WRITE;
/*!40000 ALTER TABLE `customer_eav_attribute` DISABLE KEYS */;
INSERT INTO `customer_eav_attribute` VALUES (1,1,NULL,0,NULL,1,'10',NULL,1,1,1,0),(2,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(3,1,NULL,0,NULL,1,'20',NULL,1,1,0,1),(4,0,NULL,0,NULL,0,'30',NULL,0,0,0,0),(5,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'40',NULL,0,0,0,0),(6,0,NULL,0,NULL,0,'50',NULL,0,0,0,0),(7,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'60',NULL,0,0,0,0),(8,0,NULL,0,NULL,0,'70',NULL,0,0,0,0),(9,1,NULL,0,'a:1:{s:16:\"input_validation\";s:5:\"email\";}',1,'80',NULL,1,1,1,1),(10,1,NULL,0,NULL,1,'25',NULL,1,1,1,0),(11,0,'date',0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',0,'90',NULL,1,1,1,0),(12,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(13,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(14,0,NULL,0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',1,'0',NULL,0,0,0,0),(15,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(16,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(17,0,NULL,0,'a:1:{s:15:\"max_text_length\";i:255;}',0,'100',NULL,1,1,0,1),(18,0,NULL,0,NULL,1,'0',NULL,1,1,1,0),(19,0,NULL,0,NULL,0,'0',NULL,1,1,1,0),(20,0,NULL,0,'a:0:{}',0,'110',NULL,1,1,1,0),(21,1,NULL,0,NULL,1,'28',NULL,0,0,0,0),(22,0,NULL,0,NULL,0,'10',NULL,0,0,0,0),(23,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'20',NULL,1,0,0,1),(24,0,NULL,0,NULL,0,'30',NULL,0,0,0,0),(25,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'40',NULL,1,0,0,1),(26,0,NULL,0,NULL,0,'50',NULL,0,0,0,0),(27,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'60',NULL,1,0,0,1),(28,1,NULL,2,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'70',NULL,1,0,0,1),(29,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'80',NULL,1,0,0,1),(30,1,NULL,0,NULL,1,'90',NULL,1,1,1,0),(31,1,NULL,0,NULL,1,'100',NULL,1,1,0,1),(32,1,NULL,0,NULL,1,'100',NULL,0,0,0,0),(33,1,NULL,0,'a:0:{}',1,'110','Magento\\Customer\\Model\\Attribute\\Data\\Postcode',1,1,1,1),(34,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,'120',NULL,1,1,1,1),(35,0,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',0,'130',NULL,1,0,0,1),(36,1,NULL,0,NULL,1,'140',NULL,0,0,0,0),(37,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(38,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(39,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(40,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(41,0,NULL,0,NULL,0,'0',NULL,0,0,0,0),(42,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(43,0,NULL,0,NULL,1,'0',NULL,0,0,0,0),(44,0,NULL,0,NULL,1,'0',NULL,0,0,0,0);
/*!40000 ALTER TABLE `customer_eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_eav_attribute_website`
--

DROP TABLE IF EXISTS `customer_eav_attribute_website`;
CREATE TABLE `customer_eav_attribute_website` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `is_visible` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Visible',
  `is_required` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Required',
  `default_value` text COMMENT 'Default Value',
  `multiline_count` smallint(5) unsigned DEFAULT NULL COMMENT 'Multiline Count',
  PRIMARY KEY (`attribute_id`,`website_id`),
  KEY `CUSTOMER_EAV_ATTRIBUTE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute Website';

--
-- Dumping data for table `customer_eav_attribute_website`
--

LOCK TABLES `customer_eav_attribute_website` WRITE;
/*!40000 ALTER TABLE `customer_eav_attribute_website` DISABLE KEYS */;
INSERT INTO `customer_eav_attribute_website` VALUES (1,1,NULL,NULL,NULL,NULL),(3,1,NULL,NULL,NULL,NULL),(9,1,NULL,NULL,NULL,NULL),(10,1,NULL,NULL,NULL,NULL),(11,1,NULL,NULL,NULL,NULL),(17,1,NULL,NULL,NULL,NULL),(18,1,NULL,NULL,NULL,NULL),(19,1,NULL,NULL,NULL,NULL),(20,1,NULL,NULL,NULL,NULL),(21,1,NULL,NULL,NULL,NULL),(23,1,NULL,NULL,NULL,NULL),(25,1,NULL,NULL,NULL,NULL),(27,1,NULL,NULL,NULL,NULL),(28,1,NULL,NULL,NULL,NULL),(29,1,NULL,NULL,NULL,NULL),(30,1,NULL,NULL,NULL,NULL),(31,1,NULL,NULL,NULL,NULL),(32,1,NULL,NULL,NULL,NULL),(33,1,NULL,NULL,NULL,NULL),(34,1,NULL,NULL,NULL,NULL),(35,1,NULL,NULL,NULL,NULL),(36,1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer_eav_attribute_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity`
--

DROP TABLE IF EXISTS `customer_entity`;
CREATE TABLE `customer_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Website Id',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `disable_auto_group_change` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Disable automatic group change based on VAT ID',
  `created_in` varchar(255) DEFAULT NULL COMMENT 'Created From',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'First Name',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middle Name/Initial',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Last Name',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `dob` date DEFAULT NULL COMMENT 'Date of Birth',
  `password_hash` varchar(128) DEFAULT NULL COMMENT 'Password_hash',
  `rp_token` varchar(128) DEFAULT NULL COMMENT 'Reset password token',
  `rp_token_created_at` datetime DEFAULT NULL COMMENT 'Reset password token creation time',
  `default_billing` int(10) unsigned DEFAULT NULL COMMENT 'Default Billing Address',
  `default_shipping` int(10) unsigned DEFAULT NULL COMMENT 'Default Shipping Address',
  `taxvat` varchar(50) DEFAULT NULL COMMENT 'Tax/VAT Number',
  `confirmation` varchar(64) DEFAULT NULL COMMENT 'Is Confirmed',
  `gender` smallint(5) unsigned DEFAULT NULL COMMENT 'Gender',
  `failures_num` smallint(6) DEFAULT '0' COMMENT 'Failure Number',
  `first_failure` timestamp NULL DEFAULT NULL COMMENT 'First Failure',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Lock Expiration Date',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_EMAIL_WEBSITE_ID` (`email`,`website_id`),
  KEY `CUSTOMER_ENTITY_STORE_ID` (`store_id`),
  KEY `CUSTOMER_ENTITY_WEBSITE_ID` (`website_id`),
  KEY `CUSTOMER_ENTITY_FIRSTNAME` (`firstname`),
  KEY `CUSTOMER_ENTITY_LASTNAME` (`lastname`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Customer Entity';

--
-- Dumping data for table `customer_entity`
--

LOCK TABLES `customer_entity` WRITE;
/*!40000 ALTER TABLE `customer_entity` DISABLE KEYS */;
INSERT INTO `customer_entity` VALUES ('1',1,'vietnama100@gmail.com',1,NULL,1,'2017-06-23 07:39:41','2017-06-23 07:39:41',1,0,'Thai',NULL,'Le',NULL,'Dung',NULL,NULL,'da514c87b133d812801ab01ecd619ca0efec41266315975d710ba13fa10a3366:Pr0hNvxbK1X14Oop7fOPgXLansh64zrC:1','6529f7803004f6ff61c53d35dcade9ef','2017-06-23 07:39:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity_datetime`
--

DROP TABLE IF EXISTS `customer_entity_datetime`;
CREATE TABLE `customer_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Datetime';

--
-- Table structure for table `customer_entity_decimal`
--

DROP TABLE IF EXISTS `customer_entity_decimal`;
CREATE TABLE `customer_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Decimal';

--
-- Table structure for table `customer_entity_int`
--

DROP TABLE IF EXISTS `customer_entity_int`;
CREATE TABLE `customer_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Int';

--
-- Table structure for table `customer_entity_text`
--

DROP TABLE IF EXISTS `customer_entity_text`;
CREATE TABLE `customer_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Text';

--
-- Table structure for table `customer_entity_varchar`
--

DROP TABLE IF EXISTS `customer_entity_varchar`;
CREATE TABLE `customer_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Varchar';

--
-- Table structure for table `customer_form_attribute`
--

DROP TABLE IF EXISTS `customer_form_attribute`;
CREATE TABLE `customer_form_attribute` (
  `form_code` varchar(32) NOT NULL COMMENT 'Form Code',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`form_code`,`attribute_id`),
  KEY `CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Form Attribute';

--
-- Dumping data for table `customer_form_attribute`
--

LOCK TABLES `customer_form_attribute` WRITE;
/*!40000 ALTER TABLE `customer_form_attribute` DISABLE KEYS */;
INSERT INTO `customer_form_attribute` VALUES ('adminhtml_customer',1),('adminhtml_customer',3),('adminhtml_customer',4),('customer_account_create',4),('customer_account_edit',4),('adminhtml_customer',5),('customer_account_create',5),('customer_account_edit',5),('adminhtml_customer',6),('customer_account_create',6),('customer_account_edit',6),('adminhtml_customer',7),('customer_account_create',7),('customer_account_edit',7),('adminhtml_customer',8),('customer_account_create',8),('customer_account_edit',8),('adminhtml_checkout',9),('adminhtml_customer',9),('customer_account_create',9),('customer_account_edit',9),('adminhtml_checkout',10),('adminhtml_customer',10),('adminhtml_checkout',11),('adminhtml_customer',11),('customer_account_create',11),('customer_account_edit',11),('adminhtml_checkout',17),('adminhtml_customer',17),('customer_account_create',17),('customer_account_edit',17),('adminhtml_customer',19),('customer_account_create',19),('customer_account_edit',19),('adminhtml_checkout',20),('adminhtml_customer',20),('customer_account_create',20),('customer_account_edit',20),('adminhtml_customer',21),('adminhtml_customer_address',22),('customer_address_edit',22),('customer_register_address',22),('adminhtml_customer_address',23),('customer_address_edit',23),('customer_register_address',23),('adminhtml_customer_address',24),('customer_address_edit',24),('customer_register_address',24),('adminhtml_customer_address',25),('customer_address_edit',25),('customer_register_address',25),('adminhtml_customer_address',26),('customer_address_edit',26),('customer_register_address',26),('adminhtml_customer_address',27),('customer_address_edit',27),('customer_register_address',27),('adminhtml_customer_address',28),('customer_address_edit',28),('customer_register_address',28),('adminhtml_customer_address',29),('customer_address_edit',29),('customer_register_address',29),('adminhtml_customer_address',30),('customer_address_edit',30),('customer_register_address',30),('adminhtml_customer_address',31),('customer_address_edit',31),('customer_register_address',31),('adminhtml_customer_address',32),('customer_address_edit',32),('customer_register_address',32),('adminhtml_customer_address',33),('customer_address_edit',33),('customer_register_address',33),('adminhtml_customer_address',34),('customer_address_edit',34),('customer_register_address',34),('adminhtml_customer_address',35),('customer_address_edit',35),('customer_register_address',35),('adminhtml_customer_address',36),('customer_address_edit',36),('customer_register_address',36);
/*!40000 ALTER TABLE `customer_form_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_grid_flat`
--

DROP TABLE IF EXISTS `customer_grid_flat`;
CREATE TABLE `customer_grid_flat` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `name` text COMMENT 'Name',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` int(11) DEFAULT NULL COMMENT 'Group_id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created_at',
  `website_id` int(11) DEFAULT NULL COMMENT 'Website_id',
  `confirmation` varchar(255) DEFAULT NULL COMMENT 'Confirmation',
  `created_in` text COMMENT 'Created_in',
  `dob` date DEFAULT NULL COMMENT 'Dob',
  `gender` int(11) DEFAULT NULL COMMENT 'Gender',
  `taxvat` varchar(255) DEFAULT NULL COMMENT 'Taxvat',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Lock_expires',
  `billing_full` text COMMENT 'Billing_full',
  `billing_firstname` varchar(255) DEFAULT NULL COMMENT 'Billing_firstname',
  `billing_lastname` varchar(255) DEFAULT NULL COMMENT 'Billing_lastname',
  `billing_telephone` varchar(255) DEFAULT NULL COMMENT 'Billing_telephone',
  `billing_postcode` varchar(255) DEFAULT NULL COMMENT 'Billing_postcode',
  `billing_country_id` varchar(255) DEFAULT NULL COMMENT 'Billing_country_id',
  `billing_region` varchar(255) DEFAULT NULL COMMENT 'Billing_region',
  `billing_street` varchar(255) DEFAULT NULL COMMENT 'Billing_street',
  `billing_city` varchar(255) DEFAULT NULL COMMENT 'Billing_city',
  `billing_fax` varchar(255) DEFAULT NULL COMMENT 'Billing_fax',
  `billing_vat_id` varchar(255) DEFAULT NULL COMMENT 'Billing_vat_id',
  `billing_company` varchar(255) DEFAULT NULL COMMENT 'Billing_company',
  `shipping_full` text COMMENT 'Shipping_full',
  PRIMARY KEY (`entity_id`),
  KEY `CUSTOMER_GRID_FLAT_GROUP_ID` (`group_id`),
  KEY `CUSTOMER_GRID_FLAT_CREATED_AT` (`created_at`),
  KEY `CUSTOMER_GRID_FLAT_WEBSITE_ID` (`website_id`),
  KEY `CUSTOMER_GRID_FLAT_CONFIRMATION` (`confirmation`),
  KEY `CUSTOMER_GRID_FLAT_DOB` (`dob`),
  KEY `CUSTOMER_GRID_FLAT_GENDER` (`gender`),
  KEY `CUSTOMER_GRID_FLAT_BILLING_COUNTRY_ID` (`billing_country_id`),
  FULLTEXT KEY `FTI_B691CA777399890C71AC8A4CDFB8EA99` (`name`,`email`,`created_in`,`taxvat`,`billing_full`,`billing_firstname`,`billing_lastname`,`billing_telephone`,`billing_postcode`,`billing_region`,`billing_city`,`billing_fax`,`billing_company`,`shipping_full`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='customer_grid_flat';

--
-- Dumping data for table `customer_grid_flat`
--

LOCK TABLES `customer_grid_flat` WRITE;
/*!40000 ALTER TABLE `customer_grid_flat` DISABLE KEYS */;
INSERT INTO `customer_grid_flat` VALUES ('1','Le Dung','vietnama100@gmail.com','1','2017-06-23 07:39:41','1',NULL,'Thai',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
/*!40000 ALTER TABLE `customer_grid_flat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_group`
--

DROP TABLE IF EXISTS `customer_group`;
CREATE TABLE `customer_group` (
  `customer_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Customer Group Id',
  `customer_group_code` varchar(32) NOT NULL COMMENT 'Customer Group Code',
  `tax_class_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Tax Class Id',
  PRIMARY KEY (`customer_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Customer Group';

--
-- Dumping data for table `customer_group`
--

LOCK TABLES `customer_group` WRITE;
/*!40000 ALTER TABLE `customer_group` DISABLE KEYS */;
INSERT INTO `customer_group` VALUES (0,'NOT LOGGED IN','3'),(1,'General','3'),(2,'Wholesale','3'),(3,'Retailer','3');
/*!40000 ALTER TABLE `customer_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_log`
--

DROP TABLE IF EXISTS `customer_log`;
CREATE TABLE `customer_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `customer_id` int(11) NOT NULL COMMENT 'Customer ID',
  `last_login_at` timestamp NULL DEFAULT NULL COMMENT 'Last Login Time',
  `last_logout_at` timestamp NULL DEFAULT NULL COMMENT 'Last Logout Time',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `CUSTOMER_LOG_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Customer Log Table';

--
-- Dumping data for table `customer_log`
--

LOCK TABLES `customer_log` WRITE;
/*!40000 ALTER TABLE `customer_log` DISABLE KEYS */;
INSERT INTO `customer_log` VALUES ('1','1','2017-06-23 07:39:43',NULL);
/*!40000 ALTER TABLE `customer_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_visitor`
--

DROP TABLE IF EXISTS `customer_visitor`;
CREATE TABLE `customer_visitor` (
  `visitor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Visitor ID',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `session_id` varchar(64) DEFAULT NULL COMMENT 'Session ID',
  `last_visit_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last Visit Time',
  PRIMARY KEY (`visitor_id`),
  KEY `CUSTOMER_VISITOR_CUSTOMER_ID` (`customer_id`),
  KEY `CUSTOMER_VISITOR_LAST_VISIT_AT` (`last_visit_at`)
) ENGINE=InnoDB AUTO_INCREMENT=431 DEFAULT CHARSET=utf8 COMMENT='Visitor Table';

--
-- Dumping data for table `customer_visitor`
--

LOCK TABLES `customer_visitor` WRITE;
/*!40000 ALTER TABLE `customer_visitor` DISABLE KEYS */;
INSERT INTO `customer_visitor` VALUES (1,NULL,'2r51iv0kn5941b858qmus4npk1','2017-06-06 07:49:52'),(2,NULL,'l1kflotek2puh0m6fu7jd248f1','2017-06-06 07:50:14'),(3,NULL,'fnv0sg09opr6e4uib1ccih54i0','2017-06-06 09:03:23'),(4,NULL,'k4u2qglqvufi8vup7i9lnvc427','2017-06-06 09:14:00'),(5,NULL,'9db0pk217q6ii5tpnthigtodi7','2017-06-12 15:41:35'),(6,NULL,'hm3uobs06dv0qji8a8riio4h01','2017-06-12 15:41:40'),(7,NULL,'tcpj47hv0e5ud3u0f7q19lfo77','2017-06-12 15:58:00'),(8,NULL,'5npq22jdgk4ekluie1jajvukl3','2017-06-12 15:58:05'),(9,NULL,'66ut10nsvv7u4ccp38m125jgp1','2017-06-12 16:11:37'),(10,NULL,'1h741cuh9b8u6l0mgj7rcq5fv3','2017-06-13 03:15:15'),(11,NULL,'m3hp0kp0thbsq8gu5qekkucdj3','2017-06-13 02:44:00'),(12,NULL,'4h9qe6fksfa4o6mjbpstrbpe83','2017-06-13 03:27:50'),(13,NULL,'olbght191bto1m0o302bje6ul1','2017-06-13 06:00:26'),(14,NULL,'14m7m9e1o6r3bc7emtb930noe1','2017-06-13 06:00:33'),(15,NULL,'9e8so497fkskgp8u28mbaaqgm2','2017-06-13 06:00:42'),(16,NULL,'qlri7grksp4g1rp1vjr80j8un3','2017-06-13 06:00:33'),(17,NULL,'9e8so497fkskgp8u28mbaaqgm2','2017-06-13 06:55:43'),(18,NULL,'pig8bp7ll17449hvlhsrl0amt7','2017-06-13 07:00:36'),(19,NULL,'9lhilglnm3n789jj2m96n1lh67','2017-06-13 07:00:38'),(20,NULL,'mkhaoglkgh5mmg1galdpesku67','2017-06-13 07:58:26'),(21,NULL,'a5et8n7nf6ed9gk3cd01a51k07','2017-06-13 07:00:38'),(22,NULL,'nchp30553npqim9ioh9rjg0541','2017-06-13 08:01:58'),(23,NULL,'gjpn8d0basrr16jh9akqaelsm7','2017-06-13 08:01:58'),(24,NULL,'ng5ne7pgv2f1r5gvjticlnj5p2','2017-06-13 08:02:03'),(25,NULL,'gtoujqjoji5qokt6bqnlv3qbr5','2017-06-13 08:35:22'),(26,NULL,'99r1ogo52au2ovp25f14o8s272','2017-06-13 08:02:03'),(27,NULL,'qmqljg9qckrik5ni6ph31no2v5','2017-06-13 08:02:03'),(28,NULL,'leh8b60c8t43nsouk3n2cbn5o4','2017-06-13 09:53:56'),(29,NULL,'us2mo38rp1nen97iv59i50pno6','2017-06-13 09:53:56'),(30,NULL,'4sdkle3h41p7o0gfqcctvp4no6','2017-06-13 09:53:56'),(31,NULL,'hst1bugivu9m9kbvaanr9k43e4','2017-06-13 09:53:56'),(32,NULL,'4sdkle3h41p7o0gfqcctvp4no6','2017-06-13 10:53:44'),(33,NULL,'bqu9erup4ffdl6rs5nqddf9e14','2017-06-13 10:59:04'),(34,NULL,'89d9kk0ms34sbaspjq4p57b5u0','2017-06-13 10:59:06'),(35,NULL,'8c5cdijns3fkng7c630le1bqf3','2017-06-13 11:01:01'),(36,NULL,'5ulk40e4odcr4e8c2dt5hc6s65','2017-06-13 10:59:06'),(37,NULL,'jirmakvtmmjnfbbc98539m6fo5','2017-06-13 11:10:10'),(38,NULL,'dsfecnmknnb9jfilspgd15g3r4','2017-06-13 11:10:12'),(39,NULL,'8c5cdijns3fkng7c630le1bqf3','2017-06-13 11:22:12'),(40,NULL,'2sun66197kdv6nk262j30hpsn2','2017-06-13 12:08:39'),(41,NULL,'o4uh6tgupvoov6aa929n5ajfn1','2017-06-13 12:16:52'),(42,NULL,'get3qfd5gntvj8bdakc46taah4','2017-06-13 12:16:54'),(43,NULL,'u0ae70qvncn9v67hu65qptefu3','2017-06-13 12:25:32'),(44,NULL,'043l8bk45cbe18ro2s2hh7vu50','2017-06-13 12:25:36'),(45,NULL,'18avdlo1lpdnq2kevbua5sgh43','2017-06-13 12:27:09'),(46,NULL,'fuotf3l35u6tvuhcu0fonc1k64','2017-06-13 12:27:10'),(47,NULL,'cshsllbu9h55lsjuo7fvu725v2','2017-06-13 12:29:37'),(48,NULL,'cuko3e0rf7qogfvesk87tkmqh7','2017-06-13 12:27:11'),(49,NULL,'k9glvhprnprnk9e8cvtpe7vpc6','2017-06-13 12:29:38'),(50,NULL,'2gs045ae17i8ps4rrc45bg5944','2017-06-13 12:38:57'),(51,NULL,'0bj2nih9rjaj3vqfopnv7p7gu1','2017-06-13 12:38:57'),(52,NULL,'jccq0k0v8rotjph8301pv1t837','2017-06-13 14:02:52'),(53,NULL,'nrrqh1bulv46a7ps197nk0kf67','2017-06-13 14:12:19'),(54,NULL,'rpqj2tdha8ili37o3qgmtolun2','2017-06-13 15:59:10'),(55,NULL,'fh50ceg67ia0jbubucq6095ot3','2017-06-13 15:59:10'),(56,NULL,'kbhlh6g5l0nkoh2uomdffrrkv3','2017-06-13 17:46:38'),(57,NULL,'qtg0glgdb9d317452t2ih5klq3','2017-06-13 17:46:11'),(58,NULL,'2q1i5o4je9pp3r0tfvqagsrjd4','2017-06-13 17:46:11'),(59,NULL,'s74s2ckk2s8pdghh5mu08jdf31','2017-06-14 04:30:55'),(60,NULL,'cgps07m6n54ue3fsd0okvbggo1','2017-06-14 04:32:44'),(61,NULL,'lcjo1nb67pj6g5bj6it7kqp6b1','2017-06-14 04:36:32'),(62,NULL,'7k6a70a9jg9sfn8m032cedr250','2017-06-14 04:37:17'),(63,NULL,'k2jdg7bhr5o28go5vod63b3tu0','2017-06-14 04:51:04'),(64,NULL,'8iqifl796bir0vcm9h5khf9hn5','2017-06-14 08:14:16'),(65,NULL,'lmi8jcnkgmukpot1heunvcu9e6','2017-06-14 08:44:22'),(66,NULL,'e1ki7rdmrsp8gecqv3le1e1p94','2017-06-14 09:28:57'),(67,NULL,'ip34r8e0c2jvkbb6uf3g8mk494','2017-06-14 10:20:51'),(68,NULL,'md7os0nspdd9c5ec02k2927ma7','2017-06-14 10:21:08'),(69,NULL,'kt60j33qb86dtmenqq59t445h6','2017-06-14 10:23:10'),(70,NULL,'vufs2krg02fogprlt3cmi6b616','2017-06-14 14:10:05'),(71,NULL,'4laqrl251ubuv06gngbqoug8t2','2017-06-14 14:10:05'),(72,NULL,'jfjpiqsfk9lh6dtq8d253sp3d4','2017-06-14 16:04:37'),(73,NULL,'koejam355stc48ajdaf9qjlti3','2017-06-14 16:04:40'),(74,NULL,'v3amvshvkjhc8gindsduca5q34','2017-06-14 16:04:37'),(75,NULL,'4tsrpgj1is6q90g4kkc0rfbj66','2017-06-14 16:04:38'),(76,NULL,'2bioeoc36i6nsf01gidida7v83','2017-06-14 17:28:38'),(77,NULL,'7d91at6o126sqlkmhjvdr137a6','2017-06-14 17:28:38'),(78,NULL,'m2frcm4c1uk26btjp08fu6se56','2017-06-14 17:28:38'),(79,NULL,'mqjoc1rimuek5u0s0r22n1tcv5','2017-06-15 11:25:19'),(80,NULL,'cl0qeg3gd1sk9f2c2a5ujaj0a4','2017-06-15 11:25:27'),(81,NULL,'5d48v6c3foe4i9ben0c7ggs2q4','2017-06-16 03:29:31'),(82,NULL,'08jbm0i0ntfhba9so3pomfoqj3','2017-06-16 03:29:35'),(83,NULL,'piiaqgfmqmc62k53drh075q8a1','2017-06-16 03:29:36'),(84,NULL,'1eruu1s88aa25o3eo6uledkuu6','2017-06-16 04:28:03'),(85,NULL,'mdbjnjk5n5ole6p4gp3sqi3q86','2017-06-16 04:28:07'),(86,NULL,'bjje52hu5ajvmatlb65odkb3s1','2017-06-16 04:31:58'),(87,NULL,'roc237ff3r8ia9auk3crk5cen3','2017-06-16 04:31:59'),(88,NULL,'n4d2k5gcl08rr59t064s0klg64','2017-06-16 07:37:09'),(89,NULL,'84diikjatmfb8h5deti2l4n9r0','2017-06-16 07:37:14'),(90,NULL,'la2i6jd87kdgvg63iocdumk8e6','2017-06-16 08:05:21'),(91,NULL,'q2n9qduahdj36nqrkv66t0nvb6','2017-06-16 08:47:13'),(92,NULL,'m4colsf5ftms7kmv16dd0icli1','2017-06-16 08:47:15'),(93,NULL,'5ik3k5mn5k0hvnjg9h2met6tn4','2017-06-16 08:47:19'),(94,NULL,'uhpfj74kte1gifq37q0p3o04m5','2017-06-16 08:47:23'),(95,NULL,'mqrpbs9no8djofv8vkgq70fhv2','2017-06-16 08:47:24'),(96,NULL,'n9jb62uhmpnbtu40130843cmi4','2017-06-16 09:14:42'),(97,NULL,'8mesf3e01o42o7bjc58gb1d2f3','2017-06-16 12:06:17'),(98,NULL,'djg70kn5cqi4uop44al031qcc2','2017-06-16 13:53:40'),(99,NULL,'uj94dacs4rrumddu189q6qtkt3','2017-06-16 13:53:41'),(100,NULL,'48cs8g6j95dh48v8m8j6f54e03','2017-06-18 01:17:30'),(101,NULL,'d1k5pf9kujuf469d52d5sbjap3','2017-06-18 01:17:46'),(102,NULL,'je8krvrr253saf79dtshismur5','2017-06-19 04:14:40'),(103,NULL,'63kmp1q7ick2t9k9gqaggj6gb4','2017-06-19 04:14:45'),(104,NULL,'celjo07g1j3sqp1nluctjrc1d3','2017-06-19 04:58:27'),(105,NULL,'itmebtba3paa5mn6kqnkpsauq5','2017-06-19 04:58:31'),(106,NULL,'01h2089adq5a3pvjcb9iko6ef2','2017-06-19 06:03:13'),(107,NULL,'dfh4m41rldc7qh4f05mt13f9j3','2017-06-19 06:56:46'),(108,NULL,'cpm3ut14j27rdb3nv932ms92o2','2017-06-19 07:21:04'),(109,NULL,'kaaojr5opjncf407irjpt5mj71','2017-06-19 07:21:07'),(110,NULL,'32grc246k94fnsb5cipmsrp3t0','2017-06-19 07:28:00'),(111,NULL,'6n1ungmput7deiaahs2nkgvbk6','2017-06-19 07:29:25'),(112,NULL,'u2q77r63sbi2mbcoj1a4doodp5','2017-06-19 07:32:59'),(113,NULL,'6vb8tsn8lh5tcr16h35vbe1864','2017-06-19 07:33:17'),(114,NULL,'77vn5qh6mg8sgfg7tfli7qpvl1','2017-06-19 07:37:03'),(115,NULL,'urrrm99lc10pa179uj8k8msqj4','2017-06-19 08:08:06'),(116,NULL,'9315ajstgfl2ek48ft367qtd34','2017-06-19 08:11:15'),(117,NULL,'e95rh0ih7hbndtvdsha87il9o4','2017-06-19 09:03:36'),(118,NULL,'jaaucfhnm9bmggul2amoota0j1','2017-06-19 09:14:25'),(119,NULL,'d88scn3k4a97uooh6ja81avpf5','2017-06-19 09:20:58'),(120,NULL,'e10f79dfv1j026vcbvniqdbsi6','2017-06-19 09:21:06'),(121,NULL,'a3c23fsl4hjqukgh65ajljdn93','2017-06-19 09:23:30'),(122,NULL,'h88i5dm1gr0f082o14q5rmsaq2','2017-06-19 11:19:11'),(123,NULL,'pg40aq1h79muaha8d7a537hj42','2017-06-19 11:19:25'),(124,NULL,'ojdokiq71bmh9thvj0jcb09pt7','2017-06-19 11:20:21'),(125,NULL,'vjoo9sophknc7jm8qv9ovfb2h3','2017-06-19 11:25:34'),(126,NULL,'6lvtsff2pgh7fn9dmu83mlgs97','2017-06-19 11:26:48'),(127,NULL,'9je75deidgupkad4vtcc0guv61','2017-06-19 11:27:56'),(128,NULL,'jdlnot4kndhdu8giitp6iamj03','2017-06-19 11:33:46'),(129,NULL,'tmr6l6hlgd4l96rf4an9cuin61','2017-06-19 11:37:28'),(130,NULL,'4sm74t3a0rb7hduhbe43v2udm0','2017-06-19 14:19:33'),(131,NULL,'luaks6131d2hc32u25u33r3ig7','2017-06-19 16:49:53'),(132,NULL,'ibnd2aplf3ur6vj9ufre616fn2','2017-06-20 00:30:34'),(133,NULL,'ica071f9i17o9irv7f8h6s6vi6','2017-06-20 01:42:00'),(134,NULL,'u6bqkgb2f0h40snkualbkgbso7','2017-06-20 01:43:58'),(135,NULL,'8u2mp0d65ou3fre38efke1gdk0','2017-06-20 01:48:03'),(136,NULL,'05n0te1p9nu6ka6d7mi1ntdvi1','2017-06-20 02:14:43'),(137,NULL,'k6rubnhqjici0snmq2i2bamk97','2017-06-20 02:16:49'),(138,NULL,'42r33l5ouhvkddm6sn5v7klg63','2017-06-20 02:17:05'),(139,NULL,'71anqntp1jnb3esspm6uip5952','2017-06-20 02:42:59'),(140,NULL,'vsr5v12nk2k9ek06pd7grk54t7','2017-06-20 02:45:21'),(141,NULL,'d4pf5o017j1grjesn388drn7p3','2017-06-20 03:36:58'),(142,NULL,'ivhq9pprlcb5c06vcvt8no5cr2','2017-06-20 04:44:01'),(143,NULL,'hsl2hhh85neo8nqd32hhqtesh0','2017-06-20 05:03:26'),(144,NULL,'1fd5jjbtsdvo6nae4j18mt0qb3','2017-06-20 06:00:10'),(145,NULL,'n34id48othcpheqigb40uq3ln3','2017-06-20 06:21:19'),(146,NULL,'nnjhol952egi3l10o0l3asksu3','2017-06-20 06:39:56'),(147,NULL,'4fqoqu7aa62gd453247h6vmqp5','2017-06-20 06:56:09'),(148,NULL,'vmbdg7mpsg4ov8186h9qqki753','2017-06-20 06:56:12'),(149,NULL,'k9k573svp76u5tctfh66qadch7','2017-06-20 07:04:54'),(150,NULL,'2cvvrtoqblf64s0229ajbohpa4','2017-06-20 07:09:40'),(151,NULL,'aopgjuhc4e9hsdjtvrf9st33l3','2017-06-20 07:31:22'),(152,NULL,'i8aoegg0ed9lr5270p589bcus2','2017-06-20 07:43:10'),(153,NULL,'oopqtn4k48qkfahn8hv950e9g3','2017-06-20 08:12:43'),(154,NULL,'auntmnrl4lrq46gere7q1m47n4','2017-06-20 08:13:56'),(155,NULL,'dpbkppdl1o1rgqce5igc3c17s3','2017-06-20 08:14:58'),(156,NULL,'tt9rn79kqlj4dthp2pcsdusjt4','2017-06-20 08:16:05'),(157,NULL,'5jn3h9ec12l2o8odu35rvks5b0','2017-06-20 09:11:03'),(158,NULL,'8fdlp479slrb1f9li5q6lomv03','2017-06-20 09:24:45'),(159,NULL,'r9umumvq38hiuib3as34t21aa7','2017-06-20 10:03:26'),(160,NULL,'rt9ojtn64iblnnb10ln0bqhhl3','2017-06-20 10:03:27'),(161,NULL,'6g08sc0oh9rv1j2mteb52qie46','2017-06-20 10:24:11'),(162,NULL,'t2u7tp8rns179g3eilr9048761','2017-06-20 10:24:27'),(163,NULL,'aesposp7gf0f8bnogp3io0kjb6','2017-06-20 10:25:37'),(164,NULL,'6uema7ic6dmn3fm2ip0f8785n1','2017-06-20 10:28:04'),(165,NULL,'m77si6kkoiiju70d7p3e6spdb0','2017-06-20 10:31:03'),(166,NULL,'aoksgu23r2lilkgp4hbd8lcup4','2017-06-20 10:42:19'),(167,NULL,'qf5ag25lpub9upbdth83p8oaf5','2017-06-20 10:42:33'),(168,NULL,'3sc7h5hooa9jsgjlbgvo1khfr4','2017-06-20 10:42:40'),(169,NULL,'3jov6fjrejmcn9qglfcji7gc57','2017-06-20 10:42:46'),(170,NULL,'fp05hjrhrsd860odr0b89nofe2','2017-06-20 10:42:50'),(171,NULL,'guiijfch931q3q55ta6fvjkep6','2017-06-20 10:55:35'),(172,NULL,'dmk3j0hleunc58u9q3qdfinjp6','2017-06-20 10:55:37'),(173,NULL,'2o0ih5gotgl81av0ep0hfgmt87','2017-06-20 10:56:05'),(174,NULL,'2nc5t0ire80s4i6tb32i8ekrf3','2017-06-20 10:57:30'),(175,NULL,'7c01ets6s5d1oj56erjpsbfrb0','2017-06-20 11:10:22'),(176,NULL,'9mvaouit464h2boa8kg7r3pls7','2017-06-20 11:10:24'),(177,NULL,'4939alj735ovjt3ap7thvkhas7','2017-06-20 11:10:50'),(178,NULL,'bjlrdei8adjnmr399243upr0n4','2017-06-20 11:37:04'),(179,NULL,'0asjkm3m6dba103fcoh92bcmr0','2017-06-20 11:54:39'),(180,NULL,'5fdk335c2dd89ghork4h4e7g43','2017-06-20 11:54:46'),(181,NULL,'lmuil8oqdos8uvojh63taoijg3','2017-06-20 11:54:56'),(182,NULL,'egief7n7g2v0sqks3uljdka025','2017-06-20 11:58:05'),(183,NULL,'csud3pnkvmtaurhb2frs4v2vn4','2017-06-20 11:58:49'),(184,NULL,'h403st9c20bqe1vpss2o19rf64','2017-06-20 11:59:38'),(185,NULL,'40qkhodpq44j1q27mfefmg2vq6','2017-06-20 12:01:50'),(186,NULL,'tncat8rl78l8escss17cilj9d1','2017-06-20 12:01:50'),(187,NULL,'4gmkv4m9u2b2cmu7o54goev6f1','2017-06-20 12:01:50'),(188,NULL,'ae8ilcq3l6cthg053nmo1tevq7','2017-06-20 12:01:52'),(189,NULL,'l5nr4c04ukr2dmd8in4t847u86','2017-06-20 12:02:31'),(190,NULL,'h1hr8d8k67bh80q8jvm9k74os0','2017-06-20 12:06:26'),(191,NULL,'88rjtduqsju13bhe02qpqk0v51','2017-06-20 12:13:01'),(192,NULL,'sjvps6pl4g4ai23quldi0ssc03','2017-06-20 12:16:28'),(193,NULL,'ooolh92r8oo7eeubml2ktr3qc2','2017-06-20 12:17:43'),(194,NULL,'5343cth9q67tv4754o9e130bv7','2017-06-20 12:18:03'),(195,NULL,'7mncndug4ifm0imii4ivmmb5k5','2017-06-20 12:18:26'),(196,NULL,'vto2caobit9b1qdl93uge2v9p0','2017-06-20 12:18:33'),(197,NULL,'35fterrn4dplu82r3j6gl69vu5','2017-06-20 12:19:37'),(198,NULL,'9niip9ic8c0j7a8aup4ggr4a03','2017-06-20 12:19:42'),(199,NULL,'6afkqphc8lf7bfq4in6f1bn621','2017-06-20 12:20:03'),(200,NULL,'52upd9t4fou5ij9fshc3nbvb15','2017-06-20 12:20:20'),(201,NULL,'v8ukjn91p2ltee3abugtsm84i3','2017-06-20 12:21:12'),(202,NULL,'kbomvf0j2qvr2mvjt60dm95no4','2017-06-20 12:21:33'),(203,NULL,'ng49qr7i48v6f6fluacfio7mk2','2017-06-20 12:21:52'),(204,NULL,'fjjhb3irfeled4lab4od5d48h5','2017-06-20 12:21:52'),(205,NULL,'7q07tehr9fpdg68i49uh72ikp0','2017-06-20 13:05:18'),(206,NULL,'e3n9m8otdo8rnv585dlohc4nj5','2017-06-20 12:42:15'),(207,NULL,'3q0ekv5apjgcdr668sk61e8q94','2017-06-20 12:43:45'),(208,NULL,'4vlvmumv3a8fer7o0r2lku37o7','2017-06-20 12:56:21'),(209,NULL,'jua9e21cd0nlm0hqf961fq25g6','2017-06-20 12:59:15'),(210,NULL,'m95rerllqu27kfc08810lm4uk4','2017-06-20 12:59:22'),(211,NULL,'fpl02dl66mmmve4b18icud2qv5','2017-06-20 13:00:45'),(212,NULL,'20na9vis8p7554tuc97282c8u7','2017-06-20 13:01:29'),(213,NULL,'mn2mvrr5nr84k41b66tjculel4','2017-06-20 13:06:07'),(214,NULL,'hn6l2neaek2tgkv1e7em5v22u7','2017-06-20 13:06:39'),(215,NULL,'ev7e1tbdeb2ap38eaug6spcfk3','2017-06-20 13:31:33'),(216,NULL,'g1ndjtisj2heupkv2kvav6vdg3','2017-06-20 14:20:26'),(217,NULL,'4s7cnku11gulinc8hrup82fnj0','2017-06-20 13:31:33'),(218,NULL,'up320f9dej5ggmpjel98rnt6s3','2017-06-20 14:13:58'),(219,NULL,'suh6f85tlvh0r50tr3b4d0oo14','2017-06-20 14:14:55'),(220,NULL,'lhp0ivrde846o1aj9jpd14p5n2','2017-06-20 14:15:07'),(221,NULL,'8fsiefc51rvpld93qm6al7lbj4','2017-06-20 14:32:34'),(222,NULL,'vsc7ce79ie3a2dlbmaeuihf331','2017-06-20 14:34:00'),(223,NULL,'5htkoej5tib9src3hhgftin064','2017-06-21 01:08:20'),(224,NULL,'e02tbuq8kjd3q8929r3jccm9s4','2017-06-21 00:50:41'),(225,NULL,'r2gkvn1gsvugtvmgh163onk0n5','2017-06-21 00:50:42'),(226,NULL,'13ka8ctjjpsnas5p5svs2trug2','2017-06-21 01:14:06'),(227,NULL,'vm139re18nr4v0ourj6rlt1na1','2017-06-21 01:57:18'),(228,NULL,'pgmbl52rbaem36n3ovu4gkevp4','2017-06-21 02:09:04'),(229,NULL,'c21duqof670nh4veqat3ciai94','2017-06-21 02:09:04'),(230,NULL,'f79niprfslvc90a5u2a7g1hv04','2017-06-21 02:09:04'),(231,NULL,'9kmd7tu6odq1retpoca9rodmj6','2017-06-21 02:21:48'),(232,NULL,'merkvjjnvl4d941uhpa93crtu1','2017-06-21 02:23:59'),(233,NULL,'jkj7f8keqnc1n0op7238t6dbi1','2017-06-21 03:17:36'),(234,NULL,'n1n1023hm145nhfrqreprrt2s2','2017-06-21 03:28:04'),(235,NULL,'9v4nj3omsbos67l7ts3jbvog04','2017-06-21 03:35:16'),(236,NULL,'qc4fa37a8hskllsbp0vjuh5gd0','2017-06-21 03:35:19'),(237,NULL,'8jbuie38db0ih2fjk5djdjbub0','2017-06-21 04:19:21'),(238,NULL,'hhfprhmn628kdv92empa6kdl42','2017-06-21 04:36:18'),(239,NULL,'152ctv3b2k89hb58s8jruqeg82','2017-06-21 05:08:40'),(240,NULL,'8af3nanjcdmpq6ibjn1csv6oq3','2017-06-21 05:05:33'),(241,NULL,'1qgbn992jkn5c9mediavd161c4','2017-06-21 06:43:46'),(242,NULL,'3lh6hpeh05kj4ppd4g52ij2tp3','2017-06-21 06:43:46'),(243,NULL,'gido86psqp37je2qmd3q6updc3','2017-06-21 06:43:46'),(244,NULL,'gf1dvh5a5eqdp2t06c6lbv56d5','2017-06-21 07:32:03'),(245,NULL,'tfjcnthv3603pfrfhmou6ifs43','2017-06-21 07:55:06'),(246,NULL,'feqkr2ddm395d7qnuf89n3q5c5','2017-06-21 07:55:00'),(247,NULL,'s1ea4g607mdmu0lprns5jsn5n5','2017-06-21 07:55:00'),(248,NULL,'tfjcnthv3603pfrfhmou6ifs43','2017-06-21 08:39:30'),(249,NULL,'qnd82o717mgetjghip5nbgqhu2','2017-06-21 07:59:49'),(250,NULL,'hth6132f7f2v1jde3svlupudj7','2017-06-21 08:19:48'),(251,NULL,'8268da6797m9ro9umipono1bj0','2017-06-21 08:19:24'),(252,NULL,'va0cekp8pqugvgj73026dkfhi7','2017-06-21 08:19:29'),(253,NULL,'5a8tud8qtg9qdi8n76ioek1oa2','2017-06-21 08:20:35'),(254,NULL,'0mug6aegsb8qms7frlicttqvb0','2017-06-21 08:47:26'),(255,NULL,'tfjcnthv3603pfrfhmou6ifs43','2017-06-21 08:50:03'),(256,NULL,'hq7chep5j73ahfgdnh56of6pb2','2017-06-21 08:54:14'),(257,NULL,'mnee93akc7opo4ri2dvsna0lq1','2017-06-21 08:54:21'),(258,NULL,'e9gjv43ht9q12i9i57ja9j98p7','2017-06-21 08:54:22'),(259,NULL,'1b0251e18pprkaarjql55ddo25','2017-06-21 08:55:42'),(260,NULL,'qskjooujp62fj10o272lgqmcj4','2017-06-21 08:55:42'),(261,NULL,'q21a7gggfmo5ag27f0g3d833d7','2017-06-21 09:01:50'),(262,NULL,'hth6132f7f2v1jde3svlupudj7','2017-06-21 08:59:54'),(263,NULL,'h87ffraussh4ma2bem3k8da0d3','2017-06-21 09:23:14'),(264,NULL,'0mug6aegsb8qms7frlicttqvb0','2017-06-21 09:03:56'),(265,NULL,'2qeviibipr16u9hb1dfdbj51o3','2017-06-21 09:04:12'),(266,NULL,'aonpigb0idu54m6l4noqndok74','2017-06-21 09:04:17'),(267,NULL,'hr223ust9pf35170bihg27rdr2','2017-06-21 09:12:17'),(268,NULL,'pr7jf9fuip17pa7071e2nc3k05','2017-06-21 09:06:26'),(269,NULL,'l65kd2lq8btndrt8dsvv0u1211','2017-06-21 09:13:32'),(270,NULL,'1ocni3ko8gigrfcvset7v9cub3','2017-06-21 09:27:47'),(271,NULL,'rd2fmumlmh72bc5rok3eouv3v1','2017-06-21 09:27:47'),(272,NULL,'kl26k8cdjmigobestlbl6fnu20','2017-06-21 09:39:59'),(273,NULL,'9s4nr1htv2tq4afntko9fsj1v0','2017-06-21 10:43:21'),(274,NULL,'usq6ss41rgachbl8apeftbg1j1','2017-06-21 10:54:17'),(275,NULL,'hu5oqlp2pf8q8ipt3uen7fmml5','2017-06-21 10:43:21'),(276,NULL,'uv1mmtei5udfslal4320fp8822','2017-06-21 10:45:29'),(277,NULL,'3uvgt84vpamj3pcmpvu6kibke6','2017-06-21 11:38:44'),(278,NULL,'e0ln7ar9qjnlarcgu14vepqsk7','2017-06-21 10:45:29'),(279,NULL,'8alqnlidossum0pv4c14q4f0e1','2017-06-21 11:21:01'),(280,NULL,'n0lm7evu7d96m8fdbffd59dk67','2017-06-21 11:23:59'),(281,NULL,'ri4tef0r31b0a11rcjnj6d4ri6','2017-06-22 00:35:02'),(282,NULL,'23q5ji2re4gpla4briov9jhdp5','2017-06-22 00:44:06'),(283,NULL,'87qqavpe6qo07ob59bauqh30d1','2017-06-22 01:01:19'),(284,NULL,'vrf0h51d12sp4imji97vsvc8d2','2017-06-22 01:01:29'),(285,NULL,'b51g2ku8pjek9401g61d4aag63','2017-06-22 01:01:19'),(286,NULL,'1ctqj9elm0iqoe76vcs142g6f3','2017-06-22 01:08:42'),(287,NULL,'g6jatr65r4e7gomsjvduohlk90','2017-06-22 01:44:17'),(288,NULL,'6mev4t0rgtaelrl4tpj0agkv05','2017-06-22 01:44:17'),(289,NULL,'3hc1j04lkpeaicsnlo80tpbo12','2017-06-22 01:44:18'),(290,NULL,'c2e8rdepnvqoubtimjjkc8upu2','2017-06-22 02:57:09'),(291,NULL,'3v18vbc0nfivispsfanj9990c6','2017-06-22 03:27:07'),(292,NULL,'b2bae7paebcpjvousa0sjlaf46','2017-06-22 03:17:20'),(293,NULL,'91q3r7rp3ngh9io3t58of7cjr6','2017-06-22 03:17:29'),(294,NULL,'750t4qcoecca55nlrc81p3mqu5','2017-06-22 03:49:34'),(295,NULL,'9o8tdnfadlq2m881t4jl0h1h10','2017-06-22 03:52:12'),(296,NULL,'fbrckssjda2plkagn1jlu7f9q4','2017-06-22 04:00:03'),(297,NULL,'cothp5m5808sj70ha5ao5g27h5','2017-06-22 04:00:05'),(298,NULL,'t4jro3d7n5kasafbb70bcntiu6','2017-06-22 04:00:13'),(299,NULL,'do2u6pi9j8a0kvvq3vrd1pod57','2017-06-22 04:39:27'),(300,NULL,'tcafppatjdtmd73n82ne08bfq2','2017-06-22 04:39:51'),(301,NULL,'dpcb6jg5cq89ikf8rcdqvbvvt2','2017-06-22 04:52:06'),(302,NULL,'okgp0dc5nf8df4nsiq03g2ap80','2017-06-22 04:52:26'),(303,NULL,'hb97otbbu15bcdj30ifvvgsj97','2017-06-22 04:43:39'),(304,NULL,'f0n3lgcenu98aisu020b7l3i50','2017-06-22 04:43:59'),(305,NULL,'dpcb6jg5cq89ikf8rcdqvbvvt2','2017-06-22 04:58:18'),(306,NULL,'n1o8huj4b3c360289mhfh1u285','2017-06-22 07:02:14'),(307,NULL,'g84rilq38nitu5mlggq8p8eog3','2017-06-22 06:42:53'),(308,NULL,'e9lfbr9guct6quk25i5q3lcb05','2017-06-22 06:42:53'),(309,NULL,'puu6mte4e0eqq7u4ok1ogff2n5','2017-06-22 06:42:54'),(310,NULL,'rv21bu8t2k59fg8np0s7m0g892','2017-06-22 06:47:03'),(311,NULL,'lkjkdj2k4d9gucu6t9q32n29s6','2017-06-22 06:49:16'),(312,NULL,'l3646f4v3aucfhn3ubjfnua2o1','2017-06-22 06:49:16'),(313,NULL,'okbisopi76fu1na2llb96lv1p7','2017-06-22 06:51:19'),(314,NULL,'9tcgk7gesrbfc3b0s7cdl56024','2017-06-22 07:01:25'),(315,NULL,'0pba6g6ddcl6hiatqhhinfkqp2','2017-06-22 07:07:52'),(316,NULL,'icr7vdm9tb6q13ellgkj0aj552','2017-06-22 07:29:12'),(317,NULL,'1mtt4fdav3hc171ovga47dfjp3','2017-06-22 07:29:23'),(318,NULL,'m4vce6057ecr2o5ocuccsigbi5','2017-06-22 07:49:20'),(319,NULL,'8ct9pdshf78qg7kphou42gd5k3','2017-06-22 07:57:01'),(320,NULL,'vmgl2p8jr8k3577rtrsod1lou0','2017-06-22 07:49:20'),(321,NULL,'da42ggjlgauuaueb5jqq2fg6p3','2017-06-22 07:49:20'),(322,NULL,'rtgjbnrdo3rl625j2g17vghog2','2017-06-22 08:37:07'),(323,NULL,'dm3q0pm1pv983kg3ko1u7iedq1','2017-06-22 08:37:07'),(324,NULL,'up9hc4q2p5fgbd5cmsbhevp5h0','2017-06-22 08:37:08'),(325,NULL,'4ilm2o50mobg8vdtrnubh6fak4','2017-06-22 10:39:08'),(326,NULL,'3n1i2fqp8l35vqc284b9n2svj5','2017-06-22 10:39:31'),(327,NULL,'fl8qfdl1cr3cb1cqj4725bf0h1','2017-06-22 10:39:42'),(328,NULL,'i18v92hghbin3tn80uj94egvm1','2017-06-22 10:47:46'),(329,NULL,'llthco9emf66plrb0v0vctl195','2017-06-22 10:48:10'),(330,NULL,'2n2jhdr9qj13tp9lqdf5vqn903','2017-06-23 00:57:11'),(331,NULL,'500eunlfss0f80o5g4vvoujaa4','2017-06-23 00:57:48'),(332,NULL,'g78l3po9kmeuq0cmcb19s43vg5','2017-06-23 00:57:33'),(333,NULL,'hi86274ages9nm9ea4kd6e7ue4','2017-06-23 00:58:14'),(334,NULL,'2gdakqg5a9uosucdhhaqmgrsg4','2017-06-23 00:58:15'),(335,NULL,'r2em2dr1i2iclv3sf9mu5a2nn4','2017-06-23 02:44:58'),(336,NULL,'b0h717175ntt7ed0pfsvc1b8n7','2017-06-23 02:45:38'),(337,NULL,'v3favmpqh06nkmua77ipk4sia0','2017-06-23 02:45:23'),(338,NULL,'flseh5r0af30mqfhuc3lni26f1','2017-06-23 02:52:33'),(339,NULL,'1tlckoce32qqoj4vfbborr9c56','2017-06-23 02:53:02'),(340,NULL,'ck6gupcrnaeh4reveevjohnir1','2017-06-23 03:15:00'),(341,NULL,'ck6gupcrnaeh4reveevjohnir1','2017-06-23 03:30:17'),(342,NULL,'ga3m1oe4qi0gs848q88qrsd365','2017-06-23 03:53:53'),(343,NULL,'1lrqe29ugdnll2o6f5hg3e1en6','2017-06-23 03:54:01'),(344,NULL,'5flrp10uf0ndmfratpeojgbih3','2017-06-23 03:54:04'),(345,NULL,'hn1phovldiaa5gjs4911bg5o80','2017-06-23 03:54:04'),(346,NULL,'4ssjh858bjldt1l2a16lourke4','2017-06-23 03:54:04'),(347,NULL,'2s8afm5gdv6f17895di1u0ls70','2017-06-23 03:54:04'),(348,NULL,'7g8lcc15t2uh5gsa48ad8j2fl5','2017-06-23 03:54:04'),(349,NULL,'cq4vq742cjfduiqupcclun5o82','2017-06-23 03:54:18'),(350,NULL,'gdithqdbs508ehhrlndogeujn0','2017-06-23 04:05:32'),(351,NULL,'fqkh7lrrf2io7dmoh6pbd6mvq2','2017-06-23 04:06:39'),(352,NULL,'dk5i703c70r91rsom0jdonubn6','2017-06-23 04:06:48'),(353,NULL,'iv3j3obkl2ecdfhgagpmnh9825','2017-06-23 04:06:57'),(354,NULL,'u9rd6t7mohd4p674cubsftrcp7','2017-06-23 04:49:27'),(355,NULL,'uo9hhhcalo41c9pk6q9855udd1','2017-06-23 06:08:26'),(356,NULL,'dr835fvehv29c60clu7guhr447','2017-06-23 07:00:45'),(357,NULL,'dr835fvehv29c60clu7guhr447','2017-06-23 07:49:38'),(358,NULL,'eppprp331na5v4fd79tv63spa0','2017-06-23 07:19:50'),(359,NULL,'oheku880u7a1rom7itp7nk99j2','2017-06-23 07:56:24'),(360,NULL,'16ro97a14o2mf199500e7k0vj6','2017-06-23 07:33:51'),(361,'1','aia90g3eq2kmv7phr4po279141','2017-06-23 08:18:25'),(362,NULL,'8ns4mbvivvinead43bcan9i4k2','2017-06-23 07:34:07'),(363,NULL,'3b0eajmspd1qi3vm2mkd7n9gk7','2017-06-23 07:44:46'),(364,NULL,'rum2c304q76gvfamb7i5kk1dq0','2017-06-23 07:44:59'),(365,NULL,'87krip81tqgcsumd9rcbbs6i55','2017-06-23 08:09:23'),(366,NULL,'0j84tmkrq87jdtrjpkkpbk9057','2017-06-23 08:34:23'),(367,NULL,'87krip81tqgcsumd9rcbbs6i55','2017-06-23 08:34:29'),(368,NULL,'87krip81tqgcsumd9rcbbs6i55','2017-06-23 08:36:39'),(369,NULL,'bk7hl5dfn4nvt9is7g58mufjb3','2017-06-23 08:42:17'),(370,NULL,'h65fjrbb633oh8l79ld8n1aro1','2017-06-23 08:42:25'),(371,NULL,'s78obh0rlhd3n7pt5rq16kb6c0','2017-06-23 08:42:37'),(372,NULL,'77r9cbr9lgcplenonor72nd6c3','2017-06-23 08:48:42'),(373,NULL,'nv5lf86b2a925honp8u00ampa4','2017-06-23 09:11:01'),(374,NULL,'c1deq2h8jncfpou2fjbb94jsb4','2017-06-23 08:48:50'),(375,NULL,'0j84tmkrq87jdtrjpkkpbk9057','2017-06-23 09:00:06'),(376,NULL,'nv5lf86b2a925honp8u00ampa4','2017-06-23 09:20:28'),(377,NULL,'52f5bak6ognpc3k2ek5tlfnec0','2017-06-23 09:45:16'),(378,NULL,'llkj5kqcuhqh9eis777aluouk5','2017-06-23 10:10:01'),(379,NULL,'kj4apina4rtj0qej5mqa0bctf1','2017-06-23 10:10:07'),(380,NULL,'re4giuh76hhpu66t524io1oqg6','2017-06-23 10:10:07'),(381,NULL,'0pprjq37rkt2qa7882lq1hv1a2','2017-06-23 10:10:07'),(382,NULL,'mo5gch38iu6b1rrk3cra1jpkg0','2017-06-23 15:17:01'),(383,NULL,'sh2hmd53ahk58e96bnrcoj2830','2017-06-23 15:17:38'),(384,NULL,'oqndbturoemo5tmb6omuh5k2s6','2017-06-23 15:17:09'),(385,NULL,'kvg7ss4l6tgjlkeoe83lupvkl3','2017-06-23 15:17:09'),(386,NULL,'j0g1n0l4h7oas5ssfqdvsk1rf5','2017-06-23 23:46:23'),(387,NULL,'6a5kahh1u29bmi8a9ut1hus360','2017-06-23 23:46:23'),(388,NULL,'f959q74o24slj2apn0kn3e3de5','2017-06-24 00:28:25'),(389,NULL,'6msokc3oo27vluma3gr1abakh5','2017-06-24 04:17:09'),(390,NULL,'je956tmnvr80v73g8au0rn4as3','2017-06-24 04:17:09'),(391,NULL,'a2bj4rf79i87pr4tv3e7anu2n0','2017-06-24 04:17:09'),(392,NULL,'lui3gm1qa9cjqbhqlg9vnvhr61','2017-06-24 04:17:09'),(393,NULL,'i5nu7n2j3luhdfe9mh5liao7p3','2017-06-24 04:17:18'),(394,NULL,'22fdsoc5vfjut2h44nd4rbd8c5','2017-06-24 04:17:14'),(395,NULL,'k8gphpbvtjmgohft5nu6dqocq1','2017-06-24 04:17:14'),(396,NULL,'pgdlb46m5aeoki53ipotcakk72','2017-06-24 04:36:04'),(397,NULL,'n45e02idogmh2jgddovfg65vo5','2017-06-24 04:56:08'),(398,NULL,'971bea370ihglblo3hkq31oe40','2017-06-24 05:23:28'),(399,NULL,'gs7h9t8lot85eujkaegsq0iqt6','2017-06-24 05:23:28'),(400,NULL,'hei9qrq62o7e1ldgape6d0f6s1','2017-06-24 05:24:02'),(401,NULL,'av68j69479ob4hilqpu08ki8s0','2017-06-24 06:42:37'),(402,NULL,'qbpnacfcfacsmmmf7fhm87sfg7','2017-06-24 06:44:26'),(403,NULL,'qv8rtqib6sjr7urtm1euuoses0','2017-06-24 06:44:26'),(404,NULL,'65b1ao80uuk5stbtbjhrmstgm6','2017-06-24 06:44:26'),(405,NULL,'qv8rtqib6sjr7urtm1euuoses0','2017-06-24 06:49:50'),(406,NULL,'vcg26dum24jir366cabnm30cq3','2017-06-24 06:48:29'),(407,NULL,'od5de7ut8v808q2s1rkjb0fsu6','2017-06-24 06:48:34'),(408,NULL,'vorflp261mmv9gdane4ekn4v67','2017-06-24 06:52:26'),(409,NULL,'tj8e1i729ac7ccttpggf03o0n5','2017-06-24 06:56:53'),(410,NULL,'l4sitk1fjgph0jeh0o4j4rerh2','2017-06-24 06:59:59'),(411,NULL,'jpf846bqg3lo92r38f9ldftkc3','2017-06-24 07:04:20'),(412,NULL,'1nj5nlpjgqs20jqvej978ja9g1','2017-06-24 07:06:54'),(413,NULL,'in27sviu0veqo2d50u9s1gc5r7','2017-06-24 07:11:03'),(414,NULL,'fohjpre7lfcoehg55emu13rmk7','2017-06-24 07:12:43'),(415,NULL,'6mfhk0ndlu21l0i8jmp07qur36','2017-06-24 07:15:11'),(416,NULL,'k2kp6mm1d2t0tts3p9doeobks0','2017-06-24 07:23:00'),(417,NULL,'uhjgg4685cjaqt0nnpijl90115','2017-06-24 07:21:52'),(418,NULL,'uo2msiu7acp29ft9u4el6lu2k3','2017-06-24 07:20:11'),(419,NULL,'gdu412acoc9ptqk2nqr39g0vo3','2017-06-24 08:19:27'),(420,NULL,'a4d8ahkoud15tq2l8ege4i8tu0','2017-06-24 08:20:50'),(421,NULL,'o9blgqk7a3kgded5u34slad8h0','2017-06-24 08:45:49'),(422,NULL,'omfn6e9s943h3f31vnig6qam84','2017-06-24 08:45:49'),(423,NULL,'u4kbbhlv0h6odcrh7fjgr4m2r2','2017-06-24 08:45:49'),(424,NULL,'c2ccleu742f820mipr73jd2mi3','2017-06-24 08:45:49'),(425,NULL,'h59m99fam1kosfv4f558m74rn3','2017-06-24 08:59:08'),(426,NULL,'c1k9bfci8hcnn0hnrfcogcfs20','2017-06-24 08:59:08'),(427,NULL,'i24r1egd6goeqf861pm27pres0','2017-06-24 08:59:08'),(428,NULL,'lnklk4iatu7lbfgtipmnosk102','2017-06-24 08:59:08'),(429,NULL,'3hjmfqu5aj142go998vnm73fg6','2017-06-24 09:21:08'),(430,NULL,'bl1i5m4nhtdqd6qrgpvn65nne1','2017-06-24 10:01:51');
/*!40000 ALTER TABLE `customer_visitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design_change`
--

DROP TABLE IF EXISTS `design_change`;
CREATE TABLE `design_change` (
  `design_change_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Design Change Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `design` varchar(255) DEFAULT NULL COMMENT 'Design',
  `date_from` date DEFAULT NULL COMMENT 'First Date of Design Activity',
  `date_to` date DEFAULT NULL COMMENT 'Last Date of Design Activity',
  PRIMARY KEY (`design_change_id`),
  KEY `DESIGN_CHANGE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Design Changes';

--
-- Table structure for table `design_config_grid_flat`
--

DROP TABLE IF EXISTS `design_config_grid_flat`;
CREATE TABLE `design_config_grid_flat` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `store_website_id` int(11) DEFAULT NULL COMMENT 'Store_website_id',
  `store_group_id` int(11) DEFAULT NULL COMMENT 'Store_group_id',
  `store_id` int(11) DEFAULT NULL COMMENT 'Store_id',
  `theme_theme_id` varchar(255) DEFAULT NULL COMMENT 'Theme_theme_id',
  PRIMARY KEY (`entity_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_WEBSITE_ID` (`store_website_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_GROUP_ID` (`store_group_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_ID` (`store_id`),
  FULLTEXT KEY `DESIGN_CONFIG_GRID_FLAT_THEME_THEME_ID` (`theme_theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='design_config_grid_flat';

--
-- Dumping data for table `design_config_grid_flat`
--

LOCK TABLES `design_config_grid_flat` WRITE;
/*!40000 ALTER TABLE `design_config_grid_flat` DISABLE KEYS */;
INSERT INTO `design_config_grid_flat` VALUES ('0',NULL,NULL,NULL,''),('1','1',NULL,NULL,''),('2','1','1','1','5'),('3','1','1','2','5');
/*!40000 ALTER TABLE `design_config_grid_flat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country`
--

DROP TABLE IF EXISTS `directory_country`;
CREATE TABLE `directory_country` (
  `country_id` varchar(2) NOT NULL COMMENT 'Country Id in ISO-2',
  `iso2_code` varchar(2) DEFAULT NULL COMMENT 'Country ISO-2 format',
  `iso3_code` varchar(3) DEFAULT NULL COMMENT 'Country ISO-3',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country';

--
-- Dumping data for table `directory_country`
--

LOCK TABLES `directory_country` WRITE;
/*!40000 ALTER TABLE `directory_country` DISABLE KEYS */;
INSERT INTO `directory_country` VALUES ('AD','AD','AND'),('AE','AE','ARE'),('AF','AF','AFG'),('AG','AG','ATG'),('AI','AI','AIA'),('AL','AL','ALB'),('AM','AM','ARM'),('AN','AN','ANT'),('AO','AO','AGO'),('AQ','AQ','ATA'),('AR','AR','ARG'),('AS','AS','ASM'),('AT','AT','AUT'),('AU','AU','AUS'),('AW','AW','ABW'),('AX','AX','ALA'),('AZ','AZ','AZE'),('BA','BA','BIH'),('BB','BB','BRB'),('BD','BD','BGD'),('BE','BE','BEL'),('BF','BF','BFA'),('BG','BG','BGR'),('BH','BH','BHR'),('BI','BI','BDI'),('BJ','BJ','BEN'),('BL','BL','BLM'),('BM','BM','BMU'),('BN','BN','BRN'),('BO','BO','BOL'),('BR','BR','BRA'),('BS','BS','BHS'),('BT','BT','BTN'),('BV','BV','BVT'),('BW','BW','BWA'),('BY','BY','BLR'),('BZ','BZ','BLZ'),('CA','CA','CAN'),('CC','CC','CCK'),('CD','CD','COD'),('CF','CF','CAF'),('CG','CG','COG'),('CH','CH','CHE'),('CI','CI','CIV'),('CK','CK','COK'),('CL','CL','CHL'),('CM','CM','CMR'),('CN','CN','CHN'),('CO','CO','COL'),('CR','CR','CRI'),('CU','CU','CUB'),('CV','CV','CPV'),('CX','CX','CXR'),('CY','CY','CYP'),('CZ','CZ','CZE'),('DE','DE','DEU'),('DJ','DJ','DJI'),('DK','DK','DNK'),('DM','DM','DMA'),('DO','DO','DOM'),('DZ','DZ','DZA'),('EC','EC','ECU'),('EE','EE','EST'),('EG','EG','EGY'),('EH','EH','ESH'),('ER','ER','ERI'),('ES','ES','ESP'),('ET','ET','ETH'),('FI','FI','FIN'),('FJ','FJ','FJI'),('FK','FK','FLK'),('FM','FM','FSM'),('FO','FO','FRO'),('FR','FR','FRA'),('GA','GA','GAB'),('GB','GB','GBR'),('GD','GD','GRD'),('GE','GE','GEO'),('GF','GF','GUF'),('GG','GG','GGY'),('GH','GH','GHA'),('GI','GI','GIB'),('GL','GL','GRL'),('GM','GM','GMB'),('GN','GN','GIN'),('GP','GP','GLP'),('GQ','GQ','GNQ'),('GR','GR','GRC'),('GS','GS','SGS'),('GT','GT','GTM'),('GU','GU','GUM'),('GW','GW','GNB'),('GY','GY','GUY'),('HK','HK','HKG'),('HM','HM','HMD'),('HN','HN','HND'),('HR','HR','HRV'),('HT','HT','HTI'),('HU','HU','HUN'),('ID','ID','IDN'),('IE','IE','IRL'),('IL','IL','ISR'),('IM','IM','IMN'),('IN','IN','IND'),('IO','IO','IOT'),('IQ','IQ','IRQ'),('IR','IR','IRN'),('IS','IS','ISL'),('IT','IT','ITA'),('JE','JE','JEY'),('JM','JM','JAM'),('JO','JO','JOR'),('JP','JP','JPN'),('KE','KE','KEN'),('KG','KG','KGZ'),('KH','KH','KHM'),('KI','KI','KIR'),('KM','KM','COM'),('KN','KN','KNA'),('KP','KP','PRK'),('KR','KR','KOR'),('KW','KW','KWT'),('KY','KY','CYM'),('KZ','KZ','KAZ'),('LA','LA','LAO'),('LB','LB','LBN'),('LC','LC','LCA'),('LI','LI','LIE'),('LK','LK','LKA'),('LR','LR','LBR'),('LS','LS','LSO'),('LT','LT','LTU'),('LU','LU','LUX'),('LV','LV','LVA'),('LY','LY','LBY'),('MA','MA','MAR'),('MC','MC','MCO'),('MD','MD','MDA'),('ME','ME','MNE'),('MF','MF','MAF'),('MG','MG','MDG'),('MH','MH','MHL'),('MK','MK','MKD'),('ML','ML','MLI'),('MM','MM','MMR'),('MN','MN','MNG'),('MO','MO','MAC'),('MP','MP','MNP'),('MQ','MQ','MTQ'),('MR','MR','MRT'),('MS','MS','MSR'),('MT','MT','MLT'),('MU','MU','MUS'),('MV','MV','MDV'),('MW','MW','MWI'),('MX','MX','MEX'),('MY','MY','MYS'),('MZ','MZ','MOZ'),('NA','NA','NAM'),('NC','NC','NCL'),('NE','NE','NER'),('NF','NF','NFK'),('NG','NG','NGA'),('NI','NI','NIC'),('NL','NL','NLD'),('NO','NO','NOR'),('NP','NP','NPL'),('NR','NR','NRU'),('NU','NU','NIU'),('NZ','NZ','NZL'),('OM','OM','OMN'),('PA','PA','PAN'),('PE','PE','PER'),('PF','PF','PYF'),('PG','PG','PNG'),('PH','PH','PHL'),('PK','PK','PAK'),('PL','PL','POL'),('PM','PM','SPM'),('PN','PN','PCN'),('PS','PS','PSE'),('PT','PT','PRT'),('PW','PW','PLW'),('PY','PY','PRY'),('QA','QA','QAT'),('RE','RE','REU'),('RO','RO','ROU'),('RS','RS','SRB'),('RU','RU','RUS'),('RW','RW','RWA'),('SA','SA','SAU'),('SB','SB','SLB'),('SC','SC','SYC'),('SD','SD','SDN'),('SE','SE','SWE'),('SG','SG','SGP'),('SH','SH','SHN'),('SI','SI','SVN'),('SJ','SJ','SJM'),('SK','SK','SVK'),('SL','SL','SLE'),('SM','SM','SMR'),('SN','SN','SEN'),('SO','SO','SOM'),('SR','SR','SUR'),('ST','ST','STP'),('SV','SV','SLV'),('SY','SY','SYR'),('SZ','SZ','SWZ'),('TC','TC','TCA'),('TD','TD','TCD'),('TF','TF','ATF'),('TG','TG','TGO'),('TH','TH','THA'),('TJ','TJ','TJK'),('TK','TK','TKL'),('TL','TL','TLS'),('TM','TM','TKM'),('TN','TN','TUN'),('TO','TO','TON'),('TR','TR','TUR'),('TT','TT','TTO'),('TV','TV','TUV'),('TW','TW','TWN'),('TZ','TZ','TZA'),('UA','UA','UKR'),('UG','UG','UGA'),('UM','UM','UMI'),('US','US','USA'),('UY','UY','URY'),('UZ','UZ','UZB'),('VA','VA','VAT'),('VC','VC','VCT'),('VE','VE','VEN'),('VG','VG','VGB'),('VI','VI','VIR'),('VN','VN','VNM'),('VU','VU','VUT'),('WF','WF','WLF'),('WS','WS','WSM'),('YE','YE','YEM'),('YT','YT','MYT'),('ZA','ZA','ZAF'),('ZM','ZM','ZMB'),('ZW','ZW','ZWE');
/*!40000 ALTER TABLE `directory_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country_format`
--

DROP TABLE IF EXISTS `directory_country_format`;
CREATE TABLE `directory_country_format` (
  `country_format_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Country Format Id',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id in ISO-2',
  `type` varchar(30) DEFAULT NULL COMMENT 'Country Format Type',
  `format` text NOT NULL COMMENT 'Country Format',
  PRIMARY KEY (`country_format_id`),
  UNIQUE KEY `DIRECTORY_COUNTRY_FORMAT_COUNTRY_ID_TYPE` (`country_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Format';

--
-- Table structure for table `directory_country_region`
--

DROP TABLE IF EXISTS `directory_country_region`;
CREATE TABLE `directory_country_region` (
  `region_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Region Id',
  `country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Country Id in ISO-2',
  `code` varchar(32) DEFAULT NULL COMMENT 'Region code',
  `default_name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`region_id`),
  KEY `DIRECTORY_COUNTRY_REGION_COUNTRY_ID` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8 COMMENT='Directory Country Region';

--
-- Dumping data for table `directory_country_region`
--

LOCK TABLES `directory_country_region` WRITE;
/*!40000 ALTER TABLE `directory_country_region` DISABLE KEYS */;
INSERT INTO `directory_country_region` VALUES ('1','US','AL','Alabama'),('2','US','AK','Alaska'),('3','US','AS','American Samoa'),('4','US','AZ','Arizona'),('5','US','AR','Arkansas'),('6','US','AE','Armed Forces Africa'),('7','US','AA','Armed Forces Americas'),('8','US','AE','Armed Forces Canada'),('9','US','AE','Armed Forces Europe'),('10','US','AE','Armed Forces Middle East'),('11','US','AP','Armed Forces Pacific'),('12','US','CA','California'),('13','US','CO','Colorado'),('14','US','CT','Connecticut'),('15','US','DE','Delaware'),('16','US','DC','District of Columbia'),('17','US','FM','Federated States Of Micronesia'),('18','US','FL','Florida'),('19','US','GA','Georgia'),('20','US','GU','Guam'),('21','US','HI','Hawaii'),('22','US','ID','Idaho'),('23','US','IL','Illinois'),('24','US','IN','Indiana'),('25','US','IA','Iowa'),('26','US','KS','Kansas'),('27','US','KY','Kentucky'),('28','US','LA','Louisiana'),('29','US','ME','Maine'),('30','US','MH','Marshall Islands'),('31','US','MD','Maryland'),('32','US','MA','Massachusetts'),('33','US','MI','Michigan'),('34','US','MN','Minnesota'),('35','US','MS','Mississippi'),('36','US','MO','Missouri'),('37','US','MT','Montana'),('38','US','NE','Nebraska'),('39','US','NV','Nevada'),('40','US','NH','New Hampshire'),('41','US','NJ','New Jersey'),('42','US','NM','New Mexico'),('43','US','NY','New York'),('44','US','NC','North Carolina'),('45','US','ND','North Dakota'),('46','US','MP','Northern Mariana Islands'),('47','US','OH','Ohio'),('48','US','OK','Oklahoma'),('49','US','OR','Oregon'),('50','US','PW','Palau'),('51','US','PA','Pennsylvania'),('52','US','PR','Puerto Rico'),('53','US','RI','Rhode Island'),('54','US','SC','South Carolina'),('55','US','SD','South Dakota'),('56','US','TN','Tennessee'),('57','US','TX','Texas'),('58','US','UT','Utah'),('59','US','VT','Vermont'),('60','US','VI','Virgin Islands'),('61','US','VA','Virginia'),('62','US','WA','Washington'),('63','US','WV','West Virginia'),('64','US','WI','Wisconsin'),('65','US','WY','Wyoming'),('66','CA','AB','Alberta'),('67','CA','BC','British Columbia'),('68','CA','MB','Manitoba'),('69','CA','NL','Newfoundland and Labrador'),('70','CA','NB','New Brunswick'),('71','CA','NS','Nova Scotia'),('72','CA','NT','Northwest Territories'),('73','CA','NU','Nunavut'),('74','CA','ON','Ontario'),('75','CA','PE','Prince Edward Island'),('76','CA','QC','Quebec'),('77','CA','SK','Saskatchewan'),('78','CA','YT','Yukon Territory'),('79','DE','NDS','Niedersachsen'),('80','DE','BAW','Baden-Württemberg'),('81','DE','BAY','Bayern'),('82','DE','BER','Berlin'),('83','DE','BRG','Brandenburg'),('84','DE','BRE','Bremen'),('85','DE','HAM','Hamburg'),('86','DE','HES','Hessen'),('87','DE','MEC','Mecklenburg-Vorpommern'),('88','DE','NRW','Nordrhein-Westfalen'),('89','DE','RHE','Rheinland-Pfalz'),('90','DE','SAR','Saarland'),('91','DE','SAS','Sachsen'),('92','DE','SAC','Sachsen-Anhalt'),('93','DE','SCN','Schleswig-Holstein'),('94','DE','THE','Thüringen'),('95','AT','WI','Wien'),('96','AT','NO','Niederösterreich'),('97','AT','OO','Oberösterreich'),('98','AT','SB','Salzburg'),('99','AT','KN','Kärnten'),('100','AT','ST','Steiermark'),('101','AT','TI','Tirol'),('102','AT','BL','Burgenland'),('103','AT','VB','Vorarlberg'),('104','CH','AG','Aargau'),('105','CH','AI','Appenzell Innerrhoden'),('106','CH','AR','Appenzell Ausserrhoden'),('107','CH','BE','Bern'),('108','CH','BL','Basel-Landschaft'),('109','CH','BS','Basel-Stadt'),('110','CH','FR','Freiburg'),('111','CH','GE','Genf'),('112','CH','GL','Glarus'),('113','CH','GR','Graubünden'),('114','CH','JU','Jura'),('115','CH','LU','Luzern'),('116','CH','NE','Neuenburg'),('117','CH','NW','Nidwalden'),('118','CH','OW','Obwalden'),('119','CH','SG','St. Gallen'),('120','CH','SH','Schaffhausen'),('121','CH','SO','Solothurn'),('122','CH','SZ','Schwyz'),('123','CH','TG','Thurgau'),('124','CH','TI','Tessin'),('125','CH','UR','Uri'),('126','CH','VD','Waadt'),('127','CH','VS','Wallis'),('128','CH','ZG','Zug'),('129','CH','ZH','Zürich'),('130','ES','A Coruсa','A Coruña'),('131','ES','Alava','Alava'),('132','ES','Albacete','Albacete'),('133','ES','Alicante','Alicante'),('134','ES','Almeria','Almeria'),('135','ES','Asturias','Asturias'),('136','ES','Avila','Avila'),('137','ES','Badajoz','Badajoz'),('138','ES','Baleares','Baleares'),('139','ES','Barcelona','Barcelona'),('140','ES','Burgos','Burgos'),('141','ES','Caceres','Caceres'),('142','ES','Cadiz','Cadiz'),('143','ES','Cantabria','Cantabria'),('144','ES','Castellon','Castellon'),('145','ES','Ceuta','Ceuta'),('146','ES','Ciudad Real','Ciudad Real'),('147','ES','Cordoba','Cordoba'),('148','ES','Cuenca','Cuenca'),('149','ES','Girona','Girona'),('150','ES','Granada','Granada'),('151','ES','Guadalajara','Guadalajara'),('152','ES','Guipuzcoa','Guipuzcoa'),('153','ES','Huelva','Huelva'),('154','ES','Huesca','Huesca'),('155','ES','Jaen','Jaen'),('156','ES','La Rioja','La Rioja'),('157','ES','Las Palmas','Las Palmas'),('158','ES','Leon','Leon'),('159','ES','Lleida','Lleida'),('160','ES','Lugo','Lugo'),('161','ES','Madrid','Madrid'),('162','ES','Malaga','Malaga'),('163','ES','Melilla','Melilla'),('164','ES','Murcia','Murcia'),('165','ES','Navarra','Navarra'),('166','ES','Ourense','Ourense'),('167','ES','Palencia','Palencia'),('168','ES','Pontevedra','Pontevedra'),('169','ES','Salamanca','Salamanca'),('170','ES','Santa Cruz de Tenerife','Santa Cruz de Tenerife'),('171','ES','Segovia','Segovia'),('172','ES','Sevilla','Sevilla'),('173','ES','Soria','Soria'),('174','ES','Tarragona','Tarragona'),('175','ES','Teruel','Teruel'),('176','ES','Toledo','Toledo'),('177','ES','Valencia','Valencia'),('178','ES','Valladolid','Valladolid'),('179','ES','Vizcaya','Vizcaya'),('180','ES','Zamora','Zamora'),('181','ES','Zaragoza','Zaragoza'),('182','FR','1','Ain'),('183','FR','2','Aisne'),('184','FR','3','Allier'),('185','FR','4','Alpes-de-Haute-Provence'),('186','FR','5','Hautes-Alpes'),('187','FR','6','Alpes-Maritimes'),('188','FR','7','Ardèche'),('189','FR','8','Ardennes'),('190','FR','9','Ariège'),('191','FR','10','Aube'),('192','FR','11','Aude'),('193','FR','12','Aveyron'),('194','FR','13','Bouches-du-Rhône'),('195','FR','14','Calvados'),('196','FR','15','Cantal'),('197','FR','16','Charente'),('198','FR','17','Charente-Maritime'),('199','FR','18','Cher'),('200','FR','19','Corrèze'),('201','FR','2A','Corse-du-Sud'),('202','FR','2B','Haute-Corse'),('203','FR','21','Côte-d\'Or'),('204','FR','22','Côtes-d\'Armor'),('205','FR','23','Creuse'),('206','FR','24','Dordogne'),('207','FR','25','Doubs'),('208','FR','26','Drôme'),('209','FR','27','Eure'),('210','FR','28','Eure-et-Loir'),('211','FR','29','Finistère'),('212','FR','30','Gard'),('213','FR','31','Haute-Garonne'),('214','FR','32','Gers'),('215','FR','33','Gironde'),('216','FR','34','Hérault'),('217','FR','35','Ille-et-Vilaine'),('218','FR','36','Indre'),('219','FR','37','Indre-et-Loire'),('220','FR','38','Isère'),('221','FR','39','Jura'),('222','FR','40','Landes'),('223','FR','41','Loir-et-Cher'),('224','FR','42','Loire'),('225','FR','43','Haute-Loire'),('226','FR','44','Loire-Atlantique'),('227','FR','45','Loiret'),('228','FR','46','Lot'),('229','FR','47','Lot-et-Garonne'),('230','FR','48','Lozère'),('231','FR','49','Maine-et-Loire'),('232','FR','50','Manche'),('233','FR','51','Marne'),('234','FR','52','Haute-Marne'),('235','FR','53','Mayenne'),('236','FR','54','Meurthe-et-Moselle'),('237','FR','55','Meuse'),('238','FR','56','Morbihan'),('239','FR','57','Moselle'),('240','FR','58','Nièvre'),('241','FR','59','Nord'),('242','FR','60','Oise'),('243','FR','61','Orne'),('244','FR','62','Pas-de-Calais'),('245','FR','63','Puy-de-Dôme'),('246','FR','64','Pyrénées-Atlantiques'),('247','FR','65','Hautes-Pyrénées'),('248','FR','66','Pyrénées-Orientales'),('249','FR','67','Bas-Rhin'),('250','FR','68','Haut-Rhin'),('251','FR','69','Rhône'),('252','FR','70','Haute-Saône'),('253','FR','71','Saône-et-Loire'),('254','FR','72','Sarthe'),('255','FR','73','Savoie'),('256','FR','74','Haute-Savoie'),('257','FR','75','Paris'),('258','FR','76','Seine-Maritime'),('259','FR','77','Seine-et-Marne'),('260','FR','78','Yvelines'),('261','FR','79','Deux-Sèvres'),('262','FR','80','Somme'),('263','FR','81','Tarn'),('264','FR','82','Tarn-et-Garonne'),('265','FR','83','Var'),('266','FR','84','Vaucluse'),('267','FR','85','Vendée'),('268','FR','86','Vienne'),('269','FR','87','Haute-Vienne'),('270','FR','88','Vosges'),('271','FR','89','Yonne'),('272','FR','90','Territoire-de-Belfort'),('273','FR','91','Essonne'),('274','FR','92','Hauts-de-Seine'),('275','FR','93','Seine-Saint-Denis'),('276','FR','94','Val-de-Marne'),('277','FR','95','Val-d\'Oise'),('278','RO','AB','Alba'),('279','RO','AR','Arad'),('280','RO','AG','Argeş'),('281','RO','BC','Bacău'),('282','RO','BH','Bihor'),('283','RO','BN','Bistriţa-Năsăud'),('284','RO','BT','Botoşani'),('285','RO','BV','Braşov'),('286','RO','BR','Brăila'),('287','RO','B','Bucureşti'),('288','RO','BZ','Buzău'),('289','RO','CS','Caraş-Severin'),('290','RO','CL','Călăraşi'),('291','RO','CJ','Cluj'),('292','RO','CT','Constanţa'),('293','RO','CV','Covasna'),('294','RO','DB','Dâmboviţa'),('295','RO','DJ','Dolj'),('296','RO','GL','Galaţi'),('297','RO','GR','Giurgiu'),('298','RO','GJ','Gorj'),('299','RO','HR','Harghita'),('300','RO','HD','Hunedoara'),('301','RO','IL','Ialomiţa'),('302','RO','IS','Iaşi'),('303','RO','IF','Ilfov'),('304','RO','MM','Maramureş'),('305','RO','MH','Mehedinţi'),('306','RO','MS','Mureş'),('307','RO','NT','Neamţ'),('308','RO','OT','Olt'),('309','RO','PH','Prahova'),('310','RO','SM','Satu-Mare'),('311','RO','SJ','Sălaj'),('312','RO','SB','Sibiu'),('313','RO','SV','Suceava'),('314','RO','TR','Teleorman'),('315','RO','TM','Timiş'),('316','RO','TL','Tulcea'),('317','RO','VS','Vaslui'),('318','RO','VL','Vâlcea'),('319','RO','VN','Vrancea'),('320','FI','Lappi','Lappi'),('321','FI','Pohjois-Pohjanmaa','Pohjois-Pohjanmaa'),('322','FI','Kainuu','Kainuu'),('323','FI','Pohjois-Karjala','Pohjois-Karjala'),('324','FI','Pohjois-Savo','Pohjois-Savo'),('325','FI','Etelä-Savo','Etelä-Savo'),('326','FI','Etelä-Pohjanmaa','Etelä-Pohjanmaa'),('327','FI','Pohjanmaa','Pohjanmaa'),('328','FI','Pirkanmaa','Pirkanmaa'),('329','FI','Satakunta','Satakunta'),('330','FI','Keski-Pohjanmaa','Keski-Pohjanmaa'),('331','FI','Keski-Suomi','Keski-Suomi'),('332','FI','Varsinais-Suomi','Varsinais-Suomi'),('333','FI','Etelä-Karjala','Etelä-Karjala'),('334','FI','Päijät-Häme','Päijät-Häme'),('335','FI','Kanta-Häme','Kanta-Häme'),('336','FI','Uusimaa','Uusimaa'),('337','FI','Itä-Uusimaa','Itä-Uusimaa'),('338','FI','Kymenlaakso','Kymenlaakso'),('339','FI','Ahvenanmaa','Ahvenanmaa'),('340','EE','EE-37','Harjumaa'),('341','EE','EE-39','Hiiumaa'),('342','EE','EE-44','Ida-Virumaa'),('343','EE','EE-49','Jõgevamaa'),('344','EE','EE-51','Järvamaa'),('345','EE','EE-57','Läänemaa'),('346','EE','EE-59','Lääne-Virumaa'),('347','EE','EE-65','Põlvamaa'),('348','EE','EE-67','Pärnumaa'),('349','EE','EE-70','Raplamaa'),('350','EE','EE-74','Saaremaa'),('351','EE','EE-78','Tartumaa'),('352','EE','EE-82','Valgamaa'),('353','EE','EE-84','Viljandimaa'),('354','EE','EE-86','Võrumaa'),('355','LV','LV-DGV','Daugavpils'),('356','LV','LV-JEL','Jelgava'),('357','LV','Jēkabpils','Jēkabpils'),('358','LV','LV-JUR','Jūrmala'),('359','LV','LV-LPX','Liepāja'),('360','LV','LV-LE','Liepājas novads'),('361','LV','LV-REZ','Rēzekne'),('362','LV','LV-RIX','Rīga'),('363','LV','LV-RI','Rīgas novads'),('364','LV','Valmiera','Valmiera'),('365','LV','LV-VEN','Ventspils'),('366','LV','Aglonas novads','Aglonas novads'),('367','LV','LV-AI','Aizkraukles novads'),('368','LV','Aizputes novads','Aizputes novads'),('369','LV','Aknīstes novads','Aknīstes novads'),('370','LV','Alojas novads','Alojas novads'),('371','LV','Alsungas novads','Alsungas novads'),('372','LV','LV-AL','Alūksnes novads'),('373','LV','Amatas novads','Amatas novads'),('374','LV','Apes novads','Apes novads'),('375','LV','Auces novads','Auces novads'),('376','LV','Babītes novads','Babītes novads'),('377','LV','Baldones novads','Baldones novads'),('378','LV','Baltinavas novads','Baltinavas novads'),('379','LV','LV-BL','Balvu novads'),('380','LV','LV-BU','Bauskas novads'),('381','LV','Beverīnas novads','Beverīnas novads'),('382','LV','Brocēnu novads','Brocēnu novads'),('383','LV','Burtnieku novads','Burtnieku novads'),('384','LV','Carnikavas novads','Carnikavas novads'),('385','LV','Cesvaines novads','Cesvaines novads'),('386','LV','Ciblas novads','Ciblas novads'),('387','LV','LV-CE','Cēsu novads'),('388','LV','Dagdas novads','Dagdas novads'),('389','LV','LV-DA','Daugavpils novads'),('390','LV','LV-DO','Dobeles novads'),('391','LV','Dundagas novads','Dundagas novads'),('392','LV','Durbes novads','Durbes novads'),('393','LV','Engures novads','Engures novads'),('394','LV','Garkalnes novads','Garkalnes novads'),('395','LV','Grobiņas novads','Grobiņas novads'),('396','LV','LV-GU','Gulbenes novads'),('397','LV','Iecavas novads','Iecavas novads'),('398','LV','Ikšķiles novads','Ikšķiles novads'),('399','LV','Ilūkstes novads','Ilūkstes novads'),('400','LV','Inčukalna novads','Inčukalna novads'),('401','LV','Jaunjelgavas novads','Jaunjelgavas novads'),('402','LV','Jaunpiebalgas novads','Jaunpiebalgas novads'),('403','LV','Jaunpils novads','Jaunpils novads'),('404','LV','LV-JL','Jelgavas novads'),('405','LV','LV-JK','Jēkabpils novads'),('406','LV','Kandavas novads','Kandavas novads'),('407','LV','Kokneses novads','Kokneses novads'),('408','LV','Krimuldas novads','Krimuldas novads'),('409','LV','Krustpils novads','Krustpils novads'),('410','LV','LV-KR','Krāslavas novads'),('411','LV','LV-KU','Kuldīgas novads'),('412','LV','Kārsavas novads','Kārsavas novads'),('413','LV','Lielvārdes novads','Lielvārdes novads'),('414','LV','LV-LM','Limbažu novads'),('415','LV','Lubānas novads','Lubānas novads'),('416','LV','LV-LU','Ludzas novads'),('417','LV','Līgatnes novads','Līgatnes novads'),('418','LV','Līvānu novads','Līvānu novads'),('419','LV','LV-MA','Madonas novads'),('420','LV','Mazsalacas novads','Mazsalacas novads'),('421','LV','Mālpils novads','Mālpils novads'),('422','LV','Mārupes novads','Mārupes novads'),('423','LV','Naukšēnu novads','Naukšēnu novads'),('424','LV','Neretas novads','Neretas novads'),('425','LV','Nīcas novads','Nīcas novads'),('426','LV','LV-OG','Ogres novads'),('427','LV','Olaines novads','Olaines novads'),('428','LV','Ozolnieku novads','Ozolnieku novads'),('429','LV','LV-PR','Preiļu novads'),('430','LV','Priekules novads','Priekules novads'),('431','LV','Priekuļu novads','Priekuļu novads'),('432','LV','Pārgaujas novads','Pārgaujas novads'),('433','LV','Pāvilostas novads','Pāvilostas novads'),('434','LV','Pļaviņu novads','Pļaviņu novads'),('435','LV','Raunas novads','Raunas novads'),('436','LV','Riebiņu novads','Riebiņu novads'),('437','LV','Rojas novads','Rojas novads'),('438','LV','Ropažu novads','Ropažu novads'),('439','LV','Rucavas novads','Rucavas novads'),('440','LV','Rugāju novads','Rugāju novads'),('441','LV','Rundāles novads','Rundāles novads'),('442','LV','LV-RE','Rēzeknes novads'),('443','LV','Rūjienas novads','Rūjienas novads'),('444','LV','Salacgrīvas novads','Salacgrīvas novads'),('445','LV','Salas novads','Salas novads'),('446','LV','Salaspils novads','Salaspils novads'),('447','LV','LV-SA','Saldus novads'),('448','LV','Saulkrastu novads','Saulkrastu novads'),('449','LV','Siguldas novads','Siguldas novads'),('450','LV','Skrundas novads','Skrundas novads'),('451','LV','Skrīveru novads','Skrīveru novads'),('452','LV','Smiltenes novads','Smiltenes novads'),('453','LV','Stopiņu novads','Stopiņu novads'),('454','LV','Strenču novads','Strenču novads'),('455','LV','Sējas novads','Sējas novads'),('456','LV','LV-TA','Talsu novads'),('457','LV','LV-TU','Tukuma novads'),('458','LV','Tērvetes novads','Tērvetes novads'),('459','LV','Vaiņodes novads','Vaiņodes novads'),('460','LV','LV-VK','Valkas novads'),('461','LV','LV-VM','Valmieras novads'),('462','LV','Varakļānu novads','Varakļānu novads'),('463','LV','Vecpiebalgas novads','Vecpiebalgas novads'),('464','LV','Vecumnieku novads','Vecumnieku novads'),('465','LV','LV-VE','Ventspils novads'),('466','LV','Viesītes novads','Viesītes novads'),('467','LV','Viļakas novads','Viļakas novads'),('468','LV','Viļānu novads','Viļānu novads'),('469','LV','Vārkavas novads','Vārkavas novads'),('470','LV','Zilupes novads','Zilupes novads'),('471','LV','Ādažu novads','Ādažu novads'),('472','LV','Ērgļu novads','Ērgļu novads'),('473','LV','Ķeguma novads','Ķeguma novads'),('474','LV','Ķekavas novads','Ķekavas novads'),('475','LT','LT-AL','Alytaus Apskritis'),('476','LT','LT-KU','Kauno Apskritis'),('477','LT','LT-KL','Klaipėdos Apskritis'),('478','LT','LT-MR','Marijampolės Apskritis'),('479','LT','LT-PN','Panevėžio Apskritis'),('480','LT','LT-SA','Šiaulių Apskritis'),('481','LT','LT-TA','Tauragės Apskritis'),('482','LT','LT-TE','Telšių Apskritis'),('483','LT','LT-UT','Utenos Apskritis'),('484','LT','LT-VL','Vilniaus Apskritis'),('485','BR','AC','Acre'),('486','BR','AL','Alagoas'),('487','BR','AP','Amapá'),('488','BR','AM','Amazonas'),('489','BR','BA','Bahia'),('490','BR','CE','Ceará'),('491','BR','ES','Espírito Santo'),('492','BR','GO','Goiás'),('493','BR','MA','Maranhão'),('494','BR','MT','Mato Grosso'),('495','BR','MS','Mato Grosso do Sul'),('496','BR','MG','Minas Gerais'),('497','BR','PA','Pará'),('498','BR','PB','Paraíba'),('499','BR','PR','Paraná'),('500','BR','PE','Pernambuco'),('501','BR','PI','Piauí'),('502','BR','RJ','Rio de Janeiro'),('503','BR','RN','Rio Grande do Norte'),('504','BR','RS','Rio Grande do Sul'),('505','BR','RO','Rondônia'),('506','BR','RR','Roraima'),('507','BR','SC','Santa Catarina'),('508','BR','SP','São Paulo'),('509','BR','SE','Sergipe'),('510','BR','TO','Tocantins'),('511','BR','DF','Distrito Federal');
/*!40000 ALTER TABLE `directory_country_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country_region_name`
--

DROP TABLE IF EXISTS `directory_country_region_name`;
CREATE TABLE `directory_country_region_name` (
  `locale` varchar(8) NOT NULL COMMENT 'Locale',
  `region_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Region Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`locale`,`region_id`),
  KEY `DIRECTORY_COUNTRY_REGION_NAME_REGION_ID` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Region Name';

--
-- Dumping data for table `directory_country_region_name`
--

LOCK TABLES `directory_country_region_name` WRITE;
/*!40000 ALTER TABLE `directory_country_region_name` DISABLE KEYS */;
INSERT INTO `directory_country_region_name` VALUES ('en_US','1','Alabama'),('en_US','2','Alaska'),('en_US','3','American Samoa'),('en_US','4','Arizona'),('en_US','5','Arkansas'),('en_US','6','Armed Forces Africa'),('en_US','7','Armed Forces Americas'),('en_US','8','Armed Forces Canada'),('en_US','9','Armed Forces Europe'),('en_US','10','Armed Forces Middle East'),('en_US','11','Armed Forces Pacific'),('en_US','12','California'),('en_US','13','Colorado'),('en_US','14','Connecticut'),('en_US','15','Delaware'),('en_US','16','District of Columbia'),('en_US','17','Federated States Of Micronesia'),('en_US','18','Florida'),('en_US','19','Georgia'),('en_US','20','Guam'),('en_US','21','Hawaii'),('en_US','22','Idaho'),('en_US','23','Illinois'),('en_US','24','Indiana'),('en_US','25','Iowa'),('en_US','26','Kansas'),('en_US','27','Kentucky'),('en_US','28','Louisiana'),('en_US','29','Maine'),('en_US','30','Marshall Islands'),('en_US','31','Maryland'),('en_US','32','Massachusetts'),('en_US','33','Michigan'),('en_US','34','Minnesota'),('en_US','35','Mississippi'),('en_US','36','Missouri'),('en_US','37','Montana'),('en_US','38','Nebraska'),('en_US','39','Nevada'),('en_US','40','New Hampshire'),('en_US','41','New Jersey'),('en_US','42','New Mexico'),('en_US','43','New York'),('en_US','44','North Carolina'),('en_US','45','North Dakota'),('en_US','46','Northern Mariana Islands'),('en_US','47','Ohio'),('en_US','48','Oklahoma'),('en_US','49','Oregon'),('en_US','50','Palau'),('en_US','51','Pennsylvania'),('en_US','52','Puerto Rico'),('en_US','53','Rhode Island'),('en_US','54','South Carolina'),('en_US','55','South Dakota'),('en_US','56','Tennessee'),('en_US','57','Texas'),('en_US','58','Utah'),('en_US','59','Vermont'),('en_US','60','Virgin Islands'),('en_US','61','Virginia'),('en_US','62','Washington'),('en_US','63','West Virginia'),('en_US','64','Wisconsin'),('en_US','65','Wyoming'),('en_US','66','Alberta'),('en_US','67','British Columbia'),('en_US','68','Manitoba'),('en_US','69','Newfoundland and Labrador'),('en_US','70','New Brunswick'),('en_US','71','Nova Scotia'),('en_US','72','Northwest Territories'),('en_US','73','Nunavut'),('en_US','74','Ontario'),('en_US','75','Prince Edward Island'),('en_US','76','Quebec'),('en_US','77','Saskatchewan'),('en_US','78','Yukon Territory'),('en_US','79','Niedersachsen'),('en_US','80','Baden-Württemberg'),('en_US','81','Bayern'),('en_US','82','Berlin'),('en_US','83','Brandenburg'),('en_US','84','Bremen'),('en_US','85','Hamburg'),('en_US','86','Hessen'),('en_US','87','Mecklenburg-Vorpommern'),('en_US','88','Nordrhein-Westfalen'),('en_US','89','Rheinland-Pfalz'),('en_US','90','Saarland'),('en_US','91','Sachsen'),('en_US','92','Sachsen-Anhalt'),('en_US','93','Schleswig-Holstein'),('en_US','94','Thüringen'),('en_US','95','Wien'),('en_US','96','Niederösterreich'),('en_US','97','Oberösterreich'),('en_US','98','Salzburg'),('en_US','99','Kärnten'),('en_US','100','Steiermark'),('en_US','101','Tirol'),('en_US','102','Burgenland'),('en_US','103','Vorarlberg'),('en_US','104','Aargau'),('en_US','105','Appenzell Innerrhoden'),('en_US','106','Appenzell Ausserrhoden'),('en_US','107','Bern'),('en_US','108','Basel-Landschaft'),('en_US','109','Basel-Stadt'),('en_US','110','Freiburg'),('en_US','111','Genf'),('en_US','112','Glarus'),('en_US','113','Graubünden'),('en_US','114','Jura'),('en_US','115','Luzern'),('en_US','116','Neuenburg'),('en_US','117','Nidwalden'),('en_US','118','Obwalden'),('en_US','119','St. Gallen'),('en_US','120','Schaffhausen'),('en_US','121','Solothurn'),('en_US','122','Schwyz'),('en_US','123','Thurgau'),('en_US','124','Tessin'),('en_US','125','Uri'),('en_US','126','Waadt'),('en_US','127','Wallis'),('en_US','128','Zug'),('en_US','129','Zürich'),('en_US','130','A Coruña'),('en_US','131','Alava'),('en_US','132','Albacete'),('en_US','133','Alicante'),('en_US','134','Almeria'),('en_US','135','Asturias'),('en_US','136','Avila'),('en_US','137','Badajoz'),('en_US','138','Baleares'),('en_US','139','Barcelona'),('en_US','140','Burgos'),('en_US','141','Caceres'),('en_US','142','Cadiz'),('en_US','143','Cantabria'),('en_US','144','Castellon'),('en_US','145','Ceuta'),('en_US','146','Ciudad Real'),('en_US','147','Cordoba'),('en_US','148','Cuenca'),('en_US','149','Girona'),('en_US','150','Granada'),('en_US','151','Guadalajara'),('en_US','152','Guipuzcoa'),('en_US','153','Huelva'),('en_US','154','Huesca'),('en_US','155','Jaen'),('en_US','156','La Rioja'),('en_US','157','Las Palmas'),('en_US','158','Leon'),('en_US','159','Lleida'),('en_US','160','Lugo'),('en_US','161','Madrid'),('en_US','162','Malaga'),('en_US','163','Melilla'),('en_US','164','Murcia'),('en_US','165','Navarra'),('en_US','166','Ourense'),('en_US','167','Palencia'),('en_US','168','Pontevedra'),('en_US','169','Salamanca'),('en_US','170','Santa Cruz de Tenerife'),('en_US','171','Segovia'),('en_US','172','Sevilla'),('en_US','173','Soria'),('en_US','174','Tarragona'),('en_US','175','Teruel'),('en_US','176','Toledo'),('en_US','177','Valencia'),('en_US','178','Valladolid'),('en_US','179','Vizcaya'),('en_US','180','Zamora'),('en_US','181','Zaragoza'),('en_US','182','Ain'),('en_US','183','Aisne'),('en_US','184','Allier'),('en_US','185','Alpes-de-Haute-Provence'),('en_US','186','Hautes-Alpes'),('en_US','187','Alpes-Maritimes'),('en_US','188','Ardèche'),('en_US','189','Ardennes'),('en_US','190','Ariège'),('en_US','191','Aube'),('en_US','192','Aude'),('en_US','193','Aveyron'),('en_US','194','Bouches-du-Rhône'),('en_US','195','Calvados'),('en_US','196','Cantal'),('en_US','197','Charente'),('en_US','198','Charente-Maritime'),('en_US','199','Cher'),('en_US','200','Corrèze'),('en_US','201','Corse-du-Sud'),('en_US','202','Haute-Corse'),('en_US','203','Côte-d\'Or'),('en_US','204','Côtes-d\'Armor'),('en_US','205','Creuse'),('en_US','206','Dordogne'),('en_US','207','Doubs'),('en_US','208','Drôme'),('en_US','209','Eure'),('en_US','210','Eure-et-Loir'),('en_US','211','Finistère'),('en_US','212','Gard'),('en_US','213','Haute-Garonne'),('en_US','214','Gers'),('en_US','215','Gironde'),('en_US','216','Hérault'),('en_US','217','Ille-et-Vilaine'),('en_US','218','Indre'),('en_US','219','Indre-et-Loire'),('en_US','220','Isère'),('en_US','221','Jura'),('en_US','222','Landes'),('en_US','223','Loir-et-Cher'),('en_US','224','Loire'),('en_US','225','Haute-Loire'),('en_US','226','Loire-Atlantique'),('en_US','227','Loiret'),('en_US','228','Lot'),('en_US','229','Lot-et-Garonne'),('en_US','230','Lozère'),('en_US','231','Maine-et-Loire'),('en_US','232','Manche'),('en_US','233','Marne'),('en_US','234','Haute-Marne'),('en_US','235','Mayenne'),('en_US','236','Meurthe-et-Moselle'),('en_US','237','Meuse'),('en_US','238','Morbihan'),('en_US','239','Moselle'),('en_US','240','Nièvre'),('en_US','241','Nord'),('en_US','242','Oise'),('en_US','243','Orne'),('en_US','244','Pas-de-Calais'),('en_US','245','Puy-de-Dôme'),('en_US','246','Pyrénées-Atlantiques'),('en_US','247','Hautes-Pyrénées'),('en_US','248','Pyrénées-Orientales'),('en_US','249','Bas-Rhin'),('en_US','250','Haut-Rhin'),('en_US','251','Rhône'),('en_US','252','Haute-Saône'),('en_US','253','Saône-et-Loire'),('en_US','254','Sarthe'),('en_US','255','Savoie'),('en_US','256','Haute-Savoie'),('en_US','257','Paris'),('en_US','258','Seine-Maritime'),('en_US','259','Seine-et-Marne'),('en_US','260','Yvelines'),('en_US','261','Deux-Sèvres'),('en_US','262','Somme'),('en_US','263','Tarn'),('en_US','264','Tarn-et-Garonne'),('en_US','265','Var'),('en_US','266','Vaucluse'),('en_US','267','Vendée'),('en_US','268','Vienne'),('en_US','269','Haute-Vienne'),('en_US','270','Vosges'),('en_US','271','Yonne'),('en_US','272','Territoire-de-Belfort'),('en_US','273','Essonne'),('en_US','274','Hauts-de-Seine'),('en_US','275','Seine-Saint-Denis'),('en_US','276','Val-de-Marne'),('en_US','277','Val-d\'Oise'),('en_US','278','Alba'),('en_US','279','Arad'),('en_US','280','Argeş'),('en_US','281','Bacău'),('en_US','282','Bihor'),('en_US','283','Bistriţa-Năsăud'),('en_US','284','Botoşani'),('en_US','285','Braşov'),('en_US','286','Brăila'),('en_US','287','Bucureşti'),('en_US','288','Buzău'),('en_US','289','Caraş-Severin'),('en_US','290','Călăraşi'),('en_US','291','Cluj'),('en_US','292','Constanţa'),('en_US','293','Covasna'),('en_US','294','Dâmboviţa'),('en_US','295','Dolj'),('en_US','296','Galaţi'),('en_US','297','Giurgiu'),('en_US','298','Gorj'),('en_US','299','Harghita'),('en_US','300','Hunedoara'),('en_US','301','Ialomiţa'),('en_US','302','Iaşi'),('en_US','303','Ilfov'),('en_US','304','Maramureş'),('en_US','305','Mehedinţi'),('en_US','306','Mureş'),('en_US','307','Neamţ'),('en_US','308','Olt'),('en_US','309','Prahova'),('en_US','310','Satu-Mare'),('en_US','311','Sălaj'),('en_US','312','Sibiu'),('en_US','313','Suceava'),('en_US','314','Teleorman'),('en_US','315','Timiş'),('en_US','316','Tulcea'),('en_US','317','Vaslui'),('en_US','318','Vâlcea'),('en_US','319','Vrancea'),('en_US','320','Lappi'),('en_US','321','Pohjois-Pohjanmaa'),('en_US','322','Kainuu'),('en_US','323','Pohjois-Karjala'),('en_US','324','Pohjois-Savo'),('en_US','325','Etelä-Savo'),('en_US','326','Etelä-Pohjanmaa'),('en_US','327','Pohjanmaa'),('en_US','328','Pirkanmaa'),('en_US','329','Satakunta'),('en_US','330','Keski-Pohjanmaa'),('en_US','331','Keski-Suomi'),('en_US','332','Varsinais-Suomi'),('en_US','333','Etelä-Karjala'),('en_US','334','Päijät-Häme'),('en_US','335','Kanta-Häme'),('en_US','336','Uusimaa'),('en_US','337','Itä-Uusimaa'),('en_US','338','Kymenlaakso'),('en_US','339','Ahvenanmaa'),('en_US','340','Harjumaa'),('en_US','341','Hiiumaa'),('en_US','342','Ida-Virumaa'),('en_US','343','Jõgevamaa'),('en_US','344','Järvamaa'),('en_US','345','Läänemaa'),('en_US','346','Lääne-Virumaa'),('en_US','347','Põlvamaa'),('en_US','348','Pärnumaa'),('en_US','349','Raplamaa'),('en_US','350','Saaremaa'),('en_US','351','Tartumaa'),('en_US','352','Valgamaa'),('en_US','353','Viljandimaa'),('en_US','354','Võrumaa'),('en_US','355','Daugavpils'),('en_US','356','Jelgava'),('en_US','357','Jēkabpils'),('en_US','358','Jūrmala'),('en_US','359','Liepāja'),('en_US','360','Liepājas novads'),('en_US','361','Rēzekne'),('en_US','362','Rīga'),('en_US','363','Rīgas novads'),('en_US','364','Valmiera'),('en_US','365','Ventspils'),('en_US','366','Aglonas novads'),('en_US','367','Aizkraukles novads'),('en_US','368','Aizputes novads'),('en_US','369','Aknīstes novads'),('en_US','370','Alojas novads'),('en_US','371','Alsungas novads'),('en_US','372','Alūksnes novads'),('en_US','373','Amatas novads'),('en_US','374','Apes novads'),('en_US','375','Auces novads'),('en_US','376','Babītes novads'),('en_US','377','Baldones novads'),('en_US','378','Baltinavas novads'),('en_US','379','Balvu novads'),('en_US','380','Bauskas novads'),('en_US','381','Beverīnas novads'),('en_US','382','Brocēnu novads'),('en_US','383','Burtnieku novads'),('en_US','384','Carnikavas novads'),('en_US','385','Cesvaines novads'),('en_US','386','Ciblas novads'),('en_US','387','Cēsu novads'),('en_US','388','Dagdas novads'),('en_US','389','Daugavpils novads'),('en_US','390','Dobeles novads'),('en_US','391','Dundagas novads'),('en_US','392','Durbes novads'),('en_US','393','Engures novads'),('en_US','394','Garkalnes novads'),('en_US','395','Grobiņas novads'),('en_US','396','Gulbenes novads'),('en_US','397','Iecavas novads'),('en_US','398','Ikšķiles novads'),('en_US','399','Ilūkstes novads'),('en_US','400','Inčukalna novads'),('en_US','401','Jaunjelgavas novads'),('en_US','402','Jaunpiebalgas novads'),('en_US','403','Jaunpils novads'),('en_US','404','Jelgavas novads'),('en_US','405','Jēkabpils novads'),('en_US','406','Kandavas novads'),('en_US','407','Kokneses novads'),('en_US','408','Krimuldas novads'),('en_US','409','Krustpils novads'),('en_US','410','Krāslavas novads'),('en_US','411','Kuldīgas novads'),('en_US','412','Kārsavas novads'),('en_US','413','Lielvārdes novads'),('en_US','414','Limbažu novads'),('en_US','415','Lubānas novads'),('en_US','416','Ludzas novads'),('en_US','417','Līgatnes novads'),('en_US','418','Līvānu novads'),('en_US','419','Madonas novads'),('en_US','420','Mazsalacas novads'),('en_US','421','Mālpils novads'),('en_US','422','Mārupes novads'),('en_US','423','Naukšēnu novads'),('en_US','424','Neretas novads'),('en_US','425','Nīcas novads'),('en_US','426','Ogres novads'),('en_US','427','Olaines novads'),('en_US','428','Ozolnieku novads'),('en_US','429','Preiļu novads'),('en_US','430','Priekules novads'),('en_US','431','Priekuļu novads'),('en_US','432','Pārgaujas novads'),('en_US','433','Pāvilostas novads'),('en_US','434','Pļaviņu novads'),('en_US','435','Raunas novads'),('en_US','436','Riebiņu novads'),('en_US','437','Rojas novads'),('en_US','438','Ropažu novads'),('en_US','439','Rucavas novads'),('en_US','440','Rugāju novads'),('en_US','441','Rundāles novads'),('en_US','442','Rēzeknes novads'),('en_US','443','Rūjienas novads'),('en_US','444','Salacgrīvas novads'),('en_US','445','Salas novads'),('en_US','446','Salaspils novads'),('en_US','447','Saldus novads'),('en_US','448','Saulkrastu novads'),('en_US','449','Siguldas novads'),('en_US','450','Skrundas novads'),('en_US','451','Skrīveru novads'),('en_US','452','Smiltenes novads'),('en_US','453','Stopiņu novads'),('en_US','454','Strenču novads'),('en_US','455','Sējas novads'),('en_US','456','Talsu novads'),('en_US','457','Tukuma novads'),('en_US','458','Tērvetes novads'),('en_US','459','Vaiņodes novads'),('en_US','460','Valkas novads'),('en_US','461','Valmieras novads'),('en_US','462','Varakļānu novads'),('en_US','463','Vecpiebalgas novads'),('en_US','464','Vecumnieku novads'),('en_US','465','Ventspils novads'),('en_US','466','Viesītes novads'),('en_US','467','Viļakas novads'),('en_US','468','Viļānu novads'),('en_US','469','Vārkavas novads'),('en_US','470','Zilupes novads'),('en_US','471','Ādažu novads'),('en_US','472','Ērgļu novads'),('en_US','473','Ķeguma novads'),('en_US','474','Ķekavas novads'),('en_US','475','Alytaus Apskritis'),('en_US','476','Kauno Apskritis'),('en_US','477','Klaipėdos Apskritis'),('en_US','478','Marijampolės Apskritis'),('en_US','479','Panevėžio Apskritis'),('en_US','480','Šiaulių Apskritis'),('en_US','481','Tauragės Apskritis'),('en_US','482','Telšių Apskritis'),('en_US','483','Utenos Apskritis'),('en_US','484','Vilniaus Apskritis'),('en_US','485','Acre'),('en_US','486','Alagoas'),('en_US','487','Amapá'),('en_US','488','Amazonas'),('en_US','489','Bahia'),('en_US','490','Ceará'),('en_US','491','Espírito Santo'),('en_US','492','Goiás'),('en_US','493','Maranhão'),('en_US','494','Mato Grosso'),('en_US','495','Mato Grosso do Sul'),('en_US','496','Minas Gerais'),('en_US','497','Pará'),('en_US','498','Paraíba'),('en_US','499','Paraná'),('en_US','500','Pernambuco'),('en_US','501','Piauí'),('en_US','502','Rio de Janeiro'),('en_US','503','Rio Grande do Norte'),('en_US','504','Rio Grande do Sul'),('en_US','505','Rondônia'),('en_US','506','Roraima'),('en_US','507','Santa Catarina'),('en_US','508','São Paulo'),('en_US','509','Sergipe'),('en_US','510','Tocantins'),('en_US','511','Distrito Federal');
/*!40000 ALTER TABLE `directory_country_region_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_currency_rate`
--

DROP TABLE IF EXISTS `directory_currency_rate`;
CREATE TABLE `directory_currency_rate` (
  `currency_from` varchar(3) NOT NULL COMMENT 'Currency Code Convert From',
  `currency_to` varchar(3) NOT NULL COMMENT 'Currency Code Convert To',
  `rate` decimal(24,12) NOT NULL DEFAULT '0.000000000000' COMMENT 'Currency Conversion Rate',
  PRIMARY KEY (`currency_from`,`currency_to`),
  KEY `DIRECTORY_CURRENCY_RATE_CURRENCY_TO` (`currency_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Currency Rate';

--
-- Dumping data for table `directory_currency_rate`
--

LOCK TABLES `directory_currency_rate` WRITE;
/*!40000 ALTER TABLE `directory_currency_rate` DISABLE KEYS */;
INSERT INTO `directory_currency_rate` VALUES ('EUR','EUR','1.000000000000'),('EUR','USD','1.415000000000'),('USD','EUR','0.706700000000'),('USD','GBP','0.789500000000'),('USD','USD','1.000000000000');
/*!40000 ALTER TABLE `directory_currency_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_link`
--

DROP TABLE IF EXISTS `downloadable_link`;
CREATE TABLE `downloadable_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  `number_of_downloads` int(11) DEFAULT NULL COMMENT 'Number of downloads',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(20) DEFAULT NULL COMMENT 'Link Type',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample Url',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample File',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  PRIMARY KEY (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRODUCT_ID_SORT_ORDER` (`product_id`,`sort_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Table';

--
-- Table structure for table `downloadable_link_price`
--

DROP TABLE IF EXISTS `downloadable_link_price`;
CREATE TABLE `downloadable_link_price` (
  `price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Price ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  PRIMARY KEY (`price_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_LINK_ID` (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Price Table';

--
-- Table structure for table `downloadable_link_purchased`
--

DROP TABLE IF EXISTS `downloadable_link_purchased`;
CREATE TABLE `downloadable_link_purchased` (
  `purchased_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Purchased ID',
  `order_id` int(10) unsigned DEFAULT '0' COMMENT 'Order ID',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment ID',
  `order_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Item ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of creation',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of modification',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer ID',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product name',
  `product_sku` varchar(255) DEFAULT NULL COMMENT 'Product sku',
  `link_section_title` varchar(255) DEFAULT NULL COMMENT 'Link_section_title',
  PRIMARY KEY (`purchased_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ORDER_ID` (`order_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Table';

--
-- Table structure for table `downloadable_link_purchased_item`
--

DROP TABLE IF EXISTS `downloadable_link_purchased_item`;
CREATE TABLE `downloadable_link_purchased_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item ID',
  `purchased_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Purchased ID',
  `order_item_id` int(10) unsigned DEFAULT '0' COMMENT 'Order Item ID',
  `product_id` int(10) unsigned DEFAULT '0' COMMENT 'Product ID',
  `link_hash` varchar(255) DEFAULT NULL COMMENT 'Link hash',
  `number_of_downloads_bought` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads bought',
  `number_of_downloads_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads used',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `link_title` varchar(255) DEFAULT NULL COMMENT 'Link Title',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable Flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(255) DEFAULT NULL COMMENT 'Link Type',
  `status` varchar(50) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  PRIMARY KEY (`item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_LINK_HASH` (`link_hash`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_PURCHASED_ID` (`purchased_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Item Table';

--
-- Table structure for table `downloadable_link_title`
--

DROP TABLE IF EXISTS `downloadable_link_title`;
CREATE TABLE `downloadable_link_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `DOWNLOADABLE_LINK_TITLE_LINK_ID_STORE_ID` (`link_id`,`store_id`),
  KEY `DOWNLOADABLE_LINK_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Title Table';

--
-- Table structure for table `downloadable_sample`
--

DROP TABLE IF EXISTS `downloadable_sample`;
CREATE TABLE `downloadable_sample` (
  `sample_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sample ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample URL',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample file',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`sample_id`),
  KEY `DOWNLOADABLE_SAMPLE_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Table';

--
-- Table structure for table `downloadable_sample_title`
--

DROP TABLE IF EXISTS `downloadable_sample_title`;
CREATE TABLE `downloadable_sample_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `sample_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sample ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `DOWNLOADABLE_SAMPLE_TITLE_SAMPLE_ID_STORE_ID` (`sample_id`,`store_id`),
  KEY `DOWNLOADABLE_SAMPLE_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Title Table';

--
-- Table structure for table `eav_attribute`
--

DROP TABLE IF EXISTS `eav_attribute`;
CREATE TABLE `eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_code` varchar(255) DEFAULT NULL COMMENT 'Attribute Code',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `backend_model` varchar(255) DEFAULT NULL COMMENT 'Backend Model',
  `backend_type` varchar(8) NOT NULL DEFAULT 'static' COMMENT 'Backend Type',
  `backend_table` varchar(255) DEFAULT NULL COMMENT 'Backend Table',
  `frontend_model` varchar(255) DEFAULT NULL COMMENT 'Frontend Model',
  `frontend_input` varchar(50) DEFAULT NULL COMMENT 'Frontend Input',
  `frontend_label` varchar(255) DEFAULT NULL COMMENT 'Frontend Label',
  `frontend_class` varchar(255) DEFAULT NULL COMMENT 'Frontend Class',
  `source_model` varchar(255) DEFAULT NULL COMMENT 'Source Model',
  `is_required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Required',
  `is_user_defined` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is User Defined',
  `default_value` text COMMENT 'Default Value',
  `is_unique` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Unique',
  `note` varchar(255) DEFAULT NULL COMMENT 'Note',
  PRIMARY KEY (`attribute_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_ENTITY_TYPE_ID_ATTRIBUTE_CODE` (`entity_type_id`,`attribute_code`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute';

--
-- Dumping data for table `eav_attribute`
--

LOCK TABLES `eav_attribute` WRITE;
/*!40000 ALTER TABLE `eav_attribute` DISABLE KEYS */;
INSERT INTO `eav_attribute` VALUES (1,1,'website_id',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Website','static',NULL,NULL,'select','Associate to Website',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Website',1,0,NULL,0,NULL),(2,1,'store_id',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Store','static',NULL,NULL,'select','Create In',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Store',1,0,NULL,0,NULL),(3,1,'created_in',NULL,NULL,'static',NULL,NULL,'text','Created From',NULL,NULL,0,0,NULL,0,NULL),(4,1,'prefix',NULL,NULL,'static',NULL,NULL,'text','Prefix',NULL,NULL,0,0,NULL,0,NULL),(5,1,'firstname',NULL,NULL,'static',NULL,NULL,'text','First Name',NULL,NULL,1,0,NULL,0,NULL),(6,1,'middlename',NULL,NULL,'static',NULL,NULL,'text','Middle Name/Initial',NULL,NULL,0,0,NULL,0,NULL),(7,1,'lastname',NULL,NULL,'static',NULL,NULL,'text','Last Name',NULL,NULL,1,0,NULL,0,NULL),(8,1,'suffix',NULL,NULL,'static',NULL,NULL,'text','Suffix',NULL,NULL,0,0,NULL,0,NULL),(9,1,'email',NULL,NULL,'static',NULL,NULL,'text','Email',NULL,NULL,1,0,NULL,0,NULL),(10,1,'group_id',NULL,NULL,'static',NULL,NULL,'select','Group',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Group',1,0,NULL,0,NULL),(11,1,'dob',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','static',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Frontend\\Datetime','date','Date of Birth',NULL,NULL,0,0,NULL,0,NULL),(12,1,'password_hash',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Password','static',NULL,NULL,'hidden',NULL,NULL,NULL,0,0,NULL,0,NULL),(13,1,'rp_token',NULL,NULL,'static',NULL,NULL,'hidden',NULL,NULL,NULL,0,0,NULL,0,NULL),(14,1,'rp_token_created_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,0,0,NULL,0,NULL),(15,1,'default_billing',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Billing','static',NULL,NULL,'text','Default Billing Address',NULL,NULL,0,0,NULL,0,NULL),(16,1,'default_shipping',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Shipping','static',NULL,NULL,'text','Default Shipping Address',NULL,NULL,0,0,NULL,0,NULL),(17,1,'taxvat',NULL,NULL,'static',NULL,NULL,'text','Tax/VAT Number',NULL,NULL,0,0,NULL,0,NULL),(18,1,'confirmation',NULL,NULL,'static',NULL,NULL,'text','Is Confirmed',NULL,NULL,0,0,NULL,0,NULL),(19,1,'created_at',NULL,NULL,'static',NULL,NULL,'date','Created At',NULL,NULL,0,0,NULL,0,NULL),(20,1,'gender',NULL,NULL,'static',NULL,NULL,'select','Gender',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,0,NULL,0,NULL),(21,1,'disable_auto_group_change',NULL,'Magento\\Customer\\Model\\Attribute\\Backend\\Data\\Boolean','static',NULL,NULL,'boolean','Disable Automatic Group Change Based on VAT ID',NULL,NULL,0,0,NULL,0,NULL),(22,2,'prefix',NULL,NULL,'static',NULL,NULL,'text','Prefix',NULL,NULL,0,0,NULL,0,NULL),(23,2,'firstname',NULL,NULL,'static',NULL,NULL,'text','First Name',NULL,NULL,1,0,NULL,0,NULL),(24,2,'middlename',NULL,NULL,'static',NULL,NULL,'text','Middle Name/Initial',NULL,NULL,0,0,NULL,0,NULL),(25,2,'lastname',NULL,NULL,'static',NULL,NULL,'text','Last Name',NULL,NULL,1,0,NULL,0,NULL),(26,2,'suffix',NULL,NULL,'static',NULL,NULL,'text','Suffix',NULL,NULL,0,0,NULL,0,NULL),(27,2,'company',NULL,NULL,'static',NULL,NULL,'text','Company',NULL,NULL,0,0,NULL,0,NULL),(28,2,'street',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\DefaultBackend','static',NULL,NULL,'multiline','Street Address',NULL,NULL,1,0,NULL,0,NULL),(29,2,'city',NULL,NULL,'static',NULL,NULL,'text','City',NULL,NULL,1,0,NULL,0,NULL),(30,2,'country_id',NULL,NULL,'static',NULL,NULL,'select','Country',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Source\\Country',1,0,NULL,0,NULL),(31,2,'region',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Backend\\Region','static',NULL,NULL,'text','State/Province',NULL,NULL,0,0,NULL,0,NULL),(32,2,'region_id',NULL,NULL,'static',NULL,NULL,'hidden','State/Province',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Source\\Region',0,0,NULL,0,NULL),(33,2,'postcode',NULL,NULL,'static',NULL,NULL,'text','Zip/Postal Code',NULL,NULL,0,0,NULL,0,NULL),(34,2,'telephone',NULL,NULL,'static',NULL,NULL,'text','Phone Number',NULL,NULL,1,0,NULL,0,NULL),(35,2,'fax',NULL,NULL,'static',NULL,NULL,'text','Fax',NULL,NULL,0,0,NULL,0,NULL),(36,2,'vat_id',NULL,NULL,'static',NULL,NULL,'text','VAT number',NULL,NULL,0,0,NULL,0,NULL),(37,2,'vat_is_valid',NULL,NULL,'static',NULL,NULL,'text','VAT number validity',NULL,NULL,0,0,NULL,0,NULL),(38,2,'vat_request_id',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request ID',NULL,NULL,0,0,NULL,0,NULL),(39,2,'vat_request_date',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request date',NULL,NULL,0,0,NULL,0,NULL),(40,2,'vat_request_success',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request success',NULL,NULL,0,0,NULL,0,NULL),(41,1,'updated_at',NULL,NULL,'static',NULL,NULL,'date','Updated At',NULL,NULL,0,0,NULL,0,NULL),(42,1,'failures_num',NULL,NULL,'static',NULL,NULL,'hidden','Failures Number',NULL,NULL,0,0,NULL,0,NULL),(43,1,'first_failure',NULL,NULL,'static',NULL,NULL,'date','First Failure Date',NULL,NULL,0,0,NULL,0,NULL),(44,1,'lock_expires',NULL,NULL,'static',NULL,NULL,'date','Failures Number',NULL,NULL,0,0,NULL,0,NULL),(45,3,'name',NULL,NULL,'varchar',NULL,NULL,'text','Name',NULL,NULL,1,0,NULL,0,NULL),(46,3,'is_active',NULL,NULL,'int',NULL,NULL,'select','Is Active',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,NULL,0,NULL),(47,3,'description',NULL,NULL,'text',NULL,NULL,'textarea','Description',NULL,NULL,0,0,NULL,0,NULL),(48,3,'image',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Image',NULL,NULL,0,0,NULL,0,NULL),(49,3,'meta_title',NULL,NULL,'varchar',NULL,NULL,'text','Page Title',NULL,NULL,0,0,NULL,0,NULL),(50,3,'meta_keywords',NULL,NULL,'text',NULL,NULL,'textarea','Meta Keywords',NULL,NULL,0,0,NULL,0,NULL),(51,3,'meta_description',NULL,NULL,'text',NULL,NULL,'textarea','Meta Description',NULL,NULL,0,0,NULL,0,NULL),(52,3,'display_mode',NULL,NULL,'varchar',NULL,NULL,'select','Display Mode',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Mode',0,0,NULL,0,NULL),(53,3,'landing_page',NULL,NULL,'int',NULL,NULL,'select','CMS Block',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Page',0,0,NULL,0,NULL),(54,3,'is_anchor',NULL,NULL,'int',NULL,NULL,'select','Is Anchor',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,'1',0,NULL),(55,3,'path',NULL,NULL,'static',NULL,NULL,'text','Path',NULL,NULL,0,0,NULL,0,NULL),(56,3,'position',NULL,NULL,'static',NULL,NULL,'text','Position',NULL,NULL,0,0,NULL,0,NULL),(57,3,'all_children',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(58,3,'path_in_store',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(59,3,'children',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(60,3,'custom_design',NULL,NULL,'varchar',NULL,NULL,'select','Custom Design',NULL,'Magento\\Theme\\Model\\Theme\\Source\\Theme',0,0,NULL,0,NULL),(61,3,'custom_design_from','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Frontend\\Datetime','date','Active From',NULL,NULL,0,0,NULL,0,NULL),(62,3,'custom_design_to',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Active To',NULL,NULL,0,0,NULL,0,NULL),(63,3,'page_layout',NULL,NULL,'varchar',NULL,NULL,'select','Page Layout',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(64,3,'custom_layout_update',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Customlayoutupdate','text',NULL,NULL,'textarea','Custom Layout Update',NULL,NULL,0,0,NULL,0,NULL),(65,3,'level',NULL,NULL,'static',NULL,NULL,'text','Level',NULL,NULL,0,0,NULL,0,NULL),(66,3,'children_count',NULL,NULL,'static',NULL,NULL,'text','Children Count',NULL,NULL,0,0,NULL,0,NULL),(67,3,'available_sort_by',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Sortby','text',NULL,NULL,'multiselect','Available Product Listing Sort By',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Sortby',1,0,NULL,0,NULL),(68,3,'default_sort_by',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Sortby','varchar',NULL,NULL,'select','Default Product Listing Sort By',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Sortby',1,0,NULL,0,NULL),(69,3,'include_in_menu',NULL,NULL,'int',NULL,NULL,'select','Include in Navigation Menu',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,'1',0,NULL),(70,3,'custom_use_parent_settings',NULL,NULL,'int',NULL,NULL,'select','Use Parent Category Settings',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(71,3,'custom_apply_to_products',NULL,NULL,'int',NULL,NULL,'select','Apply To Products',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(72,3,'filter_price_range',NULL,NULL,'decimal',NULL,NULL,'text','Layered Navigation Price Step',NULL,NULL,0,0,NULL,0,NULL),(73,4,'name',NULL,NULL,'varchar',NULL,NULL,'text','Product Name','validate-length maximum-length-255',NULL,1,0,NULL,0,NULL),(74,4,'sku',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Sku','static',NULL,NULL,'text','SKU','validate-length maximum-length-64',NULL,1,0,NULL,1,NULL),(75,4,'description',NULL,NULL,'text',NULL,NULL,'textarea','Description',NULL,NULL,0,0,NULL,0,NULL),(76,4,'short_description',NULL,NULL,'text',NULL,NULL,'textarea','Short Description',NULL,NULL,0,0,NULL,0,NULL),(77,4,'price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Price',NULL,NULL,1,0,NULL,0,NULL),(78,4,'special_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Special Price',NULL,NULL,0,0,NULL,0,NULL),(79,4,'special_from_date',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Special Price From Date',NULL,NULL,0,0,NULL,0,NULL),(80,4,'special_to_date',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Special Price To Date',NULL,NULL,0,0,NULL,0,NULL),(81,4,'cost',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Cost',NULL,NULL,0,1,NULL,0,NULL),(82,4,'weight',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Weight','decimal',NULL,NULL,'weight','Weight',NULL,NULL,0,0,NULL,0,NULL),(83,4,'manufacturer',NULL,NULL,'int',NULL,NULL,'select','Manufacturer',NULL,NULL,0,1,NULL,0,NULL),(84,4,'meta_title',NULL,NULL,'varchar',NULL,NULL,'text','Meta Title',NULL,NULL,0,0,NULL,0,NULL),(85,4,'meta_keyword',NULL,NULL,'text',NULL,NULL,'textarea','Meta Keywords',NULL,NULL,0,0,NULL,0,NULL),(86,4,'meta_description',NULL,NULL,'varchar',NULL,NULL,'textarea','Meta Description',NULL,NULL,0,0,NULL,0,'Maximum 255 chars. Meta Description should optimally be between 150-160 characters'),(87,4,'image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Base',NULL,NULL,0,0,NULL,0,NULL),(88,4,'small_image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Small',NULL,NULL,0,0,NULL,0,NULL),(89,4,'thumbnail',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Thumbnail',NULL,NULL,0,0,NULL,0,NULL),(90,4,'media_gallery',NULL,NULL,'static',NULL,NULL,'gallery','Media Gallery',NULL,NULL,0,0,NULL,0,NULL),(91,4,'old_id',NULL,NULL,'int',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(92,4,'tier_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Tierprice','decimal',NULL,NULL,'text','Tier Price',NULL,NULL,0,0,NULL,0,NULL),(94,4,'news_from_date',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Set Product as New from Date',NULL,NULL,0,0,NULL,0,NULL),(95,4,'news_to_date',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Set Product as New to Date',NULL,NULL,0,0,NULL,0,NULL),(96,4,'gallery',NULL,NULL,'varchar',NULL,NULL,'gallery','Image Gallery',NULL,NULL,0,0,NULL,0,NULL),(97,4,'status',NULL,NULL,'int',NULL,NULL,'select','Enable Product',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Status',0,0,'1',0,NULL),(98,4,'minimal_price',NULL,NULL,'decimal',NULL,NULL,'price','Minimal Price',NULL,NULL,0,0,NULL,0,NULL),(99,4,'visibility',NULL,NULL,'int',NULL,NULL,'select','Visibility',NULL,'Magento\\Catalog\\Model\\Product\\Visibility',0,0,'4',0,NULL),(100,4,'custom_design',NULL,NULL,'varchar',NULL,NULL,'select','New Theme',NULL,'Magento\\Theme\\Model\\Theme\\Source\\Theme',0,0,NULL,0,NULL),(101,4,'custom_design_from',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Active From',NULL,NULL,0,0,NULL,0,NULL),(102,4,'custom_design_to',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Active To',NULL,NULL,0,0,NULL,0,NULL),(103,4,'custom_layout_update',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Customlayoutupdate','text',NULL,NULL,'textarea','Layout Update XML',NULL,NULL,0,0,NULL,0,NULL),(104,4,'page_layout',NULL,NULL,'varchar',NULL,NULL,'select','Layout',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(105,4,'category_ids',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Category','static',NULL,NULL,'text','Categories',NULL,NULL,0,0,NULL,0,NULL),(106,4,'options_container',NULL,NULL,'varchar',NULL,NULL,'select','Display Product Options In',NULL,'Magento\\Catalog\\Model\\Entity\\Product\\Attribute\\Design\\Options\\Container',0,0,'container2',0,NULL),(107,4,'required_options',NULL,NULL,'static',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(108,4,'has_options',NULL,NULL,'static',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(109,4,'image_label',NULL,NULL,'varchar',NULL,NULL,'text','Image Label',NULL,NULL,0,0,NULL,0,NULL),(110,4,'small_image_label',NULL,NULL,'varchar',NULL,NULL,'text','Small Image Label',NULL,NULL,0,0,NULL,0,NULL),(111,4,'thumbnail_label',NULL,NULL,'varchar',NULL,NULL,'text','Thumbnail Label',NULL,NULL,0,0,NULL,0,NULL),(112,4,'created_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,1,0,NULL,0,NULL),(113,4,'updated_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,1,0,NULL,0,NULL),(114,4,'country_of_manufacture',NULL,NULL,'varchar',NULL,NULL,'select','Country of Manufacture',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Countryofmanufacture',0,0,NULL,0,NULL),(115,4,'quantity_and_stock_status',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Stock','int',NULL,NULL,'select','Quantity',NULL,'Magento\\CatalogInventory\\Model\\Source\\Stock',0,0,'1',0,NULL),(116,4,'custom_layout',NULL,NULL,'varchar',NULL,NULL,'select','New Layout',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(117,3,'url_key',NULL,NULL,'varchar',NULL,NULL,'text','URL Key',NULL,NULL,0,0,NULL,0,NULL),(118,3,'url_path',NULL,NULL,'varchar',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(119,4,'url_key',NULL,NULL,'varchar',NULL,NULL,'text','URL Key',NULL,NULL,0,0,NULL,0,NULL),(120,4,'url_path',NULL,NULL,'varchar',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(121,4,'msrp',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Manufacturer\'s Suggested Retail Price',NULL,NULL,0,0,NULL,0,NULL),(122,4,'msrp_display_actual_price_type',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Boolean','varchar',NULL,NULL,'select','Display Actual Price',NULL,'Magento\\Msrp\\Model\\Product\\Attribute\\Source\\Type\\Price',0,0,'0',0,NULL),(123,4,'price_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic Price',NULL,NULL,1,0,'0',0,NULL),(124,4,'sku_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic SKU',NULL,NULL,1,0,'0',0,NULL),(125,4,'weight_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic Weight',NULL,NULL,1,0,'0',0,NULL),(126,4,'price_view',NULL,NULL,'int',NULL,NULL,'select','Price View',NULL,'Magento\\Bundle\\Model\\Product\\Attribute\\Source\\Price\\View',1,0,NULL,0,NULL),(127,4,'shipment_type',NULL,NULL,'int',NULL,NULL,'select','Ship Bundle Items',NULL,'Magento\\Bundle\\Model\\Product\\Attribute\\Source\\Shipment\\Type',1,0,'0',0,NULL),(128,4,'links_purchased_separately',NULL,NULL,'int',NULL,NULL,NULL,'Links can be purchased separately',NULL,NULL,1,0,NULL,0,NULL),(129,4,'samples_title',NULL,NULL,'varchar',NULL,NULL,NULL,'Samples title',NULL,NULL,1,0,NULL,0,NULL),(130,4,'links_title',NULL,NULL,'varchar',NULL,NULL,NULL,'Links title',NULL,NULL,1,0,NULL,0,NULL),(131,4,'links_exist',NULL,NULL,'int',NULL,NULL,NULL,NULL,NULL,NULL,0,0,'0',0,NULL),(132,4,'swatch_image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Swatch',NULL,NULL,0,0,NULL,0,NULL),(133,4,'tax_class_id',NULL,NULL,'int',NULL,NULL,'select','Tax Class',NULL,'Magento\\Tax\\Model\\TaxClass\\Source\\Product',0,0,'2',0,NULL),(134,4,'gift_message_available',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Boolean','varchar',NULL,NULL,'select','Allow Gift Message',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(135,4,'m_size_liquid',NULL,NULL,'int',NULL,NULL,'select','Size',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(136,4,'m_color',NULL,NULL,'int',NULL,NULL,'select','Color',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(137,4,'m_collection',NULL,NULL,'int',NULL,NULL,'select','Collection',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(138,4,'m_category',NULL,NULL,'int',NULL,NULL,'select','Category',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(139,4,'m_finish',NULL,NULL,'int',NULL,NULL,'select','Finish',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(141,4,'m_formula',NULL,NULL,'int',NULL,NULL,'select','Formula',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(142,4,'c_benefits',NULL,NULL,'text',NULL,NULL,'textarea','Benefits',NULL,NULL,0,1,NULL,0,NULL),(143,4,'c_ingredients',NULL,NULL,'text',NULL,NULL,'textarea','Ingredients',NULL,NULL,0,1,NULL,0,NULL),(144,4,'m_skin_tone',NULL,NULL,'int',NULL,NULL,'select','Skin Tone',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(145,4,'m_scent',NULL,NULL,'int',NULL,NULL,'select','Scent',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(147,4,'c_the_difference',NULL,NULL,'text',NULL,NULL,'textarea','The Difference',NULL,NULL,0,1,NULL,0,NULL),(148,4,'c_features',NULL,NULL,'text',NULL,NULL,'textarea','Features',NULL,NULL,0,1,NULL,0,NULL),(149,4,'c_tips',NULL,NULL,'text',NULL,NULL,'textarea','Tips',NULL,NULL,0,1,NULL,0,NULL),(150,4,'m_use',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Use',NULL,NULL,0,1,'',0,NULL),(151,4,'m_products',NULL,NULL,'int',NULL,NULL,'select','Products',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(152,4,'m_skin_type',NULL,NULL,'int',NULL,NULL,'select','Skin Type',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'62',0,NULL),(155,4,'c_how_to',NULL,NULL,'text',NULL,NULL,'textarea','How To',NULL,NULL,0,1,NULL,0,NULL),(156,4,'c_technology',NULL,NULL,'text',NULL,NULL,'textarea','Technology',NULL,NULL,0,1,NULL,0,NULL),(157,4,'c_routine',NULL,NULL,'text',NULL,NULL,'textarea','Routine',NULL,NULL,0,1,NULL,0,NULL),(158,4,'m_size_weight',NULL,NULL,'int',NULL,NULL,'select','Size (Weight)',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(160,4,'m_area',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Area',NULL,NULL,0,1,'',0,NULL),(161,4,'m_skin_needs',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Skin Needs',NULL,NULL,0,1,'',0,NULL),(162,4,'m_concerns',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Concerns',NULL,NULL,0,1,'',0,NULL),(163,4,'m_ingredients',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Ingredients',NULL,NULL,0,1,'',0,NULL),(164,4,'m_coverage',NULL,NULL,'int',NULL,NULL,'select','Coverage',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(165,4,'m_benefits',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Benefits',NULL,NULL,0,1,'',0,NULL),(166,4,'m_bristle_type',NULL,NULL,'int',NULL,NULL,'select','Bristle Type',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(167,4,'m_use_with',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Use With',NULL,NULL,0,1,'',0,NULL),(168,4,'m_features',NULL,NULL,'int',NULL,NULL,'select','m_features',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(169,4,'m_quantity',NULL,NULL,'int',NULL,NULL,'select','m_quantity',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(170,4,'color',NULL,NULL,'int',NULL,NULL,'select','Color',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL);
/*!40000 ALTER TABLE `eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_group`
--

DROP TABLE IF EXISTS `eav_attribute_group`;
CREATE TABLE `eav_attribute_group` (
  `attribute_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Group Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Group Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Default Id',
  `attribute_group_code` varchar(255) NOT NULL COMMENT 'Attribute Group Code',
  `tab_group_code` varchar(255) DEFAULT NULL COMMENT 'Tab Group Code',
  PRIMARY KEY (`attribute_group_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_ATTRIBUTE_GROUP_NAME` (`attribute_set_id`,`attribute_group_name`),
  KEY `EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`)
) ENGINE=InnoDB AUTO_INCREMENT=319 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Group';

--
-- Dumping data for table `eav_attribute_group`
--

LOCK TABLES `eav_attribute_group` WRITE;
/*!40000 ALTER TABLE `eav_attribute_group` DISABLE KEYS */;
INSERT INTO `eav_attribute_group` VALUES (1,1,'General',1,1,'general',NULL),(2,2,'General',1,1,'general',NULL),(3,3,'General',10,1,'general',NULL),(4,3,'General Information',2,0,'general-information',NULL),(5,3,'Display Settings',20,0,'display-settings',NULL),(6,3,'Custom Design',30,0,'custom-design',NULL),(7,4,'Product Details',1,1,'product-details','basic'),(8,4,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(9,4,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(10,4,'Images',5,0,'image-management','basic'),(11,4,'Design',8,0,'design','advanced'),(12,4,'Autosettings',10,0,'autosettings','advanced'),(13,4,'Content',2,0,'content','basic'),(14,4,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(15,4,'Bundle Items',4,0,'bundle-items',NULL),(16,5,'General',1,1,'general',NULL),(17,6,'General',1,1,'general',NULL),(18,7,'General',1,1,'general',NULL),(19,8,'General',1,1,'general',NULL),(20,4,'Gift Options',11,0,'gift-options',NULL),(21,4,'Options',3,0,'options',NULL),(132,19,'Gift Options',11,0,'gift-options',NULL),(133,19,'Autosettings',10,0,'autosettings','advanced'),(134,19,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(135,19,'Design',8,0,'design','advanced'),(136,19,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(137,19,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(138,19,'Images',5,0,'image-management','basic'),(139,19,'Bundle Items',4,0,'bundle-items',NULL),(140,19,'Options',3,0,'options',NULL),(141,19,'Content',2,0,'content','basic'),(142,19,'Product Details',1,1,'product-details','basic'),(143,20,'Gift Options',11,0,'gift-options',NULL),(144,20,'Autosettings',10,0,'autosettings','advanced'),(145,20,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(146,20,'Design',8,0,'design','advanced'),(147,20,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(148,20,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(149,20,'Images',5,0,'image-management','basic'),(150,20,'Bundle Items',4,0,'bundle-items',NULL),(151,20,'Options',3,0,'options',NULL),(152,20,'Content',2,0,'content','basic'),(153,20,'Product Details',1,1,'product-details','basic'),(165,22,'Gift Options',11,0,'gift-options',NULL),(166,22,'Autosettings',10,0,'autosettings','advanced'),(167,22,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(168,22,'Design',8,0,'design','advanced'),(169,22,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(170,22,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(171,22,'Images',5,0,'image-management','basic'),(172,22,'Bundle Items',4,0,'bundle-items',NULL),(173,22,'Options',3,0,'options',NULL),(174,22,'Content',2,0,'content','basic'),(175,22,'Product Details',1,1,'product-details','basic'),(176,23,'Gift Options',11,0,'gift-options',NULL),(177,23,'Autosettings',10,0,'autosettings','advanced'),(178,23,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(179,23,'Design',8,0,'design','advanced'),(180,23,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(181,23,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(182,23,'Images',5,0,'image-management','basic'),(183,23,'Bundle Items',4,0,'bundle-items',NULL),(184,23,'Options',3,0,'options',NULL),(185,23,'Content',2,0,'content','basic'),(186,23,'Product Details',1,1,'product-details','basic'),(187,24,'Gift Options',11,0,'gift-options',NULL),(188,24,'Autosettings',10,0,'autosettings','advanced'),(189,24,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(190,24,'Design',8,0,'design','advanced'),(191,24,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(192,24,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(193,24,'Images',5,0,'image-management','basic'),(194,24,'Bundle Items',4,0,'bundle-items',NULL),(195,24,'Options',3,0,'options',NULL),(196,24,'Content',2,0,'content','basic'),(197,24,'Product Details',1,1,'product-details','basic'),(198,25,'Gift Options',11,0,'gift-options',NULL),(199,25,'Autosettings',10,0,'autosettings','advanced'),(200,25,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(201,25,'Design',8,0,'design','advanced'),(202,25,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(203,25,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(204,25,'Images',5,0,'image-management','basic'),(205,25,'Bundle Items',4,0,'bundle-items',NULL),(206,25,'Options',3,0,'options',NULL),(207,25,'Content',2,0,'content','basic'),(208,25,'Product Details',1,1,'product-details','basic'),(209,26,'Gift Options',11,0,'gift-options',NULL),(210,26,'Autosettings',10,0,'autosettings','advanced'),(211,26,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(212,26,'Design',8,0,'design','advanced'),(213,26,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(214,26,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(215,26,'Images',5,0,'image-management','basic'),(216,26,'Bundle Items',4,0,'bundle-items',NULL),(217,26,'Options',3,0,'options',NULL),(218,26,'Content',2,0,'content','basic'),(219,26,'Product Details',1,1,'product-details','basic'),(220,27,'Gift Options',11,0,'gift-options',NULL),(221,27,'Autosettings',10,0,'autosettings','advanced'),(222,27,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(223,27,'Design',8,0,'design','advanced'),(224,27,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(225,27,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(226,27,'Images',5,0,'image-management','basic'),(227,27,'Bundle Items',4,0,'bundle-items',NULL),(228,27,'Options',3,0,'options',NULL),(229,27,'Content',2,0,'content','basic'),(230,27,'Product Details',1,1,'product-details','basic'),(231,28,'Gift Options',11,0,'gift-options',NULL),(232,28,'Autosettings',10,0,'autosettings','advanced'),(233,28,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(234,28,'Design',8,0,'design','advanced'),(235,28,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(236,28,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(237,28,'Images',5,0,'image-management','basic'),(238,28,'Bundle Items',4,0,'bundle-items',NULL),(239,28,'Options',3,0,'options',NULL),(240,28,'Content',2,0,'content','basic'),(241,28,'Product Details',1,1,'product-details','basic'),(242,29,'Gift Options',11,0,'gift-options',NULL),(243,29,'Autosettings',10,0,'autosettings','advanced'),(244,29,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(245,29,'Design',8,0,'design','advanced'),(246,29,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(247,29,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(248,29,'Images',5,0,'image-management','basic'),(249,29,'Bundle Items',4,0,'bundle-items',NULL),(250,29,'Options',3,0,'options',NULL),(251,29,'Content',2,0,'content','basic'),(252,29,'Product Details',1,1,'product-details','basic'),(253,30,'Gift Options',11,0,'gift-options',NULL),(254,30,'Autosettings',10,0,'autosettings','advanced'),(255,30,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(256,30,'Design',8,0,'design','advanced'),(257,30,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(258,30,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(259,30,'Images',5,0,'image-management','basic'),(260,30,'Bundle Items',4,0,'bundle-items',NULL),(261,30,'Options',3,0,'options',NULL),(262,30,'Content',2,0,'content','basic'),(263,30,'Product Details',1,1,'product-details','basic'),(264,31,'Gift Options',11,0,'gift-options',NULL),(265,31,'Autosettings',10,0,'autosettings','advanced'),(266,31,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(267,31,'Design',8,0,'design','advanced'),(268,31,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(269,31,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(270,31,'Images',5,0,'image-management','basic'),(271,31,'Bundle Items',4,0,'bundle-items',NULL),(272,31,'Options',3,0,'options',NULL),(273,31,'Content',2,0,'content','basic'),(274,31,'Product Details',1,1,'product-details','basic'),(275,32,'Gift Options',11,0,'gift-options',NULL),(276,32,'Autosettings',10,0,'autosettings','advanced'),(277,32,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(278,32,'Design',8,0,'design','advanced'),(279,32,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(280,32,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(281,32,'Images',5,0,'image-management','basic'),(282,32,'Bundle Items',4,0,'bundle-items',NULL),(283,32,'Options',3,0,'options',NULL),(284,32,'Content',2,0,'content','basic'),(285,32,'Product Details',1,1,'product-details','basic'),(286,33,'Gift Options',11,0,'gift-options',NULL),(287,33,'Autosettings',10,0,'autosettings','advanced'),(288,33,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(289,33,'Design',8,0,'design','advanced'),(290,33,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(291,33,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(292,33,'Images',5,0,'image-management','basic'),(293,33,'Bundle Items',4,0,'bundle-items',NULL),(294,33,'Options',3,0,'options',NULL),(295,33,'Content',2,0,'content','basic'),(296,33,'Product Details',1,1,'product-details','basic'),(297,34,'Gift Options',11,0,'gift-options',NULL),(298,34,'Autosettings',10,0,'autosettings','advanced'),(299,34,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(300,34,'Design',8,0,'design','advanced'),(301,34,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(302,34,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(303,34,'Images',5,0,'image-management','basic'),(304,34,'Bundle Items',4,0,'bundle-items',NULL),(305,34,'Options',3,0,'options',NULL),(306,34,'Content',2,0,'content','basic'),(307,34,'Product Details',1,1,'product-details','basic'),(308,35,'Gift Options',11,0,'gift-options',NULL),(309,35,'Autosettings',10,0,'autosettings','advanced'),(310,35,'Schedule Design Update',9,0,'schedule-design-update','advanced'),(311,35,'Design',8,0,'design','advanced'),(312,35,'Advanced Pricing',7,0,'advanced-pricing','advanced'),(313,35,'Search Engine Optimization',6,0,'search-engine-optimization','basic'),(314,35,'Images',5,0,'image-management','basic'),(315,35,'Bundle Items',4,0,'bundle-items',NULL),(316,35,'Options',3,0,'options',NULL),(317,35,'Content',2,0,'content','basic'),(318,35,'Product Details',1,1,'product-details','basic');
/*!40000 ALTER TABLE `eav_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_label`
--

DROP TABLE IF EXISTS `eav_attribute_label`;
CREATE TABLE `eav_attribute_label` (
  `attribute_label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Label Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`attribute_label_id`),
  KEY `EAV_ATTRIBUTE_LABEL_STORE_ID` (`store_id`),
  KEY `EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_STORE_ID` (`attribute_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Label';

--
-- Table structure for table `eav_attribute_option`
--

DROP TABLE IF EXISTS `eav_attribute_option`;
CREATE TABLE `eav_attribute_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option';

--
-- Dumping data for table `eav_attribute_option`
--

LOCK TABLES `eav_attribute_option` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option` DISABLE KEYS */;
INSERT INTO `eav_attribute_option` VALUES ('1',20,0),('2',20,1),('3',20,3),('4',138,1),('5',138,2),('6',138,3),('7',138,4),('8',136,1),('9',136,2),('10',135,1),('11',135,2),('12',139,1),('13',139,2),('14',139,3),('17',137,1),('18',137,2),('19',141,1),('20',141,2),('21',141,3),('22',144,1),('23',144,2),('24',144,3),('25',144,4),('26',145,1),('27',145,2),('38',150,1),('39',150,2),('40',150,3),('41',151,1),('42',151,2),('43',151,3),('44',151,4),('45',151,5),('46',151,6),('47',151,7),('48',151,8),('49',145,3),('50',145,4),('51',145,5),('52',145,6),('53',145,7),('54',145,8),('55',145,9),('56',152,1),('57',152,2),('58',152,3),('59',152,4),('60',152,5),('61',152,6),('62',152,7),('63',152,8),('64',152,9),('84',151,9),('85',151,10),('86',151,11),('87',151,12),('88',151,13),('89',151,14),('90',151,16),('91',151,17),('92',151,18),('93',151,19),('94',158,1),('95',158,2),('96',158,3),('97',158,4),('98',158,5),('99',158,6),('100',158,7),('101',158,8),('102',158,9),('103',158,10),('104',158,11),('105',158,12),('106',158,13),('107',158,14),('108',158,15),('109',158,16),('110',158,17),('111',158,18),('112',158,19),('113',158,20),('114',135,3),('115',135,4),('116',135,5),('117',135,6),('118',135,7),('119',135,8),('120',135,9),('121',135,10),('124',160,1),('125',160,3),('126',160,4),('127',160,5),('128',161,1),('129',161,2),('130',161,3),('131',161,4),('132',161,5),('133',161,6),('134',161,7),('135',161,8),('136',161,9),('137',161,10),('138',162,1),('139',162,2),('140',162,3),('141',162,4),('142',162,5),('143',162,6),('144',162,7),('145',162,8),('146',162,9),('147',162,10),('148',162,11),('149',162,12),('150',162,13),('151',162,14),('152',163,1),('153',163,2),('154',163,3),('155',163,4),('156',163,5),('157',163,6),('158',141,4),('159',141,5),('160',141,6),('161',141,7),('162',141,8),('163',141,9),('164',141,10),('165',164,1),('166',164,2),('167',164,3),('168',164,4),('169',165,1),('170',165,2),('171',165,3),('172',165,4),('173',165,5),('174',165,6),('175',165,7),('176',165,8),('177',165,9),('178',165,10),('179',165,11),('180',166,1),('181',166,3),('182',166,2),('183',150,4),('184',150,5),('185',150,6),('186',150,7),('187',167,1),('188',167,2),('189',167,3),('190',150,8),('191',168,1),('192',168,4),('193',168,5),('194',168,8),('195',168,9),('196',168,10),('197',168,3),('198',168,2),('199',168,6),('200',168,7),('201',169,1),('202',169,2),('203',160,2),('204',170,1),('205',170,2),('206',170,3),('207',170,4),('208',170,5),('209',170,6);
/*!40000 ALTER TABLE `eav_attribute_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_option_swatch`
--

DROP TABLE IF EXISTS `eav_attribute_option_swatch`;
CREATE TABLE `eav_attribute_option_swatch` (
  `swatch_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Swatch ID',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `type` smallint(5) unsigned NOT NULL COMMENT 'Swatch type: 0 - text, 1 - visual color, 2 - visual image',
  `value` varchar(255) DEFAULT NULL COMMENT 'Swatch Value',
  PRIMARY KEY (`swatch_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_OPTION_SWATCH_STORE_ID_OPTION_ID` (`store_id`,`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_SWATCH_SWATCH_ID` (`swatch_id`),
  KEY `EAV_ATTR_OPT_SWATCH_OPT_ID_EAV_ATTR_OPT_OPT_ID` (`option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Magento Swatches table';

--
-- Dumping data for table `eav_attribute_option_swatch`
--

LOCK TABLES `eav_attribute_option_swatch` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option_swatch` DISABLE KEYS */;
INSERT INTO `eav_attribute_option_swatch` VALUES ('1','204',0,1,'#ad0707'),('2','205',0,1,'#009c15'),('3','206',0,1,'#f2e200'),('4','207',0,1,'#000000'),('5','208',0,1,'#ffffff'),('6','209',0,1,'#915303');
/*!40000 ALTER TABLE `eav_attribute_option_swatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_option_value`
--

DROP TABLE IF EXISTS `eav_attribute_option_value`;
CREATE TABLE `eav_attribute_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  KEY `EAV_ATTRIBUTE_OPTION_VALUE_OPTION_ID` (`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option Value';

--
-- Dumping data for table `eav_attribute_option_value`
--

LOCK TABLES `eav_attribute_option_value` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option_value` DISABLE KEYS */;
INSERT INTO `eav_attribute_option_value` VALUES ('1','1',0,'Male'),('2','2',0,'Female'),('3','3',0,'Not Specified'),('4','4',0,'Face'),('5','5',0,'Eyes'),('6','6',0,'Lips & Nails'),('7','7',0,'Brushes & Tools'),('8','8',0,'Black'),('9','9',0,'Red'),('17','17',0,'Absolue'),('18','18',0,'Bienfait'),('49','26',0,'Rose'),('50','27',0,'Basil'),('51','49',0,'Citrus'),('52','50',0,'Corriander'),('53','51',0,'Musk'),('54','52',0,'Fruity'),('55','53',0,'Rosemary'),('56','54',0,'Oriental'),('57','55',0,'Spicy'),('58','56',0,'All Types'),('59','57',0,'Combination'),('60','58',0,'Dry'),('61','59',0,'Normal'),('62','60',0,'Normal To Dry'),('63','61',0,'Normal To Oily'),('64','62',0,'Oily'),('65','63',0,'Sensitive'),('66','64',0,'Very Dry'),('86','41',0,'Body Cleanser'),('87','42',0,'Cream'),('88','43',0,'Eau De Parfum'),('89','44',0,'Eau De Toilette'),('90','45',0,'Fragrance'),('91','46',0,'Gift Set'),('92','47',0,'Powder'),('93','48',0,'Serum'),('94','84',0,'Exfoliator'),('95','85',0,'Face Cleanser'),('96','86',0,'Finishing Powder'),('97','87',0,'Face Mask'),('98','88',0,'Makeup Remover'),('99','89',0,'Mascara'),('100','90',0,'Primer'),('101','91',0,'Moisturizer'),('102','92',0,'Sun & Sunless'),('103','93',0,'Toner'),('104','12',0,'Demi-Matte'),('105','13',0,'Gloss'),('106','14',0,'High Shine'),('110','22',0,'Dark'),('111','23',0,'Fair'),('112','24',0,'Ivory'),('113','25',0,'Olive'),('116','94',0,'10 g'),('117','95',0,'20 g'),('118','96',0,'30 g'),('119','97',0,'40 g'),('120','98',0,'50 g'),('121','99',0,'60 g'),('122','100',0,'70 g'),('123','101',0,'80 g'),('124','102',0,'90 g'),('125','103',0,'100 g'),('126','104',0,'110 g'),('127','105',0,'120 g'),('128','106',0,'130 g'),('129','107',0,'140 g'),('130','108',0,'150 g'),('131','109',0,'160 g'),('132','110',0,'170 g'),('133','111',0,'180 g'),('134','112',0,'190 g'),('135','113',0,'200 g'),('136','10',0,'10 ml'),('137','11',0,'20 ml'),('138','114',0,'30 ml'),('139','115',0,'40 ml'),('140','116',0,'50 ml'),('141','117',0,'60 ml'),('142','118',0,'70 ml'),('143','119',0,'80 ml'),('144','120',0,'90 ml'),('145','121',0,'100 ml'),('164','128',0,'Lack of firmness'),('165','129',0,'Pores Wrinkles'),('166','130',0,'Dark Spots'),('167','131',0,'Exceptional Regeneration'),('168','132',0,'Lack of homogeneity'),('169','133',0,'Dullness'),('170','134',0,'Hydration'),('171','135',0,'Dryness'),('172','136',0,'Redness'),('173','137',0,'Fatigue'),('174','138',0,'Acne'),('175','139',0,'Anti-Aging'),('176','140',0,'Clarifying'),('177','141',0,'Dark Circles'),('178','142',0,'Dark Spots'),('179','143',0,'Dryness'),('180','144',0,'Dullness'),('181','145',0,'Firmness'),('182','146',0,'Oiliness'),('183','147',0,'Pores'),('184','148',0,'Purifying'),('185','149',0,'Sensitive Skin'),('186','150',0,'Uneven Skin Tone'),('187','151',0,'Wrinkles'),('188','152',0,'Dermatologist Tested'),('189','153',0,'Hypoallergenic'),('190','154',0,'Non-Comedogenic'),('191','155',0,'Ophthalmologist Tested'),('192','156',0,'Taraben-Free'),('193','157',0,'Perfumed'),('194','19',0,'Cream'),('195','20',0,'Gel'),('196','21',0,'Liquid'),('197','158',0,'Loose Powder'),('198','159',0,'Mineral Powder'),('199','160',0,'Oil'),('200','161',0,'Pencil'),('201','162',0,'Pressed Powder'),('202','163',0,'Stick'),('203','164',0,'Water'),('204','165',0,'Sheer'),('205','166',0,'Full'),('206','167',0,'Light'),('207','168',0,'Medium'),('229','187',0,'Cream'),('230','188',0,'Liquid'),('231','189',0,'Powder'),('239','38',0,'Bath & Shower'),('240','39',0,'Daily Use'),('241','40',0,'Special Occasion'),('242','183',0,'Blending'),('243','184',0,'Contouring & Highlighting'),('244','185',0,'Lining'),('245','186',0,'Powder Application'),('246','190',0,'Other'),('267','191',0,'Alcohol-Free'),('268','198',0,'Buildable'),('269','197',0,'Contains SPF'),('270','192',0,'Dermatologist Tested'),('271','193',0,'Exfoliating'),('272','199',0,'Long-Wear'),('273','200',0,'No Clumps'),('274','194',0,'Non-Comedogenic'),('275','195',0,'Ophthalmologist Tested'),('276','196',0,'Perfumed'),('277','201',0,'Travel Size'),('278','202',0,'Value Size'),('287','124',0,'Body'),('288','203',0,'Eyes'),('289','125',0,'Face'),('290','126',0,'Lips'),('291','127',0,'Legs'),('292','180',0,'Natural'),('293','182',0,'Synthetic'),('294','181',0,'Natural & Synthetic Blend'),('295','169',0,'Anti-Aging'),('296','170',0,'Anti-Aging / Mature'),('297','171',0,'Curling'),('298','172',0,'Defining'),('299','173',0,'Illuminating'),('300','174',0,'Lengthening'),('301','175',0,'Line-Smoothing'),('302','176',0,'Moisturising'),('303','177',0,'Reduces Redness'),('304','178',0,'Shine Control'),('305','179',0,'Volumizing'),('306','204',0,'Red'),('307','205',0,'Green'),('308','206',0,'Yellow'),('309','207',0,'Black'),('310','208',0,'White'),('311','209',0,'Brown');
/*!40000 ALTER TABLE `eav_attribute_option_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_set`
--

DROP TABLE IF EXISTS `eav_attribute_set`;
CREATE TABLE `eav_attribute_set` (
  `attribute_set_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Set Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Set Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`attribute_set_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_ATTRIBUTE_SET_NAME` (`entity_type_id`,`attribute_set_name`),
  KEY `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_SORT_ORDER` (`entity_type_id`,`sort_order`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Set';

--
-- Dumping data for table `eav_attribute_set`
--

LOCK TABLES `eav_attribute_set` WRITE;
/*!40000 ALTER TABLE `eav_attribute_set` DISABLE KEYS */;
INSERT INTO `eav_attribute_set` VALUES (1,1,'Default',2),(2,2,'Default',2),(3,3,'Default',1),(4,4,'Default',1),(5,5,'Default',1),(6,6,'Default',1),(7,7,'Default',1),(8,8,'Default',1),(19,4,'Sun Care & Body',0),(20,4,'Candles',0),(22,4,'Serums & Treatments',0),(23,4,'Moisturizers',0),(24,4,'Masks',0),(25,4,'Makeup - Eyes',0),(26,4,'Makeup - Face',0),(27,4,'Makeup - Nails',0),(28,4,'Makeup - Lips',0),(29,4,'Brushes & Tools',0),(30,4,'Cleanser & Toners',0),(31,4,'Eye Care',0),(32,4,'Lip Care',0),(33,4,'Fragrance',0),(34,4,'Skin Care',0),(35,4,'Cosmetic',0);
/*!40000 ALTER TABLE `eav_attribute_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity`
--

DROP TABLE IF EXISTS `eav_entity`;
CREATE TABLE `eav_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Entity Active',
  PRIMARY KEY (`entity_id`),
  KEY `EAV_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity';

--
-- Table structure for table `eav_entity_attribute`
--

DROP TABLE IF EXISTS `eav_entity_attribute`;
CREATE TABLE `eav_entity_attribute` (
  `entity_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`entity_attribute_id`),
  UNIQUE KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_ATTRIBUTE_ID` (`attribute_set_id`,`attribute_id`),
  UNIQUE KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_GROUP_ID_ATTRIBUTE_ID` (`attribute_group_id`,`attribute_id`),
  KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`),
  KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10547 DEFAULT CHARSET=utf8 COMMENT='Eav Entity Attributes';

--
-- Dumping data for table `eav_entity_attribute`
--

LOCK TABLES `eav_entity_attribute` WRITE;
/*!40000 ALTER TABLE `eav_entity_attribute` DISABLE KEYS */;
INSERT INTO `eav_entity_attribute` VALUES ('1',1,1,1,1,10),('2',1,1,1,2,20),('3',1,1,1,3,20),('4',1,1,1,4,30),('5',1,1,1,5,40),('6',1,1,1,6,50),('7',1,1,1,7,60),('8',1,1,1,8,70),('9',1,1,1,9,80),('10',1,1,1,10,25),('11',1,1,1,11,90),('12',1,1,1,12,81),('13',1,1,1,13,115),('14',1,1,1,14,120),('15',1,1,1,15,82),('16',1,1,1,16,83),('17',1,1,1,17,100),('18',1,1,1,18,85),('19',1,1,1,19,86),('20',1,1,1,20,110),('21',1,1,1,21,121),('22',2,2,2,22,10),('23',2,2,2,23,20),('24',2,2,2,24,30),('25',2,2,2,25,40),('26',2,2,2,26,50),('27',2,2,2,27,60),('28',2,2,2,28,70),('29',2,2,2,29,80),('30',2,2,2,30,90),('31',2,2,2,31,100),('32',2,2,2,32,100),('33',2,2,2,33,110),('34',2,2,2,34,120),('35',2,2,2,35,130),('36',2,2,2,36,131),('37',2,2,2,37,132),('38',2,2,2,38,133),('39',2,2,2,39,134),('40',2,2,2,40,135),('41',1,1,1,41,87),('42',1,1,1,42,100),('43',1,1,1,43,110),('44',1,1,1,44,120),('45',3,3,4,45,1),('46',3,3,4,46,2),('47',3,3,4,47,4),('48',3,3,4,48,5),('49',3,3,4,49,6),('50',3,3,4,50,7),('51',3,3,4,51,8),('52',3,3,5,52,10),('53',3,3,5,53,20),('54',3,3,5,54,30),('55',3,3,4,55,12),('56',3,3,4,56,13),('57',3,3,4,57,14),('58',3,3,4,58,15),('59',3,3,4,59,16),('60',3,3,6,60,10),('61',3,3,6,61,30),('62',3,3,6,62,40),('63',3,3,6,63,50),('64',3,3,6,64,60),('65',3,3,4,65,24),('66',3,3,4,66,25),('67',3,3,5,67,40),('68',3,3,5,68,50),('69',3,3,4,69,10),('70',3,3,6,70,5),('71',3,3,6,71,6),('72',3,3,5,72,51),('90',4,4,7,91,6),('96',4,4,8,98,8),('105',4,4,7,107,14),('106',4,4,7,108,15),('107',4,4,7,109,16),('108',4,4,7,110,17),('109',4,4,7,111,18),('110',4,4,7,112,19),('111',4,4,7,113,20),('115',3,3,4,117,3),('116',3,3,4,118,17),('118',4,4,7,120,11),('126',4,4,7,128,111),('127',4,4,7,129,112),('128',4,4,7,130,113),('129',4,4,7,131,114),('3526',4,19,136,98,8),('3612',4,19,142,91,6),('3626',4,19,142,120,11),('3634',4,19,142,107,14),('3636',4,19,142,108,15),('3640',4,19,142,109,16),('3642',4,19,142,110,17),('3644',4,19,142,111,18),('3646',4,19,142,112,19),('3648',4,19,142,113,20),('3650',4,19,142,128,111),('3652',4,19,142,129,112),('3654',4,19,142,130,113),('3656',4,19,142,131,114),('3822',4,20,147,98,8),('3908',4,20,153,91,6),('3922',4,20,153,120,11),('3930',4,20,153,107,14),('3932',4,20,153,108,15),('3936',4,20,153,109,16),('3938',4,20,153,110,17),('3940',4,20,153,111,18),('3942',4,20,153,112,19),('3944',4,20,153,113,20),('3946',4,20,153,128,111),('3948',4,20,153,129,112),('3950',4,20,153,130,113),('3952',4,20,153,131,114),('4554',4,22,169,98,8),('4646',4,22,175,91,6),('4660',4,22,175,120,11),('4668',4,22,175,107,14),('4670',4,22,175,108,15),('4674',4,22,175,109,16),('4676',4,22,175,110,17),('4678',4,22,175,111,18),('4680',4,22,175,112,19),('4682',4,22,175,113,20),('4684',4,22,175,128,111),('4686',4,22,175,129,112),('4688',4,22,175,130,113),('4690',4,22,175,131,114),('4722',4,23,180,98,8),('4814',4,23,186,91,6),('4828',4,23,186,120,11),('4836',4,23,186,107,14),('4838',4,23,186,108,15),('4842',4,23,186,109,16),('4844',4,23,186,110,17),('4846',4,23,186,111,18),('4848',4,23,186,112,19),('4850',4,23,186,113,20),('4852',4,23,186,128,111),('4854',4,23,186,129,112),('4856',4,23,186,130,113),('4858',4,23,186,131,114),('4890',4,24,191,98,8),('4982',4,24,197,91,6),('4996',4,24,197,120,11),('5004',4,24,197,107,14),('5006',4,24,197,108,15),('5010',4,24,197,109,16),('5012',4,24,197,110,17),('5014',4,24,197,111,18),('5016',4,24,197,112,19),('5018',4,24,197,113,20),('5020',4,24,197,128,111),('5022',4,24,197,129,112),('5024',4,24,197,130,113),('5026',4,24,197,131,114),('5058',4,25,202,98,8),('5150',4,25,208,91,6),('5164',4,25,208,120,11),('5172',4,25,208,107,14),('5174',4,25,208,108,15),('5178',4,25,208,109,16),('5180',4,25,208,110,17),('5182',4,25,208,111,18),('5184',4,25,208,112,19),('5186',4,25,208,113,20),('5188',4,25,208,128,111),('5190',4,25,208,129,112),('5192',4,25,208,130,113),('5194',4,25,208,131,114),('5226',4,26,213,98,8),('5318',4,26,219,91,6),('5332',4,26,219,120,11),('5340',4,26,219,107,14),('5342',4,26,219,108,15),('5346',4,26,219,109,16),('5348',4,26,219,110,17),('5350',4,26,219,111,18),('5352',4,26,219,112,19),('5354',4,26,219,113,20),('5356',4,26,219,128,111),('5358',4,26,219,129,112),('5360',4,26,219,130,113),('5362',4,26,219,131,114),('5394',4,27,224,98,8),('5486',4,27,230,91,6),('5500',4,27,230,120,11),('5508',4,27,230,107,14),('5510',4,27,230,108,15),('5514',4,27,230,109,16),('5516',4,27,230,110,17),('5518',4,27,230,111,18),('5520',4,27,230,112,19),('5522',4,27,230,113,20),('5524',4,27,230,128,111),('5526',4,27,230,129,112),('5528',4,27,230,130,113),('5530',4,27,230,131,114),('5562',4,28,235,98,8),('5654',4,28,241,91,6),('5668',4,28,241,120,11),('5676',4,28,241,107,14),('5678',4,28,241,108,15),('5682',4,28,241,109,16),('5684',4,28,241,110,17),('5686',4,28,241,111,18),('5688',4,28,241,112,19),('5690',4,28,241,113,20),('5692',4,28,241,128,111),('5694',4,28,241,129,112),('5696',4,28,241,130,113),('5698',4,28,241,131,114),('5730',4,29,246,98,8),('5822',4,29,252,91,6),('5836',4,29,252,120,11),('5844',4,29,252,107,14),('5846',4,29,252,108,15),('5850',4,29,252,109,16),('5852',4,29,252,110,17),('5854',4,29,252,111,18),('5856',4,29,252,112,19),('5858',4,29,252,113,20),('5860',4,29,252,128,111),('5862',4,29,252,129,112),('5864',4,29,252,130,113),('5866',4,29,252,131,114),('5898',4,30,257,98,8),('5990',4,30,263,91,6),('6004',4,30,263,120,11),('6012',4,30,263,107,14),('6014',4,30,263,108,15),('6018',4,30,263,109,16),('6020',4,30,263,110,17),('6022',4,30,263,111,18),('6024',4,30,263,112,19),('6026',4,30,263,113,20),('6028',4,30,263,128,111),('6030',4,30,263,129,112),('6032',4,30,263,130,113),('6034',4,30,263,131,114),('6066',4,31,268,98,8),('6158',4,31,274,91,6),('6172',4,31,274,120,11),('6180',4,31,274,107,14),('6182',4,31,274,108,15),('6186',4,31,274,109,16),('6188',4,31,274,110,17),('6190',4,31,274,111,18),('6192',4,31,274,112,19),('6194',4,31,274,113,20),('6196',4,31,274,128,111),('6198',4,31,274,129,112),('6200',4,31,274,130,113),('6202',4,31,274,131,114),('6514',4,32,279,98,8),('6606',4,32,285,91,6),('6620',4,32,285,120,11),('6628',4,32,285,107,14),('6630',4,32,285,108,15),('6634',4,32,285,109,16),('6636',4,32,285,110,17),('6638',4,32,285,111,18),('6640',4,32,285,112,19),('6642',4,32,285,113,20),('6644',4,32,285,128,111),('6646',4,32,285,129,112),('6648',4,32,285,130,113),('6650',4,32,285,131,114),('6682',4,33,290,98,8),('6774',4,33,296,91,6),('6788',4,33,296,120,11),('6796',4,33,296,107,14),('6798',4,33,296,108,15),('6802',4,33,296,109,16),('6804',4,33,296,110,17),('6806',4,33,296,111,18),('6808',4,33,296,112,19),('6810',4,33,296,113,20),('6812',4,33,296,128,111),('6814',4,33,296,129,112),('6816',4,33,296,130,113),('6818',4,33,296,131,114),('6820',4,4,7,73,2),('6822',4,4,7,74,3),('6824',4,4,7,77,5),('6826',4,4,7,82,9),('6828',4,4,7,94,13),('6830',4,4,7,95,14),('6832',4,4,7,97,1),('6834',4,4,7,99,12),('6836',4,4,7,105,11),('6838',4,4,7,114,15),('6840',4,4,7,115,8),('6842',4,4,7,123,6),('6844',4,4,7,124,4),('6846',4,4,7,125,10),('6848',4,4,7,133,7),('6850',4,4,13,75,9),('6852',4,4,13,76,10),('6854',4,4,13,142,1),('6856',4,4,13,143,4),('6858',4,4,13,147,7),('6860',4,4,13,148,2),('6862',4,4,13,149,8),('6864',4,4,13,155,3),('6866',4,4,13,156,6),('6868',4,4,13,157,5),('6870',4,4,21,135,13),('6872',4,4,21,136,5),('6874',4,4,21,137,4),('6876',4,4,21,138,3),('6878',4,4,21,139,9),('6880',4,4,21,141,10),('6882',4,4,21,144,16),('6884',4,4,21,145,12),('6886',4,4,21,150,18),('6888',4,4,21,151,11),('6890',4,4,21,152,17),('6892',4,4,21,158,14),('6894',4,4,21,160,1),('6896',4,4,21,161,15),('6898',4,4,21,162,6),('6900',4,4,21,163,8),('6902',4,4,21,164,7),('6904',4,4,21,165,2),('6906',4,4,15,127,1),('6908',4,4,10,87,1),('6910',4,4,10,88,2),('6912',4,4,10,89,3),('6914',4,4,10,90,5),('6916',4,4,10,96,6),('6918',4,4,10,132,4),('6920',4,4,9,84,2),('6922',4,4,9,85,3),('6924',4,4,9,86,4),('6926',4,4,9,119,1),('6928',4,4,8,78,1),('6930',4,4,8,79,2),('6932',4,4,8,80,3),('6934',4,4,8,81,4),('6936',4,4,8,92,5),('6938',4,4,8,121,6),('6940',4,4,8,122,7),('6942',4,4,8,126,8),('6944',4,4,11,103,3),('6946',4,4,11,104,1),('6948',4,4,11,106,2),('6950',4,4,14,100,3),('6952',4,4,14,101,1),('6954',4,4,14,102,2),('6956',4,4,14,116,4),('6958',4,4,20,134,1),('8050',4,28,241,73,2),('8052',4,28,241,74,3),('8054',4,28,241,77,5),('8056',4,28,241,82,9),('8058',4,28,241,94,13),('8060',4,28,241,95,14),('8062',4,28,241,97,1),('8064',4,28,241,99,12),('8066',4,28,241,105,11),('8068',4,28,241,114,15),('8070',4,28,241,115,8),('8072',4,28,241,123,6),('8074',4,28,241,124,4),('8076',4,28,241,125,10),('8078',4,28,241,133,7),('8080',4,28,240,75,9),('8082',4,28,240,76,10),('8084',4,28,240,142,1),('8086',4,28,240,143,4),('8088',4,28,240,147,7),('8090',4,28,240,148,2),('8092',4,28,240,149,8),('8094',4,28,240,155,3),('8096',4,28,240,156,6),('8098',4,28,240,157,5),('8100',4,28,239,136,2),('8102',4,28,239,137,5),('8104',4,28,239,139,3),('8106',4,28,239,151,1),('8108',4,28,239,164,6),('8110',4,28,239,165,4),('8112',4,28,239,168,7),('8114',4,28,238,127,1),('8116',4,28,237,87,1),('8118',4,28,237,88,2),('8120',4,28,237,89,3),('8122',4,28,237,90,5),('8124',4,28,237,96,6),('8126',4,28,237,132,4),('8128',4,28,236,84,2),('8130',4,28,236,85,3),('8132',4,28,236,86,4),('8134',4,28,236,119,1),('8136',4,28,235,78,1),('8138',4,28,235,79,2),('8140',4,28,235,80,3),('8142',4,28,235,81,4),('8144',4,28,235,92,5),('8146',4,28,235,121,6),('8148',4,28,235,122,7),('8150',4,28,235,126,8),('8152',4,28,234,103,3),('8154',4,28,234,104,1),('8156',4,28,234,106,2),('8158',4,28,233,100,3),('8160',4,28,233,101,1),('8162',4,28,233,102,2),('8164',4,28,233,116,4),('8166',4,28,231,134,1),('8168',4,27,230,73,2),('8170',4,27,230,74,3),('8172',4,27,230,77,5),('8174',4,27,230,82,9),('8176',4,27,230,94,13),('8178',4,27,230,95,14),('8180',4,27,230,97,1),('8182',4,27,230,99,12),('8184',4,27,230,105,11),('8186',4,27,230,114,15),('8188',4,27,230,115,8),('8190',4,27,230,123,6),('8192',4,27,230,124,4),('8194',4,27,230,125,10),('8196',4,27,230,133,7),('8198',4,27,229,75,9),('8200',4,27,229,76,10),('8202',4,27,229,142,1),('8204',4,27,229,143,4),('8206',4,27,229,147,7),('8208',4,27,229,148,2),('8210',4,27,229,149,8),('8212',4,27,229,155,3),('8214',4,27,229,156,6),('8216',4,27,229,157,5),('8218',4,27,228,136,1),('8220',4,27,228,139,2),('8222',4,27,228,168,3),('8224',4,27,227,127,1),('8226',4,27,226,87,1),('8228',4,27,226,88,2),('8230',4,27,226,89,3),('8232',4,27,226,90,5),('8234',4,27,226,96,6),('8236',4,27,226,132,4),('8238',4,27,225,84,2),('8240',4,27,225,85,3),('8242',4,27,225,86,4),('8244',4,27,225,119,1),('8246',4,27,224,78,1),('8248',4,27,224,79,2),('8250',4,27,224,80,3),('8252',4,27,224,81,4),('8254',4,27,224,92,5),('8256',4,27,224,121,6),('8258',4,27,224,122,7),('8260',4,27,224,126,8),('8262',4,27,223,103,3),('8264',4,27,223,104,1),('8266',4,27,223,106,2),('8268',4,27,222,100,3),('8270',4,27,222,101,1),('8272',4,27,222,102,2),('8274',4,27,222,116,4),('8276',4,27,220,134,1),('8278',4,29,252,73,2),('8280',4,29,252,74,3),('8282',4,29,252,77,5),('8284',4,29,252,82,9),('8286',4,29,252,94,13),('8288',4,29,252,95,14),('8290',4,29,252,97,1),('8292',4,29,252,99,12),('8294',4,29,252,105,11),('8296',4,29,252,114,15),('8298',4,29,252,115,8),('8300',4,29,252,123,6),('8302',4,29,252,124,4),('8304',4,29,252,125,10),('8306',4,29,252,133,7),('8308',4,29,251,75,9),('8310',4,29,251,76,10),('8312',4,29,251,142,1),('8314',4,29,251,143,4),('8316',4,29,251,147,7),('8318',4,29,251,148,2),('8320',4,29,251,149,8),('8322',4,29,251,155,3),('8324',4,29,251,156,6),('8326',4,29,251,157,5),('8328',4,29,250,150,3),('8330',4,29,250,151,1),('8332',4,29,250,160,2),('8334',4,29,250,166,5),('8336',4,29,250,167,4),('8338',4,29,249,127,1),('8340',4,29,248,87,1),('8342',4,29,248,88,2),('8344',4,29,248,89,3),('8346',4,29,248,90,5),('8348',4,29,248,96,6),('8350',4,29,248,132,4),('8352',4,29,247,84,2),('8354',4,29,247,85,3),('8356',4,29,247,86,4),('8358',4,29,247,119,1),('8360',4,29,246,78,1),('8362',4,29,246,79,2),('8364',4,29,246,80,3),('8366',4,29,246,81,4),('8368',4,29,246,92,5),('8370',4,29,246,121,6),('8372',4,29,246,122,7),('8374',4,29,246,126,8),('8376',4,29,245,103,3),('8378',4,29,245,104,1),('8380',4,29,245,106,2),('8382',4,29,244,100,3),('8384',4,29,244,101,1),('8386',4,29,244,102,2),('8388',4,29,244,116,4),('8390',4,29,242,134,1),('8392',4,20,153,73,2),('8394',4,20,153,74,3),('8396',4,20,153,77,5),('8398',4,20,153,82,9),('8400',4,20,153,94,13),('8402',4,20,153,95,14),('8404',4,20,153,97,1),('8406',4,20,153,99,12),('8408',4,20,153,105,11),('8410',4,20,153,114,15),('8412',4,20,153,115,8),('8414',4,20,153,123,6),('8416',4,20,153,124,4),('8418',4,20,153,125,10),('8420',4,20,153,133,7),('8422',4,20,152,75,9),('8424',4,20,152,76,10),('8426',4,20,152,142,1),('8428',4,20,152,143,4),('8430',4,20,152,147,7),('8432',4,20,152,148,2),('8434',4,20,152,149,8),('8436',4,20,152,155,3),('8438',4,20,152,156,6),('8440',4,20,152,157,5),('8442',4,20,151,137,2),('8444',4,20,151,141,5),('8446',4,20,151,145,3),('8448',4,20,151,150,4),('8450',4,20,151,151,1),('8452',4,20,150,127,1),('8454',4,20,149,87,1),('8456',4,20,149,88,2),('8458',4,20,149,89,3),('8460',4,20,149,90,5),('8462',4,20,149,96,6),('8464',4,20,149,132,4),('8466',4,20,148,84,2),('8468',4,20,148,85,3),('8470',4,20,148,86,4),('8472',4,20,148,119,1),('8474',4,20,147,78,1),('8476',4,20,147,79,2),('8478',4,20,147,80,3),('8480',4,20,147,81,4),('8482',4,20,147,92,5),('8484',4,20,147,121,6),('8486',4,20,147,122,7),('8488',4,20,147,126,8),('8490',4,20,146,103,3),('8492',4,20,146,104,1),('8494',4,20,146,106,2),('8496',4,20,145,100,3),('8498',4,20,145,101,1),('8500',4,20,145,102,2),('8502',4,20,145,116,4),('8504',4,20,143,134,1),('8506',4,30,263,73,2),('8508',4,30,263,74,3),('8510',4,30,263,77,5),('8512',4,30,263,82,9),('8514',4,30,263,94,13),('8516',4,30,263,95,14),('8518',4,30,263,97,1),('8520',4,30,263,99,12),('8522',4,30,263,105,11),('8524',4,30,263,114,15),('8526',4,30,263,115,8),('8528',4,30,263,123,6),('8530',4,30,263,124,4),('8532',4,30,263,125,10),('8534',4,30,263,133,7),('8536',4,30,262,75,9),('8538',4,30,262,76,10),('8540',4,30,262,142,1),('8542',4,30,262,143,4),('8544',4,30,262,147,7),('8546',4,30,262,148,2),('8548',4,30,262,149,8),('8550',4,30,262,155,3),('8552',4,30,262,156,6),('8554',4,30,262,157,5),('8556',4,30,261,137,3),('8558',4,30,261,141,7),('8560',4,30,261,151,1),('8562',4,30,261,152,4),('8564',4,30,261,160,6),('8566',4,30,261,162,2),('8568',4,30,261,168,5),('8570',4,30,260,127,1),('8572',4,30,259,87,1),('8574',4,30,259,88,2),('8576',4,30,259,89,3),('8578',4,30,259,90,5),('8580',4,30,259,96,6),('8582',4,30,259,132,4),('8584',4,30,258,84,2),('8586',4,30,258,85,3),('8588',4,30,258,86,4),('8590',4,30,258,119,1),('8592',4,30,257,78,1),('8594',4,30,257,79,2),('8596',4,30,257,80,3),('8598',4,30,257,81,4),('8600',4,30,257,92,5),('8602',4,30,257,121,6),('8604',4,30,257,122,7),('8606',4,30,257,126,8),('8608',4,30,256,103,3),('8610',4,30,256,104,1),('8612',4,30,256,106,2),('8614',4,30,255,100,3),('8616',4,30,255,101,1),('8618',4,30,255,102,2),('8620',4,30,255,116,4),('8622',4,30,253,134,1),('8624',4,31,274,73,2),('8626',4,31,274,74,3),('8628',4,31,274,77,5),('8630',4,31,274,82,9),('8632',4,31,274,94,13),('8634',4,31,274,95,14),('8636',4,31,274,97,1),('8638',4,31,274,99,12),('8640',4,31,274,105,11),('8642',4,31,274,114,15),('8644',4,31,274,115,8),('8646',4,31,274,123,6),('8648',4,31,274,124,4),('8650',4,31,274,125,10),('8652',4,31,274,133,7),('8654',4,31,273,75,9),('8656',4,31,273,76,10),('8658',4,31,273,142,1),('8660',4,31,273,143,4),('8662',4,31,273,147,7),('8664',4,31,273,148,2),('8666',4,31,273,149,8),('8668',4,31,273,155,3),('8670',4,31,273,156,6),('8672',4,31,273,157,5),('8674',4,31,272,137,3),('8676',4,31,272,141,7),('8678',4,31,272,151,1),('8680',4,31,272,152,4),('8682',4,31,272,160,6),('8684',4,31,272,162,2),('8686',4,31,272,168,5),('8688',4,31,271,127,1),('8690',4,31,270,87,1),('8692',4,31,270,88,2),('8694',4,31,270,89,3),('8696',4,31,270,90,5),('8698',4,31,270,96,6),('8700',4,31,270,132,4),('8702',4,31,269,84,2),('8704',4,31,269,85,3),('8706',4,31,269,86,4),('8708',4,31,269,119,1),('8710',4,31,268,78,1),('8712',4,31,268,79,2),('8714',4,31,268,80,3),('8716',4,31,268,81,4),('8718',4,31,268,92,5),('8720',4,31,268,121,6),('8722',4,31,268,122,7),('8724',4,31,268,126,8),('8726',4,31,267,103,3),('8728',4,31,267,104,1),('8730',4,31,267,106,2),('8732',4,31,266,100,3),('8734',4,31,266,101,1),('8736',4,31,266,102,2),('8738',4,31,266,116,4),('8740',4,31,264,134,1),('8856',4,32,285,73,2),('8858',4,32,285,74,3),('8860',4,32,285,77,5),('8862',4,32,285,82,9),('8864',4,32,285,94,13),('8866',4,32,285,95,14),('8868',4,32,285,97,1),('8870',4,32,285,99,12),('8872',4,32,285,105,11),('8874',4,32,285,114,15),('8876',4,32,285,115,8),('8878',4,32,285,123,6),('8880',4,32,285,124,4),('8882',4,32,285,125,10),('8884',4,32,285,133,7),('8886',4,32,284,75,9),('8888',4,32,284,76,10),('8890',4,32,284,142,1),('8892',4,32,284,143,4),('8894',4,32,284,147,7),('8896',4,32,284,148,2),('8898',4,32,284,149,8),('8900',4,32,284,155,3),('8902',4,32,284,156,6),('8904',4,32,284,157,5),('8906',4,32,283,136,2),('8908',4,32,283,137,5),('8910',4,32,283,139,3),('8912',4,32,283,151,1),('8914',4,32,283,164,6),('8916',4,32,283,165,4),('8918',4,32,283,168,7),('8920',4,32,282,127,1),('8922',4,32,281,87,1),('8924',4,32,281,88,2),('8926',4,32,281,89,3),('8928',4,32,281,90,5),('8930',4,32,281,96,6),('8932',4,32,281,132,4),('8934',4,32,280,84,2),('8936',4,32,280,85,3),('8938',4,32,280,86,4),('8940',4,32,280,119,1),('8942',4,32,279,78,1),('8944',4,32,279,79,2),('8946',4,32,279,80,3),('8948',4,32,279,81,4),('8950',4,32,279,92,5),('8952',4,32,279,121,6),('8954',4,32,279,122,7),('8956',4,32,279,126,8),('8958',4,32,278,103,3),('8960',4,32,278,104,1),('8962',4,32,278,106,2),('8964',4,32,277,100,3),('8966',4,32,277,101,1),('8968',4,32,277,102,2),('8970',4,32,277,116,4),('8972',4,32,275,134,1),('8974',4,25,208,73,2),('8976',4,25,208,74,3),('8978',4,25,208,77,5),('8980',4,25,208,82,9),('8982',4,25,208,94,13),('8984',4,25,208,95,14),('8986',4,25,208,97,1),('8988',4,25,208,99,12),('8990',4,25,208,105,11),('8992',4,25,208,114,15),('8994',4,25,208,115,8),('8996',4,25,208,123,6),('8998',4,25,208,124,4),('9000',4,25,208,125,10),('9002',4,25,208,133,7),('9004',4,25,207,75,9),('9006',4,25,207,76,10),('9008',4,25,207,142,1),('9010',4,25,207,143,4),('9012',4,25,207,147,7),('9014',4,25,207,148,2),('9016',4,25,207,149,8),('9018',4,25,207,155,3),('9020',4,25,207,156,6),('9022',4,25,207,157,5),('9024',4,25,206,136,2),('9026',4,25,206,137,5),('9028',4,25,206,139,3),('9030',4,25,206,151,1),('9032',4,25,206,163,8),('9034',4,25,206,164,6),('9036',4,25,206,165,4),('9038',4,25,206,168,7),('9040',4,25,205,127,1),('9042',4,25,204,87,1),('9044',4,25,204,88,2),('9046',4,25,204,89,3),('9048',4,25,204,90,5),('9050',4,25,204,96,6),('9052',4,25,204,132,4),('9054',4,25,203,84,2),('9056',4,25,203,85,3),('9058',4,25,203,86,4),('9060',4,25,203,119,1),('9062',4,25,202,78,1),('9064',4,25,202,79,2),('9066',4,25,202,80,3),('9068',4,25,202,81,4),('9070',4,25,202,92,5),('9072',4,25,202,121,6),('9074',4,25,202,122,7),('9076',4,25,202,126,8),('9078',4,25,201,103,3),('9080',4,25,201,104,1),('9082',4,25,201,106,2),('9084',4,25,200,100,3),('9086',4,25,200,101,1),('9088',4,25,200,102,2),('9090',4,25,200,116,4),('9092',4,25,198,134,1),('9094',4,26,219,73,2),('9096',4,26,219,74,3),('9098',4,26,219,77,5),('9100',4,26,219,82,9),('9102',4,26,219,94,13),('9104',4,26,219,95,14),('9106',4,26,219,97,1),('9108',4,26,219,99,12),('9110',4,26,219,105,11),('9112',4,26,219,114,15),('9114',4,26,219,115,8),('9116',4,26,219,123,6),('9118',4,26,219,124,4),('9120',4,26,219,125,10),('9122',4,26,219,133,7),('9124',4,26,218,75,9),('9126',4,26,218,76,10),('9128',4,26,218,142,1),('9130',4,26,218,143,4),('9132',4,26,218,147,7),('9134',4,26,218,148,2),('9136',4,26,218,149,8),('9138',4,26,218,155,3),('9140',4,26,218,156,6),('9142',4,26,218,157,5),('9144',4,26,217,136,8),('9146',4,26,217,137,10),('9148',4,26,217,139,4),('9150',4,26,217,141,2),('9152',4,26,217,144,7),('9154',4,26,217,151,1),('9156',4,26,217,152,3),('9158',4,26,217,163,9),('9160',4,26,217,164,5),('9162',4,26,217,165,6),('9164',4,26,216,127,1),('9166',4,26,215,87,1),('9168',4,26,215,88,2),('9170',4,26,215,89,3),('9172',4,26,215,90,5),('9174',4,26,215,96,6),('9176',4,26,215,132,4),('9178',4,26,214,84,2),('9180',4,26,214,85,3),('9182',4,26,214,86,4),('9184',4,26,214,119,1),('9186',4,26,213,78,1),('9188',4,26,213,79,2),('9190',4,26,213,80,3),('9192',4,26,213,81,4),('9194',4,26,213,92,5),('9196',4,26,213,121,6),('9198',4,26,213,122,7),('9200',4,26,213,126,8),('9202',4,26,212,103,3),('9204',4,26,212,104,1),('9206',4,26,212,106,2),('9208',4,26,211,100,3),('9210',4,26,211,101,1),('9212',4,26,211,102,2),('9214',4,26,211,116,4),('9216',4,26,209,134,1),('9218',4,24,197,73,2),('9220',4,24,197,74,3),('9222',4,24,197,77,5),('9224',4,24,197,82,9),('9226',4,24,197,94,13),('9228',4,24,197,95,14),('9230',4,24,197,97,1),('9232',4,24,197,99,12),('9234',4,24,197,105,11),('9236',4,24,197,114,15),('9238',4,24,197,115,8),('9240',4,24,197,123,6),('9242',4,24,197,124,4),('9244',4,24,197,125,10),('9246',4,24,197,133,7),('9248',4,24,196,75,9),('9250',4,24,196,76,10),('9252',4,24,196,142,1),('9254',4,24,196,143,4),('9256',4,24,196,147,7),('9258',4,24,196,148,2),('9260',4,24,196,149,8),('9262',4,24,196,155,3),('9264',4,24,196,156,6),('9266',4,24,196,157,5),('9268',4,24,195,137,3),('9270',4,24,195,141,6),('9272',4,24,195,151,1),('9274',4,24,195,152,4),('9276',4,24,195,162,2),('9278',4,24,195,168,5),('9280',4,24,194,127,1),('9282',4,24,193,87,1),('9284',4,24,193,88,2),('9286',4,24,193,89,3),('9288',4,24,193,90,5),('9290',4,24,193,96,6),('9292',4,24,193,132,4),('9294',4,24,192,84,2),('9296',4,24,192,85,3),('9298',4,24,192,86,4),('9300',4,24,192,119,1),('9302',4,24,191,78,1),('9304',4,24,191,79,2),('9306',4,24,191,80,3),('9308',4,24,191,81,4),('9310',4,24,191,92,5),('9312',4,24,191,121,6),('9314',4,24,191,122,7),('9316',4,24,191,126,8),('9318',4,24,190,103,3),('9320',4,24,190,104,1),('9322',4,24,190,106,2),('9324',4,24,189,100,3),('9326',4,24,189,101,1),('9328',4,24,189,102,2),('9330',4,24,189,116,4),('9332',4,24,187,134,1),('9334',4,23,186,73,2),('9336',4,23,186,74,3),('9338',4,23,186,77,5),('9340',4,23,186,82,9),('9342',4,23,186,94,13),('9344',4,23,186,95,14),('9346',4,23,186,97,1),('9348',4,23,186,99,12),('9350',4,23,186,105,11),('9352',4,23,186,114,15),('9354',4,23,186,115,8),('9356',4,23,186,123,6),('9358',4,23,186,124,4),('9360',4,23,186,125,10),('9362',4,23,186,133,7),('9364',4,23,185,75,9),('9366',4,23,185,76,10),('9368',4,23,185,142,1),('9370',4,23,185,143,4),('9372',4,23,185,147,7),('9374',4,23,185,148,2),('9376',4,23,185,149,8),('9378',4,23,185,155,3),('9380',4,23,185,156,6),('9382',4,23,185,157,5),('9384',4,23,184,137,3),('9386',4,23,184,141,7),('9388',4,23,184,151,1),('9390',4,23,184,152,4),('9392',4,23,184,160,6),('9394',4,23,184,162,2),('9396',4,23,184,168,5),('9398',4,23,183,127,1),('9400',4,23,182,87,1),('9402',4,23,182,88,2),('9404',4,23,182,89,3),('9406',4,23,182,90,5),('9408',4,23,182,96,6),('9410',4,23,182,132,4),('9412',4,23,181,84,2),('9414',4,23,181,85,3),('9416',4,23,181,86,4),('9418',4,23,181,119,1),('9420',4,23,180,78,1),('9422',4,23,180,79,2),('9424',4,23,180,80,3),('9426',4,23,180,81,4),('9428',4,23,180,92,5),('9430',4,23,180,121,6),('9432',4,23,180,122,7),('9434',4,23,180,126,8),('9436',4,23,179,103,3),('9438',4,23,179,104,1),('9440',4,23,179,106,2),('9442',4,23,178,100,3),('9444',4,23,178,101,1),('9446',4,23,178,102,2),('9448',4,23,178,116,4),('9450',4,23,176,134,1),('9452',4,22,175,73,2),('9454',4,22,175,74,3),('9456',4,22,175,77,5),('9458',4,22,175,82,9),('9460',4,22,175,94,13),('9462',4,22,175,95,14),('9464',4,22,175,97,1),('9466',4,22,175,99,12),('9468',4,22,175,105,11),('9470',4,22,175,114,15),('9472',4,22,175,115,8),('9474',4,22,175,123,6),('9476',4,22,175,124,4),('9478',4,22,175,125,10),('9480',4,22,175,133,7),('9482',4,22,174,75,9),('9484',4,22,174,76,10),('9486',4,22,174,142,1),('9488',4,22,174,143,4),('9490',4,22,174,147,7),('9492',4,22,174,148,2),('9494',4,22,174,149,8),('9496',4,22,174,155,3),('9498',4,22,174,156,6),('9500',4,22,174,157,5),('9502',4,22,173,137,3),('9504',4,22,173,141,6),('9506',4,22,173,151,1),('9508',4,22,173,160,5),('9510',4,22,173,162,2),('9512',4,22,173,168,4),('9514',4,22,172,127,1),('9516',4,22,171,87,1),('9518',4,22,171,88,2),('9520',4,22,171,89,3),('9522',4,22,171,90,5),('9524',4,22,171,96,6),('9526',4,22,171,132,4),('9528',4,22,170,84,2),('9530',4,22,170,85,3),('9532',4,22,170,86,4),('9534',4,22,170,119,1),('9536',4,22,169,78,1),('9538',4,22,169,79,2),('9540',4,22,169,80,3),('9542',4,22,169,81,4),('9544',4,22,169,92,5),('9546',4,22,169,121,6),('9548',4,22,169,122,7),('9550',4,22,169,126,8),('9552',4,22,168,103,3),('9554',4,22,168,104,1),('9556',4,22,168,106,2),('9558',4,22,167,100,3),('9560',4,22,167,101,1),('9562',4,22,167,102,2),('9564',4,22,167,116,4),('9566',4,22,165,134,1),('9686',4,19,142,73,2),('9688',4,19,142,74,3),('9690',4,19,142,77,5),('9692',4,19,142,82,9),('9694',4,19,142,94,13),('9696',4,19,142,95,14),('9698',4,19,142,97,1),('9700',4,19,142,99,12),('9702',4,19,142,105,11),('9704',4,19,142,114,15),('9706',4,19,142,115,8),('9708',4,19,142,123,6),('9710',4,19,142,124,4),('9712',4,19,142,125,10),('9714',4,19,142,133,7),('9716',4,19,141,75,9),('9718',4,19,141,76,10),('9720',4,19,141,142,1),('9722',4,19,141,143,4),('9724',4,19,141,147,7),('9726',4,19,141,148,2),('9728',4,19,141,149,8),('9730',4,19,141,155,3),('9732',4,19,141,156,6),('9734',4,19,141,157,5),('9736',4,19,140,137,3),('9738',4,19,140,141,7),('9740',4,19,140,151,1),('9742',4,19,140,152,4),('9744',4,19,140,160,6),('9746',4,19,140,162,2),('9748',4,19,140,168,5),('9750',4,19,140,169,8),('9752',4,19,139,127,1),('9754',4,19,138,87,1),('9756',4,19,138,88,2),('9758',4,19,138,89,3),('9760',4,19,138,90,5),('9762',4,19,138,96,6),('9764',4,19,138,132,4),('9766',4,19,137,84,2),('9768',4,19,137,85,3),('9770',4,19,137,86,4),('9772',4,19,137,119,1),('9774',4,19,136,78,1),('9776',4,19,136,79,2),('9778',4,19,136,80,3),('9780',4,19,136,81,4),('9782',4,19,136,92,5),('9784',4,19,136,121,6),('9786',4,19,136,122,7),('9788',4,19,136,126,8),('9790',4,19,135,103,3),('9792',4,19,135,104,1),('9794',4,19,135,106,2),('9796',4,19,134,100,3),('9798',4,19,134,101,1),('9800',4,19,134,102,2),('9802',4,19,134,116,4),('9804',4,19,132,134,1),('9806',4,4,7,170,116),('9808',4,33,296,73,2),('9810',4,33,296,74,3),('9812',4,33,296,77,5),('9814',4,33,296,82,9),('9816',4,33,296,94,13),('9818',4,33,296,95,14),('9820',4,33,296,97,1),('9822',4,33,296,99,12),('9824',4,33,296,105,11),('9826',4,33,296,114,15),('9828',4,33,296,115,8),('9830',4,33,296,123,6),('9832',4,33,296,124,4),('9834',4,33,296,125,10),('9836',4,33,296,133,7),('9838',4,33,295,75,9),('9840',4,33,295,76,10),('9842',4,33,295,142,1),('9844',4,33,295,143,4),('9846',4,33,295,147,7),('9848',4,33,295,148,2),('9850',4,33,295,149,8),('9852',4,33,295,155,3),('9854',4,33,295,156,6),('9856',4,33,295,157,5),('9858',4,33,294,135,8),('9860',4,33,294,137,2),('9862',4,33,294,138,1),('9864',4,33,294,145,7),('9866',4,33,294,150,9),('9868',4,33,294,151,5),('9870',4,33,294,163,4),('9872',4,33,294,168,3),('9874',4,33,294,169,6),('9876',4,33,293,127,1),('9878',4,33,292,87,1),('9880',4,33,292,88,2),('9882',4,33,292,89,3),('9884',4,33,292,90,5),('9886',4,33,292,96,6),('9888',4,33,292,132,4),('9890',4,33,291,84,2),('9892',4,33,291,85,3),('9894',4,33,291,86,4),('9896',4,33,291,119,1),('9898',4,33,290,78,1),('9900',4,33,290,79,2),('9902',4,33,290,80,3),('9904',4,33,290,81,4),('9906',4,33,290,92,5),('9908',4,33,290,121,6),('9910',4,33,290,122,7),('9912',4,33,290,126,8),('9914',4,33,289,103,3),('9916',4,33,289,104,1),('9918',4,33,289,106,2),('9920',4,33,288,100,3),('9922',4,33,288,101,1),('9924',4,33,288,102,2),('9926',4,33,288,116,4),('9928',4,33,286,134,1),('9960',4,34,301,98,8),('10054',4,34,307,91,6),('10064',4,34,307,120,11),('10074',4,34,307,107,14),('10078',4,34,307,108,15),('10080',4,34,307,109,16),('10082',4,34,307,110,17),('10084',4,34,307,111,18),('10086',4,34,307,112,19),('10088',4,34,307,113,20),('10090',4,34,307,128,111),('10092',4,34,307,129,112),('10094',4,34,307,130,113),('10096',4,34,307,131,114),('10100',4,34,307,73,2),('10102',4,34,307,74,3),('10104',4,34,307,77,5),('10106',4,34,307,82,9),('10108',4,34,307,94,13),('10110',4,34,307,95,14),('10112',4,34,307,97,1),('10114',4,34,307,99,12),('10116',4,34,307,105,11),('10118',4,34,307,114,15),('10120',4,34,307,115,8),('10122',4,34,307,123,6),('10124',4,34,307,124,4),('10126',4,34,307,125,10),('10128',4,34,307,133,7),('10130',4,34,307,170,16),('10132',4,34,306,75,9),('10134',4,34,306,76,10),('10136',4,34,306,142,1),('10138',4,34,306,143,4),('10140',4,34,306,147,7),('10142',4,34,306,148,2),('10144',4,34,306,149,8),('10146',4,34,306,155,3),('10148',4,34,306,156,6),('10150',4,34,306,157,5),('10152',4,34,305,135,13),('10154',4,34,305,136,5),('10156',4,34,305,137,4),('10158',4,34,305,138,3),('10160',4,34,305,139,8),('10162',4,34,305,141,9),('10164',4,34,305,150,15),('10166',4,34,305,151,11),('10168',4,34,305,158,14),('10170',4,34,305,160,1),('10172',4,34,305,162,6),('10174',4,34,305,163,10),('10176',4,34,305,165,2),('10178',4,34,305,168,7),('10180',4,34,305,169,12),('10182',4,34,304,127,1),('10184',4,34,303,87,1),('10186',4,34,303,88,2),('10188',4,34,303,89,3),('10190',4,34,303,90,5),('10192',4,34,303,96,6),('10194',4,34,303,132,4),('10196',4,34,302,84,2),('10198',4,34,302,85,3),('10200',4,34,302,86,4),('10202',4,34,302,119,1),('10204',4,34,301,78,1),('10206',4,34,301,79,2),('10208',4,34,301,80,3),('10210',4,34,301,81,4),('10212',4,34,301,92,5),('10214',4,34,301,121,6),('10216',4,34,301,122,7),('10218',4,34,301,126,8),('10220',4,34,300,103,3),('10222',4,34,300,104,1),('10224',4,34,300,106,2),('10226',4,34,299,100,3),('10228',4,34,299,101,1),('10230',4,34,299,102,2),('10232',4,34,299,116,4),('10234',4,34,297,134,1),('10266',4,35,312,98,8),('10360',4,35,318,91,6),('10370',4,35,318,120,11),('10380',4,35,318,107,14),('10384',4,35,318,108,15),('10386',4,35,318,109,16),('10388',4,35,318,110,17),('10390',4,35,318,111,18),('10392',4,35,318,112,19),('10394',4,35,318,113,20),('10396',4,35,318,128,111),('10398',4,35,318,129,112),('10400',4,35,318,130,113),('10402',4,35,318,131,114),('10406',4,35,318,73,2),('10408',4,35,318,74,3),('10410',4,35,318,77,5),('10412',4,35,318,82,9),('10414',4,35,318,94,13),('10416',4,35,318,95,14),('10418',4,35,318,97,1),('10420',4,35,318,99,12),('10422',4,35,318,105,11),('10424',4,35,318,114,15),('10426',4,35,318,115,8),('10428',4,35,318,123,6),('10430',4,35,318,124,4),('10432',4,35,318,125,10),('10434',4,35,318,133,7),('10436',4,35,318,170,16),('10438',4,35,317,75,9),('10440',4,35,317,76,10),('10442',4,35,317,142,1),('10444',4,35,317,143,4),('10446',4,35,317,147,7),('10448',4,35,317,148,2),('10450',4,35,317,149,8),('10452',4,35,317,155,3),('10454',4,35,317,156,6),('10456',4,35,317,157,5),('10458',4,35,316,136,6),('10460',4,35,316,137,5),('10462',4,35,316,138,4),('10464',4,35,316,139,10),('10466',4,35,316,144,15),('10468',4,35,316,150,17),('10470',4,35,316,151,12),('10472',4,35,316,152,16),('10474',4,35,316,158,14),('10476',4,35,316,160,1),('10478',4,35,316,162,7),('10480',4,35,316,163,11),('10482',4,35,316,164,8),('10484',4,35,316,165,2),('10486',4,35,316,166,3),('10488',4,35,316,167,18),('10490',4,35,316,168,9),('10492',4,35,316,169,13),('10494',4,35,315,127,1),('10496',4,35,314,87,1),('10498',4,35,314,88,2),('10500',4,35,314,89,3),('10502',4,35,314,90,5),('10504',4,35,314,96,6),('10506',4,35,314,132,4),('10508',4,35,313,84,2),('10510',4,35,313,85,3),('10512',4,35,313,86,4),('10514',4,35,313,119,1),('10516',4,35,312,78,1),('10518',4,35,312,79,2),('10520',4,35,312,80,3),('10522',4,35,312,81,4),('10524',4,35,312,92,5),('10526',4,35,312,121,6),('10528',4,35,312,122,7),('10530',4,35,312,126,8),('10532',4,35,311,103,3),('10534',4,35,311,104,1),('10536',4,35,311,106,2),('10538',4,35,310,100,3),('10540',4,35,310,101,1),('10542',4,35,310,102,2),('10544',4,35,310,116,4),('10546',4,35,308,134,1);
/*!40000 ALTER TABLE `eav_entity_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_datetime`
--

DROP TABLE IF EXISTS `eav_entity_datetime`;
CREATE TABLE `eav_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_DATETIME_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_DATETIME_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_DATETIME_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_entity_decimal`
--

DROP TABLE IF EXISTS `eav_entity_decimal`;
CREATE TABLE `eav_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_DECIMAL_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_entity_int`
--

DROP TABLE IF EXISTS `eav_entity_int`;
CREATE TABLE `eav_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_INT_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_INT_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_INT_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_entity_store`
--

DROP TABLE IF EXISTS `eav_entity_store`;
CREATE TABLE `eav_entity_store` (
  `entity_store_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Store Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `increment_prefix` varchar(20) DEFAULT NULL COMMENT 'Increment Prefix',
  `increment_last_id` varchar(50) DEFAULT NULL COMMENT 'Last Incremented Id',
  PRIMARY KEY (`entity_store_id`),
  KEY `EAV_ENTITY_STORE_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Store';

--
-- Table structure for table `eav_entity_text`
--

DROP TABLE IF EXISTS `eav_entity_text`;
CREATE TABLE `eav_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_TEXT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `EAV_ENTITY_TEXT_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_entity_type`
--

DROP TABLE IF EXISTS `eav_entity_type`;
CREATE TABLE `eav_entity_type` (
  `entity_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Type Id',
  `entity_type_code` varchar(50) NOT NULL COMMENT 'Entity Type Code',
  `entity_model` varchar(255) NOT NULL COMMENT 'Entity Model',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `entity_table` varchar(255) DEFAULT NULL COMMENT 'Entity Table',
  `value_table_prefix` varchar(255) DEFAULT NULL COMMENT 'Value Table Prefix',
  `entity_id_field` varchar(255) DEFAULT NULL COMMENT 'Entity Id Field',
  `is_data_sharing` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Data Sharing',
  `data_sharing_key` varchar(100) DEFAULT 'default' COMMENT 'Data Sharing Key',
  `default_attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Attribute Set Id',
  `increment_model` varchar(255) DEFAULT NULL COMMENT 'Increment Model',
  `increment_per_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Increment Per Store',
  `increment_pad_length` smallint(5) unsigned NOT NULL DEFAULT '8' COMMENT 'Increment Pad Length',
  `increment_pad_char` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Increment Pad Char',
  `additional_attribute_table` varchar(255) DEFAULT NULL COMMENT 'Additional Attribute Table',
  `entity_attribute_collection` varchar(255) DEFAULT NULL COMMENT 'Entity Attribute Collection',
  PRIMARY KEY (`entity_type_id`),
  KEY `EAV_ENTITY_TYPE_ENTITY_TYPE_CODE` (`entity_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Eav Entity Type';

--
-- Dumping data for table `eav_entity_type`
--

LOCK TABLES `eav_entity_type` WRITE;
/*!40000 ALTER TABLE `eav_entity_type` DISABLE KEYS */;
INSERT INTO `eav_entity_type` VALUES (1,'customer','Magento\\Customer\\Model\\ResourceModel\\Customer','Magento\\Customer\\Model\\Attribute','customer_entity',NULL,NULL,1,'default',1,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',0,8,'0','customer_eav_attribute','Magento\\Customer\\Model\\ResourceModel\\Attribute\\Collection'),(2,'customer_address','Magento\\Customer\\Model\\ResourceModel\\Address','Magento\\Customer\\Model\\Attribute','customer_address_entity',NULL,NULL,1,'default',2,NULL,0,8,'0','customer_eav_attribute','Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Collection'),(3,'catalog_category','Magento\\Catalog\\Model\\ResourceModel\\Category','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','catalog_category_entity',NULL,NULL,1,'default',3,NULL,0,8,'0','catalog_eav_attribute','Magento\\Catalog\\Model\\ResourceModel\\Category\\Attribute\\Collection'),(4,'catalog_product','Magento\\Catalog\\Model\\ResourceModel\\Product','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','catalog_product_entity',NULL,NULL,1,'default',4,NULL,0,8,'0','catalog_eav_attribute','Magento\\Catalog\\Model\\ResourceModel\\Product\\Attribute\\Collection'),(5,'order','Magento\\Sales\\Model\\ResourceModel\\Order',NULL,'sales_order',NULL,NULL,1,'default',5,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(6,'invoice','Magento\\Sales\\Model\\ResourceModel\\Order',NULL,'sales_invoice',NULL,NULL,1,'default',6,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(7,'creditmemo','Magento\\Sales\\Model\\ResourceModel\\Order\\Creditmemo',NULL,'sales_creditmemo',NULL,NULL,1,'default',7,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(8,'shipment','Magento\\Sales\\Model\\ResourceModel\\Order\\Shipment',NULL,'sales_shipment',NULL,NULL,1,'default',8,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL);
/*!40000 ALTER TABLE `eav_entity_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_varchar`
--

DROP TABLE IF EXISTS `eav_entity_varchar`;
CREATE TABLE `eav_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_VARCHAR_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';

--
-- Table structure for table `eav_form_element`
--

DROP TABLE IF EXISTS `eav_form_element`;
CREATE TABLE `eav_form_element` (
  `element_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Element Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `fieldset_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Fieldset Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`element_id`),
  UNIQUE KEY `EAV_FORM_ELEMENT_TYPE_ID_ATTRIBUTE_ID` (`type_id`,`attribute_id`),
  KEY `EAV_FORM_ELEMENT_FIELDSET_ID` (`fieldset_id`),
  KEY `EAV_FORM_ELEMENT_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Eav Form Element';

--
-- Dumping data for table `eav_form_element`
--

LOCK TABLES `eav_form_element` WRITE;
/*!40000 ALTER TABLE `eav_form_element` DISABLE KEYS */;
INSERT INTO `eav_form_element` VALUES ('1',1,NULL,23,'0'),('2',1,NULL,25,'1'),('3',1,NULL,27,'2'),('4',1,NULL,9,'3'),('5',1,NULL,28,'4'),('6',1,NULL,29,'5'),('7',1,NULL,31,'6'),('8',1,NULL,33,'7'),('9',1,NULL,30,'8'),('10',1,NULL,34,'9'),('11',1,NULL,35,'10'),('12',2,NULL,23,'0'),('13',2,NULL,25,'1'),('14',2,NULL,27,'2'),('15',2,NULL,9,'3'),('16',2,NULL,28,'4'),('17',2,NULL,29,'5'),('18',2,NULL,31,'6'),('19',2,NULL,33,'7'),('20',2,NULL,30,'8'),('21',2,NULL,34,'9'),('22',2,NULL,35,'10'),('23',3,NULL,23,'0'),('24',3,NULL,25,'1'),('25',3,NULL,27,'2'),('26',3,NULL,28,'3'),('27',3,NULL,29,'4'),('28',3,NULL,31,'5'),('29',3,NULL,33,'6'),('30',3,NULL,30,'7'),('31',3,NULL,34,'8'),('32',3,NULL,35,'9'),('33',4,NULL,23,'0'),('34',4,NULL,25,'1'),('35',4,NULL,27,'2'),('36',4,NULL,28,'3'),('37',4,NULL,29,'4'),('38',4,NULL,31,'5'),('39',4,NULL,33,'6'),('40',4,NULL,30,'7'),('41',4,NULL,34,'8'),('42',4,NULL,35,'9');
/*!40000 ALTER TABLE `eav_form_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_fieldset`
--

DROP TABLE IF EXISTS `eav_form_fieldset`;
CREATE TABLE `eav_form_fieldset` (
  `fieldset_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Fieldset Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`fieldset_id`),
  UNIQUE KEY `EAV_FORM_FIELDSET_TYPE_ID_CODE` (`type_id`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset';

--
-- Table structure for table `eav_form_fieldset_label`
--

DROP TABLE IF EXISTS `eav_form_fieldset_label`;
CREATE TABLE `eav_form_fieldset_label` (
  `fieldset_id` smallint(5) unsigned NOT NULL COMMENT 'Fieldset Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`fieldset_id`,`store_id`),
  KEY `EAV_FORM_FIELDSET_LABEL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset Label';

--
-- Table structure for table `eav_form_type`
--

DROP TABLE IF EXISTS `eav_form_type`;
CREATE TABLE `eav_form_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `theme` varchar(64) DEFAULT NULL COMMENT 'Theme',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `EAV_FORM_TYPE_CODE_THEME_STORE_ID` (`code`,`theme`,`store_id`),
  KEY `EAV_FORM_TYPE_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Eav Form Type';

--
-- Dumping data for table `eav_form_type`
--

LOCK TABLES `eav_form_type` WRITE;
/*!40000 ALTER TABLE `eav_form_type` DISABLE KEYS */;
INSERT INTO `eav_form_type` VALUES (1,'checkout_onepage_register','checkout_onepage_register',1,'',0),(2,'checkout_onepage_register_guest','checkout_onepage_register_guest',1,'',0),(3,'checkout_onepage_billing_address','checkout_onepage_billing_address',1,'',0),(4,'checkout_onepage_shipping_address','checkout_onepage_shipping_address',1,'',0);
/*!40000 ALTER TABLE `eav_form_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_type_entity`
--

DROP TABLE IF EXISTS `eav_form_type_entity`;
CREATE TABLE `eav_form_type_entity` (
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `entity_type_id` smallint(5) unsigned NOT NULL COMMENT 'Entity Type Id',
  PRIMARY KEY (`type_id`,`entity_type_id`),
  KEY `EAV_FORM_TYPE_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Type Entity';

--
-- Dumping data for table `eav_form_type_entity`
--

LOCK TABLES `eav_form_type_entity` WRITE;
/*!40000 ALTER TABLE `eav_form_type_entity` DISABLE KEYS */;
INSERT INTO `eav_form_type_entity` VALUES (1,1),(2,1),(1,2),(2,2),(3,2),(4,2);
/*!40000 ALTER TABLE `eav_form_type_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
CREATE TABLE `email_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template ID',
  `template_code` varchar(150) NOT NULL COMMENT 'Template Name',
  `template_text` text NOT NULL COMMENT 'Template Content',
  `template_styles` text COMMENT 'Templste Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) NOT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of Template Creation',
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Template Modification',
  `orig_template_code` varchar(200) DEFAULT NULL COMMENT 'Original Template Code',
  `orig_template_variables` text COMMENT 'Original Template Variables',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `EMAIL_TEMPLATE_TEMPLATE_CODE` (`template_code`),
  KEY `EMAIL_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `EMAIL_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Email Templates';

--
-- Table structure for table `flag`
--

DROP TABLE IF EXISTS `flag`;
CREATE TABLE `flag` (
  `flag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Flag Id',
  `flag_code` varchar(255) NOT NULL COMMENT 'Flag Code',
  `state` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag State',
  `flag_data` text COMMENT 'Flag Data',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Last Flag Update',
  PRIMARY KEY (`flag_id`),
  KEY `FLAG_LAST_UPDATE` (`last_update`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Flag';

--
-- Table structure for table `gift_message`
--

DROP TABLE IF EXISTS `gift_message`;
CREATE TABLE `gift_message` (
  `gift_message_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'GiftMessage Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `sender` varchar(255) DEFAULT NULL COMMENT 'Sender',
  `recipient` varchar(255) DEFAULT NULL COMMENT 'Registrant',
  `message` text COMMENT 'Message',
  PRIMARY KEY (`gift_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Gift Message';

--
-- Table structure for table `googleoptimizer_code`
--

DROP TABLE IF EXISTS `googleoptimizer_code`;
CREATE TABLE `googleoptimizer_code` (
  `code_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Google experiment code id',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Optimized entity id product id or catalog id',
  `entity_type` varchar(50) DEFAULT NULL COMMENT 'Optimized entity type',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store id',
  `experiment_script` text COMMENT 'Google experiment script',
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `GOOGLEOPTIMIZER_CODE_STORE_ID_ENTITY_ID_ENTITY_TYPE` (`store_id`,`entity_id`,`entity_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Google Experiment code';

--
-- Table structure for table `import_history`
--

DROP TABLE IF EXISTS `import_history`;
CREATE TABLE `import_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'History record Id',
  `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Started at',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `imported_file` varchar(255) DEFAULT NULL COMMENT 'Imported file',
  `execution_time` varchar(255) DEFAULT NULL COMMENT 'Execution time',
  `summary` varchar(255) DEFAULT NULL COMMENT 'Summary',
  `error_file` varchar(255) NOT NULL COMMENT 'Imported file with errors',
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import history table';

--
-- Table structure for table `importexport_importdata`
--

DROP TABLE IF EXISTS `importexport_importdata`;
CREATE TABLE `importexport_importdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity` varchar(50) NOT NULL COMMENT 'Entity',
  `behavior` varchar(10) NOT NULL DEFAULT 'append' COMMENT 'Behavior',
  `data` longtext COMMENT 'Data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import Data Table';

--
-- Table structure for table `indexer_state`
--

DROP TABLE IF EXISTS `indexer_state`;
CREATE TABLE `indexer_state` (
  `state_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Indexer State Id',
  `indexer_id` varchar(255) DEFAULT NULL COMMENT 'Indexer Id',
  `status` varchar(16) DEFAULT 'invalid' COMMENT 'Indexer Status',
  `updated` datetime DEFAULT NULL COMMENT 'Indexer Status',
  `hash_config` varchar(32) NOT NULL COMMENT 'Hash of indexer config',
  PRIMARY KEY (`state_id`),
  KEY `INDEXER_STATE_INDEXER_ID` (`indexer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Indexer State';

--
-- Dumping data for table `indexer_state`
--

LOCK TABLES `indexer_state` WRITE;
/*!40000 ALTER TABLE `indexer_state` DISABLE KEYS */;
INSERT INTO `indexer_state` VALUES ('1','design_config_grid','valid','2017-06-24 06:46:19','27baa8fe6a5369f52c8b7cbd54a3c3c4'),('2','customer_grid','valid','2017-06-24 06:46:20','a1bbcab4c6368d654719ccf6cf0e55a8'),('3','catalog_category_product','valid','2017-06-24 06:46:20','57b48d3cf1fcd64abe6b01dea3173d02'),('4','catalog_product_category','valid','2017-06-06 09:13:45','9957f66909342cc58ff2703dcd268bf4'),('5','catalog_product_price','valid','2017-06-24 06:46:20','15a819a577a149220cd0722c291de721'),('6','catalog_product_attribute','valid','2017-06-24 06:46:20','77eed0bf72b16099d299d0ab47b74910'),('7','cataloginventory_stock','valid','2017-06-24 06:46:21','78a405fd852458c326c85096099d7d5e'),('8','catalogrule_rule','valid','2017-06-24 06:46:21','5afe3cacdcb52ec3a7e68dc245679021'),('9','catalogrule_product','valid','2017-06-06 09:13:46','0ebee9e52ed424273132e8227fe646f3'),('10','catalogsearch_fulltext','valid','2017-06-24 06:46:21','4486b57e2021aa78b526c68c9af2dcab');
/*!40000 ALTER TABLE `indexer_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integration`
--

DROP TABLE IF EXISTS `integration`;
CREATE TABLE `integration` (
  `integration_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Integration ID',
  `name` varchar(255) NOT NULL COMMENT 'Integration name is displayed in the admin interface',
  `email` varchar(255) NOT NULL COMMENT 'Email address of the contact person',
  `endpoint` varchar(255) DEFAULT NULL COMMENT 'Endpoint for posting consumer credentials',
  `status` smallint(5) unsigned NOT NULL COMMENT 'Integration status',
  `consumer_id` int(10) unsigned DEFAULT NULL COMMENT 'Oauth consumer',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `setup_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Integration type - manual or config file',
  `identity_link_url` varchar(255) DEFAULT NULL COMMENT 'Identity linking Url',
  PRIMARY KEY (`integration_id`),
  UNIQUE KEY `INTEGRATION_NAME` (`name`),
  UNIQUE KEY `INTEGRATION_CONSUMER_ID` (`consumer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='integration';

--
-- Table structure for table `layout_link`
--

DROP TABLE IF EXISTS `layout_link`;
CREATE TABLE `layout_link` (
  `layout_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Defines whether Layout Update is Temporary',
  PRIMARY KEY (`layout_link_id`),
  KEY `LAYOUT_LINK_LAYOUT_UPDATE_ID` (`layout_update_id`),
  KEY `LAYOUT_LINK_STORE_ID_THEME_ID_LAYOUT_UPDATE_ID_IS_TEMPORARY` (`store_id`,`theme_id`,`layout_update_id`,`is_temporary`),
  KEY `LAYOUT_LINK_THEME_ID_THEME_THEME_ID` (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout Link';

--
-- Table structure for table `layout_update`
--

DROP TABLE IF EXISTS `layout_update`;
CREATE TABLE `layout_update` (
  `layout_update_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Layout Update Id',
  `handle` varchar(255) DEFAULT NULL COMMENT 'Handle',
  `xml` text COMMENT 'Xml',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last Update Timestamp',
  PRIMARY KEY (`layout_update_id`),
  KEY `LAYOUT_UPDATE_HANDLE` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout Updates';

--
-- Table structure for table `mageplaza_blog_author`
--

DROP TABLE IF EXISTS `mageplaza_blog_author`;
CREATE TABLE `mageplaza_blog_author` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'User ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'Display Name',
  `url_key` varchar(255) DEFAULT NULL COMMENT 'Author URL Key',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Author Created At',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Author Updated At',
  `image` varchar(255) DEFAULT NULL COMMENT 'Author Image',
  `short_description` text COMMENT 'Author Short Description',
  `facebook_link` varchar(255) DEFAULT NULL COMMENT 'Facebook Link',
  `twitter_link` varchar(255) DEFAULT NULL COMMENT 'Twitter Link',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `MAGEPLAZA_BLOG_AUTHOR_USER_ID` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Author Table';

--
-- Table structure for table `mageplaza_blog_category`
--

DROP TABLE IF EXISTS `mageplaza_blog_category`;
CREATE TABLE `mageplaza_blog_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Category ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'Category Name',
  `description` text COMMENT 'Category Description',
  `store_ids` text NOT NULL COMMENT 'Store Id',
  `url_key` varchar(255) DEFAULT NULL COMMENT 'Category URL Key',
  `enabled` int(11) DEFAULT NULL COMMENT 'Category Enabled',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Category Meta Title',
  `meta_description` text COMMENT 'Category Meta Description',
  `meta_keywords` text COMMENT 'Category Meta Keywords',
  `meta_robots` text,
  `parent_id` int(11) DEFAULT NULL COMMENT 'Category Parent Id',
  `path` varchar(255) DEFAULT NULL COMMENT 'Category Path',
  `position` int(11) DEFAULT NULL COMMENT 'Category Position',
  `level` int(11) DEFAULT NULL COMMENT 'Category Level',
  `children_count` int(11) DEFAULT NULL COMMENT 'Category Children Count',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Category Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Category Updated At',
  PRIMARY KEY (`category_id`),
  FULLTEXT KEY `FTI_4307FF9CFBF027C47FA90D505712E60C` (`name`,`description`,`url_key`,`meta_title`,`meta_description`,`meta_keywords`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Category Table';

--
-- Dumping data for table `mageplaza_blog_category`
--

LOCK TABLES `mageplaza_blog_category` WRITE;
/*!40000 ALTER TABLE `mageplaza_blog_category` DISABLE KEYS */;
INSERT INTO `mageplaza_blog_category` VALUES ('1','ROOT',NULL,'','root',NULL,NULL,NULL,NULL,NULL,NULL,'1','0','0','0','2017-06-24 06:46:17','2017-06-24 06:46:17'),('2','LES PARISIENNES',NULL,'0','les-parisiennes','1',NULL,NULL,NULL,'INDEX,FOLLOW','1','1/2','1','1','0','2017-06-24 06:57:23','2017-06-24 06:58:45'),('3','LISA ELDRIDGE TUTORIALS',NULL,'0','lisa-eldridge-tutorials','1',NULL,NULL,NULL,'INDEX,FOLLOW','1','1/3','2','1','0','2017-06-24 06:57:33','2017-06-24 07:11:38'),('4','LANCÔME MANIA',NULL,'0','lancome-mania','1',NULL,NULL,NULL,'INDEX,FOLLOW','1','1/4','3','1','0','2017-06-24 06:57:42','2017-06-24 07:11:47'),('5','BEAUTY TIPS',NULL,'0','beauty-tips','1',NULL,NULL,NULL,'INDEX,FOLLOW','1','1/5','4','1','0','2017-06-24 06:57:55','2017-06-24 07:11:58'),('6','SECRETS FOR A PERFECT SKIN',NULL,'0','secrets-for-a-perfect-skin','1',NULL,NULL,NULL,'INDEX,FOLLOW','1','1/6','5','1','0','2017-06-24 06:58:08','2017-06-24 07:08:53'),('7','MAKEUP STORIES',NULL,'0','makeup-stories','1',NULL,NULL,NULL,'INDEX,FOLLOW','1','1/7','6','1','0','2017-06-24 06:58:17','2017-06-24 07:12:18'),('8','OUT NOW',NULL,'0','out-now','1',NULL,NULL,NULL,'INDEX,FOLLOW','1','1/8','7','1','0','2017-06-24 06:58:34','2017-06-24 07:12:28');
/*!40000 ALTER TABLE `mageplaza_blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mageplaza_blog_comment`
--

DROP TABLE IF EXISTS `mageplaza_blog_comment`;
CREATE TABLE `mageplaza_blog_comment` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Comment ID',
  `post_id` int(10) unsigned NOT NULL COMMENT 'Post ID',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'User Comment ID',
  `has_reply` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Comment has reply',
  `is_reply` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is reply comment',
  `reply_id` int(10) unsigned DEFAULT '0' COMMENT 'Reply ID',
  `content` varchar(255) DEFAULT NULL COMMENT 'Comment content',
  `created_at` text COMMENT 'Comment Created At',
  PRIMARY KEY (`comment_id`),
  KEY `MAGEPLAZA_BLOG_COMMENT_COMMENT_ID` (`comment_id`),
  KEY `MAGEPLAZA_BLOG_COMMENT_ENTITY_ID` (`entity_id`),
  KEY `MAGEPLAZA_BLOG_COMMENT_POST_ID_MAGEPLAZA_BLOG_POST_POST_ID` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='mageplaza_blog_comment';

--
-- Table structure for table `mageplaza_blog_comment_like`
--

DROP TABLE IF EXISTS `mageplaza_blog_comment_like`;
CREATE TABLE `mageplaza_blog_comment_like` (
  `like_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Like ID',
  `comment_id` int(10) unsigned NOT NULL COMMENT 'Comment ID',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'User Like ID',
  PRIMARY KEY (`like_id`),
  KEY `MAGEPLAZA_BLOG_COMMENT_LIKE_LIKE_ID` (`like_id`),
  KEY `FK_1AA6C994694449283752B6F4C2373B42` (`comment_id`),
  KEY `MAGEPLAZA_BLOG_COMMENT_LIKE_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='mageplaza_blog_comment_like';

--
-- Table structure for table `mageplaza_blog_post`
--

DROP TABLE IF EXISTS `mageplaza_blog_post`;
CREATE TABLE `mageplaza_blog_post` (
  `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Post ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'Post Name',
  `short_description` text COMMENT 'Post Short Description',
  `post_content` text COMMENT 'Post Content',
  `store_ids` text NOT NULL COMMENT 'Store Id',
  `image` varchar(255) DEFAULT NULL COMMENT 'Post Image',
  `views` int(11) DEFAULT NULL COMMENT 'Post Views',
  `enabled` int(11) DEFAULT NULL COMMENT 'Post Enabled',
  `url_key` varchar(255) DEFAULT NULL COMMENT 'Post URL Key',
  `in_rss` int(11) DEFAULT NULL COMMENT 'Post In RSS',
  `allow_comment` int(11) DEFAULT NULL COMMENT 'Post Allow Comment',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Post Meta Title',
  `meta_description` text COMMENT 'Post Meta Description',
  `meta_keywords` text COMMENT 'Post Meta Keywords',
  `meta_robots` text,
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Post Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Post Updated At',
  `author_id` int(10) unsigned DEFAULT NULL COMMENT 'Author ID',
  `modifier_id` int(10) unsigned DEFAULT NULL COMMENT 'Modifier ID',
  PRIMARY KEY (`post_id`),
  FULLTEXT KEY `FTI_A72B069D87A8405566429E7ED61AF342` (`name`,`short_description`,`post_content`,`image`,`url_key`,`meta_title`,`meta_description`,`meta_keywords`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Post Table';

--
-- Dumping data for table `mageplaza_blog_post`
--

LOCK TABLES `mageplaza_blog_post` WRITE;
/*!40000 ALTER TABLE `mageplaza_blog_post` DISABLE KEYS */;
INSERT INTO `mageplaza_blog_post` VALUES ('1','What Is Nail Contouring?','Striped nails are all over our timelines, but they\'re not just standard lines. Today\'s stripes are true geometric art.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','0','/i/n/inspire_1.png',NULL,'1','what-is-nail-contouring','1','1',NULL,NULL,NULL,'INDEX,FOLLOW','2017-06-24 07:03:10','2017-06-24 07:03:10','8','8'),('2','The Sonia Rykiel X Lancôme collection','Explore the Sonia Rykiel x Lancôme makeup collection with new eyeshadow.','Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.','0','/i/n/inspire_2.png',NULL,'1','the-sonia-rykiel-x-lancome-collection','1','1',NULL,NULL,NULL,'INDEX,FOLLOW','2017-06-24 07:04:05','2017-06-24 07:04:05','8','8'),('3','Ambassadress Speak: Julia Roberts','Not taking anything too seriously. That’s happiness.','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).','0','/i/n/inspire_3.png',NULL,'1','ambassadress-speak-julia-roberts','1','1',NULL,NULL,NULL,'INDEX,FOLLOW','2017-06-24 07:06:46','2017-06-24 07:06:46','8','8');
/*!40000 ALTER TABLE `mageplaza_blog_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mageplaza_blog_post_category`
--

DROP TABLE IF EXISTS `mageplaza_blog_post_category`;
CREATE TABLE `mageplaza_blog_post_category` (
  `category_id` int(10) unsigned NOT NULL COMMENT 'Category ID',
  `post_id` int(10) unsigned NOT NULL COMMENT 'Post ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`category_id`,`post_id`),
  UNIQUE KEY `MAGEPLAZA_BLOG_POST_CATEGORY_CATEGORY_ID_POST_ID` (`category_id`,`post_id`),
  KEY `MAGEPLAZA_BLOG_POST_CATEGORY_CATEGORY_ID` (`category_id`),
  KEY `MAGEPLAZA_BLOG_POST_CATEGORY_POST_ID` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Category To Post Link Table';

--
-- Dumping data for table `mageplaza_blog_post_category`
--

LOCK TABLES `mageplaza_blog_post_category` WRITE;
/*!40000 ALTER TABLE `mageplaza_blog_post_category` DISABLE KEYS */;
INSERT INTO `mageplaza_blog_post_category` VALUES ('2','1','1'),('2','2','1'),('2','3','1'),('3','1','3'),('4','2','3'),('4','3','2'),('5','2','2'),('6','1','1'),('6','2','3'),('6','3','2'),('7','1','3'),('8','2','3'),('8','3','2');
/*!40000 ALTER TABLE `mageplaza_blog_post_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mageplaza_blog_post_product`
--

DROP TABLE IF EXISTS `mageplaza_blog_post_product`;
CREATE TABLE `mageplaza_blog_post_product` (
  `post_id` int(10) unsigned NOT NULL COMMENT 'Post ID',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`post_id`,`entity_id`),
  UNIQUE KEY `MAGEPLAZA_BLOG_POST_PRODUCT_POST_ID_ENTITY_ID` (`post_id`,`entity_id`),
  KEY `MAGEPLAZA_BLOG_POST_PRODUCT_POST_ID` (`post_id`),
  KEY `MAGEPLAZA_BLOG_POST_PRODUCT_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Post To Product Link Table';

--
-- Table structure for table `mageplaza_blog_post_tag`
--

DROP TABLE IF EXISTS `mageplaza_blog_post_tag`;
CREATE TABLE `mageplaza_blog_post_tag` (
  `post_id` int(10) unsigned NOT NULL COMMENT 'Post ID',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'Tag ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`post_id`,`tag_id`),
  UNIQUE KEY `MAGEPLAZA_BLOG_POST_TAG_POST_ID_TAG_ID` (`post_id`,`tag_id`),
  KEY `MAGEPLAZA_BLOG_POST_TAG_POST_ID` (`post_id`),
  KEY `MAGEPLAZA_BLOG_POST_TAG_TAG_ID` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Post To Tag Link Table';

--
-- Table structure for table `mageplaza_blog_post_topic`
--

DROP TABLE IF EXISTS `mageplaza_blog_post_topic`;
CREATE TABLE `mageplaza_blog_post_topic` (
  `post_id` int(10) unsigned NOT NULL COMMENT 'Post ID',
  `topic_id` int(10) unsigned NOT NULL COMMENT 'Topic ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`post_id`,`topic_id`),
  UNIQUE KEY `MAGEPLAZA_BLOG_POST_TOPIC_POST_ID_TOPIC_ID` (`post_id`,`topic_id`),
  KEY `MAGEPLAZA_BLOG_POST_TOPIC_POST_ID` (`post_id`),
  KEY `MAGEPLAZA_BLOG_POST_TOPIC_TOPIC_ID` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Post To Topic Link Table';

--
-- Table structure for table `mageplaza_blog_post_traffic`
--

DROP TABLE IF EXISTS `mageplaza_blog_post_traffic`;
CREATE TABLE `mageplaza_blog_post_traffic` (
  `post_id` int(10) unsigned NOT NULL COMMENT 'Post ID',
  `traffic_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Traffic ID',
  `numbers_view` varchar(255) DEFAULT NULL COMMENT 'Numbers View',
  PRIMARY KEY (`post_id`,`traffic_id`),
  UNIQUE KEY `MAGEPLAZA_BLOG_POST_TRAFFIC_POST_ID_TRAFFIC_ID` (`post_id`,`traffic_id`),
  KEY `MAGEPLAZA_BLOG_POST_TRAFFIC_POST_ID` (`post_id`),
  KEY `MAGEPLAZA_BLOG_POST_TRAFFIC_TRAFFIC_ID` (`traffic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Traffic Post Table';

--
-- Dumping data for table `mageplaza_blog_post_traffic`
--

LOCK TABLES `mageplaza_blog_post_traffic` WRITE;
/*!40000 ALTER TABLE `mageplaza_blog_post_traffic` DISABLE KEYS */;
INSERT INTO `mageplaza_blog_post_traffic` VALUES ('1','1','0'),('2','2','0'),('3','3','0');
/*!40000 ALTER TABLE `mageplaza_blog_post_traffic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mageplaza_blog_tag`
--

DROP TABLE IF EXISTS `mageplaza_blog_tag`;
CREATE TABLE `mageplaza_blog_tag` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tag ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'Tag Name',
  `description` text COMMENT 'Tag Description',
  `store_ids` text NOT NULL COMMENT 'Store Id',
  `enabled` int(11) DEFAULT NULL COMMENT 'Tag Enabled',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Tag Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Tag Updated At',
  `url_key` varchar(255) DEFAULT NULL COMMENT 'Tag URL Key',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Post Meta Title',
  `meta_description` text COMMENT 'Post Meta Description',
  `meta_keywords` text COMMENT 'Post Meta Keywords',
  `meta_robots` text,
  PRIMARY KEY (`tag_id`),
  FULLTEXT KEY `MAGEPLAZA_BLOG_TAG_NAME_DESCRIPTION` (`name`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Table';

--
-- Table structure for table `mageplaza_blog_topic`
--

DROP TABLE IF EXISTS `mageplaza_blog_topic`;
CREATE TABLE `mageplaza_blog_topic` (
  `topic_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Topic ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'Topic Name',
  `description` text COMMENT 'Topic Description',
  `store_ids` text NOT NULL COMMENT 'Store Id',
  `enabled` int(11) DEFAULT NULL COMMENT 'Topic Enabled',
  `url_key` varchar(255) DEFAULT NULL COMMENT 'Topic URL Key',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Topic Meta Title',
  `meta_description` text COMMENT 'Topic Meta Description',
  `meta_keywords` text COMMENT 'Topic Meta Keywords',
  `meta_robots` text,
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Topic Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Topic Updated At',
  PRIMARY KEY (`topic_id`),
  FULLTEXT KEY `FTI_DE01BC010D64FD758EE7B503DB9685F7` (`name`,`description`,`url_key`,`meta_title`,`meta_description`,`meta_keywords`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Topic Table';

--
-- Table structure for table `mageplaza_social_customer`
--

DROP TABLE IF EXISTS `mageplaza_social_customer`;
CREATE TABLE `mageplaza_social_customer` (
  `social_customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Social Customer ID',
  `social_id` varchar(255) DEFAULT NULL COMMENT 'Social Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `is_send_password_email` int(10) unsigned DEFAULT '0' COMMENT 'Is Send Password Email',
  `type` varchar(255) DEFAULT '' COMMENT 'Type',
  PRIMARY KEY (`social_customer_id`),
  KEY `MAGEPLAZA_SOCIAL_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Social Customer Table';

--
-- Table structure for table `mgs_megamenu`
--

DROP TABLE IF EXISTS `mgs_megamenu`;
CREATE TABLE `mgs_megamenu` (
  `megamenu_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Megamenu Id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `menu_type` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Menu types',
  `url` varchar(255) DEFAULT NULL COMMENT 'Item url',
  `category_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Category Id',
  `sub_category_ids` mediumtext COMMENT 'Category Ids',
  `position` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Position',
  `columns` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Columns',
  `align_menu` varchar(255) DEFAULT NULL COMMENT 'Align of menu item',
  `align_dropdown` varchar(255) DEFAULT NULL COMMENT 'Align of dropdown',
  `max_level` smallint(6) DEFAULT NULL COMMENT 'Max level of sub category',
  `dropdown_position` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Dropdown Position',
  `use_thumbnail` smallint(6) NOT NULL DEFAULT '2' COMMENT 'Thumbnail of category',
  `special_class` varchar(255) DEFAULT NULL COMMENT 'Custom class',
  `static_content` mediumtext COMMENT 'Static content',
  `top_content` mediumtext COMMENT 'Top content',
  `bottom_content` mediumtext COMMENT 'Bottom content',
  `left_content` mediumtext COMMENT 'Left content',
  `left_col` smallint(6) DEFAULT NULL COMMENT 'columns of left',
  `right_content` mediumtext COMMENT 'Right content',
  `right_col` smallint(6) DEFAULT NULL COMMENT 'columns of right',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Status',
  `from_date` timestamp NULL DEFAULT NULL COMMENT 'From date',
  `to_date` timestamp NULL DEFAULT NULL COMMENT 'To date',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Parent Id',
  `store` int(11) NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `html_label` varchar(255) DEFAULT NULL COMMENT 'Html label',
  PRIMARY KEY (`megamenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='mgs_megamenu';

--
-- Dumping data for table `mgs_megamenu`
--

LOCK TABLES `mgs_megamenu` WRITE;
/*!40000 ALTER TABLE `mgs_megamenu` DISABLE KEYS */;
INSERT INTO `mgs_megamenu` VALUES ('1','Makeup',1,NULL,'3','10,15,16,17,18,19,20,21,11,22,23,24,25,26,27,12,28,29,30,31,32,13,33,14,34,35,36,37,38,39',1,6,NULL,NULL,NULL,1,2,'col-5-custom',NULL,NULL,'<div class=\"row view-all-links\">\r\n<div class=\"col-md-2\"><a href=\"#\">VIEW ALL</a></div>\r\n<div class=\"col-md-2\"><a href=\"#\">VIEW ALL</a></div>\r\n<div class=\"col-md-2\"><a href=\"#\">VIEW ALL</a></div>\r\n</div>\r\n<div class=\"row skincare-menu-bottom\">\r\n<div class=\"col-md-2\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"24\"}}</div>\r\n<div class=\"col-md-5\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"21\"}}</div>\r\n<div class=\"col-md-5 border-left\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"22\"}}</div>\r\n</div>',NULL,0,NULL,0,1,NULL,NULL,'1','0',NULL),('2','Skincare',1,NULL,'4','40,44,45,46,47,48,49,50,51,52,53,54,55,41,56,57,58,59,60,61,62,63,64,65,66,67,42,68,69,70,71,72,73,74,75,43,76,77,78,79',2,4,NULL,NULL,NULL,1,2,NULL,NULL,NULL,'<div class=\"row skincare-menu-bottom\">\r\n<div class=\"col-md-2\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"20\"}}</div>\r\n<div class=\"col-md-5\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"17\"}}</div>\r\n<div class=\"col-md-5  border-left\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"18\"}}</div>\r\n</div>',NULL,0,NULL,0,1,NULL,NULL,'1','0',NULL),('3','Fragrance',1,NULL,'5','80,82,83,84,85,86,87,88,89,90,91,92,93,94,81,95',3,3,NULL,NULL,NULL,1,2,'col-div-2',NULL,NULL,'<div class=\"row skincare-menu-bottom\">\r\n<div class=\"col-md-2\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"28\"}}</div>\r\n<div class=\"col-md-5\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"25\"}}</div>\r\n<div class=\"col-md-5  border-left\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"26\"}}</div>\r\n</div>',NULL,0,'<div class=\"top-right\">{{widget type=\"Magento\\Cms\\Block\\Widget\\Block\" template=\"widget/static_block/default.phtml\" block_id=\"27\"}}</div>',1,1,NULL,NULL,'1','0',NULL),('4','Online exclusives',1,NULL,'7',NULL,4,1,NULL,NULL,NULL,1,2,NULL,NULL,NULL,NULL,NULL,0,NULL,0,1,NULL,NULL,'1','0',NULL),('5','Beauty Mag',1,NULL,'0',NULL,5,1,NULL,NULL,NULL,1,2,NULL,NULL,NULL,NULL,NULL,0,NULL,0,1,NULL,NULL,'1','0',NULL),('6','Elite Rewards',1,NULL,'0',NULL,6,1,NULL,NULL,NULL,1,2,NULL,NULL,NULL,NULL,NULL,0,NULL,0,1,NULL,NULL,'1','0',NULL);
/*!40000 ALTER TABLE `mgs_megamenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mgs_megamenu_parent`
--

DROP TABLE IF EXISTS `mgs_megamenu_parent`;
CREATE TABLE `mgs_megamenu_parent` (
  `parent_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Parent Id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `menu_type` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Menu types',
  `custom_class` varchar(255) DEFAULT NULL COMMENT 'Custom class',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Status',
  PRIMARY KEY (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='mgs_megamenu_parent';

--
-- Dumping data for table `mgs_megamenu_parent`
--

LOCK TABLES `mgs_megamenu_parent` WRITE;
/*!40000 ALTER TABLE `mgs_megamenu_parent` DISABLE KEYS */;
INSERT INTO `mgs_megamenu_parent` VALUES ('1','Main Menu',1,NULL,1);
/*!40000 ALTER TABLE `mgs_megamenu_parent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mgs_megamenu_store`
--

DROP TABLE IF EXISTS `mgs_megamenu_store`;
CREATE TABLE `mgs_megamenu_store` (
  `megamenu_id` int(11) NOT NULL COMMENT 'Megamenu ID',
  `store_id` int(10) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`megamenu_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='mgs_megamenu_store';

--
-- Dumping data for table `mgs_megamenu_store`
--

LOCK TABLES `mgs_megamenu_store` WRITE;
/*!40000 ALTER TABLE `mgs_megamenu_store` DISABLE KEYS */;
INSERT INTO `mgs_megamenu_store` VALUES ('1','0'),('2','0'),('3','0'),('4','0'),('5','0'),('6','0');
/*!40000 ALTER TABLE `mgs_megamenu_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_state`
--

DROP TABLE IF EXISTS `mview_state`;
CREATE TABLE `mview_state` (
  `state_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'View State Id',
  `view_id` varchar(255) DEFAULT NULL COMMENT 'View Id',
  `mode` varchar(16) DEFAULT 'disabled' COMMENT 'View Mode',
  `status` varchar(16) DEFAULT 'idle' COMMENT 'View Status',
  `updated` datetime DEFAULT NULL COMMENT 'View updated time',
  `version_id` int(10) unsigned DEFAULT NULL COMMENT 'View Version Id',
  PRIMARY KEY (`state_id`),
  KEY `MVIEW_STATE_VIEW_ID` (`view_id`),
  KEY `MVIEW_STATE_MODE` (`mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='View State';

--
-- Table structure for table `newsletter_problem`
--

DROP TABLE IF EXISTS `newsletter_problem`;
CREATE TABLE `newsletter_problem` (
  `problem_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Problem Id',
  `subscriber_id` int(10) unsigned DEFAULT NULL COMMENT 'Subscriber Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `problem_error_code` int(10) unsigned DEFAULT '0' COMMENT 'Problem Error Code',
  `problem_error_text` varchar(200) DEFAULT NULL COMMENT 'Problem Error Text',
  PRIMARY KEY (`problem_id`),
  KEY `NEWSLETTER_PROBLEM_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `NEWSLETTER_PROBLEM_QUEUE_ID` (`queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Problems';

--
-- Table structure for table `newsletter_queue`
--

DROP TABLE IF EXISTS `newsletter_queue`;
CREATE TABLE `newsletter_queue` (
  `queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Id',
  `template_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Template ID',
  `newsletter_type` int(11) DEFAULT NULL COMMENT 'Newsletter Type',
  `newsletter_text` text COMMENT 'Newsletter Text',
  `newsletter_styles` text COMMENT 'Newsletter Styles',
  `newsletter_subject` varchar(200) DEFAULT NULL COMMENT 'Newsletter Subject',
  `newsletter_sender_name` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Name',
  `newsletter_sender_email` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Email',
  `queue_status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Status',
  `queue_start_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Start At',
  `queue_finish_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Finish At',
  PRIMARY KEY (`queue_id`),
  KEY `NEWSLETTER_QUEUE_TEMPLATE_ID` (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue';

--
-- Table structure for table `newsletter_queue_link`
--

DROP TABLE IF EXISTS `newsletter_queue_link`;
CREATE TABLE `newsletter_queue_link` (
  `queue_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Link Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `subscriber_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subscriber Id',
  `letter_sent_at` timestamp NULL DEFAULT NULL COMMENT 'Letter Sent At',
  PRIMARY KEY (`queue_link_id`),
  KEY `NEWSLETTER_QUEUE_LINK_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `NEWSLETTER_QUEUE_LINK_QUEUE_ID_LETTER_SENT_AT` (`queue_id`,`letter_sent_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Link';

--
-- Table structure for table `newsletter_queue_store_link`
--

DROP TABLE IF EXISTS `newsletter_queue_store_link`;
CREATE TABLE `newsletter_queue_store_link` (
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`queue_id`,`store_id`),
  KEY `NEWSLETTER_QUEUE_STORE_LINK_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Store Link';

--
-- Table structure for table `newsletter_subscriber`
--

DROP TABLE IF EXISTS `newsletter_subscriber`;
CREATE TABLE `newsletter_subscriber` (
  `subscriber_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Subscriber Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `change_status_at` timestamp NULL DEFAULT NULL COMMENT 'Change Status At',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `subscriber_email` varchar(150) DEFAULT NULL COMMENT 'Subscriber Email',
  `subscriber_status` int(11) NOT NULL DEFAULT '0' COMMENT 'Subscriber Status',
  `subscriber_confirm_code` varchar(32) DEFAULT 'NULL' COMMENT 'Subscriber Confirm Code',
  PRIMARY KEY (`subscriber_id`),
  KEY `NEWSLETTER_SUBSCRIBER_CUSTOMER_ID` (`customer_id`),
  KEY `NEWSLETTER_SUBSCRIBER_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Subscriber';

--
-- Table structure for table `newsletter_template`
--

DROP TABLE IF EXISTS `newsletter_template`;
CREATE TABLE `newsletter_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template ID',
  `template_code` varchar(150) DEFAULT NULL COMMENT 'Template Code',
  `template_text` text COMMENT 'Template Text',
  `template_styles` text COMMENT 'Template Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) DEFAULT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `template_actual` smallint(5) unsigned DEFAULT '1' COMMENT 'Template Actual',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Added At',
  `modified_at` timestamp NULL DEFAULT NULL COMMENT 'Modified At',
  PRIMARY KEY (`template_id`),
  KEY `NEWSLETTER_TEMPLATE_TEMPLATE_ACTUAL` (`template_actual`),
  KEY `NEWSLETTER_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `NEWSLETTER_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Template';

--
-- Table structure for table `oauth_consumer`
--

DROP TABLE IF EXISTS `oauth_consumer`;
CREATE TABLE `oauth_consumer` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `name` varchar(255) NOT NULL COMMENT 'Name of consumer',
  `key` varchar(32) NOT NULL COMMENT 'Key code',
  `secret` varchar(32) NOT NULL COMMENT 'Secret code',
  `callback_url` text COMMENT 'Callback URL',
  `rejected_callback_url` text NOT NULL COMMENT 'Rejected callback URL',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `OAUTH_CONSUMER_KEY` (`key`),
  UNIQUE KEY `OAUTH_CONSUMER_SECRET` (`secret`),
  KEY `OAUTH_CONSUMER_CREATED_AT` (`created_at`),
  KEY `OAUTH_CONSUMER_UPDATED_AT` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Consumers';

--
-- Table structure for table `oauth_nonce`
--

DROP TABLE IF EXISTS `oauth_nonce`;
CREATE TABLE `oauth_nonce` (
  `nonce` varchar(32) NOT NULL COMMENT 'Nonce String',
  `timestamp` int(10) unsigned NOT NULL COMMENT 'Nonce Timestamp',
  `consumer_id` int(10) unsigned NOT NULL COMMENT 'Consumer ID',
  UNIQUE KEY `OAUTH_NONCE_NONCE_CONSUMER_ID` (`nonce`,`consumer_id`),
  KEY `OAUTH_NONCE_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` (`consumer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Nonce';

--
-- Table structure for table `oauth_token`
--

DROP TABLE IF EXISTS `oauth_token`;
CREATE TABLE `oauth_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `consumer_id` int(10) unsigned DEFAULT NULL COMMENT 'Oauth Consumer ID',
  `admin_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin user ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer user ID',
  `type` varchar(16) NOT NULL COMMENT 'Token Type',
  `token` varchar(32) NOT NULL COMMENT 'Token',
  `secret` varchar(32) NOT NULL COMMENT 'Token Secret',
  `verifier` varchar(32) DEFAULT NULL COMMENT 'Token Verifier',
  `callback_url` text NOT NULL COMMENT 'Token Callback URL',
  `revoked` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token revoked',
  `authorized` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token authorized',
  `user_type` int(11) DEFAULT NULL COMMENT 'User type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Token creation timestamp',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `OAUTH_TOKEN_TOKEN` (`token`),
  KEY `OAUTH_TOKEN_CONSUMER_ID` (`consumer_id`),
  KEY `OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` (`admin_id`),
  KEY `OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Tokens';

--
-- Table structure for table `oauth_token_request_log`
--

DROP TABLE IF EXISTS `oauth_token_request_log`;
CREATE TABLE `oauth_token_request_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log Id',
  `user_name` varchar(255) NOT NULL COMMENT 'Customer email or admin login',
  `user_type` smallint(5) unsigned NOT NULL COMMENT 'User type (admin or customer)',
  `failures_count` smallint(5) unsigned DEFAULT '0' COMMENT 'Number of failed authentication attempts in a row',
  `lock_expires_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Lock expiration time',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `OAUTH_TOKEN_REQUEST_LOG_USER_NAME_USER_TYPE` (`user_name`,`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log of token request authentication failures.';

--
-- Table structure for table `password_reset_request_event`
--

DROP TABLE IF EXISTS `password_reset_request_event`;
CREATE TABLE `password_reset_request_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `request_type` smallint(5) unsigned NOT NULL COMMENT 'Type of the event under a security control',
  `account_reference` varchar(255) DEFAULT NULL COMMENT 'An identifier for existing account or another target',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp when the event occurs',
  `ip` varchar(15) NOT NULL COMMENT 'Remote user IP',
  PRIMARY KEY (`id`),
  KEY `PASSWORD_RESET_REQUEST_EVENT_ACCOUNT_REFERENCE` (`account_reference`),
  KEY `PASSWORD_RESET_REQUEST_EVENT_CREATED_AT` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Password Reset Request Event under a security control';

--
-- Table structure for table `paypal_billing_agreement`
--

DROP TABLE IF EXISTS `paypal_billing_agreement`;
CREATE TABLE `paypal_billing_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `method_code` varchar(32) NOT NULL COMMENT 'Method Code',
  `reference_id` varchar(32) NOT NULL COMMENT 'Reference Id',
  `status` varchar(20) NOT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `agreement_label` varchar(255) DEFAULT NULL COMMENT 'Agreement Label',
  PRIMARY KEY (`agreement_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_CUSTOMER_ID` (`customer_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement';

--
-- Table structure for table `paypal_billing_agreement_order`
--

DROP TABLE IF EXISTS `paypal_billing_agreement_order`;
CREATE TABLE `paypal_billing_agreement_order` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  PRIMARY KEY (`agreement_id`,`order_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_ORDER_ORDER_ID` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement Order';

--
-- Table structure for table `paypal_cert`
--

DROP TABLE IF EXISTS `paypal_cert`;
CREATE TABLE `paypal_cert` (
  `cert_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cert Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `content` text COMMENT 'Content',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`cert_id`),
  KEY `PAYPAL_CERT_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Certificate Table';

--
-- Table structure for table `paypal_payment_transaction`
--

DROP TABLE IF EXISTS `paypal_payment_transaction`;
CREATE TABLE `paypal_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `PAYPAL_PAYMENT_TRANSACTION_TXN_ID` (`txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='PayPal Payflow Link Payment Transaction';

--
-- Table structure for table `paypal_settlement_report`
--

DROP TABLE IF EXISTS `paypal_settlement_report`;
CREATE TABLE `paypal_settlement_report` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Report Id',
  `report_date` timestamp NULL DEFAULT NULL COMMENT 'Report Date',
  `account_id` varchar(64) DEFAULT NULL COMMENT 'Account Id',
  `filename` varchar(24) DEFAULT NULL COMMENT 'Filename',
  `last_modified` timestamp NULL DEFAULT NULL COMMENT 'Last Modified',
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `PAYPAL_SETTLEMENT_REPORT_REPORT_DATE_ACCOUNT_ID` (`report_date`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Table';

--
-- Table structure for table `paypal_settlement_report_row`
--

DROP TABLE IF EXISTS `paypal_settlement_report_row`;
CREATE TABLE `paypal_settlement_report_row` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Row Id',
  `report_id` int(10) unsigned NOT NULL COMMENT 'Report Id',
  `transaction_id` varchar(19) DEFAULT NULL COMMENT 'Transaction Id',
  `invoice_id` varchar(127) DEFAULT NULL COMMENT 'Invoice Id',
  `paypal_reference_id` varchar(19) DEFAULT NULL COMMENT 'Paypal Reference Id',
  `paypal_reference_id_type` varchar(3) DEFAULT NULL COMMENT 'Paypal Reference Id Type',
  `transaction_event_code` varchar(5) DEFAULT NULL COMMENT 'Transaction Event Code',
  `transaction_initiation_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Initiation Date',
  `transaction_completion_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Completion Date',
  `transaction_debit_or_credit` varchar(2) NOT NULL DEFAULT 'CR' COMMENT 'Transaction Debit Or Credit',
  `gross_transaction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Gross Transaction Amount',
  `gross_transaction_currency` varchar(3) DEFAULT NULL COMMENT 'Gross Transaction Currency',
  `fee_debit_or_credit` varchar(2) DEFAULT NULL COMMENT 'Fee Debit Or Credit',
  `fee_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Fee Amount',
  `fee_currency` varchar(3) DEFAULT NULL COMMENT 'Fee Currency',
  `custom_field` varchar(255) DEFAULT NULL COMMENT 'Custom Field',
  `consumer_id` varchar(127) DEFAULT NULL COMMENT 'Consumer Id',
  `payment_tracking_id` varchar(255) DEFAULT NULL COMMENT 'Payment Tracking ID',
  `store_id` varchar(50) DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`row_id`),
  KEY `PAYPAL_SETTLEMENT_REPORT_ROW_REPORT_ID` (`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Row Table';

--
-- Table structure for table `persistent_session`
--

DROP TABLE IF EXISTS `persistent_session`;
CREATE TABLE `persistent_session` (
  `persistent_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Session id',
  `key` varchar(50) NOT NULL COMMENT 'Unique cookie key',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `info` text COMMENT 'Session Data',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`persistent_id`),
  UNIQUE KEY `PERSISTENT_SESSION_KEY` (`key`),
  UNIQUE KEY `PERSISTENT_SESSION_CUSTOMER_ID` (`customer_id`),
  KEY `PERSISTENT_SESSION_UPDATED_AT` (`updated_at`),
  KEY `PERSISTENT_SESSION_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Persistent Session';

--
-- Table structure for table `product_alert_price`
--

DROP TABLE IF EXISTS `product_alert_price`;
CREATE TABLE `product_alert_price` (
  `alert_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert price id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price amount',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Product alert add date',
  `last_send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert last send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert send count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_price_id`),
  KEY `PRODUCT_ALERT_PRICE_CUSTOMER_ID` (`customer_id`),
  KEY `PRODUCT_ALERT_PRICE_PRODUCT_ID` (`product_id`),
  KEY `PRODUCT_ALERT_PRICE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Price';

--
-- Table structure for table `product_alert_stock`
--

DROP TABLE IF EXISTS `product_alert_stock`;
CREATE TABLE `product_alert_stock` (
  `alert_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert stock id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Product alert add date',
  `send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Send Count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_stock_id`),
  KEY `PRODUCT_ALERT_STOCK_CUSTOMER_ID` (`customer_id`),
  KEY `PRODUCT_ALERT_STOCK_PRODUCT_ID` (`product_id`),
  KEY `PRODUCT_ALERT_STOCK_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Stock';

--
-- Table structure for table `quote`
--

DROP TABLE IF EXISTS `quote`;
CREATE TABLE `quote` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `converted_at` timestamp NULL DEFAULT NULL COMMENT 'Converted At',
  `is_active` smallint(5) unsigned DEFAULT '1' COMMENT 'Is Active',
  `is_virtual` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Virtual',
  `is_multi_shipping` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Multi Shipping',
  `items_count` int(10) unsigned DEFAULT '0' COMMENT 'Items Count',
  `items_qty` decimal(12,4) DEFAULT '0.0000' COMMENT 'Items Qty',
  `orig_order_id` int(10) unsigned DEFAULT '0' COMMENT 'Orig Order Id',
  `store_to_base_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Base Rate',
  `store_to_quote_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Quote Rate',
  `base_currency_code` varchar(255) DEFAULT NULL COMMENT 'Base Currency Code',
  `store_currency_code` varchar(255) DEFAULT NULL COMMENT 'Store Currency Code',
  `quote_currency_code` varchar(255) DEFAULT NULL COMMENT 'Quote Currency Code',
  `grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `checkout_method` varchar(255) DEFAULT NULL COMMENT 'Checkout Method',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `customer_tax_class_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Tax Class Id',
  `customer_group_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Group Id',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_prefix` varchar(40) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_firstname` varchar(255) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_middlename` varchar(40) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_lastname` varchar(255) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_suffix` varchar(40) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `customer_note` varchar(255) DEFAULT NULL COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT '1' COMMENT 'Customer Note Notify',
  `customer_is_guest` smallint(5) unsigned DEFAULT '0' COMMENT 'Customer Is Guest',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `applied_rule_ids` varchar(255) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `reserved_order_id` varchar(64) DEFAULT NULL COMMENT 'Reserved Order Id',
  `password_hash` varchar(255) DEFAULT NULL COMMENT 'Password Hash',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `global_currency_code` varchar(255) DEFAULT NULL COMMENT 'Global Currency Code',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_quote_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Quote Rate',
  `customer_taxvat` varchar(255) DEFAULT NULL COMMENT 'Customer Taxvat',
  `customer_gender` varchar(255) DEFAULT NULL COMMENT 'Customer Gender',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal With Discount',
  `is_changed` int(10) unsigned DEFAULT NULL COMMENT 'Is Changed',
  `trigger_recollect` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Trigger Recollect',
  `ext_shipping_info` text COMMENT 'Ext Shipping Info',
  `is_persistent` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Quote Persistent',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  KEY `QUOTE_CUSTOMER_ID_STORE_ID_IS_ACTIVE` (`customer_id`,`store_id`,`is_active`),
  KEY `QUOTE_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote';

--
-- Dumping data for table `quote`
--

LOCK TABLES `quote` WRITE;
/*!40000 ALTER TABLE `quote` DISABLE KEYS */;
INSERT INTO `quote` VALUES ('1',1,'2017-06-20 13:00:15','2017-06-20 13:00:19',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'14.165.39.4',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('2',1,'2017-06-21 07:58:52','2017-06-21 08:39:29',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.185.231',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('3',1,'2017-06-21 08:00:01','2017-06-21 08:00:09',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'116.103.209.185',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('4',1,'2017-06-21 08:50:04','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','28.9000','28.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.185.231',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'28.9000','28.9000','28.9000','28.9000','1',0,NULL,0,NULL),('5',1,'2017-06-21 08:54:21','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','13.0000','13.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.185.231',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'13.0000','13.0000','13.0000','13.0000','1',0,NULL,0,NULL),('6',1,'2017-06-21 08:54:22','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','28.9000','28.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.185.231',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'28.9000','28.9000','28.9000','28.9000','1',0,NULL,0,NULL),('7',1,'2017-06-21 08:55:43','2017-06-21 08:55:45',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.185.231',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('8',1,'2017-06-21 09:00:09','2017-06-21 09:00:15',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'116.103.209.185',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('9',1,'2017-06-21 09:04:24','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','28.9000','28.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.185.231',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'28.9000','28.9000','28.9000','28.9000','1',0,NULL,0,NULL),('10',1,'2017-06-21 10:54:12','2017-06-21 11:38:44',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.185.231',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('11',1,'2017-06-21 10:54:17','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','28.9000','28.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'116.103.209.185',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'28.9000','28.9000','28.9000','28.9000','1',0,NULL,0,NULL),('12',1,'2017-06-21 11:21:36','2017-06-21 11:23:59',NULL,1,0,0,'0','0.0000','0','0.0000','0.0000','USD','USD','USD','0.0000','0.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.185.231',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','1',0,NULL,0,NULL),('13',1,'2017-06-22 01:01:26','2017-06-22 01:01:29',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'116.103.217.35',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('14',1,'2017-06-22 02:58:33','2017-06-22 02:58:34',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.186.251',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('15',1,'2017-06-22 04:00:13','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','28.9000','28.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'116.103.217.35',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'28.9000','28.9000','28.9000','28.9000','1',0,NULL,0,NULL),('16',1,'2017-06-22 04:58:09','2017-06-22 04:58:18',NULL,1,0,0,'2','4.0000','0','0.0000','0.0000','USD','USD','USD','83.8000','83.8000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.186.251',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'83.8000','83.8000','83.8000','83.8000','1',0,NULL,0,NULL),('17',1,'2017-06-22 06:38:43','2017-06-22 07:01:21',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.176.186.251',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('18',1,'2017-06-22 06:52:15','2017-06-22 06:53:37',NULL,1,0,0,'0','0.0000','0','0.0000','0.0000','USD','USD','USD','0.0000','0.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'116.103.217.35',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000','1',0,NULL,0,NULL),('19',1,'2017-06-23 04:06:56','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','28.9000','28.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'117.2.207.73',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'28.9000','28.9000','28.9000','28.9000','1',0,NULL,0,NULL),('20',1,'2017-06-23 07:34:17','2017-06-23 08:18:24',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,'1','3','1','vietnama100@gmail.com',NULL,'Le',NULL,'Dung',NULL,NULL,NULL,1,0,'14.174.141.5',NULL,'000000001',NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('21',1,'2017-06-23 07:55:10','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','28.9000','28.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'117.2.217.7',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'28.9000','28.9000','28.9000','28.9000','1',0,NULL,0,NULL),('22',1,'2017-06-23 08:59:53','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','28.9000','28.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'117.2.207.73',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'28.9000','28.9000','28.9000','28.9000','1',0,NULL,0,NULL),('23',1,'2017-06-23 09:20:04','2017-06-23 09:20:23',NULL,1,0,0,'2','2.0000','0','0.0000','0.0000','USD','USD','USD','41.9000','41.9000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'14.174.141.5',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'41.9000','41.9000','41.9000','41.9000','1',0,NULL,0,NULL),('24',1,'2017-06-23 09:44:43','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','10.0000','10.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'14.174.141.5',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'10.0000','10.0000','10.0000','10.0000','1',0,NULL,0,NULL),('25',1,'2017-06-24 05:23:39','0000-00-00 00:00:00',NULL,1,0,0,'1','1.0000','0','0.0000','0.0000','USD','USD','USD','10.0000','10.0000',NULL,NULL,'3','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'113.193.195.186',NULL,NULL,NULL,NULL,'USD','1.0000','1.0000',NULL,NULL,'10.0000','10.0000','10.0000','10.0000','1',0,NULL,0,NULL);
/*!40000 ALTER TABLE `quote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_address`
--

DROP TABLE IF EXISTS `quote_address`;
CREATE TABLE `quote_address` (
  `address_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `save_in_address_book` smallint(6) DEFAULT '0' COMMENT 'Save In Address Book',
  `customer_address_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Address Id',
  `address_type` varchar(10) DEFAULT NULL COMMENT 'Address Type',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(20) DEFAULT NULL COMMENT 'Firstname',
  `middlename` varchar(20) DEFAULT NULL COMMENT 'Middlename',
  `lastname` varchar(20) DEFAULT NULL COMMENT 'Lastname',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(40) DEFAULT NULL COMMENT 'City',
  `region` varchar(40) DEFAULT NULL COMMENT 'Region',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'Region Id',
  `postcode` varchar(20) DEFAULT NULL COMMENT 'Postcode',
  `country_id` varchar(30) DEFAULT NULL COMMENT 'Country Id',
  `telephone` varchar(20) DEFAULT NULL COMMENT 'Phone Number',
  `fax` varchar(20) DEFAULT NULL COMMENT 'Fax',
  `same_as_billing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Same As Billing',
  `collect_shipping_rates` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Collect Shipping Rates',
  `shipping_method` varchar(40) DEFAULT NULL COMMENT 'Shipping Method',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `weight` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Weight',
  `subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal With Discount',
  `tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Shipping Amount',
  `base_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Shipping Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `customer_notes` text COMMENT 'Customer Notes',
  `applied_taxes` text COMMENT 'Applied Taxes',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `free_shipping` smallint(6) DEFAULT NULL,
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_id`),
  KEY `QUOTE_ADDRESS_QUOTE_ID` (`quote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address';

--
-- Dumping data for table `quote_address`
--

LOCK TABLES `quote_address` WRITE;
/*!40000 ALTER TABLE `quote_address` DISABLE KEYS */;
INSERT INTO `quote_address` VALUES ('1','1','2017-06-20 13:00:15','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2','1','2017-06-20 13:00:15','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3','2','2017-06-21 07:58:52','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4','2','2017-06-21 07:58:52','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5','3','2017-06-21 08:00:01','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6','3','2017-06-21 08:00:01','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7','4','2017-06-21 08:50:04','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8','4','2017-06-21 08:50:04','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','28.9000','28.9000','28.9000','28.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','28.9000','28.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9','5','2017-06-21 08:54:21','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('10','5','2017-06-21 08:54:21','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','13.0000','13.0000','13.0000','13.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','13.0000','13.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('11','6','2017-06-21 08:54:22','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('12','6','2017-06-21 08:54:22','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','28.9000','28.9000','28.9000','28.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','28.9000','28.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('13','7','2017-06-21 08:55:43','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('14','7','2017-06-21 08:55:43','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('15','8','2017-06-21 09:00:09','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('16','8','2017-06-21 09:00:09','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('17','9','2017-06-21 09:04:24','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('18','9','2017-06-21 09:04:24','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','28.9000','28.9000','28.9000','28.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','28.9000','28.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('19','10','2017-06-21 10:54:12','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20','10','2017-06-21 10:54:12','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('21','11','2017-06-21 10:54:17','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('22','11','2017-06-21 10:54:17','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','28.9000','28.9000','28.9000','28.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','28.9000','28.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('23','12','2017-06-21 11:21:36','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('24','12','2017-06-21 11:21:36','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','0.0000','0.0000','26.0000','26.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,'0.0000','0.0000','0.0000','26.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('25','13','2017-06-22 01:01:26','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('26','13','2017-06-22 01:01:26','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('27','14','2017-06-22 02:58:33','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('28','14','2017-06-22 02:58:33','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('29','15','2017-06-22 04:00:13','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('30','15','2017-06-22 04:00:13','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','28.9000','28.9000','28.9000','28.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','28.9000','28.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('31','16','2017-06-22 04:58:09','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('32','16','2017-06-22 04:58:09','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'2.0000','83.8000','83.8000','83.8000','83.8000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','83.8000','83.8000',NULL,'a:0:{}',NULL,'0.0000','0.0000','83.8000','83.8000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('33','17','2017-06-22 06:38:43','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('34','17','2017-06-22 06:38:43','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('35','18','2017-06-22 06:52:15','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('36','18','2017-06-22 06:52:15','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,'0.0000','0.0000','0.0000','13.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('37','19','2017-06-23 04:06:56','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('38','19','2017-06-23 04:06:56','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','28.9000','28.9000','28.9000','28.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','28.9000','28.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('39','20','2017-06-23 07:34:17','0000-00-00 00:00:00','1',0,NULL,'billing','vietnama100@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('40','20','2017-06-23 07:34:17','0000-00-00 00:00:00','1',0,NULL,'shipping','vietnama100@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('41','21','2017-06-23 07:55:10','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('42','21','2017-06-23 07:55:10','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','28.9000','28.9000','28.9000','28.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','28.9000','28.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('43','22','2017-06-23 08:59:53','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('44','22','2017-06-23 08:59:53','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'0.5000','28.9000','28.9000','28.9000','28.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','28.9000','28.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('45','23','2017-06-23 09:20:04','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('46','23','2017-06-23 09:20:04','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,'1.0000','41.9000','41.9000','41.9000','41.9000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','41.9000','41.9000',NULL,'a:0:{}',NULL,'0.0000','0.0000','41.9000','41.9000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('47','24','2017-06-23 09:44:43','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('48','24','2017-06-23 09:44:43','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'1.0000','10.0000','10.0000','10.0000','10.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','10.0000','10.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','10.0000','10.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('49','25','2017-06-24 05:23:39','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000',NULL,NULL,'0.0000','0.0000','0.0000','0.0000',NULL,'N;',NULL,NULL,NULL,'0.0000',NULL,'0.0000','0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('50','25','2017-06-24 05:23:39','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'1.0000','10.0000','10.0000','10.0000','10.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','10.0000','10.0000',NULL,'a:0:{}',NULL,'0.0000','0.0000','10.0000','10.0000','0.0000','0.0000','0.0000',NULL,'0.0000','0.0000',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `quote_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_address_item`
--

DROP TABLE IF EXISTS `quote_address_item`;
CREATE TABLE `quote_address_item` (
  `address_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Item Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Address Id',
  `quote_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Item Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `super_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Super Product Id',
  `parent_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Product Id',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `is_qty_decimal` int(10) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `discount_percent` decimal(12,4) DEFAULT NULL COMMENT 'Discount Percent',
  `no_discount` int(10) unsigned DEFAULT NULL COMMENT 'No Discount',
  `tax_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tax Percent',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `free_shipping` int(11) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_item_id`),
  KEY `QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID` (`quote_address_id`),
  KEY `QUOTE_ADDRESS_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID` (`quote_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address Item';

--
-- Table structure for table `quote_id_mask`
--

DROP TABLE IF EXISTS `quote_id_mask`;
CREATE TABLE `quote_id_mask` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `quote_id` int(10) unsigned NOT NULL COMMENT 'Quote ID',
  `masked_id` varchar(32) DEFAULT NULL COMMENT 'Masked ID',
  PRIMARY KEY (`entity_id`,`quote_id`),
  KEY `QUOTE_ID_MASK_QUOTE_ID` (`quote_id`),
  KEY `QUOTE_ID_MASK_MASKED_ID` (`masked_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='Quote ID and masked ID mapping';

--
-- Dumping data for table `quote_id_mask`
--

LOCK TABLES `quote_id_mask` WRITE;
/*!40000 ALTER TABLE `quote_id_mask` DISABLE KEYS */;
INSERT INTO `quote_id_mask` VALUES ('24','25','23f7310cb3bdcf93b21f50a219836c5f'),('15','16','2cd5e26b375c4e702f4d8c00546b02a1'),('4','5','448a04cdef5a052838e30b60e031544c'),('23','24','4c206a9d25b473227b78fbd249ea7588'),('7','8','4e1b0b804144f57d2aa300743c4d056c'),('14','15','4e5fddc340c39f49f0c93d6492bb7093'),('12','13','5048b0e291c86744fc01c919080fda0a'),('22','23','5b215ad1d34c85ef4f4960515a4da497'),('16','17','63e6dd99a11a40a25f4ad3c04e5e6a5b'),('17','18','816b891a16d96d5257fec481b66fe75d'),('21','22','8765b79fb93fa4e57140e683d1c09397'),('10','11','8b8c7fd9f89fa40859194243d7ea3684'),('19','20','8f2ce8d17607b343592e7a296a2c8d72'),('8','9','98373edd63caf2c60b00b9b1b644aee1'),('13','14','9bb5c930c47971fc1e149cdc33c54f1a'),('1','1','a1c380f44ca0e28693fff01d30943dd3'),('6','7','a92c3d48301835e6243ea9f825dde5d5'),('3','3','b3adf553a01c3e2587f70f2ef7e2c57f'),('11','12','b52a4a0cd241c7f4145a7a20d1c06e15'),('18','19','dcf1f095feda4e0a397934a2e96269a8'),('5','6','dec7258fd0aa8989b7de8803f4e51c60'),('20','21','f662ccb354b33483e3c36d8fd44fc68b'),('9','10','f7535505d356faffdcf7dca36a984274'),('2','2','fcc980932f6d9f03ca74301486f24790');
/*!40000 ALTER TABLE `quote_id_mask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_item`
--

DROP TABLE IF EXISTS `quote_item`;
CREATE TABLE `quote_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned DEFAULT '0' COMMENT 'No Discount',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Custom Price',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `original_custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Custom Price',
  `redirect_url` varchar(255) DEFAULT NULL COMMENT 'Redirect Url',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `free_shipping` smallint(6) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `QUOTE_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `QUOTE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `QUOTE_ITEM_QUOTE_ID` (`quote_id`),
  KEY `QUOTE_ITEM_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item';

--
-- Dumping data for table `quote_item`
--

LOCK TABLES `quote_item` WRITE;
/*!40000 ALTER TABLE `quote_item` DISABLE KEYS */;
INSERT INTO `quote_item` VALUES ('1','1','2017-06-20 13:00:15','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2','1','2017-06-20 13:00:19','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4','3','2017-06-21 08:00:01','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5','3','2017-06-21 08:00:09','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6','2','2017-06-21 08:39:28','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7','2','2017-06-21 08:39:29','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8','4','2017-06-21 08:50:04','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9','5','2017-06-21 08:54:21','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('10','6','2017-06-21 08:54:22','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('11','7','2017-06-21 08:55:43','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('12','7','2017-06-21 08:55:45','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('13','8','2017-06-21 09:00:09','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('14','8','2017-06-21 09:00:15','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('15','9','2017-06-21 09:04:24','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('18','11','2017-06-21 10:54:17','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('24','10','2017-06-21 11:38:42','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('25','10','2017-06-21 11:38:44','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('26','13','2017-06-22 01:01:26','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('27','13','2017-06-22 01:01:29','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('28','14','2017-06-22 02:58:33','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('29','14','2017-06-22 02:58:34','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('30','15','2017-06-22 04:00:13','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('31','16','2017-06-22 04:58:09','2017-06-22 04:58:16','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','2.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','26.0000','26.0000','0.0000','1.0000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','26.0000','26.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('32','16','2017-06-22 04:58:09','2017-06-22 04:58:18','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','2.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','57.8000','57.8000','0.0000','1.0000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','57.8000','57.8000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('37','17','2017-06-22 07:01:20','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('38','17','2017-06-22 07:01:21','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('39','19','2017-06-23 04:06:57','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('40','20','2017-06-23 07:34:17','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('41','20','2017-06-23 07:34:17','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('42','21','2017-06-23 07:55:10','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('43','22','2017-06-23 08:59:53','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('44','23','2017-06-23 09:20:04','0000-00-00 00:00:00','2',1,NULL,0,'162072-LAC-test2','HYPNÔSE DOLL EYES',NULL,NULL,NULL,0,0,'0.5000','1.0000','28.9000','28.9000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','28.9000','28.9000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'28.9000','28.9000','28.9000','28.9000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('45','23','2017-06-23 09:20:23','0000-00-00 00:00:00','3',1,NULL,0,'A00357-LAC-test3','MONSIEUR BIG MASCARA - TRAVEL SIZE',NULL,NULL,NULL,0,0,'0.5000','1.0000','13.0000','13.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','13.0000','13.0000','0.0000','0.5000','simple',NULL,NULL,NULL,NULL,NULL,'13.0000','13.0000','13.0000','13.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('46','24','2017-06-23 09:44:43','0000-00-00 00:00:00','10',1,NULL,0,'trésor in love temps  tmp-Green','trésor in love temps  tmp',NULL,NULL,NULL,0,0,'1.0000','1.0000','10.0000','10.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','10.0000','10.0000','0.0000','1.0000','configurable',NULL,NULL,NULL,NULL,NULL,'10.0000','10.0000','10.0000','10.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('47','24','2017-06-23 09:44:43','0000-00-00 00:00:00','5',1,'46',0,'trésor in love temps  tmp-Green','trésor in love temps  tmp-Green',NULL,NULL,NULL,0,0,'1.0000','1.0000','0.0000','0.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('48','25','2017-06-24 05:23:39','0000-00-00 00:00:00','10',1,NULL,0,'trésor in love temps  tmp-Green','trésor in love temps  tmp',NULL,NULL,NULL,0,0,'1.0000','1.0000','10.0000','10.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','10.0000','10.0000','0.0000','1.0000','configurable',NULL,NULL,NULL,NULL,NULL,'10.0000','10.0000','10.0000','10.0000','0.0000','0.0000',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('49','25','2017-06-24 05:23:39','0000-00-00 00:00:00','5',1,'48',0,'trésor in love temps  tmp-Green','trésor in love temps  tmp-Green',NULL,NULL,NULL,0,0,'1.0000','1.0000','0.0000','0.0000',NULL,'0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','0.0000','simple',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `quote_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_item_option`
--

DROP TABLE IF EXISTS `quote_item_option`;
CREATE TABLE `quote_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `item_id` int(10) unsigned NOT NULL COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `QUOTE_ITEM_OPTION_ITEM_ID` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item Option';

--
-- Dumping data for table `quote_item_option`
--

LOCK TABLES `quote_item_option` WRITE;
/*!40000 ALTER TABLE `quote_item_option` DISABLE KEYS */;
INSERT INTO `quote_item_option` VALUES ('1','1','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('2','2','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('4','4','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('5','5','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('6','6','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('7','7','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('8','8','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('9','9','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('10','10','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('11','11','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('12','12','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('13','13','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('14','14','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('15','15','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('18','18','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('24','24','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('25','25','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('26','26','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('27','27','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('28','28','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('29','29','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('30','30','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('31','31','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('32','32','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('37','37','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('38','38','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('39','39','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('40','40','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('41','41','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('42','42','2','info_buyRequest','a:5:{s:4:\"uenc\";s:56:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9kdW1teS10ZXN0Mi5odG1s\";s:7:\"product\";s:1:\"2\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),('43','43','2','info_buyRequest','a:3:{s:4:\"uenc\";s:96:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9jYXRhbG9nc2VhcmNoL3Jlc3VsdC8_cT1IWVBOJUMzJTk0U0UrRE9MTCtFWUVT\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('44','44','2','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),('45','45','3','info_buyRequest','a:3:{s:4:\"uenc\";s:52:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS9tYWtlLXVwLmh0bWw,\";s:7:\"product\";s:1:\"3\";s:3:\"qty\";d:1;}'),('46','46','10','info_buyRequest','a:6:{s:4:\"uenc\";s:76:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS90cmVzb3ItaW4tbG92ZS10ZW1wcy10bXAuaHRtbA,,\";s:7:\"product\";s:2:\"10\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:15:\"super_attribute\";a:1:{i:170;s:3:\"205\";}s:3:\"qty\";s:1:\"1\";}'),('47','46','10','attributes','a:1:{i:170;s:3:\"205\";}'),('48','46','5','product_qty_5','1'),('49','46','5','simple_product','5'),('50','47','5','info_buyRequest','a:6:{s:4:\"uenc\";s:76:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS90cmVzb3ItaW4tbG92ZS10ZW1wcy10bXAuaHRtbA,,\";s:7:\"product\";s:2:\"10\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:15:\"super_attribute\";a:1:{i:170;s:3:\"205\";}s:3:\"qty\";s:1:\"1\";}'),('51','47','5','parent_product_id','10'),('52','48','10','info_buyRequest','a:6:{s:4:\"uenc\";s:76:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS90cmVzb3ItaW4tbG92ZS10ZW1wcy10bXAuaHRtbA,,\";s:7:\"product\";s:2:\"10\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:15:\"super_attribute\";a:1:{i:170;s:3:\"205\";}s:3:\"qty\";s:1:\"1\";}'),('53','48','10','attributes','a:1:{i:170;s:3:\"205\";}'),('54','48','5','product_qty_5','1'),('55','48','5','simple_product','5'),('56','49','5','info_buyRequest','a:6:{s:4:\"uenc\";s:76:\"aHR0cDovL2xhbmMwbTMudmlub2JlLmNvbS90cmVzb3ItaW4tbG92ZS10ZW1wcy10bXAuaHRtbA,,\";s:7:\"product\";s:2:\"10\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:15:\"super_attribute\";a:1:{i:170;s:3:\"205\";}s:3:\"qty\";s:1:\"1\";}'),('57','49','5','parent_product_id','10');
/*!40000 ALTER TABLE `quote_item_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_payment`
--

DROP TABLE IF EXISTS `quote_payment`;
CREATE TABLE `quote_payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Payment Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `cc_type` varchar(255) DEFAULT NULL COMMENT 'Cc Type',
  `cc_number_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_last_4` varchar(255) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_cid_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Cid Enc',
  `cc_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_exp_month` varchar(255) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_exp_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Exp Year',
  `cc_ss_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Owner',
  `cc_ss_start_month` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Month',
  `cc_ss_start_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Year',
  `po_number` varchar(255) DEFAULT NULL COMMENT 'Po Number',
  `additional_data` text COMMENT 'Additional Data',
  `cc_ss_issue` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `additional_information` text COMMENT 'Additional Information',
  `paypal_payer_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Id',
  `paypal_payer_status` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Status',
  `paypal_correlation_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Correlation Id',
  PRIMARY KEY (`payment_id`),
  KEY `QUOTE_PAYMENT_QUOTE_ID` (`quote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Payment';

--
-- Dumping data for table `quote_payment`
--

LOCK TABLES `quote_payment` WRITE;
/*!40000 ALTER TABLE `quote_payment` DISABLE KEYS */;
INSERT INTO `quote_payment` VALUES ('1','20','2017-06-23 08:18:24','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,NULL,NULL,NULL,'a:1:{s:43:\"paypal_express_checkout_shipping_overridden\";i:0;}',NULL,NULL,NULL);
/*!40000 ALTER TABLE `quote_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_shipping_rate`
--

DROP TABLE IF EXISTS `quote_shipping_rate`;
CREATE TABLE `quote_shipping_rate` (
  `rate_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rate Id',
  `address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Address Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `carrier` varchar(255) DEFAULT NULL COMMENT 'Carrier',
  `carrier_title` varchar(255) DEFAULT NULL COMMENT 'Carrier Title',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `method_description` text COMMENT 'Method Description',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `error_message` text COMMENT 'Error Message',
  `method_title` text COMMENT 'Method Title',
  PRIMARY KEY (`rate_id`),
  KEY `QUOTE_SHIPPING_RATE_ADDRESS_ID` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Shipping Rate';

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating` (
  `rating_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Id',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `rating_code` varchar(64) NOT NULL COMMENT 'Rating Code',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Position On Storefront',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Rating is active.',
  PRIMARY KEY (`rating_id`),
  UNIQUE KEY `RATING_RATING_CODE` (`rating_code`),
  KEY `RATING_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Ratings';

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (1,1,'Quality',0,1),(2,1,'Value',0,1),(3,1,'Price',0,1);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_entity`
--

DROP TABLE IF EXISTS `rating_entity`;
CREATE TABLE `rating_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_code` varchar(64) NOT NULL COMMENT 'Entity Code',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `RATING_ENTITY_ENTITY_CODE` (`entity_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Rating entities';

--
-- Dumping data for table `rating_entity`
--

LOCK TABLES `rating_entity` WRITE;
/*!40000 ALTER TABLE `rating_entity` DISABLE KEYS */;
INSERT INTO `rating_entity` VALUES (1,'product'),(2,'product_review'),(3,'review');
/*!40000 ALTER TABLE `rating_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_option`
--

DROP TABLE IF EXISTS `rating_option`;
CREATE TABLE `rating_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Option Id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `code` varchar(32) NOT NULL COMMENT 'Rating Option Code',
  `value` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Option Value',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Ration option position on Storefront',
  PRIMARY KEY (`option_id`),
  KEY `RATING_OPTION_RATING_ID` (`rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Rating options';

--
-- Dumping data for table `rating_option`
--

LOCK TABLES `rating_option` WRITE;
/*!40000 ALTER TABLE `rating_option` DISABLE KEYS */;
INSERT INTO `rating_option` VALUES ('1',1,'1',1,1),('2',1,'2',2,2),('3',1,'3',3,3),('4',1,'4',4,4),('5',1,'5',5,5),('6',2,'1',1,1),('7',2,'2',2,2),('8',2,'3',3,3),('9',2,'4',4,4),('10',2,'5',5,5),('11',3,'1',1,1),('12',3,'2',2,2),('13',3,'3',3,3),('14',3,'4',4,4),('15',3,'5',5,5);
/*!40000 ALTER TABLE `rating_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_option_vote`
--

DROP TABLE IF EXISTS `rating_option_vote`;
CREATE TABLE `rating_option_vote` (
  `vote_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Vote id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote option id',
  `remote_ip` varchar(16) NOT NULL COMMENT 'Customer IP',
  `remote_ip_long` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Customer IP converted to long integer format',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `review_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Review id',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Percent amount',
  `value` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote option value',
  PRIMARY KEY (`vote_id`),
  KEY `RATING_OPTION_VOTE_OPTION_ID` (`option_id`),
  KEY `RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating option values';

--
-- Table structure for table `rating_option_vote_aggregated`
--

DROP TABLE IF EXISTS `rating_option_vote_aggregated`;
CREATE TABLE `rating_option_vote_aggregated` (
  `primary_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Vote aggregation id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `vote_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote dty',
  `vote_value_sum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'General vote sum',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote percent',
  `percent_approved` smallint(6) DEFAULT '0' COMMENT 'Vote percent approved by admin',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`primary_id`),
  KEY `RATING_OPTION_VOTE_AGGREGATED_RATING_ID` (`rating_id`),
  KEY `RATING_OPTION_VOTE_AGGREGATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating vote aggregated';

--
-- Table structure for table `rating_store`
--

DROP TABLE IF EXISTS `rating_store`;
CREATE TABLE `rating_store` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `RATING_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Store';

--
-- Table structure for table `rating_title`
--

DROP TABLE IF EXISTS `rating_title`;
CREATE TABLE `rating_title` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Rating Label',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `RATING_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Title';

--
-- Table structure for table `report_compared_product_index`
--

DROP TABLE IF EXISTS `report_compared_product_index`;
CREATE TABLE `report_compared_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `REPORT_COMPARED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `REPORT_COMPARED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reports Compared Product Index Table';

--
-- Table structure for table `report_event`
--

DROP TABLE IF EXISTS `report_event`;
CREATE TABLE `report_event` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Id',
  `logged_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Logged At',
  `event_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Type Id',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Object Id',
  `subject_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subject Id',
  `subtype` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Subtype',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`event_id`),
  KEY `REPORT_EVENT_EVENT_TYPE_ID` (`event_type_id`),
  KEY `REPORT_EVENT_SUBJECT_ID` (`subject_id`),
  KEY `REPORT_EVENT_OBJECT_ID` (`object_id`),
  KEY `REPORT_EVENT_SUBTYPE` (`subtype`),
  KEY `REPORT_EVENT_STORE_ID` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COMMENT='Reports Event Table';

--
-- Dumping data for table `report_event`
--

LOCK TABLES `report_event` WRITE;
/*!40000 ALTER TABLE `report_event` DISABLE KEYS */;
INSERT INTO `report_event` VALUES (1,'2017-06-13 06:55:39',1,'1','17',1,1),(2,'2017-06-13 07:16:21',1,'2','20',1,1),(3,'2017-06-13 07:58:24',1,'1','20',1,1),(4,'2017-06-13 08:01:59',1,'1','22',1,1),(5,'2017-06-13 08:01:59',1,'1','23',1,1),(6,'2017-06-13 08:02:15',1,'3','25',1,1),(7,'2017-06-13 08:35:25',1,'3','25',1,1),(8,'2017-06-13 17:46:35',1,'3','56',1,1),(9,'2017-06-20 13:00:15',4,'2','205',1,1),(10,'2017-06-20 13:00:19',4,'3','205',1,1),(11,'2017-06-21 07:58:52',4,'2','248',1,1),(12,'2017-06-21 08:00:01',4,'2','250',1,1),(13,'2017-06-21 08:00:09',4,'3','250',1,1),(14,'2017-06-21 08:39:28',4,'3','248',1,1),(15,'2017-06-21 08:39:29',4,'2','248',1,1),(16,'2017-06-21 08:50:04',4,'2','255',1,1),(17,'2017-06-21 08:54:21',4,'3','257',1,1),(18,'2017-06-21 08:54:22',4,'2','258',1,1),(19,'2017-06-21 08:55:43',4,'2','261',1,1),(20,'2017-06-21 08:55:45',4,'3','261',1,1),(21,'2017-06-21 09:00:09',4,'2','263',1,1),(22,'2017-06-21 09:00:15',4,'3','263',1,1),(23,'2017-06-21 09:01:50',1,'2','261',1,1),(24,'2017-06-21 09:04:24',4,'2','267',1,1),(25,'2017-06-21 09:23:10',1,'3','263',1,1),(26,'2017-06-21 10:54:12',4,'2','277',1,1),(27,'2017-06-21 10:54:14',4,'3','277',1,1),(28,'2017-06-21 10:54:17',4,'2','274',1,1),(29,'2017-06-21 11:20:18',4,'2','277',1,1),(30,'2017-06-21 11:21:36',4,'2','280',1,1),(31,'2017-06-21 11:21:58',4,'2','280',1,1),(32,'2017-06-21 11:23:04',4,'3','280',1,1),(33,'2017-06-21 11:23:36',4,'2','280',1,1),(34,'2017-06-21 11:38:42',4,'2','277',1,1),(35,'2017-06-21 11:38:44',4,'3','277',1,1),(36,'2017-06-22 01:01:26',4,'2','284',1,1),(37,'2017-06-22 01:01:29',4,'3','284',1,1),(38,'2017-06-22 02:58:33',4,'2','291',1,1),(39,'2017-06-22 02:58:34',4,'3','291',1,1),(40,'2017-06-22 04:00:13',4,'2','298',1,1),(41,'2017-06-22 04:58:09',4,'3','305',1,1),(42,'2017-06-22 04:58:09',4,'2','305',1,1),(43,'2017-06-22 06:38:43',4,'2','306',1,1),(44,'2017-06-22 06:38:45',4,'3','306',1,1),(45,'2017-06-22 06:51:19',1,'2','313',1,1),(46,'2017-06-22 06:52:15',4,'2','314',1,1),(47,'2017-06-22 06:52:23',4,'3','314',1,1),(48,'2017-06-22 07:01:20',4,'2','306',1,1),(49,'2017-06-22 07:01:21',4,'3','306',1,1),(50,'2017-06-23 04:06:56',4,'2','353',1,1),(51,'2017-06-23 07:34:17',4,'2','361',1,1),(52,'2017-06-23 07:34:17',4,'3','361',1,1),(53,'2017-06-23 07:54:55',1,'2','359',1,1),(54,'2017-06-23 07:55:10',4,'2','359',1,1),(55,'2017-06-23 08:59:53',4,'2','375',1,1),(56,'2017-06-23 09:00:01',1,'2','375',1,1),(57,'2017-06-23 09:20:04',4,'2','376',1,1),(58,'2017-06-23 09:20:23',4,'3','376',1,1),(59,'2017-06-23 09:44:10',1,'10','377',1,1),(60,'2017-06-23 09:44:43',4,'10','377',1,1),(61,'2017-06-23 10:10:01',1,'2','378',1,1),(62,'2017-06-24 05:23:39',4,'10','400',1,1);
/*!40000 ALTER TABLE `report_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_event_types`
--

DROP TABLE IF EXISTS `report_event_types`;
CREATE TABLE `report_event_types` (
  `event_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Type Id',
  `event_name` varchar(64) NOT NULL COMMENT 'Event Name',
  `customer_login` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Login',
  PRIMARY KEY (`event_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Reports Event Type Table';

--
-- Dumping data for table `report_event_types`
--

LOCK TABLES `report_event_types` WRITE;
/*!40000 ALTER TABLE `report_event_types` DISABLE KEYS */;
INSERT INTO `report_event_types` VALUES (1,'catalog_product_view',0),(2,'sendfriend_product',0),(3,'catalog_product_compare_add_product',0),(4,'checkout_cart_add_product',0),(5,'wishlist_add_product',0),(6,'wishlist_share',0);
/*!40000 ALTER TABLE `report_event_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_viewed_product_aggregated_daily`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_daily`;
CREATE TABLE `report_viewed_product_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_DAILY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Daily';

--
-- Table structure for table `report_viewed_product_aggregated_monthly`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_monthly`;
CREATE TABLE `report_viewed_product_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_MONTHLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Monthly';

--
-- Table structure for table `report_viewed_product_aggregated_yearly`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_yearly`;
CREATE TABLE `report_viewed_product_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_YEARLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Yearly';

--
-- Table structure for table `report_viewed_product_index`
--

DROP TABLE IF EXISTS `report_viewed_product_index`;
CREATE TABLE `report_viewed_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `REPORT_VIEWED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `REPORT_VIEWED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Reports Viewed Product Index Table';

--
-- Dumping data for table `report_viewed_product_index`
--

LOCK TABLES `report_viewed_product_index` WRITE;
/*!40000 ALTER TABLE `report_viewed_product_index` DISABLE KEYS */;
INSERT INTO `report_viewed_product_index` VALUES (2,'20',NULL,'2',1,'2017-06-13 07:16:21'),(6,'25',NULL,'3',1,'2017-06-13 08:02:15'),(8,'56',NULL,'3',1,'2017-06-13 17:46:35'),(9,'261',NULL,'2',1,'2017-06-21 09:01:50'),(10,'263',NULL,'3',1,'2017-06-21 09:23:10'),(11,'313',NULL,'2',1,'2017-06-22 06:51:19'),(12,'359',NULL,'2',1,'2017-06-23 07:54:55'),(13,'375',NULL,'2',1,'2017-06-23 09:00:01'),(14,'377',NULL,'10',1,'2017-06-23 09:44:10'),(15,'378',NULL,'2',1,'2017-06-23 10:10:01');
/*!40000 ALTER TABLE `report_viewed_product_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporting_counts`
--

DROP TABLE IF EXISTS `reporting_counts`;
CREATE TABLE `reporting_counts` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'Item Reported',
  `count` int(10) unsigned DEFAULT NULL COMMENT 'Count Value',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for all count related events generated via the cron job';

--
-- Table structure for table `reporting_module_status`
--

DROP TABLE IF EXISTS `reporting_module_status`;
CREATE TABLE `reporting_module_status` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Module Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Module Name',
  `active` varchar(255) DEFAULT NULL COMMENT 'Module Active Status',
  `setup_version` varchar(255) DEFAULT NULL COMMENT 'Module Version',
  `state` varchar(255) DEFAULT NULL COMMENT 'Module State',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module Status Table';

--
-- Table structure for table `reporting_orders`
--

DROP TABLE IF EXISTS `reporting_orders`;
CREATE TABLE `reporting_orders` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `total` decimal(20,2) DEFAULT NULL COMMENT 'Total From Store',
  `total_base` decimal(20,2) DEFAULT NULL COMMENT 'Total From Base Currency',
  `item_count` int(10) unsigned NOT NULL COMMENT 'Line Item Count',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for all orders';

--
-- Table structure for table `reporting_system_updates`
--

DROP TABLE IF EXISTS `reporting_system_updates`;
CREATE TABLE `reporting_system_updates` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'Update Type',
  `action` varchar(255) DEFAULT NULL COMMENT 'Action Performed',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for system updates';

--
-- Table structure for table `reporting_users`
--

DROP TABLE IF EXISTS `reporting_users`;
CREATE TABLE `reporting_users` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'User Type',
  `action` varchar(255) DEFAULT NULL COMMENT 'Action Performed',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for user actions';

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `review_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Review create date',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity id',
  `entity_pk_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `status_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Status code',
  PRIMARY KEY (`review_id`),
  KEY `REVIEW_ENTITY_ID` (`entity_id`),
  KEY `REVIEW_STATUS_ID` (`status_id`),
  KEY `REVIEW_ENTITY_PK_VALUE` (`entity_pk_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review base information';

--
-- Table structure for table `review_detail`
--

DROP TABLE IF EXISTS `review_detail`;
CREATE TABLE `review_detail` (
  `detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review detail id',
  `review_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Review id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store id',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `detail` text NOT NULL COMMENT 'Detail description',
  `nickname` varchar(128) NOT NULL COMMENT 'User nickname',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  PRIMARY KEY (`detail_id`),
  KEY `REVIEW_DETAIL_REVIEW_ID` (`review_id`),
  KEY `REVIEW_DETAIL_STORE_ID` (`store_id`),
  KEY `REVIEW_DETAIL_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review detail information';

--
-- Table structure for table `review_entity`
--

DROP TABLE IF EXISTS `review_entity`;
CREATE TABLE `review_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review entity id',
  `entity_code` varchar(32) NOT NULL COMMENT 'Review entity code',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review entities';

--
-- Dumping data for table `review_entity`
--

LOCK TABLES `review_entity` WRITE;
/*!40000 ALTER TABLE `review_entity` DISABLE KEYS */;
INSERT INTO `review_entity` VALUES (1,'product'),(2,'customer'),(3,'category');
/*!40000 ALTER TABLE `review_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_entity_summary`
--

DROP TABLE IF EXISTS `review_entity_summary`;
CREATE TABLE `review_entity_summary` (
  `primary_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Summary review entity id',
  `entity_pk_value` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Product id',
  `entity_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Entity type id',
  `reviews_count` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Qty of reviews',
  `rating_summary` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Summarized rating',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`primary_id`),
  KEY `REVIEW_ENTITY_SUMMARY_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review aggregates';

--
-- Table structure for table `review_status`
--

DROP TABLE IF EXISTS `review_status`;
CREATE TABLE `review_status` (
  `status_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Status id',
  `status_code` varchar(32) NOT NULL COMMENT 'Status code',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review statuses';

--
-- Dumping data for table `review_status`
--

LOCK TABLES `review_status` WRITE;
/*!40000 ALTER TABLE `review_status` DISABLE KEYS */;
INSERT INTO `review_status` VALUES (1,'Approved'),(2,'Pending'),(3,'Not Approved');
/*!40000 ALTER TABLE `review_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_store`
--

DROP TABLE IF EXISTS `review_store`;
CREATE TABLE `review_store` (
  `review_id` bigint(20) unsigned NOT NULL COMMENT 'Review Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`review_id`,`store_id`),
  KEY `REVIEW_STORE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review Store';

--
-- Table structure for table `sales_bestsellers_aggregated_daily`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_daily`;
CREATE TABLE `sales_bestsellers_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Daily';

--
-- Table structure for table `sales_bestsellers_aggregated_monthly`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_monthly`;
CREATE TABLE `sales_bestsellers_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Monthly';

--
-- Table structure for table `sales_bestsellers_aggregated_yearly`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_yearly`;
CREATE TABLE `sales_bestsellers_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Yearly';

--
-- Table structure for table `sales_creditmemo`
--

DROP TABLE IF EXISTS `sales_creditmemo`;
CREATE TABLE `sales_creditmemo` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `creditmemo_status` int(11) DEFAULT NULL COMMENT 'Creditmemo Status',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Invoice Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_CREDITMEMO_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_CREDITMEMO_STORE_ID` (`store_id`),
  KEY `SALES_CREDITMEMO_ORDER_ID` (`order_id`),
  KEY `SALES_CREDITMEMO_CREDITMEMO_STATUS` (`creditmemo_status`),
  KEY `SALES_CREDITMEMO_STATE` (`state`),
  KEY `SALES_CREDITMEMO_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_UPDATED_AT` (`updated_at`),
  KEY `SALES_CREDITMEMO_SEND_EMAIL` (`send_email`),
  KEY `SALES_CREDITMEMO_EMAIL_SENT` (`email_sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo';

--
-- Table structure for table `sales_creditmemo_comment`
--

DROP TABLE IF EXISTS `sales_creditmemo_comment`;
CREATE TABLE `sales_creditmemo_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_CREDITMEMO_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_COMMENT_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Comment';

--
-- Table structure for table `sales_creditmemo_grid`
--

DROP TABLE IF EXISTS `sales_creditmemo_grid`;
CREATE TABLE `sales_creditmemo_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `state` int(11) DEFAULT NULL COMMENT 'Status',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `order_base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Order Grand Total',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_CREDITMEMO_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_CREDITMEMO_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_CREDITMEMO_GRID_STATE` (`state`),
  KEY `SALES_CREDITMEMO_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_STATUS` (`order_status`),
  KEY `SALES_CREDITMEMO_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_CREDITMEMO_GRID_STORE_ID` (`store_id`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_BASE_GRAND_TOTAL` (`order_base_grand_total`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_ID` (`order_id`),
  FULLTEXT KEY `FTI_32B7BA885941A8254EE84AE650ABDC86` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Grid';

--
-- Table structure for table `sales_creditmemo_item`
--

DROP TABLE IF EXISTS `sales_creditmemo_item`;
CREATE TABLE `sales_creditmemo_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax in the creditmemo item over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_CREDITMEMO_ITEM_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Item';

--
-- Table structure for table `sales_invoice`
--

DROP TABLE IF EXISTS `sales_invoice`;
CREATE TABLE `sales_invoice` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `is_used_for_refund` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Used For Refund',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `can_void_flag` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Void Flag',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_INVOICE_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_INVOICE_STORE_ID` (`store_id`),
  KEY `SALES_INVOICE_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_INVOICE_ORDER_ID` (`order_id`),
  KEY `SALES_INVOICE_STATE` (`state`),
  KEY `SALES_INVOICE_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_UPDATED_AT` (`updated_at`),
  KEY `SALES_INVOICE_SEND_EMAIL` (`send_email`),
  KEY `SALES_INVOICE_EMAIL_SENT` (`email_sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice';

--
-- Table structure for table `sales_invoice_comment`
--

DROP TABLE IF EXISTS `sales_invoice_comment`;
CREATE TABLE `sales_invoice_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_INVOICE_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_COMMENT_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Comment';

--
-- Table structure for table `sales_invoice_grid`
--

DROP TABLE IF EXISTS `sales_invoice_grid`;
CREATE TABLE `sales_invoice_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(128) DEFAULT NULL COMMENT 'Payment Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_INVOICE_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_INVOICE_GRID_STORE_ID` (`store_id`),
  KEY `SALES_INVOICE_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_INVOICE_GRID_ORDER_ID` (`order_id`),
  KEY `SALES_INVOICE_GRID_STATE` (`state`),
  KEY `SALES_INVOICE_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_INVOICE_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_INVOICE_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_INVOICE_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_INVOICE_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  FULLTEXT KEY `FTI_95D9C924DD6A8734EB8B5D01D60F90DE` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Grid';

--
-- Table structure for table `sales_invoice_item`
--

DROP TABLE IF EXISTS `sales_invoice_item`;
CREATE TABLE `sales_invoice_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax invoiced over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_INVOICE_ITEM_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Item';

--
-- Table structure for table `sales_invoiced_aggregated`
--

DROP TABLE IF EXISTS `sales_invoiced_aggregated`;
CREATE TABLE `sales_invoiced_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_INVOICED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_INVOICED_AGGREGATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated';

--
-- Table structure for table `sales_invoiced_aggregated_order`
--

DROP TABLE IF EXISTS `sales_invoiced_aggregated_order`;
CREATE TABLE `sales_invoiced_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_INVOICED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated Order';

--
-- Table structure for table `sales_order`
--

DROP TABLE IF EXISTS `sales_order`;
CREATE TABLE `sales_order` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `state` varchar(32) DEFAULT NULL COMMENT 'State',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `protect_code` varchar(255) DEFAULT NULL COMMENT 'Protect Code',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Canceled',
  `base_discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Invoiced',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `base_shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Canceled',
  `base_shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Invoiced',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Refunded',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `base_subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Canceled',
  `base_subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Invoiced',
  `base_subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Refunded',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Canceled',
  `base_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Invoiced',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `base_total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Canceled',
  `base_total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced',
  `base_total_invoiced_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced Cost',
  `base_total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Offline Refunded',
  `base_total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Online Refunded',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `base_total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Qty Ordered',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Canceled',
  `discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Invoiced',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Canceled',
  `shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Invoiced',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Refunded',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Canceled',
  `subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Invoiced',
  `subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Refunded',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Tax Invoiced',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Total Canceled',
  `total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Total Invoiced',
  `total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Offline Refunded',
  `total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Online Refunded',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty Ordered',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  `can_ship_partially` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially',
  `can_ship_partially_item` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially Item',
  `customer_is_guest` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Is Guest',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `edit_increment` int(11) DEFAULT NULL COMMENT 'Edit Increment',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `forced_shipment_with_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Forced Do Shipment With Invoice',
  `payment_auth_expiration` int(11) DEFAULT NULL COMMENT 'Payment Authorization Expiration',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `quote_id` int(11) DEFAULT NULL COMMENT 'Quote Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `base_total_due` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Due',
  `payment_authorization_amount` decimal(12,4) DEFAULT NULL COMMENT 'Payment Authorization Amount',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `total_due` decimal(12,4) DEFAULT NULL COMMENT 'Total Due',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `increment_id` varchar(32) DEFAULT NULL COMMENT 'Increment Id',
  `applied_rule_ids` varchar(128) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_firstname` varchar(128) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_lastname` varchar(128) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_middlename` varchar(128) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_prefix` varchar(32) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_suffix` varchar(32) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_taxvat` varchar(32) DEFAULT NULL COMMENT 'Customer Taxvat',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `ext_customer_id` varchar(32) DEFAULT NULL COMMENT 'Ext Customer Id',
  `ext_order_id` varchar(32) DEFAULT NULL COMMENT 'Ext Order Id',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `hold_before_state` varchar(32) DEFAULT NULL COMMENT 'Hold Before State',
  `hold_before_status` varchar(32) DEFAULT NULL COMMENT 'Hold Before Status',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `original_increment_id` varchar(32) DEFAULT NULL COMMENT 'Original Increment Id',
  `relation_child_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Id',
  `relation_child_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Real Id',
  `relation_parent_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Id',
  `relation_parent_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Real Id',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `shipping_method` varchar(32) DEFAULT NULL COMMENT 'Shipping Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `store_name` varchar(32) DEFAULT NULL COMMENT 'Store Name',
  `x_forwarded_for` varchar(32) DEFAULT NULL COMMENT 'X Forwarded For',
  `customer_note` text COMMENT 'Customer Note',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `total_item_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Total Item Count',
  `customer_gender` int(11) DEFAULT NULL COMMENT 'Customer Gender',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `coupon_rule_name` varchar(255) DEFAULT NULL COMMENT 'Coupon Sales Rule Name',
  `paypal_ipn_customer_notified` int(11) DEFAULT '0' COMMENT 'Paypal Ipn Customer Notified',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_ORDER_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_ORDER_STATUS` (`status`),
  KEY `SALES_ORDER_STATE` (`state`),
  KEY `SALES_ORDER_STORE_ID` (`store_id`),
  KEY `SALES_ORDER_CREATED_AT` (`created_at`),
  KEY `SALES_ORDER_CUSTOMER_ID` (`customer_id`),
  KEY `SALES_ORDER_EXT_ORDER_ID` (`ext_order_id`),
  KEY `SALES_ORDER_QUOTE_ID` (`quote_id`),
  KEY `SALES_ORDER_UPDATED_AT` (`updated_at`),
  KEY `SALES_ORDER_SEND_EMAIL` (`send_email`),
  KEY `SALES_ORDER_EMAIL_SENT` (`email_sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order';

--
-- Table structure for table `sales_order_address`
--

DROP TABLE IF EXISTS `sales_order_address`;
CREATE TABLE `sales_order_address` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `customer_address_id` int(11) DEFAULT NULL COMMENT 'Customer Address Id',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `region_id` int(11) DEFAULT NULL COMMENT 'Region Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Postcode',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Lastname',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(255) DEFAULT NULL COMMENT 'City',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `telephone` varchar(255) DEFAULT NULL COMMENT 'Phone Number',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Firstname',
  `address_type` varchar(255) DEFAULT NULL COMMENT 'Address Type',
  `prefix` varchar(255) DEFAULT NULL COMMENT 'Prefix',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middlename',
  `suffix` varchar(255) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_ADDRESS_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Address';

--
-- Table structure for table `sales_order_aggregated_created`
--

DROP TABLE IF EXISTS `sales_order_aggregated_created`;
CREATE TABLE `sales_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_ORDER_AGGREGATED_CREATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Created';

--
-- Table structure for table `sales_order_aggregated_updated`
--

DROP TABLE IF EXISTS `sales_order_aggregated_updated`;
CREATE TABLE `sales_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_ORDER_AGGREGATED_UPDATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Updated';

--
-- Table structure for table `sales_order_canceling`
--

DROP TABLE IF EXISTS `sales_order_canceling`;
CREATE TABLE `sales_order_canceling` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`item_id`),
  KEY `SALES_ORDER_CANCELING_ORDER_ID` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Canceling History';

--
-- Table structure for table `sales_order_exporting`
--

DROP TABLE IF EXISTS `sales_order_exporting`;
CREATE TABLE `sales_order_exporting` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`item_id`),
  KEY `SALES_ORDER_EXPORTING_ORDER_ID` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Exporting History';

--
-- Table structure for table `sales_order_grid`
--

DROP TABLE IF EXISTS `sales_order_grid`;
CREATE TABLE `sales_order_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `order_currency_code` varchar(255) DEFAULT NULL COMMENT 'Order Currency Code',
  `shipping_name` varchar(255) DEFAULT NULL COMMENT 'Shipping Name',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group` varchar(255) DEFAULT NULL COMMENT 'Customer Group',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `payment_method` varchar(255) DEFAULT NULL COMMENT 'Payment Method',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_ORDER_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_ORDER_GRID_STATUS` (`status`),
  KEY `SALES_ORDER_GRID_STORE_ID` (`store_id`),
  KEY `SALES_ORDER_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_ORDER_GRID_BASE_TOTAL_PAID` (`base_total_paid`),
  KEY `SALES_ORDER_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_ORDER_GRID_TOTAL_PAID` (`total_paid`),
  KEY `SALES_ORDER_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `SALES_ORDER_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_ORDER_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_ORDER_GRID_CUSTOMER_ID` (`customer_id`),
  KEY `SALES_ORDER_GRID_UPDATED_AT` (`updated_at`),
  FULLTEXT KEY `FTI_65B9E9925EC58F0C7C2E2F6379C233E7` (`increment_id`,`billing_name`,`shipping_name`,`shipping_address`,`billing_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Grid';

--
-- Table structure for table `sales_order_item`
--

DROP TABLE IF EXISTS `sales_order_item`;
CREATE TABLE `sales_order_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Quote Item Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `product_options` text COMMENT 'Product Options',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'No Discount',
  `qty_backordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Backordered',
  `qty_canceled` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Canceled',
  `qty_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Invoiced',
  `qty_ordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `qty_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Refunded',
  `qty_shipped` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Shipped',
  `base_cost` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Cost',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `original_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `base_original_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Original Price',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Invoiced',
  `base_tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Invoiced',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Invoiced',
  `base_discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Invoiced',
  `amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Amount Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Amount Refunded',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Invoiced',
  `base_row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Invoiced',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `ext_order_item_id` varchar(255) DEFAULT NULL COMMENT 'Ext Order Item Id',
  `locked_do_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Invoice',
  `locked_do_ship` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Ship',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `discount_tax_compensation_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Canceled',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `free_shipping` smallint(6) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `gift_message_available` int(11) DEFAULT NULL COMMENT 'Gift Message Available',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `SALES_ORDER_ITEM_ORDER_ID` (`order_id`),
  KEY `SALES_ORDER_ITEM_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Item';

--
-- Table structure for table `sales_order_payment`
--

DROP TABLE IF EXISTS `sales_order_payment`;
CREATE TABLE `sales_order_payment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Captured',
  `shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Captured',
  `amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Amount Refunded',
  `base_amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid',
  `amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Amount Canceled',
  `base_amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Authorized',
  `base_amount_paid_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid Online',
  `base_amount_refunded_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded Online',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Amount Paid',
  `amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Amount Authorized',
  `base_amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Ordered',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded',
  `amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Amount Ordered',
  `base_amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Canceled',
  `quote_payment_id` int(11) DEFAULT NULL COMMENT 'Quote Payment Id',
  `additional_data` text COMMENT 'Additional Data',
  `cc_exp_month` varchar(12) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_ss_start_year` varchar(12) DEFAULT NULL COMMENT 'Cc Ss Start Year',
  `echeck_bank_name` varchar(128) DEFAULT NULL COMMENT 'Echeck Bank Name',
  `method` varchar(128) DEFAULT NULL COMMENT 'Method',
  `cc_debug_request_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Request Body',
  `cc_secure_verify` varchar(32) DEFAULT NULL COMMENT 'Cc Secure Verify',
  `protection_eligibility` varchar(32) DEFAULT NULL COMMENT 'Protection Eligibility',
  `cc_approval` varchar(32) DEFAULT NULL COMMENT 'Cc Approval',
  `cc_last_4` varchar(100) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_status_description` varchar(32) DEFAULT NULL COMMENT 'Cc Status Description',
  `echeck_type` varchar(32) DEFAULT NULL COMMENT 'Echeck Type',
  `cc_debug_response_serialized` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Serialized',
  `cc_ss_start_month` varchar(128) DEFAULT NULL COMMENT 'Cc Ss Start Month',
  `echeck_account_type` varchar(255) DEFAULT NULL COMMENT 'Echeck Account Type',
  `last_trans_id` varchar(32) DEFAULT NULL COMMENT 'Last Trans Id',
  `cc_cid_status` varchar(32) DEFAULT NULL COMMENT 'Cc Cid Status',
  `cc_owner` varchar(128) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_type` varchar(32) DEFAULT NULL COMMENT 'Cc Type',
  `po_number` varchar(32) DEFAULT NULL COMMENT 'Po Number',
  `cc_exp_year` varchar(4) DEFAULT NULL COMMENT 'Cc Exp Year',
  `cc_status` varchar(4) DEFAULT NULL COMMENT 'Cc Status',
  `echeck_routing_number` varchar(32) DEFAULT NULL COMMENT 'Echeck Routing Number',
  `account_status` varchar(32) DEFAULT NULL COMMENT 'Account Status',
  `anet_trans_method` varchar(32) DEFAULT NULL COMMENT 'Anet Trans Method',
  `cc_debug_response_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Body',
  `cc_ss_issue` varchar(32) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `echeck_account_name` varchar(32) DEFAULT NULL COMMENT 'Echeck Account Name',
  `cc_avs_status` varchar(32) DEFAULT NULL COMMENT 'Cc Avs Status',
  `cc_number_enc` varchar(32) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_trans_id` varchar(32) DEFAULT NULL COMMENT 'Cc Trans Id',
  `address_status` varchar(32) DEFAULT NULL COMMENT 'Address Status',
  `additional_information` text COMMENT 'Additional Information',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_PAYMENT_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Payment';

--
-- Table structure for table `sales_order_status`
--

DROP TABLE IF EXISTS `sales_order_status`;
CREATE TABLE `sales_order_status` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

--
-- Dumping data for table `sales_order_status`
--

LOCK TABLES `sales_order_status` WRITE;
/*!40000 ALTER TABLE `sales_order_status` DISABLE KEYS */;
INSERT INTO `sales_order_status` VALUES ('canceled','Canceled'),('closed','Closed'),('complete','Complete'),('fraud','Suspected Fraud'),('holded','On Hold'),('payment_review','Payment Review'),('paypal_canceled_reversal','PayPal Canceled Reversal'),('paypal_reversed','PayPal Reversed'),('pending','Pending'),('pending_payment','Pending Payment'),('pending_paypal','Pending PayPal'),('processing','Processing');
/*!40000 ALTER TABLE `sales_order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_status_history`
--

DROP TABLE IF EXISTS `sales_order_status_history`;
CREATE TABLE `sales_order_status_history` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `entity_name` varchar(32) DEFAULT NULL COMMENT 'Shows what entity history is bind to.',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_STATUS_HISTORY_PARENT_ID` (`parent_id`),
  KEY `SALES_ORDER_STATUS_HISTORY_CREATED_AT` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Status History';

--
-- Table structure for table `sales_order_status_label`
--

DROP TABLE IF EXISTS `sales_order_status_label`;
CREATE TABLE `sales_order_status_label` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`,`store_id`),
  KEY `SALES_ORDER_STATUS_LABEL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Label Table';

--
-- Table structure for table `sales_order_status_state`
--

DROP TABLE IF EXISTS `sales_order_status_state`;
CREATE TABLE `sales_order_status_state` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `state` varchar(32) NOT NULL COMMENT 'Label',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visible on front',
  PRIMARY KEY (`status`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

--
-- Dumping data for table `sales_order_status_state`
--

LOCK TABLES `sales_order_status_state` WRITE;
/*!40000 ALTER TABLE `sales_order_status_state` DISABLE KEYS */;
INSERT INTO `sales_order_status_state` VALUES ('canceled','canceled',1,1),('closed','closed',1,1),('complete','complete',1,1),('fraud','payment_review',0,1),('fraud','processing',0,1),('holded','holded',1,1),('payment_review','payment_review',1,1),('pending','new',1,1),('pending_payment','pending_payment',1,0),('processing','processing',1,1);
/*!40000 ALTER TABLE `sales_order_status_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_tax`
--

DROP TABLE IF EXISTS `sales_order_tax`;
CREATE TABLE `sales_order_tax` (
  `tax_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `percent` decimal(12,4) DEFAULT NULL COMMENT 'Percent',
  `amount` decimal(12,4) DEFAULT NULL COMMENT 'Amount',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `base_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount',
  `process` smallint(6) NOT NULL COMMENT 'Process',
  `base_real_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Real Amount',
  PRIMARY KEY (`tax_id`),
  KEY `SALES_ORDER_TAX_ORDER_ID_PRIORITY_POSITION` (`order_id`,`priority`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Table';

--
-- Table structure for table `sales_order_tax_item`
--

DROP TABLE IF EXISTS `sales_order_tax_item`;
CREATE TABLE `sales_order_tax_item` (
  `tax_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Item Id',
  `tax_id` int(10) unsigned NOT NULL COMMENT 'Tax Id',
  `item_id` int(10) unsigned DEFAULT NULL COMMENT 'Item Id',
  `tax_percent` decimal(12,4) NOT NULL COMMENT 'Real Tax Percent For Item',
  `amount` decimal(12,4) NOT NULL COMMENT 'Tax amount for the item and tax rate',
  `base_amount` decimal(12,4) NOT NULL COMMENT 'Base tax amount for the item and tax rate',
  `real_amount` decimal(12,4) NOT NULL COMMENT 'Real tax amount for the item and tax rate',
  `real_base_amount` decimal(12,4) NOT NULL COMMENT 'Real base tax amount for the item and tax rate',
  `associated_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Id of the associated item',
  `taxable_item_type` varchar(32) NOT NULL COMMENT 'Type of the taxable item',
  PRIMARY KEY (`tax_item_id`),
  UNIQUE KEY `SALES_ORDER_TAX_ITEM_TAX_ID_ITEM_ID` (`tax_id`,`item_id`),
  KEY `SALES_ORDER_TAX_ITEM_ITEM_ID` (`item_id`),
  KEY `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` (`associated_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Item';

--
-- Table structure for table `sales_payment_transaction`
--

DROP TABLE IF EXISTS `sales_payment_transaction`;
CREATE TABLE `sales_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Transaction Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `payment_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Payment Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `parent_txn_id` varchar(100) DEFAULT NULL COMMENT 'Parent Txn Id',
  `txn_type` varchar(15) DEFAULT NULL COMMENT 'Txn Type',
  `is_closed` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Closed',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `SALES_PAYMENT_TRANSACTION_ORDER_ID_PAYMENT_ID_TXN_ID` (`order_id`,`payment_id`,`txn_id`),
  KEY `SALES_PAYMENT_TRANSACTION_PARENT_ID` (`parent_id`),
  KEY `SALES_PAYMENT_TRANSACTION_PAYMENT_ID` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Payment Transaction';

--
-- Table structure for table `sales_refunded_aggregated`
--

DROP TABLE IF EXISTS `sales_refunded_aggregated`;
CREATE TABLE `sales_refunded_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_REFUNDED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_REFUNDED_AGGREGATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated';

--
-- Table structure for table `sales_refunded_aggregated_order`
--

DROP TABLE IF EXISTS `sales_refunded_aggregated_order`;
CREATE TABLE `sales_refunded_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_REFUNDED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated Order';

--
-- Table structure for table `sales_sequence_meta`
--

DROP TABLE IF EXISTS `sales_sequence_meta`;
CREATE TABLE `sales_sequence_meta` (
  `meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Prefix',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `sequence_table` varchar(32) NOT NULL COMMENT 'table for sequence',
  PRIMARY KEY (`meta_id`),
  UNIQUE KEY `SALES_SEQUENCE_META_ENTITY_TYPE_STORE_ID` (`entity_type`,`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='sales_sequence_meta';

--
-- Dumping data for table `sales_sequence_meta`
--

LOCK TABLES `sales_sequence_meta` WRITE;
/*!40000 ALTER TABLE `sales_sequence_meta` DISABLE KEYS */;
INSERT INTO `sales_sequence_meta` VALUES ('1','order',0,'sequence_order_0'),('2','invoice',0,'sequence_invoice_0'),('3','creditmemo',0,'sequence_creditmemo_0'),('4','shipment',0,'sequence_shipment_0'),('5','order',1,'sequence_order_1'),('6','invoice',1,'sequence_invoice_1'),('7','creditmemo',1,'sequence_creditmemo_1'),('8','shipment',1,'sequence_shipment_1'),('9','order',2,'sequence_order_2'),('10','invoice',2,'sequence_invoice_2'),('11','creditmemo',2,'sequence_creditmemo_2'),('12','shipment',2,'sequence_shipment_2');
/*!40000 ALTER TABLE `sales_sequence_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_sequence_profile`
--

DROP TABLE IF EXISTS `sales_sequence_profile`;
CREATE TABLE `sales_sequence_profile` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `meta_id` int(10) unsigned NOT NULL COMMENT 'Meta_id',
  `prefix` varchar(32) DEFAULT NULL COMMENT 'Prefix',
  `suffix` varchar(32) DEFAULT NULL COMMENT 'Suffix',
  `start_value` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Start value for sequence',
  `step` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Step for sequence',
  `max_value` int(10) unsigned NOT NULL COMMENT 'MaxValue for sequence',
  `warning_value` int(10) unsigned NOT NULL COMMENT 'WarningValue for sequence',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'isActive flag',
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `SALES_SEQUENCE_PROFILE_META_ID_PREFIX_SUFFIX` (`meta_id`,`prefix`,`suffix`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='sales_sequence_profile';

--
-- Dumping data for table `sales_sequence_profile`
--

LOCK TABLES `sales_sequence_profile` WRITE;
/*!40000 ALTER TABLE `sales_sequence_profile` DISABLE KEYS */;
INSERT INTO `sales_sequence_profile` VALUES ('1','1',NULL,NULL,'1','1','4294967295','4294966295',1),('2','2',NULL,NULL,'1','1','4294967295','4294966295',1),('3','3',NULL,NULL,'1','1','4294967295','4294966295',1),('4','4',NULL,NULL,'1','1','4294967295','4294966295',1),('5','5',NULL,NULL,'1','1','4294967295','4294966295',1),('6','6',NULL,NULL,'1','1','4294967295','4294966295',1),('7','7',NULL,NULL,'1','1','4294967295','4294966295',1),('8','8',NULL,NULL,'1','1','4294967295','4294966295',1),('9','9','2',NULL,'1','1','4294967295','4294966295',1),('10','10','2',NULL,'1','1','4294967295','4294966295',1),('11','11','2',NULL,'1','1','4294967295','4294966295',1),('12','12','2',NULL,'1','1','4294967295','4294966295',1);
/*!40000 ALTER TABLE `sales_sequence_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipment`
--

DROP TABLE IF EXISTS `sales_shipment`;
CREATE TABLE `sales_shipment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `total_weight` decimal(12,4) DEFAULT NULL COMMENT 'Total Weight',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `packages` text COMMENT 'Packed Products in Packages',
  `shipping_label` mediumblob COMMENT 'Shipping Label Content',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_SHIPMENT_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_SHIPMENT_STORE_ID` (`store_id`),
  KEY `SALES_SHIPMENT_TOTAL_QTY` (`total_qty`),
  KEY `SALES_SHIPMENT_ORDER_ID` (`order_id`),
  KEY `SALES_SHIPMENT_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_UPDATED_AT` (`updated_at`),
  KEY `SALES_SHIPMENT_SEND_EMAIL` (`send_email`),
  KEY `SALES_SHIPMENT_EMAIL_SENT` (`email_sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment';

--
-- Table structure for table `sales_shipment_comment`
--

DROP TABLE IF EXISTS `sales_shipment_comment`;
CREATE TABLE `sales_shipment_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_COMMENT_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Comment';

--
-- Table structure for table `sales_shipment_grid`
--

DROP TABLE IF EXISTS `sales_shipment_grid`;
CREATE TABLE `sales_shipment_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_increment_id` varchar(32) NOT NULL COMMENT 'Order Increment Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Order Increment Id',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `billing_name` varchar(128) DEFAULT NULL COMMENT 'Billing Name',
  `shipping_name` varchar(128) DEFAULT NULL COMMENT 'Shipping Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_SHIPMENT_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_SHIPMENT_GRID_STORE_ID` (`store_id`),
  KEY `SALES_SHIPMENT_GRID_TOTAL_QTY` (`total_qty`),
  KEY `SALES_SHIPMENT_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_SHIPMENT_GRID_SHIPMENT_STATUS` (`shipment_status`),
  KEY `SALES_SHIPMENT_GRID_ORDER_STATUS` (`order_status`),
  KEY `SALES_SHIPMENT_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_SHIPMENT_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_SHIPMENT_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `SALES_SHIPMENT_GRID_BILLING_NAME` (`billing_name`),
  FULLTEXT KEY `FTI_086B40C8955F167B8EA76653437879B4` (`increment_id`,`order_increment_id`,`shipping_name`,`customer_name`,`customer_email`,`billing_address`,`shipping_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Grid';

--
-- Table structure for table `sales_shipment_item`
--

DROP TABLE IF EXISTS `sales_shipment_item`;
CREATE TABLE `sales_shipment_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_ITEM_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Item';

--
-- Table structure for table `sales_shipment_track`
--

DROP TABLE IF EXISTS `sales_shipment_track`;
CREATE TABLE `sales_shipment_track` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `track_number` text COMMENT 'Number',
  `description` text COMMENT 'Description',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `carrier_code` varchar(32) DEFAULT NULL COMMENT 'Carrier Code',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_TRACK_PARENT_ID` (`parent_id`),
  KEY `SALES_SHIPMENT_TRACK_ORDER_ID` (`order_id`),
  KEY `SALES_SHIPMENT_TRACK_CREATED_AT` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Track';

--
-- Table structure for table `sales_shipping_aggregated`
--

DROP TABLE IF EXISTS `sales_shipping_aggregated`;
CREATE TABLE `sales_shipping_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_SHPP_AGGRED_PERIOD_STORE_ID_ORDER_STS_SHPP_DESCRIPTION` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `SALES_SHIPPING_AGGREGATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated';

--
-- Table structure for table `sales_shipping_aggregated_order`
--

DROP TABLE IF EXISTS `sales_shipping_aggregated_order`;
CREATE TABLE `sales_shipping_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_C05FAE47282EEA68654D0924E946761F` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated Order';

--
-- Table structure for table `salesrule`
--

DROP TABLE IF EXISTS `salesrule`;
CREATE TABLE `salesrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `uses_per_customer` int(11) NOT NULL DEFAULT '0' COMMENT 'Uses Per Customer',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `is_advanced` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Advanced',
  `product_ids` text COMMENT 'Product Ids',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `discount_qty` decimal(12,4) DEFAULT NULL COMMENT 'Discount Qty',
  `discount_step` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Discount Step',
  `apply_to_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Apply To Shipping',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `is_rss` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Rss',
  `coupon_type` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Coupon Type',
  `use_auto_generation` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Use Auto Generation',
  `uses_per_coupon` int(11) NOT NULL DEFAULT '0' COMMENT 'User Per Coupon',
  `simple_free_shipping` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `SALESRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule';

--
-- Table structure for table `salesrule_coupon`
--

DROP TABLE IF EXISTS `salesrule_coupon`;
CREATE TABLE `salesrule_coupon` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Coupon Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `usage_limit` int(10) unsigned DEFAULT NULL COMMENT 'Usage Limit',
  `usage_per_customer` int(10) unsigned DEFAULT NULL COMMENT 'Usage Per Customer',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `expiration_date` timestamp NULL DEFAULT NULL COMMENT 'Expiration Date',
  `is_primary` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Primary',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Coupon Code Creation Date',
  `type` smallint(6) DEFAULT '0' COMMENT 'Coupon Code Type',
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `SALESRULE_COUPON_CODE` (`code`),
  UNIQUE KEY `SALESRULE_COUPON_RULE_ID_IS_PRIMARY` (`rule_id`,`is_primary`),
  KEY `SALESRULE_COUPON_RULE_ID` (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon';

--
-- Table structure for table `salesrule_coupon_aggregated`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated`;
CREATE TABLE `salesrule_coupon_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALESRULE_COUPON_AGGRED_PERIOD_STORE_ID_ORDER_STS_COUPON_CODE` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_RULE_NAME` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated';

--
-- Table structure for table `salesrule_coupon_aggregated_order`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated_order`;
CREATE TABLE `salesrule_coupon_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_1094D1FBBCBB11704A29DEF3ACC37D2B` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_ORDER_RULE_NAME` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated Order';

--
-- Table structure for table `salesrule_coupon_aggregated_updated`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated_updated`;
CREATE TABLE `salesrule_coupon_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_7196FA120A4F0F84E1B66605E87E213E` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_RULE_NAME` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Aggregated Updated';

--
-- Table structure for table `salesrule_coupon_usage`
--

DROP TABLE IF EXISTS `salesrule_coupon_usage`;
CREATE TABLE `salesrule_coupon_usage` (
  `coupon_id` int(10) unsigned NOT NULL COMMENT 'Coupon Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`coupon_id`,`customer_id`),
  KEY `SALESRULE_COUPON_USAGE_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Usage';

--
-- Table structure for table `salesrule_customer`
--

DROP TABLE IF EXISTS `salesrule_customer`;
CREATE TABLE `salesrule_customer` (
  `rule_customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Customer Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `times_used` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`rule_customer_id`),
  KEY `SALESRULE_CUSTOMER_RULE_ID_CUSTOMER_ID` (`rule_id`,`customer_id`),
  KEY `SALESRULE_CUSTOMER_CUSTOMER_ID_RULE_ID` (`customer_id`,`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Customer';

--
-- Table structure for table `salesrule_customer_group`
--

DROP TABLE IF EXISTS `salesrule_customer_group`;
CREATE TABLE `salesrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `SALESRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Customer Groups Relations';

--
-- Table structure for table `salesrule_label`
--

DROP TABLE IF EXISTS `salesrule_label`;
CREATE TABLE `salesrule_label` (
  `label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Label Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  PRIMARY KEY (`label_id`),
  UNIQUE KEY `SALESRULE_LABEL_RULE_ID_STORE_ID` (`rule_id`,`store_id`),
  KEY `SALESRULE_LABEL_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Label';

--
-- Table structure for table `salesrule_product_attribute`
--

DROP TABLE IF EXISTS `salesrule_product_attribute`;
CREATE TABLE `salesrule_product_attribute` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`rule_id`,`website_id`,`customer_group_id`,`attribute_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID` (`website_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Product Attribute';

--
-- Table structure for table `salesrule_website`
--

DROP TABLE IF EXISTS `salesrule_website`;
CREATE TABLE `salesrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `SALESRULE_WEBSITE_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Websites Relations';

--
-- Table structure for table `search_query`
--

DROP TABLE IF EXISTS `search_query`;
CREATE TABLE `search_query` (
  `query_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Query ID',
  `query_text` varchar(255) DEFAULT NULL COMMENT 'Query text',
  `num_results` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Num results',
  `popularity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Popularity',
  `redirect` varchar(255) DEFAULT NULL COMMENT 'Redirect',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `display_in_terms` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Display in terms',
  `is_active` smallint(6) DEFAULT '1' COMMENT 'Active status',
  `is_processed` smallint(6) DEFAULT '0' COMMENT 'Processed status',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated at',
  PRIMARY KEY (`query_id`),
  UNIQUE KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID` (`query_text`,`store_id`),
  KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID_POPULARITY` (`query_text`,`store_id`,`popularity`),
  KEY `SEARCH_QUERY_STORE_ID` (`store_id`),
  KEY `SEARCH_QUERY_IS_PROCESSED` (`is_processed`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Search query table';

--
-- Dumping data for table `search_query`
--

LOCK TABLES `search_query` WRITE;
/*!40000 ALTER TABLE `search_query` DISABLE KEYS */;
INSERT INTO `search_query` VALUES ('1','fdfdfsf','0','2',NULL,1,1,1,0,'2017-06-23 03:54:12'),('5','HYPNÔSE DOLL EYES','1','1',NULL,1,1,1,0,'2017-06-23 08:59:48');
/*!40000 ALTER TABLE `search_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_synonyms`
--

DROP TABLE IF EXISTS `search_synonyms`;
CREATE TABLE `search_synonyms` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Synonyms Group Id',
  `synonyms` text NOT NULL COMMENT 'list of synonyms making up this group',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id - identifies the store view these synonyms belong to',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id - identifies the website id these synonyms belong to',
  PRIMARY KEY (`group_id`),
  KEY `SEARCH_SYNONYMS_STORE_ID` (`store_id`),
  KEY `SEARCH_SYNONYMS_WEBSITE_ID` (`website_id`),
  FULLTEXT KEY `SEARCH_SYNONYMS_SYNONYMS` (`synonyms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table storing various synonyms groups';

--
-- Table structure for table `sendfriend_log`
--

DROP TABLE IF EXISTS `sendfriend_log`;
CREATE TABLE `sendfriend_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `ip` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer IP address',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Log time',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  PRIMARY KEY (`log_id`),
  KEY `SENDFRIEND_LOG_IP` (`ip`),
  KEY `SENDFRIEND_LOG_TIME` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Send to friend function log storage table';

--
-- Table structure for table `sequence_creditmemo_0`
--

DROP TABLE IF EXISTS `sequence_creditmemo_0`;
CREATE TABLE `sequence_creditmemo_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_creditmemo_1`
--

DROP TABLE IF EXISTS `sequence_creditmemo_1`;
CREATE TABLE `sequence_creditmemo_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_creditmemo_2`
--

DROP TABLE IF EXISTS `sequence_creditmemo_2`;
CREATE TABLE `sequence_creditmemo_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_invoice_0`
--

DROP TABLE IF EXISTS `sequence_invoice_0`;
CREATE TABLE `sequence_invoice_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_invoice_1`
--

DROP TABLE IF EXISTS `sequence_invoice_1`;
CREATE TABLE `sequence_invoice_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_invoice_2`
--

DROP TABLE IF EXISTS `sequence_invoice_2`;
CREATE TABLE `sequence_invoice_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_order_0`
--

DROP TABLE IF EXISTS `sequence_order_0`;
CREATE TABLE `sequence_order_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_order_1`
--

DROP TABLE IF EXISTS `sequence_order_1`;
CREATE TABLE `sequence_order_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sequence_order_1`
--

LOCK TABLES `sequence_order_1` WRITE;
/*!40000 ALTER TABLE `sequence_order_1` DISABLE KEYS */;
INSERT INTO `sequence_order_1` VALUES ('1');
/*!40000 ALTER TABLE `sequence_order_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_order_2`
--

DROP TABLE IF EXISTS `sequence_order_2`;
CREATE TABLE `sequence_order_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_shipment_0`
--

DROP TABLE IF EXISTS `sequence_shipment_0`;
CREATE TABLE `sequence_shipment_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_shipment_1`
--

DROP TABLE IF EXISTS `sequence_shipment_1`;
CREATE TABLE `sequence_shipment_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sequence_shipment_2`
--

DROP TABLE IF EXISTS `sequence_shipment_2`;
CREATE TABLE `sequence_shipment_2` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `session_id` varchar(255) NOT NULL COMMENT 'Session Id',
  `session_expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Date of Session Expiration',
  `session_data` mediumblob NOT NULL COMMENT 'Session Data',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Database Sessions Storage';

--
-- Table structure for table `setup_module`
--

DROP TABLE IF EXISTS `setup_module`;
CREATE TABLE `setup_module` (
  `module` varchar(50) NOT NULL COMMENT 'Module',
  `schema_version` varchar(50) DEFAULT NULL COMMENT 'Schema Version',
  `data_version` varchar(50) DEFAULT NULL COMMENT 'Data Version',
  PRIMARY KEY (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module versions registry';

--
-- Dumping data for table `setup_module`
--

LOCK TABLES `setup_module` WRITE;
/*!40000 ALTER TABLE `setup_module` DISABLE KEYS */;
INSERT INTO `setup_module` VALUES ('Acommerce_All','2.0.0','2.0.0'),('Acommerce_AutoCancel','2.0.0','2.0.0'),('Acommerce_Ccpp','2.0.0','2.0.0'),('Acommerce_CPMSConnect','2.0.0','2.0.0'),('Acommerce_Kbank','2.0.0','2.0.0'),('Acommerce_OneTwoThree','2.0.0','2.0.0'),('Magento_AdminNotification','2.0.0','2.0.0'),('Magento_AdvancedPricingImportExport','2.0.0','2.0.0'),('Magento_Authorization','2.0.0','2.0.0'),('Magento_Authorizenet','2.0.0','2.0.0'),('Magento_Backend','2.0.0','2.0.0'),('Magento_Backup','2.0.0','2.0.0'),('Magento_Braintree','2.0.0','2.0.0'),('Magento_Bundle','2.0.2','2.0.2'),('Magento_BundleImportExport','2.0.0','2.0.0'),('Magento_CacheInvalidate','2.0.0','2.0.0'),('Magento_Captcha','2.0.0','2.0.0'),('Magento_Catalog','2.1.3','2.1.3'),('Magento_CatalogImportExport','2.0.0','2.0.0'),('Magento_CatalogInventory','2.0.1','2.0.1'),('Magento_CatalogRule','2.0.1','2.0.1'),('Magento_CatalogRuleConfigurable','2.0.0','2.0.0'),('Magento_CatalogSearch','2.0.0','2.0.0'),('Magento_CatalogUrlRewrite','2.0.0','2.0.0'),('Magento_CatalogWidget','2.0.0','2.0.0'),('Magento_Checkout','2.0.0','2.0.0'),('Magento_CheckoutAgreements','2.0.1','2.0.1'),('Magento_Cms','2.0.1','2.0.1'),('Magento_CmsUrlRewrite','2.0.0','2.0.0'),('Magento_Config','2.0.0','2.0.0'),('Magento_ConfigurableImportExport','2.0.0','2.0.0'),('Magento_ConfigurableProduct','2.0.0','2.0.0'),('Magento_Contact','2.0.0','2.0.0'),('Magento_Cookie','2.0.0','2.0.0'),('Magento_Cron','2.0.0','2.0.0'),('Magento_CurrencySymbol','2.0.0','2.0.0'),('Magento_Customer','2.0.9','2.0.9'),('Magento_CustomerImportExport','2.0.0','2.0.0'),('Magento_Deploy','2.0.0','2.0.0'),('Magento_Developer','2.0.0','2.0.0'),('Magento_Dhl','2.0.0','2.0.0'),('Magento_Directory','2.0.0','2.0.0'),('Magento_Downloadable','2.0.1','2.0.1'),('Magento_DownloadableImportExport','2.0.0','2.0.0'),('Magento_Eav','2.0.0','2.0.0'),('Magento_Email','2.0.0','2.0.0'),('Magento_EncryptionKey','2.0.0','2.0.0'),('Magento_Fedex','2.0.0','2.0.0'),('Magento_GiftMessage','2.0.1','2.0.1'),('Magento_GoogleAdwords','2.0.0','2.0.0'),('Magento_GoogleAnalytics','2.0.0','2.0.0'),('Magento_GoogleOptimizer','2.0.0','2.0.0'),('Magento_GroupedImportExport','2.0.0','2.0.0'),('Magento_GroupedProduct','2.0.1','2.0.1'),('Magento_ImportExport','2.0.1','2.0.1'),('Magento_Indexer','2.0.0','2.0.0'),('Magento_Integration','2.2.0','2.2.0'),('Magento_LayeredNavigation','2.0.0','2.0.0'),('Magento_Marketplace','1.0.0','1.0.0'),('Magento_MediaStorage','2.0.0','2.0.0'),('Magento_Msrp','2.1.3','2.1.3'),('Magento_Multishipping','2.0.0','2.0.0'),('Magento_NewRelicReporting','2.0.0','2.0.0'),('Magento_Newsletter','2.0.0','2.0.0'),('Magento_OfflinePayments','2.0.0','2.0.0'),('Magento_OfflineShipping','2.0.0','2.0.0'),('Magento_PageCache','2.0.0','2.0.0'),('Magento_Payment','2.0.0','2.0.0'),('Magento_Paypal','2.0.0','2.0.0'),('Magento_Persistent','2.0.0','2.0.0'),('Magento_ProductAlert','2.0.0','2.0.0'),('Magento_ProductVideo','2.0.0.2','2.0.0.2'),('Magento_Quote','2.0.3','2.0.3'),('Magento_Reports','2.0.0','2.0.0'),('Magento_RequireJs','2.0.0','2.0.0'),('Magento_Review','2.0.0','2.0.0'),('Magento_Rss','2.0.0','2.0.0'),('Magento_Rule','2.0.0','2.0.0'),('Magento_Sales','2.0.3','2.0.3'),('Magento_SalesInventory','1.0.0','1.0.0'),('Magento_SalesRule','2.0.1','2.0.1'),('Magento_SalesSequence','2.0.0','2.0.0'),('Magento_SampleData','2.0.0','2.0.0'),('Magento_Search','2.0.4','2.0.4'),('Magento_Security','2.0.1','2.0.1'),('Magento_SendFriend','2.0.0','2.0.0'),('Magento_Shipping','2.0.0','2.0.0'),('Magento_Sitemap','2.0.0','2.0.0'),('Magento_Store','2.0.0','2.0.0'),('Magento_Swagger','2.0.0','2.0.0'),('Magento_Swatches','2.0.1','2.0.1'),('Magento_SwatchesLayeredNavigation','2.0.0','2.0.0'),('Magento_Tax','2.0.1','2.0.1'),('Magento_TaxImportExport','2.0.0','2.0.0'),('Magento_Theme','2.0.1','2.0.1'),('Magento_Translation','2.0.0','2.0.0'),('Magento_Ui','2.0.0','2.0.0'),('Magento_Ups','2.0.0','2.0.0'),('Magento_UrlRewrite','2.0.0','2.0.0'),('Magento_User','2.0.1','2.0.1'),('Magento_Usps','2.0.0','2.0.0'),('Magento_Variable','2.0.0','2.0.0'),('Magento_Vault','2.0.2','2.0.2'),('Magento_Version','2.0.0','2.0.0'),('Magento_Webapi','2.0.0','2.0.0'),('Magento_WebapiSecurity','2.0.0','2.0.0'),('Magento_Weee','2.0.0','2.0.0'),('Magento_Widget','2.0.0','2.0.0'),('Magento_Wishlist','2.0.0','2.0.0'),('Mageplaza_Blog','2.4.2','2.4.2'),('Mageplaza_Core','1.0.0','1.0.0'),('Mageplaza_SocialLogin','1.0.0','1.0.0'),('MGS_Core','0.1.0','0.1.0'),('MGS_Mmegamenu','1.0.0.0','1.0.0.0'),('Solwin_Instagram','1.0.0','1.0.0'),('Zenon_Customproduct','1.0.0','1.0.0'),('Zenon_Themesetting','1.0.0','1.0.0');
/*!40000 ALTER TABLE `setup_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_tablerate`
--

DROP TABLE IF EXISTS `shipping_tablerate`;
CREATE TABLE `shipping_tablerate` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `website_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `dest_country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Destination coutry ISO/2 or ISO/3 code',
  `dest_region_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Destination Region Id',
  `dest_zip` varchar(10) NOT NULL DEFAULT '*' COMMENT 'Destination Post Code (Zip)',
  `condition_name` varchar(20) NOT NULL COMMENT 'Rate Condition name',
  `condition_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rate condition value',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `cost` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Cost',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `UNQ_D60821CDB2AFACEE1566CFC02D0D4CAA` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Shipping Tablerate';

--
-- Table structure for table `sitemap`
--

DROP TABLE IF EXISTS `sitemap`;
CREATE TABLE `sitemap` (
  `sitemap_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sitemap Id',
  `sitemap_type` varchar(32) DEFAULT NULL COMMENT 'Sitemap Type',
  `sitemap_filename` varchar(32) DEFAULT NULL COMMENT 'Sitemap Filename',
  `sitemap_path` varchar(255) DEFAULT NULL COMMENT 'Sitemap Path',
  `sitemap_time` timestamp NULL DEFAULT NULL COMMENT 'Sitemap Time',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`sitemap_id`),
  KEY `SITEMAP_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='XML Sitemap';

--
-- Table structure for table `stock_item_importing`
--

DROP TABLE IF EXISTS `stock_item_importing`;
CREATE TABLE `stock_item_importing` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `total_items` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Total Items',
  `success_items` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Total Items',
  `failed_items` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'failed_items',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stock Item Importing';

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `store_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Store Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Sort Order',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Activity',
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `STORE_CODE` (`code`),
  KEY `STORE_WEBSITE_ID` (`website_id`),
  KEY `STORE_IS_ACTIVE_SORT_ORDER` (`is_active`,`sort_order`),
  KEY `STORE_GROUP_ID` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Stores';

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` VALUES (0,'admin',0,0,'Admin',0,1),(1,'default',1,1,'Thai',0,1),(2,'en',1,1,'English',2,1);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_group`
--

DROP TABLE IF EXISTS `store_group`;
CREATE TABLE `store_group` (
  `group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Group Name',
  `root_category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Root Category Id',
  `default_store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Store Id',
  PRIMARY KEY (`group_id`),
  KEY `STORE_GROUP_WEBSITE_ID` (`website_id`),
  KEY `STORE_GROUP_DEFAULT_STORE_ID` (`default_store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Store Groups';

--
-- Dumping data for table `store_group`
--

LOCK TABLES `store_group` WRITE;
/*!40000 ALTER TABLE `store_group` DISABLE KEYS */;
INSERT INTO `store_group` VALUES (0,0,'Default','0',0),(1,1,'Lancome Store','2',1);
/*!40000 ALTER TABLE `store_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_website`
--

DROP TABLE IF EXISTS `store_website`;
CREATE TABLE `store_website` (
  `website_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Website Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `name` varchar(64) DEFAULT NULL COMMENT 'Website Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Group Id',
  `is_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Defines Is Website Default',
  PRIMARY KEY (`website_id`),
  UNIQUE KEY `STORE_WEBSITE_CODE` (`code`),
  KEY `STORE_WEBSITE_SORT_ORDER` (`sort_order`),
  KEY `STORE_WEBSITE_DEFAULT_GROUP_ID` (`default_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Websites';

--
-- Dumping data for table `store_website`
--

LOCK TABLES `store_website` WRITE;
/*!40000 ALTER TABLE `store_website` DISABLE KEYS */;
INSERT INTO `store_website` VALUES (0,'admin','Admin',0,0,0),(1,'base','Lancome Website',0,1,1);
/*!40000 ALTER TABLE `store_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_calculation`
--

DROP TABLE IF EXISTS `tax_calculation`;
CREATE TABLE `tax_calculation` (
  `tax_calculation_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `tax_calculation_rule_id` int(11) NOT NULL COMMENT 'Tax Calculation Rule Id',
  `customer_tax_class_id` smallint(6) NOT NULL COMMENT 'Customer Tax Class Id',
  `product_tax_class_id` smallint(6) NOT NULL COMMENT 'Product Tax Class Id',
  PRIMARY KEY (`tax_calculation_id`),
  KEY `TAX_CALCULATION_TAX_CALCULATION_RULE_ID` (`tax_calculation_rule_id`),
  KEY `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID` (`customer_tax_class_id`),
  KEY `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID` (`product_tax_class_id`),
  KEY `TAX_CALC_TAX_CALC_RATE_ID_CSTR_TAX_CLASS_ID_PRD_TAX_CLASS_ID` (`tax_calculation_rate_id`,`customer_tax_class_id`,`product_tax_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation';

--
-- Table structure for table `tax_calculation_rate`
--

DROP TABLE IF EXISTS `tax_calculation_rate`;
CREATE TABLE `tax_calculation_rate` (
  `tax_calculation_rate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Id',
  `tax_country_id` varchar(2) NOT NULL COMMENT 'Tax Country Id',
  `tax_region_id` int(11) NOT NULL COMMENT 'Tax Region Id',
  `tax_postcode` varchar(21) DEFAULT NULL COMMENT 'Tax Postcode',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `rate` decimal(12,4) NOT NULL COMMENT 'Rate',
  `zip_is_range` smallint(6) DEFAULT NULL COMMENT 'Zip Is Range',
  `zip_from` int(10) unsigned DEFAULT NULL COMMENT 'Zip From',
  `zip_to` int(10) unsigned DEFAULT NULL COMMENT 'Zip To',
  PRIMARY KEY (`tax_calculation_rate_id`),
  KEY `TAX_CALCULATION_RATE_TAX_COUNTRY_ID_TAX_REGION_ID_TAX_POSTCODE` (`tax_country_id`,`tax_region_id`,`tax_postcode`),
  KEY `TAX_CALCULATION_RATE_CODE` (`code`),
  KEY `IDX_CA799F1E2CB843495F601E56C84A626D` (`tax_calculation_rate_id`,`tax_country_id`,`tax_region_id`,`zip_is_range`,`tax_postcode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate';

--
-- Dumping data for table `tax_calculation_rate`
--

LOCK TABLES `tax_calculation_rate` WRITE;
/*!40000 ALTER TABLE `tax_calculation_rate` DISABLE KEYS */;
INSERT INTO `tax_calculation_rate` VALUES ('1','US','12','*','US-CA-*-Rate 1','8.2500',NULL,NULL,NULL),('2','US','43','*','US-NY-*-Rate 1','8.3750',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tax_calculation_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_calculation_rate_title`
--

DROP TABLE IF EXISTS `tax_calculation_rate_title`;
CREATE TABLE `tax_calculation_rate_title` (
  `tax_calculation_rate_title_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Title Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`tax_calculation_rate_title_id`),
  KEY `TAX_CALCULATION_RATE_TITLE_TAX_CALCULATION_RATE_ID_STORE_ID` (`tax_calculation_rate_id`,`store_id`),
  KEY `TAX_CALCULATION_RATE_TITLE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate Title';

--
-- Table structure for table `tax_calculation_rule`
--

DROP TABLE IF EXISTS `tax_calculation_rule`;
CREATE TABLE `tax_calculation_rule` (
  `tax_calculation_rule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rule Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `calculate_subtotal` int(11) NOT NULL COMMENT 'Calculate off subtotal option',
  PRIMARY KEY (`tax_calculation_rule_id`),
  KEY `TAX_CALCULATION_RULE_PRIORITY_POSITION` (`priority`,`position`),
  KEY `TAX_CALCULATION_RULE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rule';

--
-- Table structure for table `tax_class`
--

DROP TABLE IF EXISTS `tax_class`;
CREATE TABLE `tax_class` (
  `class_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Class Id',
  `class_name` varchar(255) NOT NULL COMMENT 'Class Name',
  `class_type` varchar(8) NOT NULL DEFAULT 'CUSTOMER' COMMENT 'Class Type',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Tax Class';

--
-- Dumping data for table `tax_class`
--

LOCK TABLES `tax_class` WRITE;
/*!40000 ALTER TABLE `tax_class` DISABLE KEYS */;
INSERT INTO `tax_class` VALUES (2,'Taxable Goods','PRODUCT'),(3,'Retail Customer','CUSTOMER');
/*!40000 ALTER TABLE `tax_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_order_aggregated_created`
--

DROP TABLE IF EXISTS `tax_order_aggregated_created`;
CREATE TABLE `tax_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TAX_ORDER_AGGRED_CREATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `TAX_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregation';

--
-- Table structure for table `tax_order_aggregated_updated`
--

DROP TABLE IF EXISTS `tax_order_aggregated_updated`;
CREATE TABLE `tax_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TAX_ORDER_AGGRED_UPDATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregated Updated';

--
-- Table structure for table `theme`
--

DROP TABLE IF EXISTS `theme`;
CREATE TABLE `theme` (
  `theme_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Theme identifier',
  `parent_id` int(11) DEFAULT NULL COMMENT 'Parent Id',
  `theme_path` varchar(255) DEFAULT NULL COMMENT 'Theme Path',
  `theme_title` varchar(255) NOT NULL COMMENT 'Theme Title',
  `preview_image` varchar(255) DEFAULT NULL COMMENT 'Preview Image',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Theme Featured',
  `area` varchar(255) NOT NULL COMMENT 'Theme Area',
  `type` smallint(6) NOT NULL COMMENT 'Theme type: 0:physical, 1:virtual, 2:staging',
  `code` text COMMENT 'Full theme code, including package',
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Core theme';

--
-- Dumping data for table `theme`
--

LOCK TABLES `theme` WRITE;
/*!40000 ALTER TABLE `theme` DISABLE KEYS */;
INSERT INTO `theme` VALUES ('1',NULL,'Magento/blank','Magento Blank','preview_image_59365e49739d8.jpeg',0,'frontend',0,'Magento/blank'),('2','1','Magento/luma','Magento Luma','preview_image_59365e49875d1.jpeg',0,'frontend',0,'Magento/luma'),('3',NULL,'Magento/backend','Magento 2 backend',NULL,0,'adminhtml',0,'Magento/backend'),('4','1','hoatam/lancome','Hoatam Lancome',NULL,0,'frontend',0,'hoatam/lancome'),('5','1','shopstack/lancome','Shopstack Lancome',NULL,0,'frontend',0,'shopstack/lancome');
/*!40000 ALTER TABLE `theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theme_file`
--

DROP TABLE IF EXISTS `theme_file`;
CREATE TABLE `theme_file` (
  `theme_files_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Theme files identifier',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme Id',
  `file_path` varchar(255) DEFAULT NULL COMMENT 'Relative path to file',
  `file_type` varchar(32) NOT NULL COMMENT 'File Type',
  `content` longtext NOT NULL COMMENT 'File Content',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Temporary File',
  PRIMARY KEY (`theme_files_id`),
  KEY `THEME_FILE_THEME_ID_THEME_THEME_ID` (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Core theme files';

--
-- Table structure for table `translation`
--

DROP TABLE IF EXISTS `translation`;
CREATE TABLE `translation` (
  `key_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Key Id of Translation',
  `string` varchar(255) NOT NULL DEFAULT 'Translate String' COMMENT 'Translation String',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `translate` varchar(255) DEFAULT NULL COMMENT 'Translate',
  `locale` varchar(20) NOT NULL DEFAULT 'en_US' COMMENT 'Locale',
  `crc_string` bigint(20) NOT NULL DEFAULT '1591228201' COMMENT 'Translation String CRC32 Hash',
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `TRANSLATION_STORE_ID_LOCALE_CRC_STRING_STRING` (`store_id`,`locale`,`crc_string`,`string`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Translations';

--
-- Table structure for table `ui_bookmark`
--

DROP TABLE IF EXISTS `ui_bookmark`;
CREATE TABLE `ui_bookmark` (
  `bookmark_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Bookmark identifier',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User Id',
  `namespace` varchar(255) NOT NULL COMMENT 'Bookmark namespace',
  `identifier` varchar(255) NOT NULL COMMENT 'Bookmark Identifier',
  `current` smallint(6) NOT NULL COMMENT 'Mark current bookmark per user and identifier',
  `title` varchar(255) DEFAULT NULL COMMENT 'Bookmark title',
  `config` longtext COMMENT 'Bookmark config',
  `created_at` datetime NOT NULL COMMENT 'Bookmark created at',
  `updated_at` datetime NOT NULL COMMENT 'Bookmark updated at',
  PRIMARY KEY (`bookmark_id`),
  KEY `UI_BOOKMARK_USER_ID_NAMESPACE_IDENTIFIER` (`user_id`,`namespace`,`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='Bookmark';

--
-- Dumping data for table `ui_bookmark`
--

LOCK TABLES `ui_bookmark` WRITE;
/*!40000 ALTER TABLE `ui_bookmark` DISABLE KEYS */;
INSERT INTO `ui_bookmark` VALUES ('1','1','cms_page_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('2','1','cms_page_listing','current',0,NULL,'{\"current\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('3','1','cms_block_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('4','1','cms_block_listing','current',0,NULL,'{\"current\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('5','3','product_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"color\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"color\":22,\"news_from_date\":23,\"news_to_date\":24,\"custom_design\":25,\"custom_design_from\":26,\"custom_design_to\":27,\"page_layout\":28,\"country_of_manufacture\":29,\"custom_layout\":30,\"url_key\":31,\"msrp\":32,\"tax_class_id\":33,\"gift_message_available\":34,\"actions\":35}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('6','3','product_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"color\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"m_features\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"actions\":34,\"m_features\":35}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('7','3','design_config_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"default\":{\"visible\":true,\"sorting\":false},\"store_website_id\":{\"visible\":true,\"sorting\":false},\"store_group_id\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"theme_theme_id\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"default\":0,\"store_website_id\":1,\"store_group_id\":2,\"store_id\":3,\"theme_theme_id\":4,\"actions\":5}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('8','3','cms_page_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"search\":{\"value\":\"\"},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('9','3','cms_page_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"search\":{\"value\":\"\"},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('10','3','cms_block_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('11','3','cms_block_listing','current',0,NULL,'{\"current\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":30},\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('12','3','design_config_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"default\":{\"visible\":true,\"sorting\":false},\"store_website_id\":{\"visible\":true,\"sorting\":false},\"store_group_id\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"theme_theme_id\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"default\":0,\"store_website_id\":1,\"store_group_id\":2,\"store_id\":3,\"theme_theme_id\":4,\"actions\":5}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('13','2','product_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"actions\":34}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('14','2','product_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"actions\":34}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('15','4','product_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"actions\":34}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('16','4','product_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"m_features\":{\"visible\":true,\"sorting\":false},\"m_quantity\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"actions\":34,\"m_features\":35,\"m_quantity\":36}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('17','2','cms_block_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8},\"search\":{\"value\":\"\"}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('18','2','cms_block_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":false},\"title\":{\"visible\":true,\"sorting\":\"asc\"},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":50},\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8},\"search\":{\"value\":\"\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('19','1','product_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"actions\":34}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('20','1','product_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"actions\":34}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('21','8','design_config_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"default\":{\"visible\":true,\"sorting\":false},\"store_website_id\":{\"visible\":true,\"sorting\":false},\"store_group_id\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"theme_theme_id\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"default\":0,\"store_website_id\":1,\"store_group_id\":2,\"store_id\":3,\"theme_theme_id\":4,\"actions\":5}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('22','8','design_config_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"default\":{\"visible\":true,\"sorting\":false},\"store_website_id\":{\"visible\":true,\"sorting\":false},\"store_group_id\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"theme_theme_id\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"default\":0,\"store_website_id\":1,\"store_group_id\":2,\"store_id\":3,\"theme_theme_id\":4,\"actions\":5}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('23','8','cms_block_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"search\":{\"value\":\"\"},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('24','8','cms_block_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"search\":{\"value\":\"\"},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('26','8','product_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"m_features\":{\"visible\":true,\"sorting\":false},\"m_quantity\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"m_features\":34,\"m_quantity\":35,\"actions\":36}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('27','8','product_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"m_features\":{\"visible\":true,\"sorting\":false},\"m_quantity\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"color\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"news_from_date\":22,\"news_to_date\":23,\"custom_design\":24,\"custom_design_from\":25,\"custom_design_to\":26,\"page_layout\":27,\"country_of_manufacture\":28,\"custom_layout\":29,\"url_key\":30,\"msrp\":31,\"tax_class_id\":32,\"gift_message_available\":33,\"m_features\":34,\"m_quantity\":35,\"actions\":36,\"color\":37}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('28','8','cms_page_listing','current',0,NULL,'{\"current\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('29','8','cms_page_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('30','8','product_attributes_listing','current',0,NULL,'{\"current\":{\"displayMode\":\"grid\",\"columns\":{\"attribute_code\":{\"visible\":true,\"sorting\":\"asc\"},\"frontend_label\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_required\":{\"visible\":true,\"sorting\":false},\"is_user_defined\":{\"visible\":true,\"sorting\":false},\"is_visible\":{\"visible\":true,\"sorting\":false},\"is_global\":{\"visible\":true,\"sorting\":false},\"is_searchable\":{\"visible\":true,\"sorting\":false},\"is_comparable\":{\"visible\":true,\"sorting\":false},\"is_filterable\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"attribute_code\":1,\"frontend_label\":2,\"is_required\":3,\"is_user_defined\":4,\"is_visible\":5,\"is_global\":6,\"is_searchable\":7,\"is_comparable\":8,\"is_filterable\":9}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('31','8','product_attributes_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"displayMode\":\"grid\",\"columns\":{\"attribute_code\":{\"visible\":true,\"sorting\":\"asc\"},\"frontend_label\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_required\":{\"visible\":true,\"sorting\":false},\"is_user_defined\":{\"visible\":true,\"sorting\":false},\"is_visible\":{\"visible\":true,\"sorting\":false},\"is_global\":{\"visible\":true,\"sorting\":false},\"is_searchable\":{\"visible\":true,\"sorting\":false},\"is_comparable\":{\"visible\":true,\"sorting\":false},\"is_filterable\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"attribute_code\":1,\"frontend_label\":2,\"is_required\":3,\"is_user_defined\":4,\"is_visible\":5,\"is_global\":6,\"is_searchable\":7,\"is_comparable\":8,\"is_filterable\":9}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('32','8','mageplaza_blog_post_listing','current',0,NULL,'{\"current\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"post_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"enabled\":{\"visible\":true,\"sorting\":false},\"in_rss\":{\"visible\":true,\"sorting\":false},\"allow_comment\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"created_at\":{\"visible\":true,\"sorting\":false},\"updated_at\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"post_id\":1,\"name\":2,\"enabled\":3,\"in_rss\":4,\"allow_comment\":5,\"created_at\":6,\"updated_at\":7,\"actions\":8}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),('33','8','mageplaza_blog_post_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"post_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"enabled\":{\"visible\":true,\"sorting\":false},\"in_rss\":{\"visible\":true,\"sorting\":false},\"allow_comment\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"created_at\":{\"visible\":true,\"sorting\":false},\"updated_at\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"post_id\":1,\"name\":2,\"enabled\":3,\"in_rss\":4,\"allow_comment\":5,\"created_at\":6,\"updated_at\":7,\"actions\":8}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ui_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `url_rewrite`
--

DROP TABLE IF EXISTS `url_rewrite`;
CREATE TABLE `url_rewrite` (
  `url_rewrite_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rewrite Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Entity type code',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `request_path` varchar(255) DEFAULT NULL COMMENT 'Request Path',
  `target_path` varchar(255) DEFAULT NULL COMMENT 'Target Path',
  `redirect_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Redirect Type',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `is_autogenerated` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is rewrite generated automatically flag',
  `metadata` varchar(255) DEFAULT NULL COMMENT 'Meta data for url rewrite',
  PRIMARY KEY (`url_rewrite_id`),
  UNIQUE KEY `URL_REWRITE_REQUEST_PATH_STORE_ID` (`request_path`,`store_id`),
  KEY `URL_REWRITE_TARGET_PATH` (`target_path`),
  KEY `URL_REWRITE_STORE_ID_ENTITY_ID` (`store_id`,`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8 COMMENT='Url Rewrites';

--
-- Dumping data for table `url_rewrite`
--

LOCK TABLES `url_rewrite` WRITE;
/*!40000 ALTER TABLE `url_rewrite` DISABLE KEYS */;
INSERT INTO `url_rewrite` VALUES ('1','cms-page','1','no-route','cms/page/view/page_id/1',0,1,NULL,1,NULL),('2','cms-page','2','home','cms/page/view/page_id/2',0,1,NULL,1,NULL),('3','cms-page','3','enable-cookies','cms/page/view/page_id/3',0,1,NULL,1,NULL),('4','cms-page','4','privacy-policy-cookie-restriction-mode','cms/page/view/page_id/4',0,1,NULL,1,NULL),('5','category','3','make-up.html','catalog/category/view/id/3',0,1,NULL,1,NULL),('6','category','3','make-up.html','catalog/category/view/id/3',0,2,NULL,1,NULL),('7','category','4','skincare.html','catalog/category/view/id/4',0,1,NULL,1,NULL),('8','category','4','skincare.html','catalog/category/view/id/4',0,2,NULL,1,NULL),('9','category','5','fragrance.html','catalog/category/view/id/5',0,1,NULL,1,NULL),('10','category','5','fragrance.html','catalog/category/view/id/5',0,2,NULL,1,NULL),('11','category','6','best-sellers.html','catalog/category/view/id/6',0,1,NULL,1,NULL),('12','category','6','best-sellers.html','catalog/category/view/id/6',0,2,NULL,1,NULL),('13','category','7','online-exclusives.html','catalog/category/view/id/7',0,1,NULL,1,NULL),('14','category','7','online-exclusives.html','catalog/category/view/id/7',0,2,NULL,1,NULL),('17','cms-page','5','privacy-policy','cms/page/view/page_id/5',0,1,NULL,1,NULL),('18','cms-page','5','privacy-policy','cms/page/view/page_id/5',0,2,NULL,1,NULL),('19','cms-page','6','terms-conditions','cms/page/view/page_id/6',0,1,NULL,1,NULL),('20','cms-page','6','terms-conditions','cms/page/view/page_id/6',0,2,NULL,1,NULL),('21','cms-page','7','cookies','cms/page/view/page_id/7',0,1,NULL,1,NULL),('22','cms-page','7','cookies','cms/page/view/page_id/7',0,2,NULL,1,NULL),('25','product','1','dummy-test1.html','catalog/product/view/id/1',0,1,NULL,1,NULL),('26','product','1','make-up/dummy-test1.html','catalog/product/view/id/1/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('27','product','1','dummy-test1.html','catalog/product/view/id/1',0,2,NULL,1,NULL),('28','product','1','make-up/dummy-test1.html','catalog/product/view/id/1/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('37','category','8','magazine.html','catalog/category/view/id/8',0,1,NULL,1,NULL),('38','category','8','new.html','magazine.html',301,1,NULL,0,NULL),('39','category','8','magazine.html','catalog/category/view/id/8',0,2,NULL,1,NULL),('40','category','8','new.html','magazine.html',301,2,NULL,0,NULL),('41','category','9','gift-sets.html','catalog/category/view/id/9',0,1,NULL,1,NULL),('42','category','9','gift-sets.html','catalog/category/view/id/9',0,2,NULL,1,NULL),('43','category','10','make-up/face.html','catalog/category/view/id/10',0,1,NULL,1,NULL),('44','category','10','make-up/face.html','catalog/category/view/id/10',0,2,NULL,1,NULL),('45','category','11','make-up/eyes.html','catalog/category/view/id/11',0,1,NULL,1,NULL),('46','category','11','make-up/eyes.html','catalog/category/view/id/11',0,2,NULL,1,NULL),('47','category','12','make-up/lips.html','catalog/category/view/id/12',0,1,NULL,1,NULL),('48','category','12','make-up/lips.html','catalog/category/view/id/12',0,2,NULL,1,NULL),('49','category','13','make-up/nails.html','catalog/category/view/id/13',0,1,NULL,1,NULL),('50','category','13','make-up/nails.html','catalog/category/view/id/13',0,2,NULL,1,NULL),('51','category','14','make-up/more-make-up.html','catalog/category/view/id/14',0,1,NULL,1,NULL),('52','category','14','make-up/more-make-up.html','catalog/category/view/id/14',0,2,NULL,1,NULL),('53','category','15','make-up/face/foundation.html','catalog/category/view/id/15',0,1,NULL,1,NULL),('54','category','15','make-up/face/foundation.html','catalog/category/view/id/15',0,2,NULL,1,NULL),('55','category','16','make-up/face/primer.html','catalog/category/view/id/16',0,1,NULL,1,NULL),('56','category','16','make-up/face/primer.html','catalog/category/view/id/16',0,2,NULL,1,NULL),('57','category','17','make-up/face/tinted-moisturisers.html','catalog/category/view/id/17',0,1,NULL,1,NULL),('58','category','17','make-up/face/tinted-moisturisers.html','catalog/category/view/id/17',0,2,NULL,1,NULL),('59','category','18','make-up/face/concealer.html','catalog/category/view/id/18',0,1,NULL,1,NULL),('60','category','18','make-up/face/concealer.html','catalog/category/view/id/18',0,2,NULL,1,NULL),('61','category','19','make-up/face/powder.html','catalog/category/view/id/19',0,1,NULL,1,NULL),('62','category','19','make-up/face/powder.html','catalog/category/view/id/19',0,2,NULL,1,NULL),('63','category','20','make-up/face/blushes-and-bronzers.html','catalog/category/view/id/20',0,1,NULL,1,NULL),('64','category','20','make-up/face/blushes-and-bronzers.html','catalog/category/view/id/20',0,2,NULL,1,NULL),('65','category','21','make-up/face/highlighter.html','catalog/category/view/id/21',0,1,NULL,1,NULL),('66','category','21','make-up/face/highlighter.html','catalog/category/view/id/21',0,2,NULL,1,NULL),('67','category','22','make-up/eyes/mascara.html','catalog/category/view/id/22',0,1,NULL,1,NULL),('68','category','22','make-up/eyes/mascara.html','catalog/category/view/id/22',0,2,NULL,1,NULL),('69','category','23','make-up/eyes/waterproof-mascara.html','catalog/category/view/id/23',0,1,NULL,1,NULL),('70','category','23','make-up/eyes/waterproof-mascara.html','catalog/category/view/id/23',0,2,NULL,1,NULL),('71','category','24','make-up/eyes/eyeshadows.html','catalog/category/view/id/24',0,1,NULL,1,NULL),('72','category','24','make-up/eyes/eyeshadows.html','catalog/category/view/id/24',0,2,NULL,1,NULL),('73','category','25','make-up/eyes/eyeliners-and-eye-pencils.html','catalog/category/view/id/25',0,1,NULL,1,NULL),('74','category','25','make-up/eyes/eyeliners-and-eye-pencils.html','catalog/category/view/id/25',0,2,NULL,1,NULL),('75','category','26','make-up/eyes/eyebrows.html','catalog/category/view/id/26',0,1,NULL,1,NULL),('76','category','26','make-up/eyes/eyebrows.html','catalog/category/view/id/26',0,2,NULL,1,NULL),('77','category','27','make-up/eyes/eye-makeup-remover.html','catalog/category/view/id/27',0,1,NULL,1,NULL),('78','category','27','make-up/eyes/eye-makeup-remover.html','catalog/category/view/id/27',0,2,NULL,1,NULL),('79','category','28','make-up/lips/lipsticks.html','catalog/category/view/id/28',0,1,NULL,1,NULL),('80','category','28','make-up/lips/lipsticks.html','catalog/category/view/id/28',0,2,NULL,1,NULL),('81','category','29','make-up/lips/lip-oil.html','catalog/category/view/id/29',0,1,NULL,1,NULL),('82','category','29','make-up/lips/lip-oil.html','catalog/category/view/id/29',0,2,NULL,1,NULL),('83','category','30','make-up/lips/lip-gloss.html','catalog/category/view/id/30',0,1,NULL,1,NULL),('84','category','30','make-up/lips/lip-gloss.html','catalog/category/view/id/30',0,2,NULL,1,NULL),('85','category','31','make-up/lips/lip-liner-and-pencils.html','catalog/category/view/id/31',0,1,NULL,1,NULL),('86','category','31','make-up/lips/lip-liner-and-pencils.html','catalog/category/view/id/31',0,2,NULL,1,NULL),('87','category','32','make-up/lips/liquid-lipstick.html','catalog/category/view/id/32',0,1,NULL,1,NULL),('88','category','32','make-up/lips/liquid-lipstick.html','catalog/category/view/id/32',0,2,NULL,1,NULL),('89','category','33','make-up/nails/nail-colour.html','catalog/category/view/id/33',0,1,NULL,1,NULL),('90','category','33','make-up/nails/nail-colour.html','catalog/category/view/id/33',0,2,NULL,1,NULL),('91','category','34','make-up/more-make-up/complexion-finder.html','catalog/category/view/id/34',0,1,NULL,1,NULL),('92','category','34','make-up/more-make-up/complexion-finder.html','catalog/category/view/id/34',0,2,NULL,1,NULL),('93','category','35','make-up/more-make-up/mascara-finder.html','catalog/category/view/id/35',0,1,NULL,1,NULL),('94','category','35','make-up/more-make-up/mascara-finder.html','catalog/category/view/id/35',0,2,NULL,1,NULL),('95','category','36','make-up/more-make-up/gift-finder.html','catalog/category/view/id/36',0,1,NULL,1,NULL),('96','category','36','make-up/more-make-up/gift-finder.html','catalog/category/view/id/36',0,2,NULL,1,NULL),('97','category','37','make-up/more-make-up/summer-2017-makeup-collection.html','catalog/category/view/id/37',0,1,NULL,1,NULL),('98','category','37','make-up/more-make-up/summer-2017-makeup-collection.html','catalog/category/view/id/37',0,2,NULL,1,NULL),('99','category','38','make-up/more-make-up/olympia-le-tan-makeup-collection.html','catalog/category/view/id/38',0,1,NULL,1,NULL),('100','category','38','make-up/more-make-up/olympia-le-tan-makeup-collection.html','catalog/category/view/id/38',0,2,NULL,1,NULL),('101','category','39','make-up/more-make-up/engraving-makeup.html','catalog/category/view/id/39',0,1,NULL,1,NULL),('102','category','39','make-up/more-make-up/engraving-makeup.html','catalog/category/view/id/39',0,2,NULL,1,NULL),('103','category','40','skincare/by-product-category.html','catalog/category/view/id/40',0,1,NULL,1,NULL),('104','category','40','skincare/by-product-category.html','catalog/category/view/id/40',0,2,NULL,1,NULL),('105','category','41','skincare/by-skin-need.html','catalog/category/view/id/41',0,1,NULL,1,NULL),('106','category','41','skincare/by-skin-need.html','catalog/category/view/id/41',0,2,NULL,1,NULL),('107','category','42','skincare/by-range.html','catalog/category/view/id/42',0,1,NULL,1,NULL),('108','category','42','skincare/by-range.html','catalog/category/view/id/42',0,2,NULL,1,NULL),('109','category','43','skincare/more-skincare.html','catalog/category/view/id/43',0,1,NULL,1,NULL),('110','category','43','skincare/more-skincare.html','catalog/category/view/id/43',0,2,NULL,1,NULL),('111','category','44','skincare/by-product-category/the-serums.html','catalog/category/view/id/44',0,1,NULL,1,NULL),('112','category','44','skincare/by-product-category/the-serums.html','catalog/category/view/id/44',0,2,NULL,1,NULL),('113','category','45','skincare/by-product-category/the-concentrates.html','catalog/category/view/id/45',0,1,NULL,1,NULL),('114','category','45','skincare/by-product-category/the-concentrates.html','catalog/category/view/id/45',0,2,NULL,1,NULL),('115','category','46','skincare/by-product-category/the-creams.html','catalog/category/view/id/46',0,1,NULL,1,NULL),('116','category','46','skincare/by-product-category/the-creams.html','catalog/category/view/id/46',0,2,NULL,1,NULL),('117','category','47','skincare/by-product-category/the-night-creams.html','catalog/category/view/id/47',0,1,NULL,1,NULL),('118','category','47','skincare/by-product-category/the-night-creams.html','catalog/category/view/id/47',0,2,NULL,1,NULL),('119','category','48','skincare/by-product-category/the-exceptional-skincare.html','catalog/category/view/id/48',0,1,NULL,1,NULL),('120','category','48','skincare/by-product-category/the-exceptional-skincare.html','catalog/category/view/id/48',0,2,NULL,1,NULL),('123','category','49','skincare/by-product-category/the-eye-care-and-lip-care.html','catalog/category/view/id/49',0,1,NULL,1,NULL),('124','category','49','skincare/by-product-category/the-eye-care-lip-care.html','skincare/by-product-category/the-eye-care-and-lip-care.html',301,1,NULL,0,NULL),('125','category','49','skincare/by-product-category/the-eye-care-and-lip-care.html','catalog/category/view/id/49',0,2,NULL,1,NULL),('126','category','49','skincare/by-product-category/the-eye-care-lip-care.html','skincare/by-product-category/the-eye-care-and-lip-care.html',301,2,NULL,0,NULL),('127','category','50','skincare/by-product-category/the-cleansers-and-toners.html','catalog/category/view/id/50',0,1,NULL,1,NULL),('128','category','50','skincare/by-product-category/the-cleansers-and-toners.html','catalog/category/view/id/50',0,2,NULL,1,NULL),('129','category','51','skincare/by-product-category/the-masks-and-exfoliators.html','catalog/category/view/id/51',0,1,NULL,1,NULL),('130','category','51','skincare/by-product-category/the-masks-and-exfoliators.html','catalog/category/view/id/51',0,2,NULL,1,NULL),('131','category','52','skincare/by-product-category/the-bodycare.html','catalog/category/view/id/52',0,1,NULL,1,NULL),('132','category','52','skincare/by-product-category/the-bodycare.html','catalog/category/view/id/52',0,2,NULL,1,NULL),('133','category','53','skincare/by-product-category/the-suncare.html','catalog/category/view/id/53',0,1,NULL,1,NULL),('134','category','53','skincare/by-product-category/the-suncare.html','catalog/category/view/id/53',0,2,NULL,1,NULL),('135','category','54','skincare/by-product-category/the-men-care.html','catalog/category/view/id/54',0,1,NULL,1,NULL),('136','category','54','skincare/by-product-category/the-men-care.html','catalog/category/view/id/54',0,2,NULL,1,NULL),('137','category','55','skincare/by-product-category/the-fluids.html','catalog/category/view/id/55',0,1,NULL,1,NULL),('138','category','55','skincare/by-product-category/the-fluids.html','catalog/category/view/id/55',0,2,NULL,1,NULL),('139','category','56','skincare/by-skin-need/anti-aging.html','catalog/category/view/id/56',0,1,NULL,1,NULL),('140','category','56','skincare/by-skin-need/anti-aging.html','catalog/category/view/id/56',0,2,NULL,1,NULL),('141','category','57','skincare/by-skin-need/lack-of-firmness.html','catalog/category/view/id/57',0,1,NULL,1,NULL),('142','category','57','skincare/by-skin-need/lack-of-firmness.html','catalog/category/view/id/57',0,2,NULL,1,NULL),('143','category','58','skincare/by-skin-need/pores.html','catalog/category/view/id/58',0,1,NULL,1,NULL),('144','category','58','skincare/by-skin-need/pores.html','catalog/category/view/id/58',0,2,NULL,1,NULL),('145','category','59','skincare/by-skin-need/wrinkles.html','catalog/category/view/id/59',0,1,NULL,1,NULL),('146','category','59','skincare/by-skin-need/wrinkles.html','catalog/category/view/id/59',0,2,NULL,1,NULL),('147','category','60','skincare/by-skin-need/dark-spots.html','catalog/category/view/id/60',0,1,NULL,1,NULL),('148','category','60','skincare/by-skin-need/dark-spots.html','catalog/category/view/id/60',0,2,NULL,1,NULL),('149','category','61','skincare/by-skin-need/exceptional-regeneration.html','catalog/category/view/id/61',0,1,NULL,1,NULL),('150','category','61','skincare/by-skin-need/exceptional-regeneration.html','catalog/category/view/id/61',0,2,NULL,1,NULL),('151','category','62','skincare/by-skin-need/lack-of-homogeneity.html','catalog/category/view/id/62',0,1,NULL,1,NULL),('152','category','62','skincare/by-skin-need/lack-of-homogeneity.html','catalog/category/view/id/62',0,2,NULL,1,NULL),('153','category','63','skincare/by-skin-need/dullness.html','catalog/category/view/id/63',0,1,NULL,1,NULL),('154','category','63','skincare/by-skin-need/dullness.html','catalog/category/view/id/63',0,2,NULL,1,NULL),('155','category','64','skincare/by-skin-need/hydration.html','catalog/category/view/id/64',0,1,NULL,1,NULL),('156','category','64','skincare/by-skin-need/hydration.html','catalog/category/view/id/64',0,2,NULL,1,NULL),('157','category','65','skincare/by-skin-need/dryness.html','catalog/category/view/id/65',0,1,NULL,1,NULL),('158','category','65','skincare/by-skin-need/dryness.html','catalog/category/view/id/65',0,2,NULL,1,NULL),('159','category','66','skincare/by-skin-need/redness.html','catalog/category/view/id/66',0,1,NULL,1,NULL),('160','category','66','skincare/by-skin-need/redness.html','catalog/category/view/id/66',0,2,NULL,1,NULL),('161','category','67','skincare/by-skin-need/fatigue.html','catalog/category/view/id/67',0,1,NULL,1,NULL),('162','category','67','skincare/by-skin-need/fatigue.html','catalog/category/view/id/67',0,2,NULL,1,NULL),('163','category','68','skincare/by-range/genifique.html','catalog/category/view/id/68',0,1,NULL,1,NULL),('164','category','68','skincare/by-range/genifique.html','catalog/category/view/id/68',0,2,NULL,1,NULL),('165','category','69','skincare/by-range/hydra-zen.html','catalog/category/view/id/69',0,1,NULL,1,NULL),('166','category','69','skincare/by-range/hydra-zen.html','catalog/category/view/id/69',0,2,NULL,1,NULL),('167','category','70','skincare/by-range/renergie.html','catalog/category/view/id/70',0,1,NULL,1,NULL),('168','category','70','skincare/by-range/renergie.html','catalog/category/view/id/70',0,2,NULL,1,NULL),('169','category','71','skincare/by-range/absolue.html','catalog/category/view/id/71',0,1,NULL,1,NULL),('170','category','71','skincare/by-range/absolue.html','catalog/category/view/id/71',0,2,NULL,1,NULL),('171','category','72','skincare/by-range/visionnaire.html','catalog/category/view/id/72',0,1,NULL,1,NULL),('172','category','72','skincare/by-range/visionnaire.html','catalog/category/view/id/72',0,2,NULL,1,NULL),('173','category','73','skincare/by-range/blanc-expert.html','catalog/category/view/id/73',0,1,NULL,1,NULL),('174','category','73','skincare/by-range/blanc-expert.html','catalog/category/view/id/73',0,2,NULL,1,NULL),('175','category','74','skincare/by-range/energie-de-vie.html','catalog/category/view/id/74',0,1,NULL,1,NULL),('176','category','74','skincare/by-range/energie-de-vie.html','catalog/category/view/id/74',0,2,NULL,1,NULL),('177','category','75','skincare/by-range/nutrix.html','catalog/category/view/id/75',0,1,NULL,1,NULL),('178','category','75','skincare/by-range/nutrix.html','catalog/category/view/id/75',0,2,NULL,1,NULL),('179','category','76','skincare/more-skincare/skin-beauty-consultation.html','catalog/category/view/id/76',0,1,NULL,1,NULL),('180','category','76','skincare/more-skincare/skin-beauty-consultation.html','catalog/category/view/id/76',0,2,NULL,1,NULL),('181','category','77','skincare/more-skincare/gift-finder.html','catalog/category/view/id/77',0,1,NULL,1,NULL),('182','category','77','skincare/more-skincare/gift-finder.html','catalog/category/view/id/77',0,2,NULL,1,NULL),('183','category','78','skincare/more-skincare/olympia-le-tan-makeup-collection.html','catalog/category/view/id/78',0,1,NULL,1,NULL),('184','category','78','skincare/more-skincare/olympia-le-tan-makeup-collection.html','catalog/category/view/id/78',0,2,NULL,1,NULL),('185','category','79','skincare/more-skincare/summer-2017-makeup-collection.html','catalog/category/view/id/79',0,1,NULL,1,NULL),('186','category','79','skincare/more-skincare/summer-2017-makeup-collection.html','catalog/category/view/id/79',0,2,NULL,1,NULL),('187','category','80','fragrance/women-s-perfumes.html','catalog/category/view/id/80',0,1,NULL,1,NULL),('188','category','80','fragrance/women-s-perfumes.html','catalog/category/view/id/80',0,2,NULL,1,NULL),('189','category','81','fragrance/men-s-perfume.html','catalog/category/view/id/81',0,1,NULL,1,NULL),('190','category','81','fragrance/men-s-perfume.html','catalog/category/view/id/81',0,2,NULL,1,NULL),('191','category','82','fragrance/women-s-perfumes/la-vie-est-belle.html','catalog/category/view/id/82',0,1,NULL,1,NULL),('192','category','82','fragrance/women-s-perfumes/la-vie-est-belle.html','catalog/category/view/id/82',0,2,NULL,1,NULL),('193','category','83','fragrance/women-s-perfumes/tresor.html','catalog/category/view/id/83',0,1,NULL,1,NULL),('194','category','83','fragrance/women-s-perfumes/tresor.html','catalog/category/view/id/83',0,2,NULL,1,NULL),('195','category','84','fragrance/women-s-perfumes/tresor-midnight-rose.html','catalog/category/view/id/84',0,1,NULL,1,NULL),('196','category','84','fragrance/women-s-perfumes/tresor-midnight-rose.html','catalog/category/view/id/84',0,2,NULL,1,NULL),('197','category','85','fragrance/women-s-perfumes/tresor-in-love.html','catalog/category/view/id/85',0,1,NULL,1,NULL),('198','category','85','fragrance/women-s-perfumes/tresor-in-love.html','catalog/category/view/id/85',0,2,NULL,1,NULL),('199','category','86','fragrance/women-s-perfumes/la-nuit-tresor.html','catalog/category/view/id/86',0,1,NULL,1,NULL),('200','category','86','fragrance/women-s-perfumes/la-nuit-tresor.html','catalog/category/view/id/86',0,2,NULL,1,NULL),('201','category','87','fragrance/women-s-perfumes/the-world-of-o.html','catalog/category/view/id/87',0,1,NULL,1,NULL),('202','category','87','fragrance/women-s-perfumes/the-world-of-o.html','catalog/category/view/id/87',0,2,NULL,1,NULL),('203','category','88','fragrance/women-s-perfumes/miracle.html','catalog/category/view/id/88',0,1,NULL,1,NULL),('204','category','88','fragrance/women-s-perfumes/miracle.html','catalog/category/view/id/88',0,2,NULL,1,NULL),('205','category','89','fragrance/women-s-perfumes/hypnose.html','catalog/category/view/id/89',0,1,NULL,1,NULL),('206','category','89','fragrance/women-s-perfumes/hypnose.html','catalog/category/view/id/89',0,2,NULL,1,NULL),('207','category','90','fragrance/women-s-perfumes/poeme.html','catalog/category/view/id/90',0,1,NULL,1,NULL),('208','category','90','fragrance/women-s-perfumes/poeme.html','catalog/category/view/id/90',0,2,NULL,1,NULL),('209','category','91','fragrance/women-s-perfumes/magie-noire.html','catalog/category/view/id/91',0,1,NULL,1,NULL),('210','category','91','fragrance/women-s-perfumes/magie-noire.html','catalog/category/view/id/91',0,2,NULL,1,NULL),('211','category','92','fragrance/women-s-perfumes/maison-lancome.html','catalog/category/view/id/92',0,1,NULL,1,NULL),('212','category','92','fragrance/women-s-perfumes/maison-lancome.html','catalog/category/view/id/92',0,2,NULL,1,NULL),('213','category','93','fragrance/women-s-perfumes/mother-s-day.html','catalog/category/view/id/93',0,1,NULL,1,NULL),('214','category','93','fragrance/women-s-perfumes/mother-s-day.html','catalog/category/view/id/93',0,2,NULL,1,NULL),('215','category','94','fragrance/women-s-perfumes/engraving-fragrance.html','catalog/category/view/id/94',0,1,NULL,1,NULL),('216','category','94','fragrance/women-s-perfumes/engraving-fragrance.html','catalog/category/view/id/94',0,2,NULL,1,NULL),('217','category','95','fragrance/men-s-perfume/hypnose-men.html','catalog/category/view/id/95',0,1,NULL,1,NULL),('218','category','95','fragrance/men-s-perfume/hypnose-men.html','catalog/category/view/id/95',0,2,NULL,1,NULL),('219','category','96','samples.html','catalog/category/view/id/96',0,1,NULL,1,NULL),('220','category','96','samples.html','catalog/category/view/id/96',0,2,NULL,1,NULL),('225','product','3','monsieur-big-mascara-travel-size.html','catalog/product/view/id/3',0,1,NULL,1,NULL),('226','product','3','make-up/monsieur-big-mascara-travel-size.html','catalog/product/view/id/3/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('227','product','3','monsieur-big-mascara-travel-size.html','catalog/product/view/id/3',0,2,NULL,1,NULL),('228','product','3','make-up/monsieur-big-mascara-travel-size.html','catalog/product/view/id/3/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('229','product','10','tresor-in-love-temps-tmp.html','catalog/product/view/id/10',0,1,NULL,1,NULL),('230','product','10','fragrance/tresor-in-love-temps-tmp.html','catalog/product/view/id/10/category/5',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('231','product','10','fragrance/women-s-perfumes/tresor-in-love-temps-tmp.html','catalog/product/view/id/10/category/80',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"80\";}'),('232','product','10','fragrance/men-s-perfume/tresor-in-love-temps-tmp.html','catalog/product/view/id/10/category/81',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"81\";}'),('233','product','10','tresor-in-love-temps-tmp.html','catalog/product/view/id/10',0,2,NULL,1,NULL),('234','product','10','fragrance/tresor-in-love-temps-tmp.html','catalog/product/view/id/10/category/5',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"5\";}'),('235','product','10','fragrance/women-s-perfumes/tresor-in-love-temps-tmp.html','catalog/product/view/id/10/category/80',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"80\";}'),('236','product','10','fragrance/men-s-perfume/tresor-in-love-temps-tmp.html','catalog/product/view/id/10/category/81',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"81\";}'),('237','product','2','dummy-test2.html','catalog/product/view/id/2',0,1,NULL,1,NULL),('238','product','2','make-up/dummy-test2.html','catalog/product/view/id/2/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('239','product','2','skincare/dummy-test2.html','catalog/product/view/id/2/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('240','product','2','make-up/face/dummy-test2.html','catalog/product/view/id/2/category/10',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),('241','product','2','make-up/face/foundation/dummy-test2.html','catalog/product/view/id/2/category/15',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"15\";}'),('242','product','2','make-up/face/primer/dummy-test2.html','catalog/product/view/id/2/category/16',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"16\";}'),('243','product','2','dummy-test2.html','catalog/product/view/id/2',0,2,NULL,1,NULL),('244','product','2','make-up/dummy-test2.html','catalog/product/view/id/2/category/3',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),('245','product','2','skincare/dummy-test2.html','catalog/product/view/id/2/category/4',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),('246','product','2','make-up/face/dummy-test2.html','catalog/product/view/id/2/category/10',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"10\";}'),('247','product','2','make-up/face/foundation/dummy-test2.html','catalog/product/view/id/2/category/15',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"15\";}'),('248','product','2','make-up/face/primer/dummy-test2.html','catalog/product/view/id/2/category/16',0,2,NULL,1,'a:1:{s:11:\"category_id\";s:2:\"16\";}');
/*!40000 ALTER TABLE `url_rewrite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variable`
--

DROP TABLE IF EXISTS `variable`;
CREATE TABLE `variable` (
  `variable_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Variable Code',
  `name` varchar(255) DEFAULT NULL COMMENT 'Variable Name',
  PRIMARY KEY (`variable_id`),
  UNIQUE KEY `VARIABLE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variables';

--
-- Table structure for table `variable_value`
--

DROP TABLE IF EXISTS `variable_value`;
CREATE TABLE `variable_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Value Id',
  `variable_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Variable Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `plain_value` text COMMENT 'Plain Text Value',
  `html_value` text COMMENT 'Html Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `VARIABLE_VALUE_VARIABLE_ID_STORE_ID` (`variable_id`,`store_id`),
  KEY `VARIABLE_VALUE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variable Value';

--
-- Table structure for table `vault_payment_token`
--

DROP TABLE IF EXISTS `vault_payment_token`;
CREATE TABLE `vault_payment_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `public_hash` varchar(128) NOT NULL COMMENT 'Hash code for using on frontend',
  `payment_method_code` varchar(128) NOT NULL COMMENT 'Payment method code',
  `type` varchar(128) NOT NULL COMMENT 'Type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `expires_at` timestamp NULL DEFAULT NULL COMMENT 'Expires At',
  `gateway_token` varchar(255) NOT NULL COMMENT 'Gateway Token',
  `details` text COMMENT 'Details',
  `is_active` tinyint(1) NOT NULL COMMENT 'Is active flag',
  `is_visible` tinyint(1) NOT NULL COMMENT 'Is visible flag',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `VAULT_PAYMENT_TOKEN_HASH_UNIQUE_INDEX_PUBLIC_HASH` (`public_hash`),
  UNIQUE KEY `UNQ_54DCE14AEAEA03B587F9EF723EB10A10` (`payment_method_code`,`customer_id`,`gateway_token`),
  KEY `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Vault tokens of payment';

--
-- Table structure for table `vault_payment_token_order_payment_link`
--

DROP TABLE IF EXISTS `vault_payment_token_order_payment_link`;
CREATE TABLE `vault_payment_token_order_payment_link` (
  `order_payment_id` int(10) unsigned NOT NULL COMMENT 'Order payment Id',
  `payment_token_id` int(10) unsigned NOT NULL COMMENT 'Payment token Id',
  PRIMARY KEY (`order_payment_id`,`payment_token_id`),
  KEY `FK_4ED894655446D385894580BECA993862` (`payment_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Order payments to vault token';

--
-- Table structure for table `weee_tax`
--

DROP TABLE IF EXISTS `weee_tax`;
CREATE TABLE `weee_tax` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `country` varchar(2) DEFAULT NULL COMMENT 'Country',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT 'State',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`value_id`),
  KEY `WEEE_TAX_WEBSITE_ID` (`website_id`),
  KEY `WEEE_TAX_ENTITY_ID` (`entity_id`),
  KEY `WEEE_TAX_COUNTRY` (`country`),
  KEY `WEEE_TAX_ATTRIBUTE_ID` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Weee Tax';

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
CREATE TABLE `widget` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Widget Id',
  `widget_code` varchar(255) DEFAULT NULL COMMENT 'Widget code for template directive',
  `widget_type` varchar(255) DEFAULT NULL COMMENT 'Widget Type',
  `parameters` text COMMENT 'Parameters',
  PRIMARY KEY (`widget_id`),
  KEY `WIDGET_WIDGET_CODE` (`widget_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Preconfigured Widgets';

--
-- Table structure for table `widget_instance`
--

DROP TABLE IF EXISTS `widget_instance`;
CREATE TABLE `widget_instance` (
  `instance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Instance Id',
  `instance_type` varchar(255) DEFAULT NULL COMMENT 'Instance Type',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Widget Title',
  `store_ids` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Store ids',
  `widget_parameters` text COMMENT 'Widget parameters',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  PRIMARY KEY (`instance_id`),
  KEY `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instances of Widget for Package Theme';

--
-- Table structure for table `widget_instance_page`
--

DROP TABLE IF EXISTS `widget_instance_page`;
CREATE TABLE `widget_instance_page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Page Id',
  `instance_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Id',
  `page_group` varchar(25) DEFAULT NULL COMMENT 'Block Group Type',
  `layout_handle` varchar(255) DEFAULT NULL COMMENT 'Layout Handle',
  `block_reference` varchar(255) DEFAULT NULL COMMENT 'Container',
  `page_for` varchar(25) DEFAULT NULL COMMENT 'For instance entities',
  `entities` text COMMENT 'Catalog entities (comma separated)',
  `page_template` varchar(255) DEFAULT NULL COMMENT 'Path to widget template',
  PRIMARY KEY (`page_id`),
  KEY `WIDGET_INSTANCE_PAGE_INSTANCE_ID` (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instance of Widget on Page';

--
-- Table structure for table `widget_instance_page_layout`
--

DROP TABLE IF EXISTS `widget_instance_page_layout`;
CREATE TABLE `widget_instance_page_layout` (
  `page_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Page Id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  UNIQUE KEY `WIDGET_INSTANCE_PAGE_LAYOUT_LAYOUT_UPDATE_ID_PAGE_ID` (`layout_update_id`,`page_id`),
  KEY `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout updates';

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `wishlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist ID',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer ID',
  `shared` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sharing flag (0 or 1)',
  `sharing_code` varchar(32) DEFAULT NULL COMMENT 'Sharing encrypted code',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Last updated date',
  PRIMARY KEY (`wishlist_id`),
  UNIQUE KEY `WISHLIST_CUSTOMER_ID` (`customer_id`),
  KEY `WISHLIST_SHARED` (`shared`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Wishlist main Table';

--
-- Dumping data for table `wishlist`
--

LOCK TABLES `wishlist` WRITE;
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;
INSERT INTO `wishlist` VALUES ('1','1',0,'7d874d1de3a565eee4b6139c1c182725','2017-06-23 07:39:43');
/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist_item`
--

DROP TABLE IF EXISTS `wishlist_item`;
CREATE TABLE `wishlist_item` (
  `wishlist_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist item ID',
  `wishlist_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Wishlist ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Add date and time',
  `description` text COMMENT 'Short description of wish list item',
  `qty` decimal(12,4) NOT NULL COMMENT 'Qty',
  PRIMARY KEY (`wishlist_item_id`),
  KEY `WISHLIST_ITEM_WISHLIST_ID` (`wishlist_id`),
  KEY `WISHLIST_ITEM_PRODUCT_ID` (`product_id`),
  KEY `WISHLIST_ITEM_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist items';

--
-- Table structure for table `wishlist_item_option`
--

DROP TABLE IF EXISTS `wishlist_item_option`;
CREATE TABLE `wishlist_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `wishlist_item_id` int(10) unsigned NOT NULL COMMENT 'Wishlist Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `FK_A014B30B04B72DD0EAB3EECD779728D6` (`wishlist_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist Item Option Table';

ALTER TABLE `admin_passwords`
  ADD CONSTRAINT `ADMIN_PASSWORDS_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE;

ALTER TABLE `admin_user_session`
  ADD CONSTRAINT `ADMIN_USER_SESSION_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE;

ALTER TABLE `authorization_rule`
  ADD CONSTRAINT `AUTHORIZATION_RULE_ROLE_ID_AUTHORIZATION_ROLE_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `authorization_role` (`role_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_datetime`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_DTIME_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_decimal`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_DEC_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_int`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_INT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_text`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_TEXT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_entity_varchar`
  ADD CONSTRAINT `CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_ENTT_VCHR_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_category_product`
  ADD CONSTRAINT `CAT_CTGR_PRD_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_CTGR_PRD_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_compare_item`
  ADD CONSTRAINT `CATALOG_COMPARE_ITEM_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATALOG_COMPARE_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATALOG_COMPARE_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `catalog_eav_attribute`
  ADD CONSTRAINT `CATALOG_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_option`
  ADD CONSTRAINT `CAT_PRD_BNDL_OPT_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_option_value`
  ADD CONSTRAINT `CAT_PRD_BNDL_OPT_VAL_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_price_index`
  ADD CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_selection`
  ADD CONSTRAINT `CAT_PRD_BNDL_SELECTION_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_BNDL_SELECTION_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_bundle_selection_price`
  ADD CONSTRAINT `CAT_PRD_BNDL_SELECTION_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DCF37523AA05D770A70AA4ED7C2616E4` FOREIGN KEY (`selection_id`) REFERENCES `catalog_product_bundle_selection` (`selection_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity`
  ADD CONSTRAINT `CAT_PRD_ENTT_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_datetime`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_DTIME_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_decimal`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_DEC_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_gallery`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_GLR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_int`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_INT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_media_gallery`
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_media_gallery_value`
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_VAL_ID_CAT_PRD_ENTT_MDA_GLR_VAL_ID` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_media_gallery_value_to_entity`
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_A6C6C8FAA386736921D3A7C4B50B1185` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_media_gallery_value_video`
  ADD CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6FDF205946906B0E653E60AA769899F8` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_text`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_TEXT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_tier_price`
  ADD CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_entity_varchar`
  ADD CONSTRAINT `CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_ENTT_VCHR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_index_price`
  ADD CONSTRAINT `CATALOG_PRODUCT_INDEX_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_IDX_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_IDX_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_index_tier_price`
  ADD CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_index_website`
  ADD CONSTRAINT `CAT_PRD_IDX_WS_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link`
  ADD CONSTRAINT `CATALOG_PRODUCT_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_LNK_LNKED_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`linked_product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_LNK_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link_attribute`
  ADD CONSTRAINT `CAT_PRD_LNK_ATTR_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link_attribute_decimal`
  ADD CONSTRAINT `CAT_PRD_LNK_ATTR_DEC_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AB2EFA9A14F7BCF1D5400056203D14B6` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link_attribute_int`
  ADD CONSTRAINT `CAT_PRD_LNK_ATTR_INT_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_D6D878F8BA2A4282F8DDED7E6E3DE35C` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_link_attribute_varchar`
  ADD CONSTRAINT `CAT_PRD_LNK_ATTR_VCHR_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DEE9C4DA61CFCC01DFCF50F0D79CEA51` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option`
  ADD CONSTRAINT `CAT_PRD_OPT_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_price`
  ADD CONSTRAINT `CATALOG_PRODUCT_OPTION_PRICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_OPT_PRICE_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_title`
  ADD CONSTRAINT `CATALOG_PRODUCT_OPTION_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_OPT_TTL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_type_price`
  ADD CONSTRAINT `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B523E3378E8602F376CC415825576B7F` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_type_title`
  ADD CONSTRAINT `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C085B9CF2C2A302E8043FDEA1937D6A2` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_option_type_value`
  ADD CONSTRAINT `CAT_PRD_OPT_TYPE_VAL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_relation`
  ADD CONSTRAINT `CAT_PRD_RELATION_CHILD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`child_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_RELATION_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_super_attribute`
  ADD CONSTRAINT `CAT_PRD_SPR_ATTR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_super_attribute_label`
  ADD CONSTRAINT `CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_309442281DF7784210ED82B2CC51E5D5` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_super_link`
  ADD CONSTRAINT `CAT_PRD_SPR_LNK_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_SPR_LNK_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_product_website`
  ADD CONSTRAINT `CATALOG_PRODUCT_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_PRD_WS_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `catalog_url_rewrite_product_category`
  ADD CONSTRAINT `CAT_URL_REWRITE_PRD_CTGR_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CAT_URL_REWRITE_PRD_CTGR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_BB79E64705D7F17FE181F23144528FC8` FOREIGN KEY (`url_rewrite_id`) REFERENCES `url_rewrite` (`url_rewrite_id`) ON DELETE CASCADE;

ALTER TABLE `cataloginventory_stock_item`
  ADD CONSTRAINT `CATINV_STOCK_ITEM_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATINV_STOCK_ITEM_STOCK_ID_CATINV_STOCK_STOCK_ID` FOREIGN KEY (`stock_id`) REFERENCES `cataloginventory_stock` (`stock_id`) ON DELETE CASCADE;

ALTER TABLE `catalogrule_customer_group`
  ADD CONSTRAINT `CATALOGRULE_CUSTOMER_GROUP_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE;

ALTER TABLE `catalogrule_group_website`
  ADD CONSTRAINT `CATALOGRULE_GROUP_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATRULE_GROUP_WS_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE;

ALTER TABLE `catalogrule_website`
  ADD CONSTRAINT `CATALOGRULE_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CATALOGRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `checkout_agreement_store`
  ADD CONSTRAINT `CHECKOUT_AGREEMENT_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CHKT_AGRT_STORE_AGRT_ID_CHKT_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `checkout_agreement` (`agreement_id`) ON DELETE CASCADE;

ALTER TABLE `cms_block_store`
  ADD CONSTRAINT `CMS_BLOCK_STORE_BLOCK_ID_CMS_BLOCK_BLOCK_ID` FOREIGN KEY (`block_id`) REFERENCES `cms_block` (`block_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CMS_BLOCK_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `cms_page_store`
  ADD CONSTRAINT `CMS_PAGE_STORE_PAGE_ID_CMS_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`page_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CMS_PAGE_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity`
  ADD CONSTRAINT `CUSTOMER_ADDRESS_ENTITY_PARENT_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_datetime`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_DTIME_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_decimal`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_DEC_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_int`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_INT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_text`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_TEXT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_address_entity_varchar`
  ADD CONSTRAINT `CSTR_ADDR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_ADDR_ENTT_VCHR_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_eav_attribute`
  ADD CONSTRAINT `CUSTOMER_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `customer_eav_attribute_website`
  ADD CONSTRAINT `CSTR_EAV_ATTR_WS_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CSTR_EAV_ATTR_WS_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity`
  ADD CONSTRAINT `CUSTOMER_ENTITY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `CUSTOMER_ENTITY_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE SET NULL;

ALTER TABLE `customer_entity_datetime`
  ADD CONSTRAINT `CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity_decimal`
  ADD CONSTRAINT `CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity_int`
  ADD CONSTRAINT `CUSTOMER_ENTITY_INT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_INT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity_text`
  ADD CONSTRAINT `CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_TEXT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_entity_varchar`
  ADD CONSTRAINT `CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `customer_form_attribute`
  ADD CONSTRAINT `CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `design_change`
  ADD CONSTRAINT `DESIGN_CHANGE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `directory_country_region_name`
  ADD CONSTRAINT `DIR_COUNTRY_REGION_NAME_REGION_ID_DIR_COUNTRY_REGION_REGION_ID` FOREIGN KEY (`region_id`) REFERENCES `directory_country_region` (`region_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_link`
  ADD CONSTRAINT `DOWNLOADABLE_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_link_price`
  ADD CONSTRAINT `DOWNLOADABLE_LINK_PRICE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `DOWNLOADABLE_LINK_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_link_purchased`
  ADD CONSTRAINT `DL_LNK_PURCHASED_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `DOWNLOADABLE_LINK_PURCHASED_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE SET NULL;

ALTER TABLE `downloadable_link_purchased_item`
  ADD CONSTRAINT `DL_LNK_PURCHASED_ITEM_ORDER_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`order_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `DL_LNK_PURCHASED_ITEM_PURCHASED_ID_DL_LNK_PURCHASED_PURCHASED_ID` FOREIGN KEY (`purchased_id`) REFERENCES `downloadable_link_purchased` (`purchased_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_link_title`
  ADD CONSTRAINT `DOWNLOADABLE_LINK_TITLE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `DOWNLOADABLE_LINK_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_sample`
  ADD CONSTRAINT `DOWNLOADABLE_SAMPLE_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `downloadable_sample_title`
  ADD CONSTRAINT `DL_SAMPLE_TTL_SAMPLE_ID_DL_SAMPLE_SAMPLE_ID` FOREIGN KEY (`sample_id`) REFERENCES `downloadable_sample` (`sample_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `DOWNLOADABLE_SAMPLE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute`
  ADD CONSTRAINT `EAV_ATTRIBUTE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_group`
  ADD CONSTRAINT `EAV_ATTR_GROUP_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_label`
  ADD CONSTRAINT `EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ATTRIBUTE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_option`
  ADD CONSTRAINT `EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_option_swatch`
  ADD CONSTRAINT `EAV_ATTRIBUTE_OPTION_SWATCH_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ATTR_OPT_SWATCH_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_option_value`
  ADD CONSTRAINT `EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ATTR_OPT_VAL_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE;

ALTER TABLE `eav_attribute_set`
  ADD CONSTRAINT `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity`
  ADD CONSTRAINT `EAV_ENTITY_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_attribute`
  ADD CONSTRAINT `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTT_ATTR_ATTR_GROUP_ID_EAV_ATTR_GROUP_ATTR_GROUP_ID` FOREIGN KEY (`attribute_group_id`) REFERENCES `eav_attribute_group` (`attribute_group_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_datetime`
  ADD CONSTRAINT `EAV_ENTITY_DATETIME_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTT_DTIME_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_decimal`
  ADD CONSTRAINT `EAV_ENTITY_DECIMAL_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_int`
  ADD CONSTRAINT `EAV_ENTITY_INT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_INT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_store`
  ADD CONSTRAINT `EAV_ENTITY_STORE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_text`
  ADD CONSTRAINT `EAV_ENTITY_TEXT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_TEXT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_entity_varchar`
  ADD CONSTRAINT `EAV_ENTITY_VARCHAR_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_element`
  ADD CONSTRAINT `EAV_FORM_ELEMENT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_FORM_ELEMENT_FIELDSET_ID_EAV_FORM_FIELDSET_FIELDSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `EAV_FORM_ELEMENT_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_fieldset`
  ADD CONSTRAINT `EAV_FORM_FIELDSET_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_fieldset_label`
  ADD CONSTRAINT `EAV_FORM_FIELDSET_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_FORM_FSET_LBL_FSET_ID_EAV_FORM_FSET_FSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_type`
  ADD CONSTRAINT `EAV_FORM_TYPE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `eav_form_type_entity`
  ADD CONSTRAINT `EAV_FORM_TYPE_ENTITY_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `EAV_FORM_TYPE_ENTT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE;

ALTER TABLE `googleoptimizer_code`
  ADD CONSTRAINT `GOOGLEOPTIMIZER_CODE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `integration`
  ADD CONSTRAINT `INTEGRATION_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `layout_link`
  ADD CONSTRAINT `LAYOUT_LINK_LAYOUT_UPDATE_ID_LAYOUT_UPDATE_LAYOUT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `LAYOUT_LINK_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `LAYOUT_LINK_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_blog_author`
  ADD CONSTRAINT `MAGEPLAZA_BLOG_AUTHOR_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_blog_comment`
  ADD CONSTRAINT `MAGEPLAZA_BLOG_COMMENT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `MAGEPLAZA_BLOG_COMMENT_POST_ID_MAGEPLAZA_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `mageplaza_blog_post` (`post_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_blog_comment_like`
  ADD CONSTRAINT `FK_1AA6C994694449283752B6F4C2373B42` FOREIGN KEY (`comment_id`) REFERENCES `mageplaza_blog_comment` (`comment_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `MAGEPLAZA_BLOG_COMMENT_LIKE_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_blog_post_category`
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_CATEGORY_POST_ID_MAGEPLAZA_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `mageplaza_blog_post` (`post_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_CTGR_CTGR_ID_MAGEPLAZA_BLOG_CTGR_CTGR_ID` FOREIGN KEY (`category_id`) REFERENCES `mageplaza_blog_category` (`category_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_blog_post_product`
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_PRD_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_PRODUCT_POST_ID_MAGEPLAZA_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `mageplaza_blog_post` (`post_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_blog_post_tag`
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_TAG_POST_ID_MAGEPLAZA_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `mageplaza_blog_post` (`post_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_TAG_TAG_ID_MAGEPLAZA_BLOG_TAG_TAG_ID` FOREIGN KEY (`tag_id`) REFERENCES `mageplaza_blog_tag` (`tag_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_blog_post_topic`
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_TOPIC_POST_ID_MAGEPLAZA_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `mageplaza_blog_post` (`post_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_TOPIC_TOPIC_ID_MAGEPLAZA_BLOG_TOPIC_TOPIC_ID` FOREIGN KEY (`topic_id`) REFERENCES `mageplaza_blog_topic` (`topic_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_blog_post_traffic`
  ADD CONSTRAINT `MAGEPLAZA_BLOG_POST_TRAFFIC_POST_ID_MAGEPLAZA_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `mageplaza_blog_post` (`post_id`) ON DELETE CASCADE;

ALTER TABLE `mageplaza_social_customer`
  ADD CONSTRAINT `MAGEPLAZA_SOCIAL_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_problem`
  ADD CONSTRAINT `NEWSLETTER_PROBLEM_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `NLTTR_PROBLEM_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_queue`
  ADD CONSTRAINT `NEWSLETTER_QUEUE_TEMPLATE_ID_NEWSLETTER_TEMPLATE_TEMPLATE_ID` FOREIGN KEY (`template_id`) REFERENCES `newsletter_template` (`template_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_queue_link`
  ADD CONSTRAINT `NEWSLETTER_QUEUE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `NLTTR_QUEUE_LNK_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_queue_store_link`
  ADD CONSTRAINT `NEWSLETTER_QUEUE_STORE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `NEWSLETTER_QUEUE_STORE_LINK_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `newsletter_subscriber`
  ADD CONSTRAINT `NEWSLETTER_SUBSCRIBER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `oauth_nonce`
  ADD CONSTRAINT `OAUTH_NONCE_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `oauth_token`
  ADD CONSTRAINT `OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`admin_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `OAUTH_TOKEN_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `paypal_billing_agreement`
  ADD CONSTRAINT `PAYPAL_BILLING_AGREEMENT_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PAYPAL_BILLING_AGREEMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `paypal_billing_agreement_order`
  ADD CONSTRAINT `PAYPAL_BILLING_AGREEMENT_ORDER_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PAYPAL_BILLING_AGRT_ORDER_AGRT_ID_PAYPAL_BILLING_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `paypal_billing_agreement` (`agreement_id`) ON DELETE CASCADE;

ALTER TABLE `paypal_cert`
  ADD CONSTRAINT `PAYPAL_CERT_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `paypal_settlement_report_row`
  ADD CONSTRAINT `FK_E183E488F593E0DE10C6EBFFEBAC9B55` FOREIGN KEY (`report_id`) REFERENCES `paypal_settlement_report` (`report_id`) ON DELETE CASCADE;

ALTER TABLE `persistent_session`
  ADD CONSTRAINT `PERSISTENT_SESSION_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PERSISTENT_SESSION_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `product_alert_price`
  ADD CONSTRAINT `PRODUCT_ALERT_PRICE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PRODUCT_ALERT_PRICE_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PRODUCT_ALERT_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `product_alert_stock`
  ADD CONSTRAINT `PRODUCT_ALERT_STOCK_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PRODUCT_ALERT_STOCK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `PRODUCT_ALERT_STOCK_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `quote`
  ADD CONSTRAINT `QUOTE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `quote_address`
  ADD CONSTRAINT `QUOTE_ADDRESS_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `quote_address_item`
  ADD CONSTRAINT `QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID_QUOTE_ADDRESS_ADDRESS_ID` FOREIGN KEY (`quote_address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`quote_item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ADDR_ITEM_PARENT_ITEM_ID_QUOTE_ADDR_ITEM_ADDR_ITEM_ID` FOREIGN KEY (`parent_item_id`) REFERENCES `quote_address_item` (`address_item_id`) ON DELETE CASCADE;

ALTER TABLE `quote_id_mask`
  ADD CONSTRAINT `QUOTE_ID_MASK_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `quote_item`
  ADD CONSTRAINT `QUOTE_ITEM_PARENT_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`parent_item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ITEM_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `QUOTE_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `quote_item_option`
  ADD CONSTRAINT `QUOTE_ITEM_OPTION_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE;

ALTER TABLE `quote_payment`
  ADD CONSTRAINT `QUOTE_PAYMENT_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `quote_shipping_rate`
  ADD CONSTRAINT `QUOTE_SHIPPING_RATE_ADDRESS_ID_QUOTE_ADDRESS_ADDRESS_ID` FOREIGN KEY (`address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE;

ALTER TABLE `rating`
  ADD CONSTRAINT `RATING_ENTITY_ID_RATING_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `rating_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `rating_option`
  ADD CONSTRAINT `RATING_OPTION_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE;

ALTER TABLE `rating_option_vote`
  ADD CONSTRAINT `RATING_OPTION_VOTE_OPTION_ID_RATING_OPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `rating_option` (`option_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE;

ALTER TABLE `rating_option_vote_aggregated`
  ADD CONSTRAINT `RATING_OPTION_VOTE_AGGREGATED_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `RATING_OPTION_VOTE_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `rating_store`
  ADD CONSTRAINT `RATING_STORE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `RATING_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `rating_title`
  ADD CONSTRAINT `RATING_TITLE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `RATING_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_compared_product_index`
  ADD CONSTRAINT `REPORT_CMPD_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_CMPD_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_COMPARED_PRODUCT_INDEX_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `report_event`
  ADD CONSTRAINT `REPORT_EVENT_EVENT_TYPE_ID_REPORT_EVENT_TYPES_EVENT_TYPE_ID` FOREIGN KEY (`event_type_id`) REFERENCES `report_event_types` (`event_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_EVENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_viewed_product_aggregated_daily`
  ADD CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_DAILY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_viewed_product_aggregated_monthly`
  ADD CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_MONTHLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_viewed_product_aggregated_yearly`
  ADD CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_YEARLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `report_viewed_product_index`
  ADD CONSTRAINT `REPORT_VIEWED_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REPORT_VIEWED_PRODUCT_INDEX_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `review`
  ADD CONSTRAINT `REVIEW_ENTITY_ID_REVIEW_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `review_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REVIEW_STATUS_ID_REVIEW_STATUS_STATUS_ID` FOREIGN KEY (`status_id`) REFERENCES `review_status` (`status_id`) ON DELETE NO ACTION;

ALTER TABLE `review_detail`
  ADD CONSTRAINT `REVIEW_DETAIL_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `REVIEW_DETAIL_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REVIEW_DETAIL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `review_entity_summary`
  ADD CONSTRAINT `REVIEW_ENTITY_SUMMARY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `review_store`
  ADD CONSTRAINT `REVIEW_STORE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REVIEW_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_bestsellers_aggregated_daily`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_bestsellers_aggregated_monthly`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_bestsellers_aggregated_yearly`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_creditmemo`
  ADD CONSTRAINT `SALES_CREDITMEMO_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_CREDITMEMO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_creditmemo_comment`
  ADD CONSTRAINT `SALES_CREDITMEMO_COMMENT_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_creditmemo_item`
  ADD CONSTRAINT `SALES_CREDITMEMO_ITEM_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_invoice`
  ADD CONSTRAINT `SALES_INVOICE_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_INVOICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_invoice_comment`
  ADD CONSTRAINT `SALES_INVOICE_COMMENT_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_invoice_item`
  ADD CONSTRAINT `SALES_INVOICE_ITEM_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_invoiced_aggregated`
  ADD CONSTRAINT `SALES_INVOICED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_invoiced_aggregated_order`
  ADD CONSTRAINT `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order`
  ADD CONSTRAINT `SALES_ORDER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `SALES_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order_address`
  ADD CONSTRAINT `SALES_ORDER_ADDRESS_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_aggregated_created`
  ADD CONSTRAINT `SALES_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order_aggregated_updated`
  ADD CONSTRAINT `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order_canceling`
  ADD CONSTRAINT `SALES_ORDER_CANCELING_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_exporting`
  ADD CONSTRAINT `SALES_ORDER_EXPORTING_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_item`
  ADD CONSTRAINT `SALES_ORDER_ITEM_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_order_payment`
  ADD CONSTRAINT `SALES_ORDER_PAYMENT_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_status_history`
  ADD CONSTRAINT `SALES_ORDER_STATUS_HISTORY_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_status_label`
  ADD CONSTRAINT `SALES_ORDER_STATUS_LABEL_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_STATUS_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `sales_order_status_state`
  ADD CONSTRAINT `SALES_ORDER_STATUS_STATE_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE;

ALTER TABLE `sales_order_tax_item`
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`associated_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_TAX_ID_SALES_ORDER_TAX_TAX_ID` FOREIGN KEY (`tax_id`) REFERENCES `sales_order_tax` (`tax_id`) ON DELETE CASCADE;

ALTER TABLE `sales_payment_transaction`
  ADD CONSTRAINT `FK_B99FF1A06402D725EBDB0F3A7ECD47A2` FOREIGN KEY (`parent_id`) REFERENCES `sales_payment_transaction` (`transaction_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_PAYMENT_TRANSACTION_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_PAYMENT_TRANSACTION_PAYMENT_ID_SALES_ORDER_PAYMENT_ENTT_ID` FOREIGN KEY (`payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_refunded_aggregated`
  ADD CONSTRAINT `SALES_REFUNDED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_refunded_aggregated_order`
  ADD CONSTRAINT `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_sequence_profile`
  ADD CONSTRAINT `SALES_SEQUENCE_PROFILE_META_ID_SALES_SEQUENCE_META_META_ID` FOREIGN KEY (`meta_id`) REFERENCES `sales_sequence_meta` (`meta_id`) ON DELETE CASCADE;

ALTER TABLE `sales_shipment`
  ADD CONSTRAINT `SALES_SHIPMENT_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_SHIPMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_shipment_comment`
  ADD CONSTRAINT `SALES_SHIPMENT_COMMENT_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_shipment_item`
  ADD CONSTRAINT `SALES_SHIPMENT_ITEM_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_shipment_track`
  ADD CONSTRAINT `SALES_SHIPMENT_TRACK_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `sales_shipping_aggregated`
  ADD CONSTRAINT `SALES_SHIPPING_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `sales_shipping_aggregated_order`
  ADD CONSTRAINT `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

ALTER TABLE `salesrule_coupon`
  ADD CONSTRAINT `SALESRULE_COUPON_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_coupon_aggregated`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_coupon_aggregated_order`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_coupon_aggregated_updated`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_coupon_usage`
  ADD CONSTRAINT `SALESRULE_COUPON_USAGE_COUPON_ID_SALESRULE_COUPON_COUPON_ID` FOREIGN KEY (`coupon_id`) REFERENCES `salesrule_coupon` (`coupon_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_COUPON_USAGE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_customer`
  ADD CONSTRAINT `SALESRULE_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_CUSTOMER_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_customer_group`
  ADD CONSTRAINT `SALESRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_CUSTOMER_GROUP_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_label`
  ADD CONSTRAINT `SALESRULE_LABEL_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_product_attribute`
  ADD CONSTRAINT `SALESRULE_PRD_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRD_ATTR_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `salesrule_website`
  ADD CONSTRAINT `SALESRULE_WEBSITE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `search_query`
  ADD CONSTRAINT `SEARCH_QUERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `search_synonyms`
  ADD CONSTRAINT `SEARCH_SYNONYMS_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SEARCH_SYNONYMS_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `sitemap`
  ADD CONSTRAINT `SITEMAP_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `store`
  ADD CONSTRAINT `STORE_GROUP_ID_STORE_GROUP_GROUP_ID` FOREIGN KEY (`group_id`) REFERENCES `store_group` (`group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `STORE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `store_group`
  ADD CONSTRAINT `STORE_GROUP_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `tax_calculation`
  ADD CONSTRAINT `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`customer_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`product_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALC_TAX_CALC_RATE_ID_TAX_CALC_RATE_TAX_CALC_RATE_ID` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALC_TAX_CALC_RULE_ID_TAX_CALC_RULE_TAX_CALC_RULE_ID` FOREIGN KEY (`tax_calculation_rule_id`) REFERENCES `tax_calculation_rule` (`tax_calculation_rule_id`) ON DELETE CASCADE;

ALTER TABLE `tax_calculation_rate_title`
  ADD CONSTRAINT `FK_37FB965F786AD5897BB3AE90470C42AB` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALCULATION_RATE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `tax_order_aggregated_created`
  ADD CONSTRAINT `TAX_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `tax_order_aggregated_updated`
  ADD CONSTRAINT `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `theme_file`
  ADD CONSTRAINT `THEME_FILE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE;

ALTER TABLE `translation`
  ADD CONSTRAINT `TRANSLATION_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

ALTER TABLE `ui_bookmark`
  ADD CONSTRAINT `UI_BOOKMARK_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE;

ALTER TABLE `variable_value`
  ADD CONSTRAINT `VARIABLE_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `VARIABLE_VALUE_VARIABLE_ID_VARIABLE_VARIABLE_ID` FOREIGN KEY (`variable_id`) REFERENCES `variable` (`variable_id`) ON DELETE CASCADE;

ALTER TABLE `vault_payment_token`
  ADD CONSTRAINT `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `vault_payment_token_order_payment_link`
  ADD CONSTRAINT `FK_4ED894655446D385894580BECA993862` FOREIGN KEY (`payment_token_id`) REFERENCES `vault_payment_token` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CF37B9D854256534BE23C818F6291CA2` FOREIGN KEY (`order_payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `weee_tax`
  ADD CONSTRAINT `WEEE_TAX_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_COUNTRY_DIRECTORY_COUNTRY_COUNTRY_ID` FOREIGN KEY (`country`) REFERENCES `directory_country` (`country_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_ENTITY_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

ALTER TABLE `widget_instance`
  ADD CONSTRAINT `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE;

ALTER TABLE `widget_instance_page`
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_INSTANCE_ID_WIDGET_INSTANCE_INSTANCE_ID` FOREIGN KEY (`instance_id`) REFERENCES `widget_instance` (`instance_id`) ON DELETE CASCADE;

ALTER TABLE `widget_instance_page_layout`
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID_WIDGET_INSTANCE_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `widget_instance_page` (`page_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_LYT_LYT_UPDATE_ID_LYT_UPDATE_LYT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE;

ALTER TABLE `wishlist`
  ADD CONSTRAINT `WISHLIST_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

ALTER TABLE `wishlist_item`
  ADD CONSTRAINT `WISHLIST_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WISHLIST_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `WISHLIST_ITEM_WISHLIST_ID_WISHLIST_WISHLIST_ID` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`wishlist_id`) ON DELETE CASCADE;

ALTER TABLE `wishlist_item_option`
  ADD CONSTRAINT `FK_A014B30B04B72DD0EAB3EECD779728D6` FOREIGN KEY (`wishlist_item_id`) REFERENCES `wishlist_item` (`wishlist_item_id`) ON DELETE CASCADE;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */; 
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-24 10:08:41 GMT