<?php

namespace Mageplaza\Blog\Block\Home;

use Mageplaza\Blog\Block\Frontend;
use Mageplaza\Blog\Block\Post\Rss\Category;


class Allblog extends Frontend
{
    public function getCategoryList()
    {
        return $this->helperData->getCategoryList();
    }
    public function getCategoryUrl($category)
    {
        return $this->helperData->getCategoryUrl($category);
    }
    public function getPostList($type = null, $id = null){

        return $this->helperData->getPostList($type = null, $id = null);
    }
}
