<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Block\Form;

use Magento\Payment\Block\Form;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Model\Config;

/**
 * Kbank payment block form
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

class Kbank extends Form
{
    // @codingStandardsIgnoreStart
    /**
     * Template
     *
     * @var string
     */
    protected $_instructions;

    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'Acommerce_Kbank::form.phtml';

    /**
     * Payment config model
     *
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentConfig;
    // @codingStandardsIgnoreEnd

    /**
     * Construct
     *
     * @param Context $context       Context
     * @param Config  $paymentConfig Payment Config
     * @param array   $data          Data
     */
    public function __construct(
        Context $context,
        Config $paymentConfig,
        array $data=[]
    ) {
        parent::__construct($context, $data);
        $this->_paymentConfig = $paymentConfig;

    }//end __construct()


    /**
     * Get instructions text from config
     *
     * @return null|string
     */
    public function getInstructions()
    {
        if ($this->_instructions === null) {
            $method = $this->getMethod();
            $this->_instructions = $method->getConfigData('instructions');
        }

        return $this->_instructions;

    }//end getInstructions()


}//end class

?>
