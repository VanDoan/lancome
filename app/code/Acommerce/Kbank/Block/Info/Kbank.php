<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Block\Info;

use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Model\Config;
use Magento\Payment\Block\Info as BlockInfo;

/**
 * Kbank payment Block Info
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Kbank extends BlockInfo
{
    // @codingStandardsIgnoreStart
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'Acommerce_Kbank::info/kbank.phtml';

    /**
     * Payment config model
     *
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentConfig;
    // @codingStandardsIgnoreEnd

    /**
     * Constructor
     *
     * @param Context $context       Context
     * @param Config  $paymentConfig Payment Config
     * @param array   $data          data
     */
    public function __construct(
        Context $context,
        Config $paymentConfig,
        array $data=[]
    ) {
        parent::__construct($context, $data);
        $this->_paymentConfig = $paymentConfig;

    }//end __construct()


    /**
     * Get Cc Type Name
     *
     * @return string
     */
    public function getCcTypeName()
    {
        $types  = $this->_paymentConfig->getCcTypes();
        $ccType = $this->getInfo()->getCcType();

        if (isset($types[$ccType]) === true) {
            return $types[$ccType];
        }

        if (empty($ccType) === true) {
            return __('N/A');
        }

        return __($ccType);

    }//end getCcTypeName()


    /**
     * Prepare credit card related payment info
     *
     * @param DataObject $transport transport
     *
     * @return DataObject
     */

    // @codingStandardsIgnoreStart
    protected function _prepareSpecificInformation($transport=null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }

        $transport = parent::_prepareSpecificInformation($transport);
        $data      = array();

        if ($this->getInfo()->getCcNumberEnc()) {
            $data[(string) __('Credit Card Number')]
                = $this->getInfo()->getCcNumberEnc();
        } else {
            $data[(string) __('Credit Card Number')] = 'N/A';
        }

        $additionalInfo = $this->getInfo()->getAdditionalInformation();
        if ($additionalInfo) {
            if (isset($additionalInfo['FILLSPACE']) === true) {
                $ccType = str_replace('X', '', $additionalInfo['FILLSPACE']);
                if (empty($ccType) === true) {
                    $ccType = 'N/A';
                }

                $data[(string) __('Credit Card Type')] = $ccType;
            } else {
                $data[(string) __('Credit Card Type')] = 'N/A';
            }

            if (isset($additionalInfo['AUTHCODE']) === true) {
                $authCode = $additionalInfo['AUTHCODE'];

                $authCode = str_replace('X', '', $additionalInfo['AUTHCODE']);
                if (empty($authCode) === true) {
                    $authCode = 'N/A';
                }

                $data[(string) __('Auth Code')] = $authCode;
            } else {
                $data[(string) __('Auth Code')] = 'N/A';
            }
            //\Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug(var_export($additionalInfo, true));

            if (isset($additionalInfo['PLANID']) === true) {
               if ($additionalInfo['PLANID'] == "999") {
                    $paymonth = $additionalInfo['PAYMONTH'];

                    //$paymonth = str_replace('X', '', $additionalInfo['PAYMONTH']);
                    if (empty($paymonth) === true) {
                       $paymonth = 'N/A';
                        }

                    $data[(string) __('Installment')] = 'Yes';
                    $data[(string) __('Number of Months')] = $paymonth.' Months';
               }
            }

        }//end if

        $ccStatus = $this->getInfo()->getCcStatus();
        if (empty($ccStatus) !== false) {
            if ($ccStatus === 'A') {
                $ccStatus = 'Approved';
            } else {
                $ccStatus = 'Not Approved';
            }

            $data[(string) __('Status')] = __($ccStatus);
        }

        return $transport->setData(array_merge($data, $transport->getData()));

    }//end prepareSpecificInformation()
    // @codingStandardsIgnoreEnd

    /**
     * Convert to pdf
     *
     * @return string
     */
    public function toPdf()
    {
        $this->setTemplate('Acommerce_Kbank::info/pdf/kbank.phtml');
        return $this->toHtml();

    }//end toPdf()


}//end class

?>
