<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\OrderFactory;
use Magento\Checkout\Model\Session;
use Acommerce\Kbank\Helper\Data;

/**
 * Kbank redirect template
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Redirect extends Template
{

    /**
     * Grandtotal number of digits
     *
     * @var number
     */
    const ALLOW_AMOUNT_NUMBER = 12;

    /**
     * Order Factory
     *
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * Checkout Session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * Order id
     *
     * @var string
     */
    protected $orderId;

    /**
     * Kbank Helper
     *
     * @var \Acommerce\Kbank\Helper
     */
    protected $helperData;

    /**
     * Constructor
     *
     * @param Context      $context         Context
     * @param OrderFactory $orderFactory    Order Factory
     * @param Session      $checkoutSession Checkout Session
     * @param Data         $helperData      Data Helper
     * @param Array        $data            Data
     *                                      return
     *                                      void
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        Session $checkoutSession,
        Data $helperData,
        array $data=[]
    ) {
        $this->orderFactory    = $orderFactory;
        $this->checkoutSession = $checkoutSession;
        $this->helperData      = $helperData;

        if (empty($this->checkoutSession->getKbankLastOrderId() === false)) {
            $this->setOrderId($this->checkoutSession->getKbankLastOrderId());
            $this->checkoutSession->unsKbankLastOrderId();
        }

        parent::__construct($context, $data);

    }//end __construct()


    /**
     * Set order id
     *
     * @param int $orderId Order Id
     *
     * @return void
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

    }//end setOrderId()


    /**
     * Get order id
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;

    }//end getOrderId()


    /**
     * Get order object
     *
     * @return object
     */
    public function getOrderObject()
    {
        $orderObj = '';
        if (empty($this->getOrderId()) === false) {
            $orderObj = $this->orderFactory->create();
            $orderObj->load($this->getOrderId());
        }

        return $orderObj;

    }//end getOrderObject()


    /**
     * Get order id
     *
     * @return string
     */
    public function getIncrementId()
    {
        $incrementId = '';
        $orderObj    = $this->getOrderObject();
        if (empty($orderObj) === false) {
            $incrementId = $orderObj->getIncrementId();
        }

        return $incrementId;

    }//end getIncrementId()


    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getIncrementId();

    }//end getDescription()


    /**
     * Convert to leading zero format
     *
     * @param decimal $amount          Amount
     * @param int     $numberOfDitgits Number Of Ditgits
     *
     * @return string
     */
    public function convertToLeadingZeroTotalFormat(
        $amount,
        $numberOfDitgits=self::ALLOW_AMOUNT_NUMBER
    ) {
        return str_pad(($amount * 100), $numberOfDitgits, '0', STR_PAD_LEFT);

    }//end convertToLeadingZeroTotalFormat()


    /**
     * Get billing address object
     *
     * @return object
     */
    public function getBillingAddressObject()
    {
        $billingAddressObj = '';
        $orderObj = $this->getOrderObject();
        if (empty($orderObj) === false) {
            $billingAddressObj = $orderObj->getBillingAddress();
        }

        return $billingAddressObj;

    }//end getBillingAddressObject()


    /**
     * Get shipping address object
     *
     * @return object
     */
    public function getShippingAddressObject()
    {
        $shippingAddressObj = '';
        $orderObj           = $this->getOrderObject();
        if (empty($orderObj) === false) {
            $shippingAddressObj = $orderObj->getShippingAddress();
        }

        return $shippingAddressObj;

    }//end getShippingAddressObject()


    /**
     * Get customer email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        $customerEmail     = '';
        $billingAddressObj = $this->getBillingAddressObject();
        if (empty($billingAddressObj) === false) {
            $customerEmail = $billingAddressObj->getEmail();
        }

        return $customerEmail;

    }//end getCustomerEmail()


    /**
     * Get Form Action Url
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->helperData->getFormActionUrl();

    }//end getFormActionUrl()


    /**
     * Get merchant id
     *
     * @return string
     */
    public function getMerchantId()
    {
        if ($this->checkInstallment() == 1 ) {
            return $this->helperData->getInstallmentMerchantId();
        } else {
            return $this->helperData->getMerchantId();
        }

    }//end getMerchantId()


    /**
     * Get Terminal Id
     *
     * @return string
     */
    public function getTerminalId()
    {
        if ($this->checkInstallment() == 1 ) {
            return $this->helperData->getInstallmentTerminalId();
        } else {
            return $this->helperData->getTerminalId();
        }

    }//end getTerminalId()


    /**
     * Get grand total
     *
     * @return string
     */
    public function getGrandTotal()
    {
        return $this->helperData->getGrandTotal($this->getOrderObject());

    }//end getGrandTotal()


    /**
     * Get Return Url
     *
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->helperData->getReturnUrl();

    }//end getReturnUrl()


    /**
     * Get Return Url
     *
     * @return string
     */
    public function getBackendUrl()
    {
        return $this->helperData->getBackendUrl();

    }//end getBackendUrl()


    /**
     * Get Server Ip
     *
     * @return string
     */
    public function getServerIp()
    {
        return $this->helperData->getServerIp();

    }//end getServerIp()


    /**
     * Get Shop Id
     *
     * @return string
     */
    public function getShopId()
    {
        if ($this->checkInstallment() == 1 ) {
            return $this->helperData->getInstallmentShopId();
        } else {
            return $this->helperData->getShopId();
        }

    }//end getShopId()


    /**
     * Get Check Sum
     *
     * @return string
     */
    public function getCheckSum()
    {
        $data = $this->helperData->getRequestArray(
            $this->getOrderObject(),
            $this->checkInstallment(),
            $this->getInstallmentMonth()
        );
        return $this->helperData->checkSum($data);

    }//end getCheckSum()

    /**
     * Chekc Installment
     *
     * @return string
     */
    public function checkInstallment()
    {
        $data = $this->getOrderObject()->getPayment()->getAdditionalInformation();
        if (isset($data['installment'])) {
            $installment = $data['installment'];
        } else {
            $installment = 0;
        }

        return $installment;

    }//end checkInstallment()

    /**
     * Get Installment Month
     *
     * @return string
     */
    public function getInstallmentMonth()
    {
        if ($this->checkInstallment() == 1 ) {
            $data = $this->getOrderObject()
                ->getPayment()
                ->getAdditionalInformation();

            $installment = $data['installmentBankMonth'];
            return $installment;
        } else {
            return '';
        }

    }//end getMerchantId()

}//end class

?>
