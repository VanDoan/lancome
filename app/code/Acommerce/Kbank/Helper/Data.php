<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\UrlInterface;

/**
 * Kbank payment Helper
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Data extends AbstractHelper
{

    /**
     * Grandtotal number of digits
     *
     * @var number
     */
    const ALLOW_AMOUNT_NUMBER = 12;

    /**
     * Scope Config
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Url Interface
     *
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * Construct
     *
     * @param Context               $context      Context
     * @param StoreManagerInterface $storeManager Store Manager Interface
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->scopeConfig  = $context->getScopeConfig();
        $this->urlInterface = $context->getUrlBuilder();

    }//end __construct()


    /**
     * Get merchant id
     *
     * @return string
     */
    public function getMerchantId()
    {
        return $this->getConfig('payment/kbank/merchant_id');

    }//end getMerchantId()

    /**
     * Get installlment merchant id
     *
     * @return string
     */
    public function getInstallmentMerchantId()
    {
        return $this->getConfig('payment/kbank/installlment_merchant_id');

    }//end getInstallmentMerchantId()


    /**
     * Get Terminal Id
     *
     * @return string
     */
    public function getTerminalId()
    {
        return $this->getConfig('payment/kbank/terminal_id');

    }//end getTerminalId()

    /**
     * Get Installment Terminal Id
     *
     * @return string
     */
    public function getInstallmentTerminalId()
    {
        return $this->getConfig('payment/kbank/installlment_terminal_id');

    }//end getInstallmentTerminalId()


    /**
     * Get Return Url
     *
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->urlInterface->getUrl(
            'kbank/response', array('_secure' => true)
        );

    }//end getReturnUrl()


    /**
     * Get Return Url
     *
     * @return string
     */
    public function getBackendUrl()
    {
        return $this->urlInterface->getUrl(
            'kbank/merchant', array('_secure' => true)
        );

    }//end getBackendUrl()


    /**
     * Get Server Ip
     *
     * @return string
     */
    public function getServerIp()
    {
        return $this->getConfig('payment/kbank/server_ip');

    }//end getServerIp()


    /**
     * Get Shop Id
     *
     * @return string
     */
    public function getShopId()
    {
        return $this->getConfig('payment/kbank/shop_id');

    }//end getShopId()

    /**
     * Get Installment Shop Id
     *
     * @return string
     */
    public function getInstallmentShopId()
    {
        return $this->getConfig('payment/kbank/installlment_shop_id');

    }//end getInstallmentShopId()



    /**
     * Get secret key
     *
     * @return string
     */
    public function getSecretKey()
    {
        return $this->getConfig('payment/kbank/secret_key');

    }//end getSecretKey()


    /**
     * Get grand total
     *
     * @param Order $order Order
     *
     * @return int
     */
    public function getGrandTotal($order)
    {
        $grandTotal = 0;
        if (empty($order) === false) {
            $grandTotal = $order->getGrandTotal();
            $grandTotal = $this->convertToLeadingZeroTotalFormat($grandTotal);
        }

        return $grandTotal;

    }//end getGrandTotal()


    /**
     * Convert to leading zero format
     *
     * @param decimal $amount          Amount
     * @param int     $numberOfDitgits Number Of Ditgits
     *
     * @return string
     */
    public function convertToLeadingZeroTotalFormat(
        $amount,
        $numberOfDitgits=self::ALLOW_AMOUNT_NUMBER
    ) {
        return str_pad(($amount * 100), $numberOfDitgits, '0', STR_PAD_LEFT);

    }//end convertToLeadingZeroTotalFormat()


    /**
     * Get Form Action Url
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        $testmode = $this->getConfig('payment/kbank/test');
        if ($testmode === '1') {
            return $this->getConfig('payment/kbank/test_gateway');
        } else {
            return $this->getConfig('payment/kbank/gateway');
        }

    }//end getFormActionUrl()


    /**
     * Get Request Array
     *
     * @param Order  $order            Order
     * @param string $installment      Installment
     * @param string $installmentMonth Installment Month
     *
     * @return array
     */
    public function getRequestArray($order,$installment,$installmentMonth)
    {
        if ($installment == 1) {
            $data = array(
                 'MERCHANT2'   => $this->getInstallmentMerchantId(),
                 'TERM2'       => $this->getInstallmentTerminalId(),
                 'AMOUNT2'     => $this->getGrandTotal($order),
                 'URL2'        => $this->getReturnUrl(),
                 'RESPURL'     => $this->getBackendUrl(),
                 'IPCUST2'     => $this->getServerIp(),
                 'DETAIL2'     => $order->getIncrementId(),
                 'INVMERCHANT' => $order->getIncrementId(),
                 'FILLSPACE'   => 'N',
                 'SHOPID'      => $this->getInstallmentShopId(),
                 'PAYTERM2'    => $installmentMonth,
                );
        } else {
            $data = array(
                 'MERCHANT2'   => $this->getMerchantId(),
                 'TERM2'       => $this->getTerminalId(),
                 'AMOUNT2'     => $this->getGrandTotal($order),
                 'URL2'        => $this->getReturnUrl(),
                 'RESPURL'     => $this->getBackendUrl(),
                 'IPCUST2'     => $this->getServerIp(),
                 'DETAIL2'     => $order->getIncrementId(),
                 'INVMERCHANT' => $order->getIncrementId(),
                 'FILLSPACE'   => 'N',
                 'SHOPID'      => $this->getShopId(),
                 'PAYTERM2'    => null,
                );
        }

        return $data;

    }//end getRequestArray()


    /**
     * Get Check Sum
     *
     * @param array $data Data
     *
     * @return string
     */
    public function checkSum($data)
    {
        $chkSum  = implode($data);
        $chkSum .= $this->getSecretKey();
        return md5($chkSum);

    }//end checkSum()


    /**
     * Get config value
     *
     * @param string $configPath Config Path
     *
     * @return string
     */
    public function getConfig($configPath)
    {
        return $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

    }//end getConfig()


}//end class

?>
