/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url',
        'Magento_Checkout/js/model/quote',
        'jquery',
        'ko'
    ],
    function (
            Component,
            additionalValidators,
            url,
            quote,
            $,
            ko
            ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Acommerce_Kbank/payment/kbank',
                installment: 0,
                installmentType: '',
                installmentBank : ''
            },

            initObservable: function () {
                this._super()
                    .observe([
                        'installment',
                        'installmentType',
                        'installmentBank'
                    ]);

                return this;
            },

            initialize: function () {
                var self = this;

                this._super();

                //Set credit card number to credit card data object
                this.installmentType.subscribe(function (value) {
                  //alert(value);
                })
            },

            getData: function () {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'installment_bank_id': this.installmentBank(),
                        'installment_month': this.installmentType(),
                        'installment': this.installment()
                    }
                };
            },

            /** Returns payment method instructions */
            getInstructions: function() {
                //return "Kbank instruction";
                return window.checkoutConfig.payment.instructions[this.item.method];
            },

            /** Returns allow installment */
            getAllowInstallment: function() {
                var arrowInts =  window.checkoutConfig.payment.InstallmentAllow[this.item.method];
                var installmentItem = false;

                $.each(window.checkoutConfig.payment.InstallmentDetails[this.item.method],
                function (index, el)
                {
                    if(el.min_amount < quote.totals._latestValue.grand_total && el.min_amount != ''){
                      installmentItem = true;
                    }
                });

                return arrowInts & installmentItem;
            },

            getInstallmentBank: function() {

              var banks = [];
              $.each(window.checkoutConfig.payment.InstallmentBank[this.item.method],
                function (index, el)
              {
                    banks.push(el)
              });
                //console.log(banks);
                return banks;
            },

            getCheckInstallment:  ko.observable(function () {

                //alert($("#kbank"+ '_installmentMethod').is(":checked"));
                //return $("#kbank"+ '_installmentMethod').is(":checked");
                return false;

            }),


            getInstallmentDetails: function() {
              var details = [];
              //console.log(window.checkoutConfig.payment.InstallmentDetails[this.item.method]);

              $.each(window.checkoutConfig.payment.InstallmentDetails[this.item.method],
                function (index, el)
              {
                  if(el.min_amount < quote.totals._latestValue.grand_total && el.min_amount != ''){
                    details.push(el)
                    //alert(index);
                  }
              });
              //console.log(details);
                return details;
            },

            /** Returns send check to info */
            getMailingAddress: function() {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },

            /**
             * Place order.
             */
            placeOrder: function (data, event) {
                var self = this;

                if (event) {
                    event.preventDefault();
                }

                if (this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);

                    this.getPlaceOrderDeferredObject()
                        .fail(
                            function () {
                                self.isPlaceOrderActionAllowed(true);
                            }
                        ).done(
                            function () {
                                self.afterPlaceOrder();

                                if (self.redirectAfterPlaceOrder) {
                                    window.location.replace(url.build('kbank/redirect/'));
                                }
                            }
                        );

                    return true;
                }

                return false;
            }
        });
    }
);
