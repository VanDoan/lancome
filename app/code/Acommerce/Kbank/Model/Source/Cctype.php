<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Model\Source;

use Magento\Payment\Model\Source\Cctype as MagentoCctype;

/**
 * Kbank payment method model
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia *
 */
class Cctype extends MagentoCctype
{


    /**
     * Get Allowed Types
     *
     * @return array
     */
    public function getAllowedTypes()
    {
        return array(
                'VI',
                'MC',
                'AE',
                'DI',
                'JCB',
                'OT',
               );

    }//end getAllowedTypes()


}//end class

?>
