<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Payment\Model\Method\AbstractMethod;

/**
 * Kbank payment method model
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class InstructionsConfigProvider implements ConfigProviderInterface
{

    // @codingStandardsIgnoreStart
    /**
     * Method Codes
     *
     * @var array
     */
    protected $_methodCodes = array(Kbank::PAYMENT_METHOD);
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Methods
     *
     * @var array
     */
    protected $methods = array();

    /**
     * @var Escaper
     */
    protected $escaper;
    //@codingStandardsIgnoreEnd


    /**
     * Construct
     *
     * @param PaymentHelper $paymentHelper Payment Helper
     * @param Escaper       $escaper       Escaper
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        Escaper $escaper
    ) {
        $this->escaper = $escaper;
        foreach ($this->_methodCodes as $code) {
            $this->methods[$code] = $paymentHelper->getMethodInstance($code);
        }

    }//end __construct()


    /**
     * Get Config Data
     *
     * @return string
     */
    public function getConfig()
    {
        $config = array();
        foreach ($this->_methodCodes as $code) {
            if ($this->methods[$code]->isAvailable() === true) {
                $config['payment']['instructions'][$code]
                    = $this->getInstructions($code);
                $config['payment']['InstallmentAllow'][$code]
                    = $this->getAllowInstallment($code);
                $config['payment']['InstallmentDetails'][$code]
                    = $this->getInstallmentDetails($code);
                $config['payment']['InstallmentBank'][$code]
                    = $this->getInstallmentBank($code);
            }
        }

        return $config;

    }//end getConfig()


    /**
     * Get instructions text from config
     *
     * @param string $code Payment Code
     *
     * @return string
     */
    protected function getInstructions($code)
    {
        $instructions = $this->escaper->escapeHtml(
            $this->methods[$code]->getInstructions()
        );
        return nl2br($instructions);

    }//end getInstructions()


    /**
     * Get Allow Installment
     *
     * @param string $code Code
     *
     * @return string
     */
    protected function getAllowInstallment($code)
    {
        $allowInstallment = $this->methods[$code]->getInstallmentEnable();
        return $allowInstallment;

    }//end getAllowInstallment()

    /**
     * Get Installment Detail
     *
     * @param string $code Code
     *
     * @return string
     */
    protected function getInstallmentDetails($code)
    {
        $installments = $this->methods[$code]->getInstallments();
        return $installments;

    }//end getAllowInstallment()

    /**
     * Get Bank Installment
     *
     * @param string $code Code
     *
     * @return string
     */
    protected function getInstallmentBank($code)
    {
        $installments = $this->methods[$code]->getInstallmentBank();
        return $installments;

    }//end getAllowInstallment()


}//end class

?>
