<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Payment\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Framework\UrlInterface;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Payment\Model\Method\AbstractMethod;


/**
 * Kbank payment method model
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Kbank extends AbstractMethod
{

    // @codingStandardsIgnoreStart
    const PAYMENT_METHOD = 'kbank';

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = self::PAYMENT_METHOD;

    /**
     * Url Interface
     *
     * @var UrlInterface
     */
    protected $_urlInterface;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    /**
     * Supported Currency Codes
     *
     * @var array
     */
    protected $_supportedCurrencyCodes = array('THB');

    /**
     * Form Block Type
     *
     * @var string
     */
    protected $_formBlockType = 'Acommerce\Kbank\Block\Form\Kbank';

    /**
     * Info Block Type
     *
     * @var string
     */
    protected $_infoBlockType = 'Acommerce\Kbank\Block\Info\Kbank';

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_isInitializeNeeded = true;
    // @codingStandardsIgnoreEnd

    /**
     * Constructor Class
     *
     * @param Context                    $context                Context
     * @param Registry                   $registry               Registry
     * @param ExtensionAttributesFactory $extensionFactory       Extension Factory
     * @param AttributeValueFactory      $customAttributeFactory Custom Attr. Factory
     * @param Data                       $paymentData            Payment Data
     * @param ScopeConfigInterface       $scopeConfig            Scope Config
     * @param Logger                     $logger                 Logger
     * @param UrlInterface               $urlInterface           Url Interface
     * @param AbstractResource           $resource               Resource
     * @param AbstractDb                 $resourceCollection     Resource Collection
     * @param Array                      $data                   Data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        UrlInterface $urlInterface,
        AbstractResource $resource=null,
        AbstractDb $resourceCollection=null,
        array $data=[]
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );

        $this->_urlInterface = $urlInterface;

    }//end __construct()


    /**
     * Availability for currency
     *
     * @param string $currencyCode Currency Code
     *
     * @return bool
     */
    public function canUseForCurrency($currencyCode)
    {
        if (in_array($currencyCode, $this->_supportedCurrencyCodes) === false) {
            return false;
        }

        return true;

    }//end canUseForCurrency()


    /**
     * Get Order Place Redirec tUrl
     *
     * @return bool
     */
    public function getOrderPlaceRedirectUrl()
    {
        return $this->_urlInterface->getUrl(
            'kbank/redirect',
            array('_secure' => true)
        );

    }//end getOrderPlaceRedirectUrl()


    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));

    }//end getInstructions()

    /**
     * Get Installment Enable
     *
     * @return string
     */
    public function getInstallmentEnable()
    {
        return trim($this->getConfigData('installment_enable'));

    }//end getInstructions()

    /**
     * Get Installments
     *
     * @return string
     */
    public function getInstallments()
    {
        return unserialize(trim($this->getConfigData('installments')));

    }//end getInstructions()

    /**
     * Get Installment Bank
     *
     * @return string
     */
    public function getInstallmentBank()
    {
        return unserialize(trim($this->getConfigData('installment_bank')));

    }//end getInstructions()

    /**
     * Assign Data
     *
     * @param DataObject $data Data
     *
     * @return $this
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);

        $infoInstance = $this->getInfoInstance();
        $additionalData = $data->getData();

        if ($additionalData) {
            if ($additionalData['additional_data']['installment'] == 1) {

                $data->setData(
                    'additional_information',
                    array(
                        'installmentBankId' =>
                        $additionalData['additional_data']['installment_bank_id'],
                        'installmentBankMonth' =>
                            $additionalData['additional_data']['installment_month']
                        )
                );

                $infoInstance->setAdditionalInformation(
                    'installmentBankId',
                    $additionalData['additional_data']['installment_bank_id']
                );
                $infoInstance->setAdditionalInformation(
                    'installmentBankMonth',
                    $additionalData['additional_data']['installment_month']
                );
                $infoInstance->setAdditionalInformation(
                    'installment',
                    $additionalData['additional_data']['installment']
                );
            }
        }

        return $this;
    }


}//end class

?>
