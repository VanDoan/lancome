<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Cron;


use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Acommerce\Kbank\Model\Kbank;
use Acommerce\Kbank\Helper\Data as KbankData;
use Magento\Sales\Model\Order;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Kbank payment method model
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class SyncOrderStatus
{

    /**
    * Scope Config
    *
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
    protected $scopeConfig;

    /**
     * Stores Config
     *
     * @var StoresConfig
     */
    protected $storesConfig;

    /**
     * Sales Order Collection Factory
     *
     * @var CollectionFactory
     */
    protected $salesOrderCollectionFactory;

    /**
     * Time Zone
     *
     * @var Timezone
     */
    protected $timeZone;

    /**
     * Logger
     *
     * @var Logger
     */
    protected $logger;

    /**
     * Constructor
     *
     * @param CollectionFactory    $collectionFactory Collection Factory
     * @param KbankData            $kbankData         Kbank Data
     * @param ScopeConfigInterface $scopeConfig       Scope Config
     * @param TimezoneInterface    $timezone          Time Zone
     * @param Logger               $logger            Logger
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        KbankData $kbankData,
        ScopeConfigInterface $scopeConfig,
        TimezoneInterface $timezone,
        \Acommerce\Kbank\Logger\Logger $logger
    ) {
        $this->logger   = $logger;
        $this->salesOrderCollectionFactory = $collectionFactory;
        $this->storesConfig = $kbankData;
        $this->scopeConfig       = $scopeConfig;
        $this->timeZone        = $timezone;

    }//end __construct()



    /**
     * Clean expired quotes (cron process)
     *
     * @return void
     */
    public function execute()
    {
        echo "SyncOrderStatus.";

        if ($this->getConfig('payment/kbank/inquiry_enable') === '0') {
            echo "Cronjob Disable!!<br />Exit execute()<br />";
            exit();
        }

        $orderCollection = $this->salesOrderCollectionFactory->create();
        $orderCollection->addFieldToFilter(
            'main_table.state',
            array('eq' => Order::STATE_NEW)
        );
        $orderCollection->addFieldToFilter(
            'created_at',
            array('lteq' => new \Zend_Db_Expr('DATE_SUB(NOW(), INTERVAL 30 MINUTE)'))
        );

        $orderCollection->getSelect()->joinInner(
            array(
              'payment' => $orderCollection->getTable('sales_order_payment')
            ),
            'main_table.entity_id = payment.parent_id AND payment.method = \''.
            Kbank::PAYMENT_METHOD.'\'',
            array('payment_method' => 'payment.method')
        );

        $userAgent  = 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) '.
        'Gecko/20030624 Netscape/7.1 (ax)';
        $userName   = $this->storesConfig->getConfig('payment/kbank/inquiry_user');
        $nquiryUrl  = $this->storesConfig->getConfig('payment/kbank/inquiry_url');
        $NmerchantId = $this->storesConfig->getConfig('payment/kbank/merchant_id');
        $ImerchantId = $this->storesConfig
            ->getConfig('payment/kbank/installlment_merchant_id');

        //$orderCollection->load();
        if ($orderCollection->getSize() > 0) {
            foreach ($orderCollection as $order) {

                echo $order->getIncrementId();


                $additionalInfo = $order->getPayment()->getAdditionalInformation();
                if (isset($additionalInfo['installmentBankMonth'])) {
                     $merchantId = $ImerchantId;
                     echo "in<br />";
                } else {
                     $merchantId = $NmerchantId;
                     echo "non<br />";
                }

                $orderId = preg_replace('#\D#', '', $order->getIncrementId());
                //$orderId = preg_replace('#\D#', '', '16111002418');
                $orderId = sprintf('%012s', $orderId);

                $format = \IntlDateFormatter::SHORT;
                $status = 'A';
                $amount = number_format($order->getBaseGrandTotal(), 2, '', '');
                //$amount = '1680000';
                $amount = sprintf('%012s', $amount);

                $createdAt = new \DateTime($order->getCreatedAt());
                //$date = '10112016';

                $date = $this->timeZone->formatDateTime(
                    $createdAt,
                    $format,
                    $format,
                    null,
                    $this->timeZone->getConfigTimezone('store', $order->getStore()),
                    'ddMMyyyy'
                );

                $dataString = sprintf(
                    'USERNAME=%s&TMERCHANTID=%s&TDATE=%s&TINVOICE=%s&'.
                    'TAMOUNT=%s&TSTATUS=%s&cmdPost=POST',
                    $userName,
                    $merchantId,
                    $date,
                    $orderId,
                    $amount,
                    $status
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $nquiryUrl);
                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
                $result = curl_exec($ch);

                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                // echo 'HTTP code: ' . $httpcode."<br />";
                // echo "Data > ".$dataString."<br/>";
                // echo "URL > ".$nquiryUrl."<br/>";
                // echo "User > ".$userAgent."<br />";
                // echo "Result > ".$result.'<br/>';

                echo '<br/>'.$result.'<br/>';

                $cardType = array('001' => 'VISACARDXXXXXXXXXXXX',
                                  '002' => 'MASTERCARDXXXXXXXXXX',
                                  '003' => 'KBANKCARDXXXXXXXXXXX',
                                  '004' => 'JCBCARDXXXXXXXXXXXXX',
                                  '005' => 'CUPCARDXXXXXXXXXXXXX',
                                  '007' => 'AMEXCARDXXXXXXXXXXXX',
                                );

                $responseCode = substr($result, 97, 2);
                $approvalCode = substr($result, 99, 6);
                $cardNo = substr($result, 58, 16);
                $type = substr($result, 105, 3);
                $amount = substr($result, 85, 15);
                $refCode = substr($result, 108, 20);
                $revInvoice = substr($result, 32, 12);
                $currCode = substr($result, 29, 3);

                $errCode = array(
                  '01','05','12','13','14','17','41','43','50','51','54','58'
                  );

                if ($responseCode == '00') {
                    echo "Change to processingg<br />";
                    $this->logger->info(
                        $cardNo.'::'.$orderId.'::'.
                        $date.'::'.$responseCode.'::'.$approvalCode
                    );

                    $cOrder = $order->loadByIncrementId($order->getIncrementId());
                    $cOrder->setState("processing")->setStatus("processing");
                    $cOrder->save();

                    $payment = $order->getPayment();

                    if (empty($cardNo) === false) {
                        $payment->setCcLast4($cardNo);
                        $payment->setCcNumberEnc($cardNo);
                    }

                    $payment->setAmountPaid($this->convertToDecimal($amount));

                    if (isset($cardType[$type]) === true) {
                          $payment->setCcType($cardType[$type]);
                    }

                    if ($responseCode === '00') {
                              $status = 'A';
                    } else {
                            $status = null;
                    }

                    //a:11:{s:12:"method_title";s:22:"Credit Card/Debit Card";s:8:"HOSTRESP";s:2:"00";s:7:"REFCODE";s:12:"XXXXXXXXXXXX";s:8:"AUTHCODE";s:6:"009217";s:9:"RETURNINV";s:12:"001000000292";s:4:"UAID";s:36:"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";s:10:"CARDNUMBER";s:16:"409344XXXXXX9984";s:6:"AMOUNT";s:12:"000000269000";s:6:"CURISO";s:3:"764";s:9:"FILLSPACE";s:20:"VISACARDXXXXXXXXXXXX";s:11:"MD5CHECKSUM";s:32:"bc804febe52ecb5eea8c6b045933bd8a";}
                    $responses = array('HOSTRESP' => $responseCode,
                                    'REFCODE' => $refCode,
                                    'AUTHCODE' => $approvalCode,
                                    'RETURNINV' => $revInvoice,
                                    'UAID' => 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
                                    'CARDNUMBER' => $cardNo,
                                    'AMOUNT' => $amount,
                                    'CURISO' => $currCode,
                                    'FILLSPACE' => $cardType[$type]
                                  );

                    $additionalPayment = $payment->getAdditionalInformation();
                    $additionalPayment = array_merge($additionalPayment, $responses);
                    $payment->setAdditionalInformation($additionalPayment);
                    $payment->setCcStatus($status);
                    $payment->save();
                }

            }//end foreach
        }//end if

    }//end execute()

    /**
     * Get config value
     *
     * @param string $configPath Config Path
     *
     * @return string
     */
    public function getConfig($configPath)
    {
        return $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

    }//end getConfig()

    /**
     * Convert String To Decimal
     *
     * @param string $value String Value
     *
     * @return decimal
     */
    protected function convertToDecimal($value)
    {
        $number = (float) $value;
        return ($number / 100);

    }//end convertToDecimal()

}//end class

?>
