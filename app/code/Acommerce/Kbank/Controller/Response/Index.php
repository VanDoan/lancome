<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\Kbank\Controller\Response;

use Magento\Framework\View\Result\PageFactory;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Request\Http;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Framework\App\Action\Context;
use Acommerce\Kbank\Helper\Data;

/**
 * Kbank payment controller
 *
 * @category Acommerce_Kbank
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Index extends Action
{

    /**
     * Url Interface
     *
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * Response Factory
     *
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * Request
     *
     * @var Http
     */
    protected $request;

    /**
     * Invoice Service
     *
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * Invoice Sender
     *
     * @var InvoiceSender
     */
    protected $orderSender;

    /**
     * Helper Data
     *
     * @var Helper
     */
    protected $helperData;

    /**
     * Check Redirect
     *
     * @var bool
     */
    protected $isRedirect = true;

    /**
     * Constructor
     *
     * @param Context         $context           Context
     * @param PageFactory     $resultPageFactory Result Page Factory
     * @param Http            $request           Request
     * @param ResponseFactory $responseFactory   Response Factory
     * @param InvoiceService  $invoiceService    Invoice Service
     * @param OrderSender     $orderSender       Order Sender
     * @param Data            $helperData        Helper Data
     *
     * @return void
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Http $request,
        ResponseFactory $responseFactory,
        InvoiceService $invoiceService,
        OrderSender $orderSender,
        Data $helperData
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->request          = $request;
        $this->urlInterface     = $context->getUrl();
        $this->responseFactory  = $responseFactory;
        $this->invoiceService   = $invoiceService;
        $this->orderSender      = $orderSender;
        $this->helperData       = $helperData;
        parent::__construct($context);

    }//end __construct()


    /**
     * Execute
     *
     * @return void
     */
    public function execute()
    {
        $responses = $this->request->getParams();

        if (isset($responses['RETURNINV']) === false) {
            $this->setRedirect('home');
            exit();
        }

        $orderId = substr($responses['RETURNINV'], -10);
        $order   = $this->_objectManager->create(
            'Magento\Sales\Model\Order'
        )->loadByIncrementId($orderId);

        if (empty($order->getId()) === false) {
            $payment = $order->getPayment();

            $additionalPayment = $payment->getAdditionalInformation();
            $additionalPayment = array_merge($additionalPayment, $responses);

            if (isset($responses['CARDNUMBER']) === true) {
                $payment->setCcLast4($responses['CARDNUMBER']);
                $payment->setCcNumberEnc($responses['CARDNUMBER']);
            }

            if (isset($responses['AMOUNT']) === true) {
                $payment->setAmountPaid(
                    $this->convertToDecimal($responses['AMOUNT'])
                );
            }

            if (isset($responses['FILLSPACE']) === true) {
                $payment->setCcType($this->getCcType($responses['FILLSPACE']));
            }

            if (isset($responses['HOSTRESP']) !== false) {
                if ($responses['HOSTRESP'] === '00') {
                    $status = 'A';
                } else {
                    $status = null;
                }

                $payment->setCcStatus($status);
            }

            $payment->setAdditionalInformation($additionalPayment);
            $payment->save();

            if (isset($responses['HOSTRESP']) === true) {
                $response = $responses['HOSTRESP'];
                if ($response === '00') {
                    $md5Checksum = array_pop($responses);
                    $checkSum    = $this->helperData->checkSum($responses);
                    if ($md5Checksum === $checkSum) {
                        if ($order->canInvoice() === true) {
                            $invoice = $this->invoiceService
                                ->prepareInvoice($order);
                            $invoice->register();
                            $invoice->save();
                            $transactionSave = $this->_objectManager->create(
                                'Magento\Framework\DB\Transaction'
                            )->addObject(
                                $invoice
                            )->addObject(
                                $invoice->getOrder()
                            );
                            $transactionSave->save();

                            $order->addStatusHistoryComment(
                                __(
                                    'Notified customer about invoice #%1.',
                                    $invoice->getIncrementId()
                                )
                            )->save();
                        }

                        if ($order->getCanSendNewEmailFlag() === true) {
                            try {
                                $this->orderSender->send($order);
                            } catch (\Exception $e) {
                                $this->_logger->critical($e);
                            }
                        }

                        $orderState = Order::STATE_PROCESSING;
                        $order->setState($orderState)
                            ->setStatus($orderState)
                            ->save();
                    }//end if
                } else {
                    $this->setRedirect('failure');
                    exit();
                }//end if
            }//end if

            $this->setRedirect('success');
            exit();
        } else {
            $this->setRedirect('failure');
            exit();
        }//end if

    }//end execute()


    /**
     * Convert String To Decimal
     *
     * @param string $value String Value
     *
     * @return decimal
     */
    protected function convertToDecimal($value)
    {
        $number = (float) $value;
        return ($number / 100);

    }//end convertToDecimal()


    /**
     * Get CC Type
     *
     * @param string $type CC Type
     *
     * @return string
     */
    protected function getCcType($type)
    {
        if (substr($type, 0, 2) === 'VI') {
            return 'VI';
        }

        return 'MC';

    }//end getCcType()


    /**
     * Set redirect
     *
     * @param string $type Type of redirect
     *
     * @return void
     */
    protected function setRedirect($type='success')
    {
        if ($this->isRedirect === true) {
            if ($type === 'success') {
                $redirectUrl = $this->urlInterface
                    ->getUrl('checkout/onepage/success');
            } else if ($type === 'home') {
                $redirectUrl = $this->urlInterface->getUrl('/');
            } else {
                $redirectUrl = $this->urlInterface
                    ->getUrl('checkout/onepage/failure');
            }

            $this->responseFactory->create()->setRedirect(
                $redirectUrl
            )->sendResponse();
        }

    }//end setRedirect()


}//end class

?>