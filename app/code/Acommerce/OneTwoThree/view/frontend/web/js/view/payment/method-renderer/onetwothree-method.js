/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'underscore',
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url',
        'Magento_Ui/js/modal/confirm',
        'mage/translate'
    ],
    function (
            _,
            $,
            Component,
            additionalValidators,
            url,
            confirm,
            $t
        ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Acommerce_OneTwoThree/payment/onetwothree',
                additionalData: {},
            },

            /** Returns send check to info */
            getMailingAddress: function() {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },

            /**
             * @returns {Object}
             */
            getData: function () {
                var data = this._super();

                var agent = $('input[name=agent_one23cash]:checked');
                if(agent.length > 0) {
                    this.additionalData['agent_one23cash'] = agent.val();
                }

                data['additional_data'] = this.additionalData;
                return data;
            },

            /**
             * @return {Boolean}
             */
            validate: function () {
                var agent = $('input[name=agent_one23cash]:checked');
                if(agent.length == 0) {
                    return false;
                }
                return true;
            },

            /**
             * Place order.
             */
            placeOrder: function (data, event) {
                var self = this;

                if (event) {
                    event.preventDefault();
                }

                if(!this.validate()) {
                    confirm({
                        title: $t('Payment Error'),
                        content: $t('Please specify payment option'),
                        actions: {
                            confirm: function(){},
                            cancel: function(){},
                            always: function(){}
                        },buttons: [{
                            text: $t('OK'),
                            class: 'action-primary action-accept',

                            /**
                             * Click handler.
                             */
                            click: function (event) {
                                this.closeModal(event, true);
                            }
                        }]
                    });
                }

                if (this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);

                    this.getPlaceOrderDeferredObject()
                        .fail(
                            function () {
                                self.isPlaceOrderActionAllowed(true);
                            }
                        ).done(
                            function () {
                                self.afterPlaceOrder();

                                if (self.redirectAfterPlaceOrder) {
                                    window.location.replace(url.build('onetwothree/redirect/'));
                                }
                            }
                        );

                    return true;
                }

                return false;
            },

        });
    }
);
