<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\OneTwoThree\Controller\Response;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Request\Http;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Framework\App\Action\Context;
use Acommerce\OneTwoThree\Helper\Data;

/**
 * OneTwoThree payment controller
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Index extends Action
{

    /**
     * Result Page Factory
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Url Interface
     *
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * Request
     *
     * @var Http
     */
    protected $request;

    /**
     * Order Factory
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Invoice Sender
     *
     * @var InvoiceSender
     */
    protected $orderSender;

    /**
     * Helper Data
     *
     * @var Helper
     */
    protected $helperData;

    /**
     * Check Redirect
     *
     * @var bool
     */
    protected $isRedirect = true;

    protected $orderId;

    protected $type = 'MerchantUrl';

    protected $responseData = array('Version' => '1.1');

    /**
     * Constructor
     *
     * @param Context      $context           Context
     * @param PageFactory  $resultPageFactory Result Page Factory
     * @param Http         $request           Request
     * @param OrderFactory $orderFactory      Order Factory
     * @param OrderSender  $orderSender       Order Sender
     * @param Data         $helperData        Helper Data
     *
     * @return void
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Http $request,
        OrderFactory $orderFactory,
        OrderSender $orderSender,
        Data $helperData
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->request          = $request;
        $this->urlInterface     = $context->getUrl();
        $this->orderFactory     = $orderFactory;
        $this->orderSender      = $orderSender;
        $this->helperData       = $helperData;
        parent::__construct($context);

    }//end __construct()


    /**
     * Execute
     *
     * @return void
     */
    public function execute()
    {
        $responses = $this->request->getParams();

        if (isset($responses['OneTwoThreeRes'])) {
            try {

                $decryptData = $this->helperData->decryptData(
                    $responses['OneTwoThreeRes'],
                    $this->type
                );

                $this->setOrderId($decryptData['InvoiceNo']);
                $order = $this->getOrderObject();
                $hash = $this->helperData->hashData(
                    $this->helperData->getMerchantId().
                    $decryptData['InvoiceNo'].
                    $decryptData['RefNo1']
                );

                $this->responseData['TimeStamp'] = $this->helperData
                    ->getTimestamp();

                $this->responseData['MessageID'] = str_replace(
                    array('-', ':', ' '),
                    '',
                    $this->responseData['TimeStamp']
                );

                $this->_responseData['MerchantID'] = $this->helperData
                    ->getMerchantId();

                if ($order->getId()) {
                    if ($hash != $decryptData['HashValue']) {
                        $this->responseData['FailureReason'] = 'Invalid Hash Value';
                        $this->responseData['ResponseCode'] = '01';
                        $this->setRedirect('failure');
                    } else if ($decryptData['ResponseCode'] == '000'
                        || $decryptData['ResponseCode'] == '001'
                    ) {

                        $payment = $order->getPayment();

                        $responses = array(
                                    'RefNo1' => $decryptData['RefNo1'],
                                    'ResponseCode' => $decryptData['ResponseCode'],
                                    'AgentCode' => $decryptData['AgentCode'],
                                    'ChannelCode' => $decryptData['ChannelCode'],
                                    'SlipUrl' => $decryptData['SlipUrl'],
                                );

                        $additionalPayment = $payment->getAdditionalInformation();
                        $additionalPayment = array_merge(
                            $additionalPayment,
                            $responses
                        );

                        $payment->setAdditionalInformation($additionalPayment);
                        $payment->save();

                        if ($order->getCanSendNewEmailFlag() === true) {
                            try {
                                $this->orderSender->send($order);
                            } catch (\Exception $e) {
                                $this->_logger->critical($e);
                            }
                        }

                        if (($decryptData['ResponseCode'] == '000'
                            || $decryptData['ResponseCode'] == '015')
                            && $this->type == 'APICall'
                        ) {
                            $orderState = Order::STATE_PROCESSING;
                            $order->setState($orderState)
                                ->setStatus($orderState)
                                ->save();
                        }
                    }
                    $this->responseData['FailureReason'] = 'SUCCESS';
                    $this->responseData['ResponseCode'] = '00';
                    $this->setRedirect('success');
                } else {
                    $this->responseData['FailureReason'] = 'Invalid Invoice No';
                    $this->responseData['ResponseCode'] = '01';
                    $this->setRedirect('failure');
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->setRedirect('failure');
            }

        } else {
            $this->responseData['FailureReason'] = 'Invalid Response';
            $this->responseData['ResponseCode'] = '01';
            $this->setRedirect('failure');
        }
    }

    /**
     * Set redirect
     *
     * @param string $type Type of redirect
     *
     * @return void
     */
    protected function setRedirect($type='success')
    {
        if ($this->isRedirect === true) {
            if ($type === 'success') {
                $redirectUrl = $this->urlInterface
                    ->getUrl('checkout/onepage/success');
            } else if ($type === 'home') {
                $redirectUrl = $this->urlInterface->getUrl('/');
            } else {
                $redirectUrl = $this->urlInterface
                    ->getUrl('checkout/onepage/failure');
            }

            $this->getResponse()->setRedirect($redirectUrl);
        }

    }//end setRedirect()

    /**
     * Set order id
     *
     * @param int $orderId Order Id
     *
     * @return void
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

    }//end setOrderId()


    /**
     * Get order id
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;

    }//end getOrderId()


    /**
     * Get order object
     *
     * @return object
     */
    public function getOrderObject()
    {
        $orderObj = '';
        if (empty($this->getOrderId()) === false) {
            $orderObj = $this->orderFactory->create();
            $orderObj->loadByIncrementId($this->getOrderId());
        }

        return $orderObj;

    }//end getOrderObject()

}//end class

?>