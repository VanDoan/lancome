<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\OneTwoThree\Controller\Redirect;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\UrlInterface;

/**
 * OneTwoThree payment controller
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

class Index extends Action
{

    /**
     * Result Page Factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Scope Config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Checkout Session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * Url Interface
     *
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * Response Factory
     *
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * Constructor
     *
     * @param Context              $context           Context
     * @param PageFactory          $resultPageFactory ResultPageFactory
     * @param ScopeConfigInterface $scopeConfig       ScopeConfig
     * @param Session              $checkoutSession   CheckoutSession
     * @param ResponseFactory      $responseFactory   ResponseFactory
     *
     * @return void
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        ResponseFactory $responseFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->scopeConfig       = $scopeConfig;
        $this->checkoutSession   = $checkoutSession;
        $this->urlInterface      = $context->getUrl();
        $this->responseFactory   = $responseFactory;
        parent::__construct($context);

    }//end __construct()


    /**
     * Execute
     *
     * @return void
     */
    public function execute()
    {
        $isActive    = $this->getConfig('payment/onetwothree/active');
        $lastOrderId = $this->checkoutSession->getLastOrderId();
        if (empty($lastOrderId) === false && $isActive === '1') {
            $orderId = $this->checkoutSession->getLastOrderId();
            if (empty($orderId) === false) {
                $this->checkoutSession->setOneTwoThreeLastOrderId($orderId);
            }
        } else {
            $denyAccessUrl = $this->urlInterface->getUrl('/');
            $this->responseFactory->create()->setRedirect(
                $denyAccessUrl
            )->sendResponse();
            exit();
        }

        //$block = $this->_view->getLayout()->getBlock('onetwothree_redirect');

        $resultPage = $this->resultPageFactory->create();
        $block = $resultPage->getLayout()->getBlock('onetwothree_redirect');

        $this->getResponse()->setHeader('Content-type', 'text/html');
        $this->getResponse()->setBody($block->toHtml());
        //return $resultPage;

    }//end execute()


    /**
     * Get config value
     *
     * @param string $configPath Config Path
     *
     * @return string
     */
    public function getConfig($configPath)
    {
        return $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

    }//end getConfig()


}//end class

?>
