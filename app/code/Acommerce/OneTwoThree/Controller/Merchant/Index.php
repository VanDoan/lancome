<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\OneTwoThree\Controller\Merchant;

use Acommerce\OneTwoThree\Controller\Response\Index as ResponseIndex;

/**
 * OneTwoThree payment controller
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Index extends ResponseIndex
{

    /**
     * Check Redirect
     *
     * @var bool
     */

}//end class

?>
