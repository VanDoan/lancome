<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\OneTwoThree\Cron;

use Magento\Store\Model\StoresConfig;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Acommerce\OneTwoThree\Model\OneTwoThree;
use Magento\Sales\Model\Order;

/**
 * OneTwoThree payment method model
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class SyncOrderStatus
{

    /**
     * StoresConfig
     *
     * @var StoresConfig
     */
    protected $storesConfig;

    /**
     * Collection Factory
     *
     * @var CollectionFactory
     */
    protected $salesOrderCollectionFactory;

    /**
     * Constructor
     *
     * @param StoresConfig      $storesConfig      Stores Config
     * @param CollectionFactory $collectionFactory Collection Factory
     */
    public function __construct(
        StoresConfig $storesConfig,
        CollectionFactory $collectionFactory
    ) {
        $this->storesConfig = $storesConfig;
        $this->salesOrderCollectionFactory = $collectionFactory;

    }//end __construct()


    /**
     * Get SalesOrder Collection Factory
     *
     * @return CollectionFactory
     */
    public function getSalesOrderCollectionFactory()
    {
        return $this->salesOrderCollectionFactory;

    }//end getSalesOrderCollectionFactory()


    /**
     * Clean expired quotes (cron process)
     *
     * @return void
     */
    public function execute()
    {

    }//end execute()


}//end class

?>
