<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\OneTwoThree\Block\Info;

use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Model\Config;
use Magento\Payment\Block\Info as BlockInfo;

/**
 * OneTwoThree payment Block Info
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class OneTwoThree extends BlockInfo
{
    // @codingStandardsIgnoreStart
    /**
     * Path Of Template
     *
     * @var string
     */
    protected $_template = 'Acommerce_OneTwoThree::info/onetwothree.phtml';
    // @codingStandardsIgnoreEnd

    /**
     * Payment config model
     *
     * @var \Magento\Payment\Model\Config
     */
    protected $paymentConfig;

    protected $statuses = array('000' => 'SUCCESS (PAID)',
                    '001' => 'SUCCESS (PENDING)',
                    '002' => 'TIMEOUT',
                    '003' => 'INVALID MESSAGE',
                    '004' => 'INVALID PROFILE(MERCHANT) ID',
                    '005' => 'DUPLICATED INVOICE',
                    '006' => 'INVALID AMOUNT',
                    '007' => 'INSUFFICIENT BALANCE',
                    '008' => 'INVALID CURRENCY CODE',
                    '009' => 'PAYMENT EXPIRED',
                    '010' => 'PAYMENT CANCELED BY PAYER',
                    '011' => 'INVALID PAYEE ID',
                    '012' => 'INVALID CUSTOMER ID',
                    '013' => 'ACCOUNT DOES NOT EXIST',
                    '014' => 'AUTHENTICATION FAILED',
                    '015' => 'SUCCESS (PAID MORE MISMATCHED)',
                    '016' => 'SUCCESS (PAID LESS MISMATCHED)',
                    '017' => 'SUCCESS (PAID EXPIRED)',
                    '998' => 'INTERNAL ERROR',
                    '999' => 'SYSTEM ERROR',
                        );

    /**
     * Constructor
     *
     * @param Context $context       Context
     * @param Config  $paymentConfig Payment Config
     * @param array   $data          data
     */
    public function __construct(
        Context $context,
        Config $paymentConfig,
        array $data=[]
    ) {
        parent::__construct($context, $data);
        $this->paymentConfig = $paymentConfig;

    }//end __construct()


    /**
     * Get Cc Type Name
     *
     * @return string
     */
    public function getCcTypeName()
    {
        $types  = $this->paymentConfig->getCcTypes();
        $ccType = $this->getInfo()->getCcType();

        if (isset($types[$ccType]) === true) {
            return $types[$ccType];
        }

        if (empty($ccType) === true) {
            return __('N/A');
        }

        return __($ccType);

    }//end getCcTypeName()


    /**
     * Prepare credit card related payment info
     *
     * @param DataObject $transport transport
     *
     * @return DataObject
     */

    // @codingStandardsIgnoreStart
    protected function _prepareSpecificInformation($transport=null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }

        $transport = parent::_prepareSpecificInformation($transport);
        $data      = array();

        $additionalInfo = $this->getInfo()->getAdditionalInformation();
        if ($additionalInfo) {
            if (isset($additionalInfo['ChannelCode']) === true) {
                $channelCode = $additionalInfo['ChannelCode'];
                if (empty($ccType) === true) {
                    $ccType = 'N/A';
                }

                $data[(string) __('Channel Code')] = $channelCode;
            } else {
                $data[(string) __('Channel Code')] = 'N/A';
            }

            if (isset($additionalInfo['AgentCode']) === true) {
                $agentCode = $additionalInfo['AgentCode'];
                $data[(string) __('Agent Code')] = $agentCode;
            } else {
                $data[(string) __('Agent Code')] = 'N/A';
            }

            if (isset($additionalInfo['RefNo1']) === true) {
                $refNo1 = $additionalInfo['RefNo1'];
                $data[(string) __('Payment Ref. Code')] = $refNo1;
            } else {
                $data[(string) __('Payment Ref. Code')] = 'N/A';
            }

            if (isset($additionalInfo['ResponseCode']) === true) {
                $responseCode = $additionalInfo['ResponseCode'];
                if(isset($this->statuses[$responseCode])) {
                    $data[(string) __('Status')] = $this->statuses[$responseCode];
                } else {
                    $data[(string) __('Status')] = 'N/A';
                }
            } else {
                $data[(string) __('Status')] = 'N/A';
            }

            if (isset($additionalInfo['SlipUrl']) === true) {
                $slipUrl = $additionalInfo['SlipUrl'];
                $data[(string) __('Slip Url')] = $slipUrl;;
            } else {
                $data[(string) __('Slip Url')] = 'N/A';
            }
        }//end if

        return $transport->setData(array_merge($data, $transport->getData()));

    }//end prepareSpecificInformation()
    // @codingStandardsIgnoreEnd

    /**
     * Convert to pdf
     *
     * @return string
     */
    public function toPdf()
    {
        $this->setTemplate('Acommerce_OneTwoThree::info/pdf/onetwothree.phtml');
        return $this->toHtml();

    }//end toPdf()


}//end class

?>
