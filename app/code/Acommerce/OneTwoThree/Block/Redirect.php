<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\OneTwoThree\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\ResponseFactory;
use Magento\Checkout\Model\Session;
use Acommerce\OneTwoThree\Helper\Data;

/**
 * OneTwoThree redirect template
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Redirect extends Template
{

    /**
     * Grandtotal number of digits
     *
     * @var number
     */
    const ALLOW_AMOUNT_NUMBER = 12;

    /**
     * Order Factory
     *
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * Response Factory
     *
     * @var \Magento\Framework\App\ResponseFactory
     */
    protected $responseFactory;

    /**
     * Checkout Session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * Order id
     *
     * @var string
     */
    protected $orderId;

    /**
     * OneTwoThree Helper
     *
     * @var \Acommerce\OneTwoThree\Helper
     */
    protected $helperData;

    /**
     * Constructor
     *
     * @param Context         $context         Context
     * @param OrderFactory    $orderFactory    Order Factory
     * @param ResponseFactory $responseFactory Response Factory
     * @param Session         $checkoutSession Checkout Session
     * @param Data            $helperData      Data Helper
     * @param Array           $data            Data
     *
     * return void
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        ResponseFactory $responseFactory,
        Session $checkoutSession,
        Data $helperData,
        array $data=[]
    ) {
        $this->orderFactory    = $orderFactory;
        $this->responseFactory = $responseFactory;
        $this->checkoutSession = $checkoutSession;
        $this->helperData      = $helperData;

        if (empty($this->checkoutSession->getOneTwoThreeLastOrderId() === false)) {
            $this->setOrderId($this->checkoutSession->getOneTwoThreeLastOrderId());
            $this->checkoutSession->unsOneTwoThreeLastOrderId();
        }

        parent::__construct($context, $data);

    }//end __construct()


    /**
     * Set order id
     *
     * @param int $orderId Order Id
     *
     * @return void
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

    }//end setOrderId()


    /**
     * Get order id
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;

    }//end getOrderId()


    /**
     * Get order object
     *
     * @return object
     */
    public function getOrderObject()
    {
        $orderObj = '';
        if (empty($this->getOrderId()) === false) {
            $orderObj = $this->orderFactory->create();
            $orderObj->load($this->getOrderId());
        }

        return $orderObj;

    }//end getOrderObject()


    /**
     * Get order id
     *
     * @return string
     */
    public function getIncrementId()
    {
        $incrementId = '';
        $orderObj    = $this->getOrderObject();
        if (empty($orderObj) === false) {
            $incrementId = $orderObj->getIncrementId();
        }

        return $incrementId;

    }//end getIncrementId()


    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getIncrementId();

    }//end getDescription()


    /**
     * Convert to leading zero format
     *
     * @param decimal $amount          Amount
     * @param int     $numberOfDitgits Number Of Ditgits
     *
     * @return string
     */
    public function convertToLeadingZeroTotalFormat(
        $amount,
        $numberOfDitgits=self::ALLOW_AMOUNT_NUMBER
    ) {
        return str_pad(($amount * 100), $numberOfDitgits, '0', STR_PAD_LEFT);

    }//end convertToLeadingZeroTotalFormat()


    /**
     * Get billing address object
     *
     * @return object
     */
    public function getBillingAddressObject()
    {
        $billingAddressObj = '';
        $orderObj = $this->getOrderObject();
        if (empty($orderObj) === false) {
            $billingAddressObj = $orderObj->getBillingAddress();
        }

        return $billingAddressObj;

    }//end getBillingAddressObject()


    /**
     * Get shipping address object
     *
     * @return object
     */
    public function getShippingAddressObject()
    {
        $shippingAddressObj = '';
        $orderObj           = $this->getOrderObject();
        if (empty($orderObj) === false) {
            $shippingAddressObj = $orderObj->getShippingAddress();
        }

        return $shippingAddressObj;

    }//end getShippingAddressObject()


    /**
     * Get customer email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        $customerEmail     = '';
        $billingAddressObj = $this->getBillingAddressObject();
        if (empty($billingAddressObj) === false) {
            $customerEmail = $billingAddressObj->getEmail();
        }

        return $customerEmail;

    }//end getCustomerEmail()


    /**
     * Get customer firstname
     *
     * @return string
     */
    public function getCustomerFirstname()
    {
        $customerFirstname     = '';
        $billingAddressObj = $this->getBillingAddressObject();
        if (empty($billingAddressObj) === false) {
            $customerFirstname = $billingAddressObj->getFirstname();
        }

        return $customerFirstname;

    }//end getCustomerFirstname()


    /**
     * Get customer lastname
     *
     * @return string
     */
    public function getCustomerLastname()
    {
        $customerLastname     = '';
        $billingAddressObj = $this->getBillingAddressObject();
        if (empty($billingAddressObj) === false) {
            $customerLastname = $billingAddressObj->getLastname();
        }

        return $customerLastname;

    }//end getCustomerLastname()

    /**
     * Get Form Action Url
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->helperData->getFormActionUrl();

    }//end getFormActionUrl()

    /**
     * Get Request Data
     *
     * @return String
     **/
    public function getRequestData()
    {
        $order = $this->getOrderObject();
        $detail = '';
        $items = array();
        $i = 1;
        foreach ($order->getItemsCollection() as $item) {
            if ($item->isDeleted() || $item->getParentItemId()) {
                continue;
            }
            $itemName = $item->getSku();
            $price = $this->convertToLeadingZeroTotalFormat(
                $item->getBaseRowTotalInclTax()
            );
            $items[] = array('PaymentItem'=>array(
                'id' => $i,
                'name' => $itemName,
                'price' => $price,
                'quantity' => (int) $item->getQtyOrdered())
            );
            $i++;
        }

        $amount = $this->convertToLeadingZeroTotalFormat(
            (int)$order->getBaseGrandTotal()
        );

        $name = $this->getCustomerFirstname().' '.$this->getCustomerLastname();
        $email = $this->getCustomerEmail();

        $payment = $order->getPayment();
        $channel = $payment->getAdditionalInformation('ChannelCode');
        $agent = $payment->getAdditionalInformation('AgentCode');

        $invNo = preg_replace('#\D#', '', $order->getIncrementId());
        $hash = $this->helperData->hashData(
            $this->helperData->getMerchantId().$invNo.$amount
        );

        $reqData = array(
            'Version'                   => $this->helperData->getVersion(),
            'TimeStamp'                 => $this->helperData->getTimestamp(),
            'MessageID'                 => $order->getIncrementId(),
            'MerchantID'                => $this->helperData->getMerchantId(),
            'InvoiceNo'                 => $invNo,
            'Amount'                    => $amount,
            'Discount'                  => $this->convertToLeadingZeroTotalFormat(0),
            'ServiceFee'                => $this->convertToLeadingZeroTotalFormat(0),
            'ShippingFee'               => $this->convertToLeadingZeroTotalFormat(0),
            'CurrencyCode'              => $this->helperData->getCurrencyCode(),
            'CountryCode'               => $this->helperData->getCountryCode(),
            'ProductDesc'               => substr($this->getDescription(), 0, 255),
            'PaymentItems'              => $items,
            'PayerName'                 => $name,
            'PayerEmail'                => $email,
            'ShippingAddress'           => '',
            'MerchantUrl'               => $this->helperData->getMerchantUrl(),
            'APICallUrl'                => $this->helperData->getApiCallUrl(),
            'AgentCode'                 => $agent,
            'ChannelCode'               => $channel,
            'PayInSlipInfo'             => $this->helperData->getSlipInfo(),
            'userDefined1'              => $this->helperData->getUserDefined(1),
            'userDefined2'              => $this->helperData->getUserDefined(2),
            'userDefined3'              => $this->helperData->getUserDefined(3),
            'userDefined4'              => $this->helperData->getUserDefined(4),
            'userDefined5'              => $this->helperData->getUserDefined(5),
            'HashValue'                 => $hash,

        );

        $xml = $this->_helperData->getSimpleXml($reqData);
        $files = $this->_helperData->writeRequest(
            $xml,
            array('Request','Xml','Encrypt'),
            $this->getOrderObject()
        );

        return $this->_helperData->encryptData(
            $files['source'],
            $files['output'],
            true
        );
    }

}//end class

?>
