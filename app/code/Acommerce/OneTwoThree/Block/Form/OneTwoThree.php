<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\OneTwoThree\Block\Form;

use Magento\Payment\Block\Form;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Model\Config;

/**
 * OneTwoThree payment block form
 *
 * @category Acommerce_OneTwoThree
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

class OneTwoThree extends Form
{
    // @codingStandardsIgnoreStart
    /**
     * Instructions
     *
     * @var string
     */
    protected $instructions;

    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'Acommerce_OneTwoThree::form.phtml';

    /**
     * Payment config model
     *
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentConfig;
    // @codingStandardsIgnoreEnd

    /**
     * Construct
     *
     * @param Context $context       Context
     * @param Config  $paymentConfig Payment Config
     * @param array   $data          Data
     */
    public function __construct(
        Context $context,
        Config $paymentConfig,
        array $data=[]
    ) {
        parent::__construct($context, $data);
        $this->_paymentConfig = $paymentConfig;

    }//end __construct()


    /**
     * Get instructions text from config
     *
     * @return null|string
     */
    public function getInstructions()
    {
        if ($this->instructions === null) {
            $method = $this->getMethod();
            $this->instructions = $method->getConfigData('instructions');
        }

        return $this->instructions;

    }//end getInstructions()


}//end class

?>
