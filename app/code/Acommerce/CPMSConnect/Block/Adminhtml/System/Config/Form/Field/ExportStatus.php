<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */

namespace Acommerce\CPMSConnect\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
/**
 * Rander Mapping Between Payment and Order Status
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */
class ExportStatus extends AbstractFieldArray
{
    // @codingStandardsIgnoreStart
    /**
     * Grid columns
     *
     * @var array
     */
    protected $_columns = array();

    /**
     * Payment Method Renderer;
     *
     * @var object
     */
    private $_paymentMethodRenderer;

    /**
     * Order Status Renderer;
     *
     * @var object
     */
    private $_orderStatusRenderer;

    /**
     * Enable the "Add after" button or not
     *
     * @var bool
     */
    protected $_addAfter = true;

    /**
     * Label of add button
     *
     * @var string
     */
    protected $_addButtonLabel;

    /**
     * Template
     *
     * @var string
     */
    protected $_template 
        = 'Acommerce_CPMSConnect::system/config/form/field/array.phtml';
        
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Check if columns are defined, set template
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_addButtonLabel = __('Add');

    }//end _construct()
    // @codingStandardsIgnoreEnd


    // @codingStandardsIgnoreStart
    /**
     * Add a column to array-grid
     *
     * @param string $name   Column Name
     * @param array  $params Params
     *
     * @return void
     */
    public function addColumn($name, $params)
    {
        $this->_columns[$name] = [
            'label' => $this->_getParam($params, 'label', 'Column'),
            'size' => $this->_getParam($params, 'size', false),
            'style' => $this->_getParam($params, 'style'),
            'class' => $this->_getParam($params, 'class'),
            'renderer' => false,
        ];
        if (!empty($params['renderer']) 
            && $params['renderer'] instanceof \Magento\Framework\View\Element\AbstractBlock
        ) {
            $this->_columns[$name]['renderer'] = $params['renderer'];
        }
    }
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Returns renderer for country element
     *
     * @return \Magento\Braintree\Block\Adminhtml\Form\Field\Countries
     */
    protected function getPaymentRenderer()
    {
        if (!$this->_paymentMethodRenderer) {
            $this->_paymentMethodRenderer = $this->getLayout()->createBlock(
                'Acommerce\CPMSConnect\Block\Adminhtml\System\Config\Form\Field\Render\Payments', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_paymentMethodRenderer;
    }
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Returns renderer for country element
     *
     * @return \Magento\Braintree\Block\Adminhtml\Form\Field\Countries
     */
    protected function getOrderStatusRenderer()
    {
        if (!$this->_orderStatusRenderer) {
            $this->_orderStatusRenderer = $this->getLayout()->createBlock(
                'Acommerce\CPMSConnect\Block\Adminhtml\System\Config\Form\Field\Render\Status',
                '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_orderStatusRenderer;
    }
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Prepare Array Row
     * 
     * @param DataObject $row Data Row
     *
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $paymentMethod = $row->getPaymentMethod();
        $options = [];
        if ($paymentMethod) {
            $options['option_' . $this->getPaymentRenderer()
                ->calcOptionHash($paymentMethod)] = 'selected="selected"';
        }

        $orderStatus = $row->getOrderStatus();
        if ($orderStatus) {
            $options['option_' . $this->getOrderStatusRenderer()
                ->calcOptionHash($orderStatus)] = 'selected="selected"';
        }

        $row->setData('option_extra_attrs', $options);
    }
    // @codingStandardsIgnoreStart

    // @codingStandardsIgnoreStart
    /**
    * Prepare To Render
    *
    * @return void
    */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'payment_method', [
                'label' => __('Payment Method'),
                'renderer' => $this->getPaymentRenderer(),
                ]
        );

        $this->addColumn(
            'order_status', [
                'label' => __('Order Status'),
                'renderer' => $this->getOrderStatusRenderer(),
            ]
        );

        $this->_addAfter       = false;
        $this->_addButtonLabel = __('Add');

    }//end _prepareToRender()
    // @codingStandardsIgnoreEnd
    

}//end class

?>