<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category CPMSConnect_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\CPMSConnect\Cron;

use Magento\Sales\Model\ResourceModel\Order\CollectionFactory
    as OrderCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\ResourceModel\Website\CollectionFactory
    as WebsiteCollectionFactory;
use Magento\Framework\Stdlib\DateTime as StdlibDateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface as DateTimeTimezoneInterface;
use Magento\Directory\Model\CountryFactory;
use Acommerce\CPMSConnect\Logger\Logger;
use Acommerce\CPMSConnect\Model\OrderExportFactory;


/**
 * Export Sales Order To Cpms
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class OrderExporting
{

    /**
     *  Date Time
     *
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $dateTime;

    /**
     *  Local Date Time
     *
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     *  Sales Order Collection
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     *  App Config Scope Config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $appConfigScopeConfig;

    /**
     *  Payment Methods
     *
     * @var array
     */
    protected $paymentMethods;

    /**
     *  Shipping Methods
     *
     * @var array
     */
    protected $shippingMethods;

    /**
     *  Website Collection Factory
     *
     * @var \Acommerce\Storepickup\Model\ResourceModel\Store\CollectionFactory
     */
    protected $websiteCollectionFactory;

    /**
     *  Country Factory
     *
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $countryFactory;

    /**
     *  Countries
     *
     * @var array
     */
    protected $countries = array();

    /**
     *  Order Type
     *
     * @var string
     */
    protected $orderType;

    /**
     *  Partner Id
     *
     * @var string
     */
    protected $partnerId;

    /**
     *  Dropship Item
     *
     * @var string
     */
    protected $dropshipItem;

    /**
     *  Website ID
     *
     * @var string
     */
    protected $websiteId;

    /**
     *  COD Fee Item
     *
     * @var string
     */
    protected $codFreeItem;

    /**
     *  Shipping Fee Item
     *
     * @var string
     */
    protected $shippingFeeItem;

    /**
     *  Shipping Fee Item
     *
     * @var string
     */
    protected $discountItem;

    /**
     *  Order Id
     *
     * @var string
     */
    protected $orderId;

    /**
     *  Fullfillment URL
     *
     * @var string
     */
    protected $fullfillmentUrl;

    /**
     *  Dropship URL
     *
     * @var string
     */
    protected $dropshipUrl;

    /**
     *  Partner Channel Id
     *
     * @var string
     */
    protected $channelId;

    /**
     *  Tokem Id
     *
     * @var string
     */
    protected $tokenId;

    /**
     *  Logger
     *
     * @var Acommerce\CPMSConnect\Logger
     */
    protected $logger;

    /**
     *  Sales Order Export History
     *
     * @var Acommerce\CPMSConnect\Model\OrderExportFactory
     */
    protected $orderExportFactory;


    // @codingStandardsIgnoreStart

    /**
     *  Construct
     *
     * @param StdlibDateTime            $dateTime                 Date Time
     * @param DateTimeTimezoneInterface $localeDate               Local Time
     * @param OrderCollectionFactory    $orderCollectionFactory   Order Collection Factory
     * @param ScopeConfigInterface      $appConfigScopeConfig     App Config Scope Config
     * @param WebsiteCollectionFactory  $websiteCollectionFactory Website Collection Factory
     * @param CountryFactory            $countryFactory           Country Factory
     */
    public function __construct(
        StdlibDateTime $dateTime,
        DateTimeTimezoneInterface $localeDate,
        OrderCollectionFactory $orderCollectionFactory,
        ScopeConfigInterface $appConfigScopeConfig,
        WebsiteCollectionFactory $websiteCollectionFactory,
        CountryFactory $countryFactory,
        Logger  $logger,
        OrderExportFactory $orderExportFactory
    ) {
        $this->dateTime = $dateTime;
        $this->localeDate = $localeDate;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->appConfigScopeConfig = $appConfigScopeConfig;
        $this->websiteCollectionFactory = $websiteCollectionFactory;
        $this->countryFactory = $countryFactory;
        $this->logger = $logger;
        $this->orderExportFactory = $orderExportFactory;
    }
    //@codingStandardsIgnoreEnd

    /**
     * Function Execute()
     *
     * @return void
     */
    public function execute()
    {

        $websites = $this->websiteCollectionFactory->create();

        if ($websites->getSize() > 0) {
            foreach ($websites as $website) {
                if ((int) $website->getId() === 0) {
                    continue;
                }

                $this->websiteId = $website->getId();

                $isEnable = (int) $this->getConfig(
                    'cpms_connect/order_exporting/cronjob'
                );

                $this->orderType = (int) $this->getConfig(
                    'cpms_connect/order_exporting/order_type'
                );

                $this->dropshipItem = $this->getConfig(
                    'cpms_connect/order_exporting/dropship_sku'
                );

                $this->codFreeItem = $this->getConfig(
                    'cpms_connect/order_exporting/cod_fee'
                );

                $this->shippingFeeItem = $this->getConfig(
                    'cpms_connect/order_exporting/shipping_fee'
                );

                $this->discountItem = $this->getConfig(
                    'cpms_connect/order_exporting/discount_item'
                );

                $this->partnerId = $this->getConfig(
                    'cpms_connect/api_config/partner_code'
                );

                $this->channelId = $this->getConfig(
                    'cpms_connect/api_config/channel'
                );

                $this->fullfillmentUrl = $this->getConfig(
                    'cpms_connect/order_exporting/fulfillment_url'
                );

                $this->dropshipUrl = $this->getConfig(
                    'cpms_connect/order_exporting/dropship_url'
                );

                //echo \Acommerce\CPMSConnect\Model\Source\OrderType::DROPSHIP;

                if ($isEnable === 1) {

                    $orders = $this->getOrderCollection($website);
                    $this->tokenId = $this->getTokenApi();

                    //echo $tokenId;

                    if ($orders->getSize() > 0) {
                        foreach ($orders as $order) {
                            $this->orderId = $order->getIncrementId();
                            $data = $this->convertOrderToArray($order);
                            $result = $this->exportOrder($data);

                            if ($result === true) {
                                $orderExportHistory = $this->orderExportFactory
                                    ->create();
                                $orderExportHistory->loadByOrderId($order->getId());

                                $orderExportHistory->setOrderId($order->getId());
                                $orderExportHistory->save();
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Export Data To CPMS
     *
     * @param string $data JSON Data
     *
     * @return array
     */
    protected function exportOrder($data)
    {

        $headers = array(
                'X-Subject-Token: '.$this->tokenId,
                'Content-type: application/json;charset=utf-8',
                'User-Agent: Awesome-Products-App',
                );

        $dropShip = \Acommerce\CPMSConnect\Model\Source\OrderType::DROPSHIP;

        if ($this->orderType === $dropShip) {
            $url = $this->dropshipUrl;
        } else {
            $url = $this->fullfillmentUrl;
        }

        $url = str_replace(":channelID", $this->channelId, $url);
        $url = str_replace(":orderId", $this->orderId, $url);
        $url = str_replace(":shippingPartnerID", $this->partnerId, $url);
        $url = str_replace(":shipOrderId", $this->orderId, $url);

        $timeOut = 50;

        //echo $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeOut);
        $response = curl_exec($ch);

        $responseCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $responseData = json_decode($response, true);

        if (isset($responseData['error']) === true) {
            $this->logger->info(var_export($responseData, true));
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get Country Name By Code
     *
     * @param string $countryCode Country Code
     *
     * @return array
     */
    protected function getCountryName($countryCode)
    {
        if (isset($this->countries[$countryCode]) === false) {
            $country = $this->countryFactory->create()->loadByCode($countryCode);
            if ($country->getId() !== false) {
                $this->countries[$countryCode] = $country->getName();
            } else {
                return false;
            }
        }
        return $this->countries[$countryCode];
    }

    /**
     * Convert Sales Order Data To Array
     *
     * @param \Magento\Sales\Model\Order $order Sale Order
     *
     * @return array
     */
    protected function convertOrderToArray($order)
    {
        $customerInfo      = array();
        $orderShipmentInfo = array();
        $saleorder         = array();
        $saleorderItems    = array();

        $incrementId     = $order->getIncrementId();
        $orderId         = $order->getId();
        $shippingAddress = $order->getShippingAddress();
        $billingAddress  = $order->getBillingAddress();
        $payment         = $order->getPayment()->getMethod();
        $saleOrderDate   = str_replace(' ', 'T', $order->getCreatedAt()).'Z';

        $invoiceDate     = false;

        if ($order->hasInvoice()) {
            foreach ($order->getAllInvoices as $invoice) {
                $saleOrderDate = str_replace(' ', 'T', $invoice->getCreatedAt()).'Z';
                break;
            }
        }

        $customerInfo = $this->covertOrderAddressToArray($billingAddress);
        $shipmentInfo = $this->covertOrderAddressToArray($shippingAddress);

        $dropShip = \Acommerce\CPMSConnect\Model\Source\OrderType::DROPSHIP;

        if ($this->orderType !== $dropShip) {
            foreach ($order->getAllItems() as $item) {
                if ($item->getProductType() == 'simple') {
                    $temp = array();

                    $tempGrossTotal = 0;

                    if ($parentItem = $item->getParentItem()) {

                        if ($parentItem->getRowTotalInclTax()) {
                            $tempGrossTotal = $parentItem->getRowTotalInclTax();
                        } else {
                            $tempGrossTotal = $parentItem->getRowTotalInclTax();
                        }

                    } else {
                        if ($item->getRowTotalInclTax()) {
                            $tempGrossTotal = $item->getRowTotalInclTax();
                        } else {
                            $tempGrossTotal = $item->getRowTotalInclTax();
                        }
                    }

                    $temp = array(
                            "partnerId" => $this->partnerId,
                            "itemId" => $item->getSku(),
                            "qty" => intval($item->getQtyOrdered()),
                            "subTotal"=> (float)$tempGrossTotal
                    );

                    $saleorderItems[] = $temp;
                }
            }

            if ($order->getCodFee() > 0.001) {
                $itemId = $this->codFreeItem;
                $rate   = $order->getCodFee() + $order->getCodTaxAmount();

                $temp   = array(
                        "partnerId" => $this->partnerId,
                        "itemId"    => $itemId,
                        "qty"       => intval(1),
                        "subTotal"  => (float)$rate
                );

                $saleorderItems[] = $temp;
            }

            if ($order->getShippingInclTax()  > 0.001) {
                $itemId = $this->shippingFeeItem;
                $rate   = $order->getShippingInclTax();;

                $temp   = array(
                        "partnerId" => $this->partnerId,
                        "itemId"    => $itemId,
                        "qty"       => intval(1),
                        "subTotal"  => (float)$rate
                );

                $saleorderItems[] = $temp;
            }

            $discount = abs($order->getDiscountAmount());
            if ((float)$discount > 0.001) {
                $itemId = $this->discountItem;
                $rate   = - abs($order->getDiscountAmount());

                $temp   = array(
                        "partnerId" => $this->partnerId,
                        "itemId"    => $itemId,
                        "qty"       => intval(1),
                        "subTotal"  => (float)$rate
                );

                $saleorderItems[] = $temp;
            }

        } else {
            $dropShip   = array();
            $grossTotal = $order->getBaseGrandTotal();

            $dropShip   = array(
                "partnerId" => $this->partnerId,
                "itemId"    => $this->dropshipItem,
                "qty"       => intval(1),
                "subTotal"  => (float)$grossTotal
            );

            $saleorderItems[] = $dropShip;
        }

        $customerBranchCode = '';
        $customerTaxType = '';
        $grandTotal = 0;

        $customerTaxId = (string) $billingAddress->getVatId();

        if (!empty($customerTaxId)) {
            $customerTaxType = $this->getTaxType($billingAddress->getVatType());

            if ($billingAddress->getVatType() == '2') {
                if (empty($billingAddress->getBranchNo()) === false) {
                    $customerBranchCode = $billingAddress->getBranchNo();
                }
            }
        }

        //var_dump($order->getShippingMethod());

        $paymentMethod = $this->getPaymentMethod($order->getPayment()->getMethod());
        $shippingMethod = $this->getShippingMethod($order->getShippingMethod());

        if ($order->getBaseGrandTotal() > 0.01) {
            $grandTotal = $order->getBaseGrandTotal();
        }

        $saleorder = array(
            "orderCreatedTime"   => $saleOrderDate,
            "customerInfo"       => $customerInfo,
            "orderShipmentInfo"  => $shipmentInfo,
            "paymentType"        => $paymentMethod,
            "shippingType"       => $shippingMethod,
            "grossTotal"         => (float) $grandTotal,
            "currUnit"           => $order->getOrderCurrencyCode(),
            "customerTaxId"      => $customerTaxId,
            "customerTaxType"    => $customerTaxType,
            "customerBranchCode" => $customerBranchCode,
            "orderItems"         => $saleorderItems,
        );

        $dropShip = \Acommerce\CPMSConnect\Model\Source\OrderType::DROPSHIP;

        if ($this->orderType === $dropShip) {
            $shipOrder = array();
            $shipOrder['shipCreatedTime'] = $saleorder['orderCreatedTime'];
            $shipOrder['shipServiceType'] = 'DROP_SHIP';

            $addressee = $this->getConfig(
                'cpms_connect/order_exporting/addressee'
            );
            $address1 = $this->getConfig(
                'cpms_connect/order_exporting/address1'
            );
            $address2 = $this->getConfig(
                'cpms_connect/order_exporting/address2'
            );
            $subDistrict = $this->getConfig(
                'cpms_connect/order_exporting/sub_district'
            );
            $district = $this->getConfig(
                'cpms_connect/order_exporting/district'
            );
            $province = $this->getConfig(
                'cpms_connect/order_exporting/province'
            );
            $postalCode = $this->getConfig(
                'cpms_connect/order_exporting/postalcode'
            );
            $country = $this->getConfig(
                'cpms_connect/order_exporting/country'
            );
            $phone = $this->getConfig(
                'cpms_connect/order_exporting/phone'
            );
            $email = $this->getConfig(
                'cpms_connect/order_exporting/email'
            );

            $shipSender = array("addressee"     => $addressee,
                                  "address1"    => $address1,
                                  "address2"    => $address2,
                                  "subDistrict" => $subDistrict,
                                  "district"    => $district,
                                  "city"        => "",
                                  "province"    => $province,
                                  "postalCode"  => $postalCode,
                                  "country"     => $country,
                                  "phone"       => $phone,
                                  "email "      => $email
                                );

            $shipOrder['shipSender']       = $shipSender;
            $shipOrder['shipShipment']     = $saleorder['orderShipmentInfo'];
            $shipOrder['shipPickup']       = $shipSender;
            $shipOrder['shipInsurance']    = false;
            $shipOrder['shipCurrency']     = $saleorder['currUnit'];
            $shipOrder['shipShippingType'] = $saleorder['shippingType'];
            $shipOrder['shipPaymentType']  = $saleorder['paymentType'];
            $shipOrder['shipGrossTotal']   = $saleorder['grossTotal'];
            $shipOrder['shipPackages']     = array(new \stdClass());

            $saleorder = $shipOrder;
        }

        $saleorder = json_encode($saleorder, JSON_UNESCAPED_UNICODE);
        return $saleorder;
    }

    /**
     * Get Tax Type
     *
     * @param string $type Tax Type Id
     *
     * @return string
     */
    protected function getTaxType($type)
    {
        $textType = array(
                          '1' => 'Personal',
                          '2' => 'Juristic Person',
                          '3' => 'Non-Thai Persoanl'
                        );
        return isset($textType[$type]) ? $textType[$type] : '';
    }

    /**
     * Convert Order Address To Array
     *
     * @param string $address Config Path
     *
     * @return string
     */
    protected function covertOrderAddressToArray($address)
    {
        $addressee   = '';
        $subDistrict = '';
        $district    = '';
        $city        = '';
        $province    = '';
        $postalCode  = '';
        $country     = '';
        $phone       = '';
        $email       = '';

        $order       = $address->getOrder();


        if ($address->getFirstname()) {
            $addressee .= $address->getFirstname();
        }

        if ($address->getLastname()) {
            $addressee .= ' '.$address->getLastname();
        }

        if ($address->getDistrict()) {
            $subDistrict = $address->getDistrict();
        }

        if ($address->getCity()) {
            $district = $address->getCity();
        }

        if ($address->getRegion()) {
            $province = $address->getRegion();
        }

        if ($address->getPostcode()) {
            $postalCode = $address->getPostcode();
        }

        if ($address->getCountryId()) {
            $country = $this->getCountryName($address->getCountryId());
        }

        if ($address->getTelephone()) {
            $phone = $address->getTelephone();
        }

        if ($order->getCustomerEmail()) {
            $email = $order->getCustomerEmail();
        } else {
            //$email = $shippingAddress->getCustomerEmail();
        }

        $addressInfo =  array(
             "addressee"   => trim($addressee),
             "address1"    => $this->getAddress($address->getStreet(), 1),
             "address2"    => $this->getAddress($address->getStreet(), 2),
             "subDistrict" => $subDistrict,
             "district"    => $district,
             "city"        => "",
             "province"    => $province,
             "postalCode"  => $postalCode,
             "country"     => $country,
             "phone"       => $phone,
             "email"       => $email
        );

        if ($address->getAddressType() === 'shipping') {
            $addressInfo['address2'] = $addressInfo['address2'].' '.
            $subDistrict.' '. $district;
        }

        return $addressInfo;
    }

    /**
     * Get Streets
     *
     * @param string $streets Config Path
     * @param int    $row     Row
     *
     * @return string
     */
    protected function getAddress($streets, $row)
    {
        $streets = (is_array($streets)) ? $streets : explode("\n", $streets);
        $itemCount =  floor(count($streets) / 2);
        $itemCount = ($itemCount < 1) ? 1 : $itemCount;

        if ($row == 1) {
            return preg_replace(
                '~\r\n?~', ' ', implode("\r\n", array_slice($streets, 0, $itemCount))
            );
        } else {
            return preg_replace(
                '~\r\n?~', ' ', implode("\r\n", array_slice($streets, $itemCount))
            );
        }
    }

    /**
     * Get Order Collection
     *
     * @param string $path Config Path
     *
     * @return string
     */
    protected function getConfig($path)
    {
        $scopeWebsite = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;

        return $this->appConfigScopeConfig
            ->getValue($path, $scopeWebsite, $this->websiteId);
    }

    /**
     * Get Order Collection
     *
     * @param \Magento\Store\Model\Website $website Website
     *
     * @return collection
     */
    protected function getOrderCollection($website)
    {
        $storeIds = $website->getStoreIds();

        $orders = $this->orderCollectionFactory->create();

        $orders->getSelect()
            ->joinInner(
                array('payment' => 'sales_order_payment'),
                'main_table.entity_id = payment.parent_id', 'payment.method'
            );

        $orders->getSelect()->where($this->getPaymentCondition());

        $orders->getSelect()->joinLeft(
            array('history' => 'sales_order_exporting'),
            'main_table.entity_id = history.order_id',
            array(
                'is_exported' =>
                new \Zend_Db_Expr('IF(history.item_id IS NULL, 0, 1)')
                )
        );

        $orders->getSelect()->where('IF(history.item_id IS NULL, 0, 1) = 0');

        //$orders->getSelect()->where('is_exported_to_cpms = 0');

        if (count($storeIds) > 0) {
            $orders->addFieldToFilter(
                'main_table.store_id', array('in' => $storeIds)
            );
        }

        //echo $orders->getSelect()->__toString();
        return $orders;
    }

    /**
     * Get Payment Condition
     *
     * @return string
     */
    protected function getPaymentCondition()
    {
        $exportStatuses = $this->appConfigScopeConfig
            ->getValue('cpms_connect/order_exporting/export_status');

        $exportStatuses = unserialize($exportStatuses);
        $conditions = array();
        if ($exportStatuses) {
            if (count($exportStatuses) > 0) {
                foreach ($exportStatuses as $exportStatus) {
                    $conditions[] = "(payment.method = '".
                    $exportStatus['payment_method']."' AND main_table.status ='".
                    $exportStatus['order_status']."')";
                }
                return implode(' OR ', $conditions);
            }
        }
        return '(1=1)';
    }

    /**
     * Get Payment Method Mapping
     *
     * @param string $method Method
     *
     * @return array
     */
    public function getPaymentMethod($method)
    {

        if (!$this->paymentMethods) {
            $paymentMapping = $this->appConfigScopeConfig
                ->getValue('cpms_connect/order_exporting/payment_mapping');
            $this->paymentMethods = unserialize($paymentMapping);
        }

        if ($this->paymentMethods) {
            foreach ($this->paymentMethods as $paymentMethod) {

                if ($paymentMethod['payment_method'] == $method) {
                    return $paymentMethod['value'];
                }
            }
        }
        return false;
    }

    /**
     * Get Shipping Method Mapping
     *
     * @param string $method Method
     *
     * @return array
     */
    public function getShippingMethod($method)
    {

        if (!$this->shippingMethods) {
            $shippingMapping = $this->appConfigScopeConfig
                ->getValue('cpms_connect/order_exporting/shipping_mapping');
            $this->shippingMethods = unserialize($shippingMapping);
        }

        if ($this->shippingMethods) {
            foreach ($this->shippingMethods as $shippingMethod) {
                $shippingCode  = $shippingMethod['shipping_method'];

                if (strpos($method, $shippingCode) !== false) {
                    return $shippingMethod['value'];
                }
            }
        }
        return false;
    }

    /**
     * Get Credit Card Type
     *
     * @param string $type Type
     *
     * @return string
     */
    protected function getCreditCardType($type)
    {
        $creditCardTypes = array('VI' => 'Visa', 'MC' => 'Master Card');
        return isset($creditCardTypes[$type]) ? $creditCardTypes[$type] : '';
    }

    /**
     * Get Product Attribute
     *
     * @param int    $productId Product Id
     * @param string $attr      Attribute Name
     * @param int    $store     Store Id

     * @return string
     */
    protected function getProductAttribure($productId, $attr, $store)
    {
        $resporce = $this->productFactory->create()->getResource();
        $attrValue = $resporce->getAttributeRawValue($productId, $attr, $store);
        return $attrValue;
    }


    /**
     * Get Cpms Token Api Key
     *
     * @return string
     */
    protected function getTokenApi()
    {

        $user   = $this->getConfig(
            'cpms_connect/api_config/authentication_user'
        );
        $apiKey = $this->getConfig(
            'cpms_connect/api_config/api_key'
        );
        $url    = $this->getConfig(
            'cpms_connect/api_config/auth_url_api'
        );

        $auth = array('auth' =>
            array('apiKeyCredentials' =>
                array('username' => $user, 'apiKey' => $apiKey)));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        $header = array();
        $header[] = 'Content-Type: application/json;charset=UTF-8';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($auth));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        if (curl_errno($ch) === 0) {
            $result = json_decode($response, true);
            if (isset($result['token']) === true) {
                $token = $result['token'];
                return $token['token_id'];
            }
        }
        curl_close($ch);
        return false;
    }

}
