<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\AutoCancel\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Acommerce\AutoCancel\Model\OrderCancel as OrderCancelModel;

/**
 * Sales Order Canceling History Resource Model
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class OrderCancel extends AbstractDb
{
    /**
     * Define main and locale region name tables
     *
     * @return void
     */
    // @codingStandardsIgnoreStart
    protected function _construct()
    {
        $this->_init('sales_order_canceling', 'item_id');

    }//end _construct()
    // @codingStandardsIgnoreEnd

    /**
     * Load Sales Order Canceling History By Sales Order Id
     *
     * @param OrderCancelModel $object  Sales Order Canceling
     * @param int              $orderId Order Id
     *
     * @return $this
     */
    public function loadByOrderId(OrderCancelModel $object, $orderId)
    {
        $connection = $this->getConnection();

        $select     = $connection->select();

        $select->from(array('main_table' => $this->getMainTable()));
        $select->where('order_id = ?', $orderId);

        $data = $connection->fetchRow($select);
        if ($data) {
            $object->setData($data);
        }

        $this->_afterLoad($object);

        return $this;

    }//end loadByName()


}//end class
