<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\AutoCancel\Model\ResourceModel\OrderCancel;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Sales Order Canceling History Collection
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Collection extends AbstractCollection
{

    // @codingStandardsIgnoreStart
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Acommerce\AutoCancel\Model\OrderCancel',
            'Acommerce\AutoCancel\Model\ResourceModel\OrderCancel'
        );

    }//end _construct()
    // @codingStandardsIgnoreEnd


}//end class