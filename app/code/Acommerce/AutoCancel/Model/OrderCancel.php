<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\AutoCancel\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Sales Order Canceling Hostory
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class OrderCancel extends AbstractModel
{

    // @codingStandardsIgnoreStart
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\AutoCancel\Model\ResourceModel\OrderCancel');

    }//end _construct()
    // @codingStandardsIgnoreEnd

    /**
     * Load Sales Order Canceling History By Order Id
     *
     * @param string $orderId Order Id
     *
     * @return $this
     */
    public function loadByOrderId($orderId)
    {
        $this->_getResource()->loadByOrderId($this, $orderId);
        return $this;

    }//end loadByOrderId()


}//end class