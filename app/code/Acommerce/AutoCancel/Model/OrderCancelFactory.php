<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\AutoCancel\Model;

use Magento\Framework\ObjectManagerInterface;

/**
 * Sales Order Canceling History Factory
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class OrderCancelFactory
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;


    /**
     * Construct
     *
     * @param ObjectManagerInterface $objectManager Object Manager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;

    }//end __construct()


    /**
     * Create Sales Order Canceling History Model
     *
     * @param array $arguments Arguments
     *
     * @return OrderCancel
     */
    public function create(array $arguments=array())
    {
        return $this->objectManager->create(
            'Acommerce\AutoCancel\Model\OrderCancel', $arguments
        );

    }//end create()


}//end class
