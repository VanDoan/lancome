<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\AutoCancel\Block\Adminhtml\Ordercancel;

use Magento\Backend\Block\Widget\Grid\Extended as WidgetGridExtended;
use Magento\Sales\Model\ResourceModel\Order\Grid\Collection
    as OrderGridCollection;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as BackendHelper;
use Magento\Sales\Model\ResourceModel\Order\Status\Collection
    as OrderStatusCollection;
use Magento\Config\Model\Config\Source\Yesno;


/**
 * Sales Order Canceling History Widget Grid Extended
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Grid extends WidgetGridExtended
{

    /**
     * Sales Order Grid Collection
     *
     * @var OrderGridCollection
     */
    protected $orderGridCollection;


    /**
     * Order Status Collection
     *
     * @var OrderStatusCollection
     */
    protected $orderStatusCollection;

    /**
     *  App Config Scope Config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     *  Yes No Option
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $sourceYesno;

    /**
     * Construct
     *
     * @param Context               $context               Context
     * @param BackendHelper         $backendHelper         Backend Helper
     * @param OrderGridCollection   $orderGridCollection   Order Grid Collection
     * @param OrderStatusCollection $orderStatusCollection Order Status Collection
     * @param SourceYesno           $sourceYesno           Yesno Option
     * @param array                 $data                  Data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        BackendHelper $backendHelper,
        OrderGridCollection $orderGridCollection,
        OrderStatusCollection $orderStatusCollection,
        Yesno $sourceYesno,
        array $data = []
    ) {
        $this->orderGridCollection   = $orderGridCollection;
        $this->orderStatusCollection = $orderStatusCollection;
        $this->scopeConfig           = $context->getScopeConfig();
        $this->sourceYesno           = $sourceYesno;
        parent::__construct($context, $backendHelper, $data);
    }

    // @codingStandardsIgnoreStart
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('orderGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);

    }
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Prepare Collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        try {

            $collection =$this->orderGridCollection->load();

            $collection->getSelect()
                ->joinInner(
                    array('payment' => 'sales_order_payment'),
                    'main_table.entity_id = payment.parent_id', 'payment.method'
                );

            $collection->getSelect()->joinInner(
                array('history' => 'sales_order_canceling'),
                'main_table.entity_id = history.order_id',
                array(
                'is_canceled' =>
                new \Zend_Db_Expr('IF(history.item_id IS NULL, 0, 1)'),
                'canceled_date' => 'history.created_at'
                )
            );

            $this->setCollection($collection);
            parent::_prepareCollection();
            return $this;
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }
    // @codingStandardsIgnoreStart

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'increment_id',
            [
                'header' => __('Order ID'),
                'type' => 'text',
                'index' => 'increment_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        /*$this->addColumn(
            'store_id',
            [
                'header' => __('Purchase Point'),
                'index' => 'store_name',
                'class' => 'store_id'
            ]
        );
        */
        $this->addColumn(
            'created_at',
            [
                'header' => __('Purchase Date'),
                'index' => 'created_at',
                'filter_index' => 'main_table.created_at',
                'type' => 'datetime',
            ]
        );
        $this->addColumn(
            'billing_name',
            [
                'header' => __('Bill-to Name'),
                'index' => 'billing_name',
                'class' => 'name'
            ]
        );
        $this->addColumn(
            'shipping_name',
            [
                'header' => __('Ship-to Name'),
                'index' => 'shipping_name',
                'class' => 'name'
            ]
        );
        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'index' => 'customer_email',
                'class' => 'email'
            ]
        );

        $this->addColumn(
            'base_grand_total',
            [
                'header' => __('Grand Total (Base)'),
                'type'   => 'currency',
                'index'  => 'base_grand_total',
                'class'  => 'base_grand_total'
            ]
        );

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'type'  => 'options',
                'index' => 'status',
                'class' => 'status',
                'options' => $this->orderStatusCollection->toOptionHash()
            ]
        );

        /*$this->addColumn(
            'is_exported',
            [
                'header' => __('Is Canceled'),
                'type'  => 'options',
                'index' => 'is_canceled',
                'class' => 'status',
                'filter_index' => new \Zend_Db_Expr('IF(history.item_id IS NULL, 0, 1)'),
                'options' => $this->sourceYesno ->toArray()
            ]
        );*/

        $this->addColumn(
            'exported_date',
            [
                'header' => __('Canceled Date'),
                'index' => 'canceled_date',
                'filter_index' => 'history.created_at',
                'type' => 'datetime',
            ]
        );

        //$this->addExportType('*/*/exportCsv', __('CSV'));
        //$this->addExportType('*/*/exportExcel', __('Excel XML'));


        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * Get Order Collection
     *
     * @param string $path Config Path
     *
     * @return string
     */
    protected function getConfig($path)
    {
        $scopeWebsite = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;

        return $this->scopeConfig
            ->getValue($path, $scopeWebsite);
    }

    /**
     * Get Grid Url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return false;
    }

}
