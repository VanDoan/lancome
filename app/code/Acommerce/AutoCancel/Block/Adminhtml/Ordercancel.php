<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\AutoCancel\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Container as GridContainer;

/**
 * Sales Order Canceling History Widget Grid Container
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Ordercancel extends GridContainer
{
    // @codingStandardsIgnoreStart
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {

        $this->_controller = 'adminhtml_ordercancel';/*block grid.php directory*/
        $this->_blockGroup = 'Acommerce_AutoCancel';
        $this->_headerText = __('Sales Order Canceling History');
        parent::_construct();
        $this->removeButton('add');
    }
    // @codingStandardsIgnoreEnd
}