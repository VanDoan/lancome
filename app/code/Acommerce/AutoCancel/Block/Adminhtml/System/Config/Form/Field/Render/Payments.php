<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */

namespace Acommerce\AutoCancel\Block\Adminhtml\System\Config\Form\Field\Render;

use Magento\Framework\View\Element\Context;
use Magento\Payment\Model\Config;

/**
 * Retrive Active Payment Methods To Select Box
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Payments extends \Magento\Framework\View\Element\Html\Select
{
    /**
     * Method List
     *
     * @var array
     */
    protected $paymentModelConfig;


    /**
     * Scope Config
     *
     * @var array
     */
    protected $scopeConfig;


    /**
     * Constructor
     *
     * @param Context $context            Context
     * @param Config  $paymentModelConfig Payment ModelConfig
     * @param array   $data               Data
     */
    public function __construct(
        Context $context,
        Config $paymentModelConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->paymentModelConfig = $paymentModelConfig;
        $this->scopeConfig = $context->getScopeConfig();
    }

    // @codingStandardsIgnoreStart
    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $payments = $this->paymentModelConfig->getActiveMethods();
            $methods = array();
            foreach ($payments as $paymentCode=>$paymentModel) {
                $paymentTitle = $this->scopeConfig
                    ->getValue('payment/'.$paymentCode.'/title');
                $this->addOption($paymentCode, $paymentTitle);
            }
        }
        return parent::_toHtml();
    }
    // @codingStandardsIgnoreEnd

    /**
     * Sets name for input element
     *
     * @param string $value Value
     *
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}