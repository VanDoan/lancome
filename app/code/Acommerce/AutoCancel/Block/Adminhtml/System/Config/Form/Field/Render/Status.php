<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */

namespace Acommerce\AutoCancel\Block\Adminhtml\System\Config\Form\Field\Render;

use Magento\Framework\View\Element\Context;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory
    as OrderStatusCollection;
/**
 * Retrive Order Status To Select Box
 *
 * @category AutoCancel_Syncstock
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Status extends \Magento\Framework\View\Element\Html\Select
{
    /**
    * Method List
    *
    * @var array
    */
    protected $statusCollectionFactory;


    /**
     * Constructor
     *
     * @param Context               $context                 Context
     * @param OrderStatusCollection $statusCollectionFactory
     Status Collection Factory
     * @param array                 $data                    Data
     */
    public function __construct(
        Context $context,
        OrderStatusCollection $statusCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->statusCollectionFactory = $statusCollectionFactory;
    }

    // @codingStandardsIgnoreStart
    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $statuses = $this->statusCollectionFactory->create()->load();

            foreach ($statuses as $status) {
                $this->addOption($status->getStatus(), $status->getLabel());
            }
        }
        return parent::_toHtml();
    }
    //// @codingStandardsIgnoreEnd

    /**
     * Sets name for input element
     *
     * @param string $value Value
     *
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}