<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */

namespace Acommerce\AutoCancel\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
/**
 * Render Payment Mapping
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */
class PaymentMapping extends AbstractFieldArray
{
    // @codingStandardsIgnoreStart
    /**
     * Grid columns
     *
     * @var array
     */
    protected $_columns = array();

    /**
     * Payment Method Renderer
     *
     * @var DataObject
     */
    protected $_paymentMethodRenderer;

    /**
     * Order Status Renderer
     *
     * @var DataObject
     */
    protected $_orderStatusRenderer;

    /**
     * Enable the "Add after" button or not
     *
     * @var bool
     */
    protected $_addAfter = true;

    /**
     * Label of add button
     *
     * @var string
     */
    protected $_addButtonLabel;

    /**
    * Template
    *
    * @var string
    */
    protected $_template
        = 'Acommerce_AutoCancel::system/config/form/field/array.phtml';

    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_addButtonLabel = __('Add');

    }//end _construct()
    // @codingStandardsIgnoreEnd


    // @codingStandardsIgnoreStart
    /**
     * Add a column to array-grid
     *
     * @param string $name   Column Name
     * @param array  $params Params
     *
     * @return void
     */
    public function addColumn($name, $params)
    {
        $this->_columns[$name] = [
            'label' => $this->_getParam($params, 'label', 'Column'),
            'size' => $this->_getParam($params, 'size', false),
            'style' => $this->_getParam($params, 'style'),
            'class' => $this->_getParam($params, 'class'),
            'renderer' => false,
        ];
        if (!empty($params['renderer'])
            && $params['renderer'] instanceof \Magento\Framework\View\Element\AbstractBlock
        ) {
            $this->_columns[$name]['renderer'] = $params['renderer'];
        }
    }
    //@codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Returns renderer for payment element
     *
     * @return \Magento\Braintree\Block\Adminhtml\Form\Field\Countries
     */
    protected function getPaymentRenderer()
    {
        if (!$this->_paymentMethodRenderer) {
            $this->_paymentMethodRenderer = $this->getLayout()->createBlock(
                'Acommerce\AutoCancel\Block\Adminhtml\System\Config\Form\Field\Render\Payments',
                '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_paymentMethodRenderer;
    }
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Prepare Array Row
     *
     * @param DataObject $row Data Row
     *
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $paymentMethod = $row->getPaymentMethod();
        $options = [];
        if ($paymentMethod) {
            $options['option_' . $this->getPaymentRenderer()
                ->calcOptionHash($paymentMethod)] = 'selected="selected"';
        }

        $row->setData('option_extra_attrs', $options);

    }
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
    * Prepare To Render
    *
    * @return void
    */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'payment_method', [
                'label' => __('Payment Method'),
                'renderer' => $this->getPaymentRenderer(),
                'class' => "required-entry"
                ]
        );

        $this->addColumn('value', array(
            'label' => __('Hour(s)'),
            'class' => "required-entry validate-digits")
        );

        $this->_addAfter       = false;
        $this->_addButtonLabel = __('Add');

    }//end _prepareToRender()
    // @codingStandardsIgnoreEnd


}//end class

?>