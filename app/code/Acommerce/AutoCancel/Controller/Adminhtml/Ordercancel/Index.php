<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\AutoCancel\Controller\Adminhtml\Ordercancel;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * CPMS Controllers
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Index extends Action
{
    /**
     * Result Page Factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultFactory;

    /**
     * Construct
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
        $this->resultFactory = $context->getResultFactory();
    }

    /**
     * Execute
     *
     * @return PageFactory $resultFactory
     */
    public function execute()
    {
        $resultFactory = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->initPage($resultFactory);
        $resultFactory->getConfig()
            ->getTitle()->prepend(__('Sales Order Canceling History'));

        return $resultFactory;
    }

    /**
     * Init page.
     *
     * @param ResultPage $resultFactory resultFactory
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultFactory)
    {
        $resultFactory->setActiveMenu('Acommerce_AutoCancel::ordercancel')
            ->addBreadcrumb(__('Acommerce'), __('Acommerce'))
            ->addBreadcrumb(
                __('Sales Order Canceling History'),
                __('Sales Order Canceling History')
            );

        return $resultFactory;
    }

    // @codingStandardsIgnoreStart
    /**
     * Check the permission to run it.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Acommerce_AutoCancel::ordercancel');
    }
    // @codingStandardsIgnoreEnd
}
