<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */

namespace Acommerce\AutoCancel\Helper;

/**
 * Helper
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    //
}

