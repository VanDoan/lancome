<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\AutoCancel\Cron;

use Magento\Sales\Model\ResourceModel\Order\CollectionFactory
    as OrderCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ResourceModel\Website\CollectionFactory
    as WebsiteCollectionFactory;
use Acommerce\AutoCancel\Model\OrderCancelFactory;
use Acommerce\AutoCancel\Logger\Logger;

/**
 * Auto Cancel Sales Order
 *
 * @category Acommerce_AutoCancel
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class OrderCanceling
{

    /**
     *  Sales Order Collection
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     *  App Config Scope Config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $appConfigScopeConfig;

    /**
     *  Website Collection Factory
     *
     * @var \Acommerce\Storepickup\Model\ResourceModel\Store\CollectionFactory
     */
    protected $websiteCollectionFactory;

    /**
     *  Website ID
     *
     * @var string
     */
    protected $websiteId;

    /**
     *  Sales Cancel Factory
     *
     * @var Acommerce\AutoCancel\Model\OrderCancelFactory
     */
    protected $orderCancelFactory;

    /**
     *  Logger
     *
     * @var Acommerce\AutoCancel\Logger
     */
    protected $logger;


    // @codingStandardsIgnoreStart
    /**
     *  Construct
     *
     * @param OrderCollectionFactory   $orderCollectionFactory   Order Collection Factory
     * @param ScopeConfigInterface     $appConfigScopeConfig     App Config Scope Config
     * @param WebsiteCollectionFactory $websiteCollectionFactory Website Collection Factory
     * @param OrderCancelFactory       $orderCancelFactory       Order Cancel Factory
     * @param Logger                   $Logger                   Logger
     */
    public function __construct(
        OrderCollectionFactory $orderCollectionFactory,
        ScopeConfigInterface $appConfigScopeConfig,
        WebsiteCollectionFactory $websiteCollectionFactory,
        OrderCancelFactory $orderCancelFactory,
        Logger  $logger
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->appConfigScopeConfig = $appConfigScopeConfig;
        $this->websiteCollectionFactory = $websiteCollectionFactory;
        $this->orderCancelFactory = $orderCancelFactory;
        $this->logger = $logger;

    }
    //@codingStandardsIgnoreEnd

    /**
     * Function Execute()
     *
     * @return void
     */
    public function execute()
    {

        $websites = $this->websiteCollectionFactory->create();

        if ($websites->getSize() > 0) {
            foreach ($websites as $website) {
                if ((int) $website->getId() === 0) {
                    continue;
                }

                $this->websiteId = $website->getId();

                $isEnable = (int) $this->getConfig(
                    'autocancel/setting/cronjob'
                );

                if ($isEnable === 1) {

                    $orders = $this->getOrderCollection($website);

                    if ($orders->getSize() > 0) {
                        foreach ($orders as $order) {
                            if ($order->canCancel()) {
                                try {
                                    $order->cancel()->save();
                                    $orderCancelHistory = $this->orderCancelFactory
                                        ->create();
                                    $orderCancelHistory->loadByOrderId(
                                        $order->getId()
                                    );
                                    $orderCancelHistory->setOrderId($order->getId());
                                    $orderCancelHistory->save();
                                } catch (Exception $e) {
                                    $this->logger->addInfo($e->getMessage());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Get Order Collection
     *
     * @param string $path Config Path
     *
     * @return string
     */
    protected function getConfig($path)
    {
        $scopeWebsite = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;

        return $this->appConfigScopeConfig
            ->getValue($path, $scopeWebsite, $this->websiteId);
    }

    /**
     * Get Order Collection
     *
     * @param \Magento\Store\Model\Website $website Website
     *
     * @return collection
     */
    protected function getOrderCollection($website)
    {
        $storeIds = $website->getStoreIds();

        $orders = $this->orderCollectionFactory->create();
        $orders->addFieldToFilter(
            'main_table.status',
            array('in' => array('pending', 'pending_payment'))
        );

        $orders->getSelect()
            ->joinInner(
                array('payment' => 'sales_order_payment'),
                'main_table.entity_id = payment.parent_id', 'payment.method'
            );

        $orders->getSelect()->where($this->getPaymentCondition());

        if (count($storeIds) > 0) {
            $orders->addFieldToFilter(
                'main_table.store_id', array('in' => $storeIds)
            );
        }

        //echo $orders->getSelect()->__toString();
        return $orders;
    }

    /**
     * Get Payment Condition
     *
     * @return string
     */
    protected function getPaymentCondition()
    {
        $paymentsMapping = $this->appConfigScopeConfig
            ->getValue('autocancel/setting/payment_mapping');

        $paymentsMapping = unserialize($paymentsMapping);
        $conditions = array();

        if ($paymentsMapping && count($paymentsMapping) > 0) {
            foreach ($paymentsMapping as $paymentMapping) {
                $hours  = (int) $paymentMapping['value'];
                if ($hours > 0) {
                    $conditions[] = "(payment.method = '".
                    $paymentMapping['payment_method'].
                    "' AND main_table.created_at <= DATE_SUB(NOW(),INTERVAL " .
                    $paymentMapping['value'] . " HOUR))";
                }
            }
            return implode(' OR ', $conditions);
        } else {
            return '(1=0)';
        }
    }
}
